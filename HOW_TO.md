# How To
A guide to doing things with Polyapp

[[_TOC_]]

## Logging In and Changing your Password
When you first access Polyapp you should be presented with an error page. This happens because you are not signed in. In the upper right hand corner, click the Settings Icon > Sign Out. You should be taken to the login page at /signIn

Your first log in should be with the super admin account. The super admin's email is "support@polyapp.tech" and its password is "polyapp".

After logging in your first priority should be to change the super admin account's password. From the [home page](https://polyapp-public.appspot.com/) there should be a link to Edit Users. Click it and then select the Polyapp user from the table.
You can now set a new password in the "New Password" field. Then click "Done" all the way at the bottom of the page. If the password is set successfully you will be taken to a new page.

## Creating a Task
View a walkthrough of creating a Task: https://youtu.be/SLCJnjIGEZk

1. From the [home page](https://polyapp-public.appspot.com/), click "[Edit Tasks](https://polyapp-public.appspot.com/polyappChangeData?industry=polyapp&domain=TaskSchemaUX&task=gqyLWclKrvEaWHalHqUdHghrt&schema=gjGLMlcNApJyvRjmgQbopsXPI&ux=hRpIEGPgKvxSzvnCeDpAVcwgJ)" under Development Links.
2. Click the "Create" button.
3. Fill out the form with at least one Field.
4. Click "Done".
5. You will be taken to an empty table. This shows all data currently associated with your Task's Schema (there is no data yet).
6. Click "Create Data".
7. You are now viewing your form.

When you create a new Task, you are actually creating a new Task, Schema, and UX. The UX comprises everything you see and interact with in the user interface. The Schema comprises the "columns" of the Task, where one Field is one Column. It also holds information about the data types. The Task holds information like the Task's Name, Help Text, validation information, and its Bots. Bots can be triggered when the Task loads (prior to its rendering on the server), continuously (when a field's value changes), or at Done (when the Done button is pressed).

## Creating New Bots and Actions
Creating new Tasks means creating things people can see and interact with. But often you want additional behavior which goes beyond simple validation. For this you need Bots.

Bots are named, and contain a list of Actions and some help text. When a Bot is run its Actions are run in sequence.
Actions are also named (often the same name as their Bot) and have some help text (also often identical). When an Action is run its code is run. To know which code to run the file "handlers/bot.go", func RunAction, matches an Action's Name to a function call. The functions run by Actions are all stored in the Actions package. Typically, Action Functions do some calculations and then store additional data, or edit the data being stored in some way. But they can also modify the DOM directly via ModDOMs, redirect the browser to a new URL, or open a new tab.

### Examples
* func ActionHomeLoad completely changes the HTML of the page. It does this by using ModDOMs and it gets triggered by a Bot which runs At Load.
* func ActionEditRole runs at Done, and is about as simple as a Bot gets. It is used to edit a Role collection document when its mirror Data document is updated.
* func ActionEditTask runs at Done, and it creates or updates a Task, Schema, and UX document. It is pretty complicated and suffers from a lack of factoring.
* func ActionUserActivity is designed to run when a User opens a Task or clicks Done on a Task. It logs data about these button presses.
