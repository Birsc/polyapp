package integrity

import (
	"fmt"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// ValidateLanguages ensures the change to the schema will not break any of the underlying languages where a
// 'language' is Go or SQL.
func ValidateLanguages(queryable common.Queryable) error {
	simplified, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("simplify: %w", err)
	}
	err = ValidateSerialization(simplified)
	if err != nil {
		return fmt.Errorf("ValidateSerialization: %w", err)
	}
	return nil
}
