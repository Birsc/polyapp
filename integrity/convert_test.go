package integrity

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
)

func TestBackPopulate(t *testing.T) {
	testBackPopulateSchema := common.Schema{
		FirestoreID:  "TestBackPopulate",
		NextSchemaID: nil,
		IndustryID:   "TestBackPopulate",
		DomainID:     "TestBackPopulate",
		SchemaID:     "TestBackPopulate",
		DataTypes: map[string]string{"field a": "S", "field b": "B", "field c": "I", "field d": "F", "field e": "Ref",
			"field ARef": "ARef", "field AS": "AS", "field AB": "AB", "field AI": "AI", "field AF": "AF"},
		DataHelpText: map[string]string{"field a": "a", "field b": "a", "field c": "c", "field d": "d", "field e": "e",
			"field ARef": "a", "field AS": "a", "field AB": "a", "field AI": "a", "field AF": "a"},
		DataKeys:   []string{"field a", "field b", "field c", "field d", "field e", "field ARef", "field AS", "field AB", "field AI", "field AF"},
		Deprecated: nil,
	}
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		t.Fatal("firestoreCRUD.GetClient: " + err.Error())
	}
	err = firestoreCRUD.CreateSchema(ctx, client, &testBackPopulateSchema)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("firestoreCRUD.CreateSchema: " + err.Error())
	}
	expectedData := &common.Data{
		FirestoreID: "TestBackPopulate",
		IndustryID:  "TestBackPopulate",
		DomainID:    "TestBackPopulate",
		SchemaID:    "TestBackPopulate",
		Deprecated:  nil,
		Ref:         map[string]*string{"field e": common.String("")},
		S:           map[string]*string{"field a": common.String("")},
		I:           map[string]*int64{"field c": common.Int64(0)},
		B:           map[string]*bool{"field b": common.Bool(false)},
		F:           map[string]*float64{"field d": common.Float64(0)},
		ARef:        map[string][]string{"field ARef": {}},
		AS:          map[string][]string{"field AS": {}},
		AI:          map[string][]int64{"field AI": {}},
		AB:          map[string][]bool{"field AB": {}},
		AF:          map[string][]float64{"field AF": {}},
		Nils:        nil,
	}

	emptyData := &common.Data{}
	emptyData.Init(nil)
	minimalData := &common.Data{
		FirestoreID: "TestBackPopulate",
		SchemaID:    "TestBackPopulate",
		IndustryID:  "TestBackPopulate",
		DomainID:    "TestBackPopulate",
	}
	minimalData.Init(nil)

	type args struct {
		data *common.Data
	}
	tests := []struct {
		name     string
		args     args
		wantErr  bool
		wantData *common.Data
	}{
		{
			name: "data nil",
			args: args{
				data: nil,
			},
			wantErr:  true,
			wantData: nil,
		},
		{
			name: "empty data",
			args: args{
				data: emptyData,
			},
			wantErr:  true,
			wantData: nil,
		},
		{
			name: "minimally populated data",
			args: args{
				data: minimalData,
			},
			wantErr:  false,
			wantData: expectedData,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := BackPopulate(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("BackPopulate() error = %v, wantErr %v", err, tt.wantErr)
			}
			// I want to use reflect.deepequal but it fails to dereference the string pointers, so common.String("") != common.String("")
			if err == nil && (*tt.wantData.S["field a"] != *tt.args.data.S["field a"] || !reflect.DeepEqual(tt.wantData.AB, tt.args.data.AB)) {
				t.Errorf("ReadAll() gotData = %v, want %v", tt.args.data, tt.wantData)
			}
		})
	}
}
