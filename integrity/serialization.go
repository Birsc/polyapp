package integrity

import (
	"errors"
	"strings"
	"unicode"
)

// ValidateSerialization ensures all of the serialization schemes Polyapp uses can tolerate the input data structure
// That means JSON and Google's Protobuf. More information:
// https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A8-Insecure_Deserialization
// This function is substantially similar to the ValidateFile function.
func ValidateSerialization(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified should not be nil")
	}
	for k := range simplified {
		if k == "" {
			return errors.New("field name can't be empty")
		}
		for _, c := range k {
			// The requirements in this loop are for JSON.
			// I haven't found any good references on what is allowed in Protobuf other than 'unicode characters'.
			switch {
			case strings.ContainsRune("!#$%&()*+-./:<=>?@[]^_{|}~ ", c):
				// Backslash and quote chars are reserved, but
				// otherwise any punctuation chars are allowed
				// in a tag name.
			case !unicode.IsLetter(c) && !unicode.IsDigit(c):
				// seems required by https://stackoverflow.com/questions/15139449/google-protocol-buffer-error-encountered-string-containing-invalid-utf-8-data
				return errors.New("invalid")
			}
		}

		// There is also a 'checkValid' scan which is run by json, but I'm unwilling to replicate all of its unexported
		// logic in this function.
	}
	return nil
}
