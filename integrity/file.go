package integrity

import (
	"errors"
	"strings"
	"unicode"
)

// ValidateFile ensures the information being stored in files will not break the storage medium.
func ValidateFile(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified should not be nil")
	}
	for k := range simplified {
		// this is more or less a copy of 'isValidTag' in encode.go in the json package.
		if k == "" {
			return errors.New("field name can't be empty")
		}
		for _, c := range k {
			switch {
			case strings.ContainsRune("!#$%&()*+-./:<=>?@[]^_{|}~ ", c):
				// Backslash and quote chars are reserved, but
				// otherwise any punctuation chars are allowed
				// in a tag name.
			case !unicode.IsLetter(c) && !unicode.IsDigit(c):
				return errors.New("invalid name")
			}
		}

		// There is also a 'checkValid' scan which is run by json, but I'm unwilling to replicate all of its unexported
		// logic in this function.
	}
	return nil
}
