package integrity

import (
	"bytes"
	"testing"
)

func TestValidateElasticsearch(t *testing.T) {
	type args struct {
		simplified map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "nil",
			args:    args{simplified: nil},
			wantErr: true,
		},
		{
			name:    "empty",
			args:    args{simplified: make(map[string]interface{})},
			wantErr: false,
		},
		{
			name: "empty name",
			args: args{simplified: map[string]interface{}{
				"": "hi",
			}},
			wantErr: true,
		},
		{
			name: "short",
			args: args{simplified: map[string]interface{}{
				"a":   "B",
				"qed": true,
			}},
			wantErr: false,
		},
		{
			name: "medium",
			args: args{simplified: map[string]interface{}{
				"abcdefghijklmnopqrstuvwxyz0987654321()&$%!$)&%^*@&[}{}[|[]>\"<:/<;<:|\\}]{{=++-_~`,?": "hi",
			}},
			wantErr: false,
		},
		{
			name: "too long",
			args: args{simplified: map[string]interface{}{
				"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
					"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
					"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
					"0": "hi",
			}},
			wantErr: true,
		},
		{
			name: "medium valid",
			args: args{simplified: map[string]interface{}{
				"shorter key": "45098230982456890)($%098234509813908)(*()&$%!$)&%^*@& dsaf d[}{}[S}| f[] FD>fds.f\"<:/<fl;<L:|\\}]{{=++-_~`,?",
			}},
			wantErr: false,
		},
		{
			name: "polyapp Test",
			args: args{simplified: map[string]interface{}{
				"polyapp": "polyapp value",
			}},
			wantErr: false,
		},
		{
			name: "polyapp value test",
			args: args{simplified: map[string]interface{}{
				"anything": "polyapp",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateElasticsearch(tt.args.simplified); (err != nil) != tt.wantErr {
				t.Errorf("ValidateElasticsearch() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}

	var longField bytes.Buffer
	String25 := "0123456789012345678901234"
	for i := 0; i < 999_999; i++ {
		longField.WriteString(String25)
	}
	m := make(map[string]interface{})
	m["hi"] = longField.String()
	err := ValidateElasticsearch(m)
	if err != nil {
		t.Error("should NOT fail if length is under 25MB")
	}

	for i := 0; i < 1; i++ {
		longField.WriteString(String25)
	}
	m["hi"] = longField.String()
	err = ValidateElasticsearch(m)
	if err == nil {
		t.Error("should fail if field exceeds 25MB including the key name")
	}

}
