package integrity

import "testing"

func TestValidFirestoreFieldName(t *testing.T) {
	type args struct {
		key string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "nilTest",
			args: args{key: ""},
			want: "Can't be empty",
		},
		{
			name: "shortTest",
			args: args{key: "a"},
			want: "",
		},
		{
			name: "invalid char in middle",
			args: args{key: "hi. there"},
			want: "Please do not use any of these characters: . [ ] * `",
		},
		{
			name: "invalid char at beginning",
			args: args{key: "*hi there"},
			want: "Please do not use any of these characters: . [ ] * `",
		},
		{
			name: "invalid char at end",
			args: args{key: "hi there]"},
			want: "Please do not use any of these characters: . [ ] * `",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ValidFirestoreFieldName(tt.args.key); got != tt.want {
				t.Errorf("ValidFirestoreFieldName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateFirestore(t *testing.T) {
	type args struct {
		simplified map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "nil",
			args:    args{simplified: nil},
			wantErr: true,
		},
		{
			name:    "empty",
			args:    args{simplified: make(map[string]interface{})},
			wantErr: false,
		},
		{
			name: "single entry",
			args: args{simplified: map[string]interface{}{
				"one": "thing",
			}},
			wantErr: false,
		},
		{
			name: "many entries second one failing",
			args: args{simplified: map[string]interface{}{
				"a":                                      "B",
				"polyapp.Thing":                          "some value",
				"something else right here: sdlkdsljkds": "kljasdkljadsfklj asd;lk asdlk;j af;la",
			}},
			wantErr: true,
		},
		{
			name: "many entries none failing",
			args: args{simplified: map[string]interface{}{
				"aardvark": "lk afds;k adfs;klf alk;dj safkl;jdsklf;jdkslajfkjkkf lsjdslkf jdskljfs. lksdfklj., dsfjlk dsf.kjfds.j dfsklj sdfakljlk908456u *()*(756432092309089g098634509456902890",
				"afslkjj lwet lkj gklcvixopu4983689)!@$_&9089pguf-04954-6278q-+=-_~@#$%^&()?/<>,:'\"{}|\\907 2q589p051389-5": "hi",
				"8978914": "blegh",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateFirestore(tt.args.simplified); (err != nil) != tt.wantErr {
				t.Errorf("ValidateFirestore() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
