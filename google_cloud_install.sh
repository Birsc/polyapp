#! /bin/bash
# This script deploys Polyapp on Google Cloud.

echo "Welcome to the Polyapp Installation Program"
echo ""
echo "You can re-run this program without breaking anything in Google Cloud."
echo "If the program encounters an error, it is often best to kill it (Ctrl + C) and restart it."
echo "Press Enter to proceed in a new Google Cloud Project. Enter a Google Cloud Project ID and press enter to use that Project instead."
read GOOGLE_CLOUD_PROJECT
# Remove leading whitespace and reduce trailing whitespace
$GOOGLE_CLOUD_PROJECT | xargs

if [ "$GOOGLE_CLOUD_PROJECT" = "" ] || [ "$GOOGLE_CLOUD_PROJECT" = " " ]
then
  GOOGLE_CLOUD_PROJECT=polyapp-$RANDOM$RANDOM$RANDOM
  gcloud projects create $GOOGLE_CLOUD_PROJECT --name=Polyapp
else
  echo "Polyapp will be installed into $GOOGLE_CLOUD_PROJECT which may disrupt its existing App Engine, Firestore, Datastore, Firebase, and other cloud services."
  echo "Press Ctrl + C to cancel"
fi
gcloud config set project $GOOGLE_CLOUD_PROJECT
export GOOGLE_CLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT

function doContact()
{
  echo "pinging"
  USER_EMAIL=$(gcloud auth list --format="value(account)" --limit=1)
  THROWAWAY=$(curl --silent "https://polyapp-hub.appspot.com/?installing=true&email=$USER_EMAIL")
}

echo "May we curl polyapp-hub.appspot.com with your email address so Polyapp LLC knows who is using Polyapp?"
echo "Please enter a number choice."l
select yn in "Yes" "No"; do
  case $yn in
    Yes ) doContact; break;;
    No ) break;;
  esac
done


# Need a billing account to be set up and linked
FIRST_BILLING_ACCOUNT=$(gcloud beta billing accounts list --filter=open=true --format="value(name)" --limit=1)
if [ "$FIRST_BILLING_ACCOUNT" = "" ]
then
  echo "No active billing accounts were found. Please create a billing account and then re-run this program."
  exit 1
else
  IS_ENABLED=$(gcloud beta billing projects list --billing-account $FIRST_BILLING_ACCOUNT --filter=PROJECT_ID=$GOOGLE_CLOUD_PROJECT --format="value(billingEnabled)")
  if [ "$IS_ENABLED" = "True" ]; then
    echo "Billing account exists and is already set up for this project."
  else
    echo "Using billing account number $FIRST_BILLING_ACCOUNT for $GOOGLE_CLOUD_PROJECT"
    gcloud beta billing projects link $GOOGLE_CLOUD_PROJECT --billing-account $FIRST_BILLING_ACCOUNT
  fi
fi

echo "Beginning Firebase setup"
gcloud services enable firebase.googleapis.com --project=$GOOGLE_CLOUD_PROJECT

go build -o initGCP ./cmd/initGCP/main.go
./initGCP $GOOGLE_CLOUD_PROJECT

echo ""
echo "Although we have automatically set up Firebase, we haven't automatically configured Firebase Authentication."
echo "You need to do this manually. Go here: https://console.firebase.google.com/project/"$GOOGLE_CLOUD_PROJECT"/authentication/providers"
read -p "Press enter to proceed"
echo ""
echo ""
echo "Click 'Get Started'"
read -p "Press enter to proceed"
echo ""
echo "You need to enable 3 options. The Email/Password option, the Google option, and the anonymous option."
read -p "Press enter to proceed"
echo ""
echo "Under Authorized Domains add: https://"$GOOGLE_CLOUD_PROJECT".appspot.com"
read -p "Press enter to proceed"
echo ""
echo "We are now done with setting up Firebase! Polyapp will now be able to authenticate your identity. The remaining steps will have you manually handle Google Cloud deployment options."
read -p "Press enter to proceed"
echo ""

CREATE_SUCCESS=$(gcloud app create)
if [[ $CREATE_SUCCESS == *"already contains an App Engine application"* ]]; then
  echo "gcloud app is already deployed"
elif [ "$CREATE_SUCCESS" = "" ]; then
  echo ""
else
  echo "gcloud app create error: $CREATE_SUCCESS"
  exit 1
fi

# There will be an interactive session where you choose the location of your App Engine application.

# Make sure we create the database in Firestore mode
gcloud services enable firestore.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
REGION=$(gcloud app describe --format="value(locationId.scope())")
gcloud services enable appengine.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
gcloud firestore databases create --region=$REGION --project=$GOOGLE_CLOUD_PROJECT

# The application relies upon a few dual-key indexes
gcloud firestore indexes composite create --collection-group=data --field-config=field-path="\`polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_User ID\`",order=ascending --field-config=field-path="\`polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_Event Time\`",order=descending --async
gcloud firestore indexes composite create --collection-group=data --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=polyappSchemaID,order=ascending --async
gcloud firestore indexes composite create --collection-group=data --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=Name,order=ascending --async
gcloud firestore indexes composite create --collection-group=task --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=Name,order=ascending --async

# Need a Cloud Storage Bucket to help store big blobs.
# It is important to make sure our BUCKET starts with a # and there is only 1 such bucket.
# See firestoreCRUD/data.go where it references google_cloud_install.sh for more information.
OUT=$(gsutil ls | grep -G "gs://[0-9]")
if [ "$OUT" = "" ]; then
  echo "Creating a new storage bucket"
  BUCKET=$RANDOM$RANDOM$RANDOM
  gsutil mb gs://$BUCKET
fi

# Enable Secrets Manager and give App Engine access so we can store API Keys there
gcloud services enable secretmanager.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
gcloud projects add-iam-policy-binding $GOOGLE_CLOUD_PROJECT --member=serviceAccount:$GOOGLE_CLOUD_PROJECT@appspot.gserviceaccount.com --role=roles/secretmanager.admin

DEPLOY_SUCCESS=$(gcloud --quiet app deploy ./cmd/defaultservice/app.yaml)
if [ "$DEPLOY_SUCCESS" = "" ]; then
  echo ""
else
  # If at first you don't succeed, try, try again
  gcloud --quiet app deploy ./cmd/defaultservice/app.yaml
fi

echo "Beginning install"
STATUSCODE=$(curl --silent --output /dev/stderr --write-out "%{http_code}" 'https://'$GOOGLE_CLOUD_PROJECT'.appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/')

if test $STATUSCODE -ne 200; then
  STATUSCODE=$(curl --silent --output /dev/stderr --write-out "%{http_code}" 'https://'$GOOGLE_CLOUD_PROJECT'.appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/')

  if test $STATUSCODE -ne 200; then
      exit 1
  fi
fi

echo ""
echo "Polyapp should now be installed. Thank you for trying Polyapp!"
echo "Open Polyapp: https://$GOOGLE_CLOUD_PROJECT.appspot.com/signIn"
echo ""
echo "After opening Polyapp, click Settings in upper right > Sign Out"
echo "You can sign in again with email 'support@polyapp.tech' and password 'polyapp'"
echo "You can change the password from Home > Edit Users > polyappAdminUser > enter the new password under 'New Password' and click 'Done'"
