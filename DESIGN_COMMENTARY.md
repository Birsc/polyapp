# Design Commentary
When you are reading through the design you may occasionally stop to ask, "Why?" Why was this implemented this way? What could posssibly have motivated someone to make this decision? This commentary intends to answer those questions.

[[_TOC_]]

## Why Polyapp?
#### The Original Problem: Why Polyapp was built
Systems get built with developers. Then the developers grow the system by adding new features, but some customers don't want some features. So they add configuration to the system. Then they realize they need more and more complex configuration, breaking down on geographic location, customer name, customer types, etc. and [a death cycle begins](http://mikehadlow.blogspot.com/2012/05/configuration-complexity-clock.html).
In other words, instead of creating new apps for each geographic location, each customer, each business unit, etc., developers add on to their current app because doing so is seen as being cheaper and easier than building a new one for each customer from the ground up.

So I asked how to make this better. What is the current process for creating and improving an app and how do I make it 10 times faster? How do I avoid configuration bloat?

Here is a development cycle I have used before: Person is doing job -> Person finds Problem -> Tells Manager about the problem -> Manager agrees it is a problem -> [middlemen] -> Someone tells a developer to fix the problem -> Developer Fixes problem -> Developer Deploys Solution -> Person gets solution.

#### The Polyapp Solution
There are many ways to speed up this cycle, from removing people in the middle to creating extremely configurable solutions to customer education. But those are business solutions, not technical solutions, and don't represent a 10x improvement. Polyapp wants this process to look like this:

Person is doing job -> Person finds Problem -> Person talks to their manager about the problem -> Person and Manager edit their Task to fix the problem -> Person gets solution.

For this to work, the system must:
* Educate people on how the system works and why it works that way. (Polyapp requires Help Text everywhere for this reason).
* Be simple enough that anyone can learn all there is to know about a Task. (The complexity of a Task in Polyapp is limited for this reason).
* Allow modification of the tasks people are doing as part of their jobs. (All Tasks in Polyapp are built with Polyapp so you don't need a developer to help you most of the time)
* Be resilient when things break. (Polyapp fails fast when a problem is encountered so it is obvious when you make a mistake during Task redesign. Polyapp converts data into new data formats, handles column additions and handles column removal automatically. TODO we need to implement column renaming. Code needed per Task is minimized and removable and configurable.)

Some other natural side effects of this solution are that the tasks are smaller and there are a lot of them. You could even say there are Many - Apps, or Poly - Apps. Polyapp.

#### The Optimal Solution
Polyapp also wants to create a new cycle that looks like this:

Person is doing job -> Polyapp finds a problem -> Polyapp creates a solution -> Polyapp proposes a solution -> Person and Manager accept solution -> Polyapp implements the solution.

Unfortunately not enough development on Polyapp has been done for me to implement features like this. And any such features must be manually verifiable and understandable, meaning the original solution is a precondition to working on this. But I would like to eventually implement this cycle.

OK. Let's get back to the purpose of this document: describing why different design decisions were made and when they were made.

## Why History?
* Why is history included? It is included as a way to stave off obvious questions, like "why did you write your own dumb version of a web framework" or "why aren't you using a common front end library" or "why are you using technology X?"
* Do I think you care about the history? Not really. That's why I try to keep it short.

## Why Database Design?
* Why not create a new collection for every Schema in "Data"? Why stick it all into the Firestore collection named "data" with this complicated prefix system? When I was developing Polyapp I was concerned about some limits. Firestore supports unlimited collections, but not unlimited multi-key indexes. I knew I would want at least one multi-key index which covered all of the schemas (Industry, Domain, Collection). I could have created this index in collection schema instead of collection data. But I thought I would be adding more dual-key indexes similar to that one which would need to cover each data collection. This never happened, and now all of the data is inside one collection.
This is certainly something I would be open to changing but right now the only negatives to this setup are (a) it is hard to read field-values in the Firestore console and (b) Adding a new dual-key index to the data collection takes a very long time (hours or days). 
The downsides to making the change include (a) difficulty navigating the collections in the Firestore console and (b) There will be a disconnect between field keys in the UI (which must keep prefixes to prevent conflicts) and keys on the database.
For now, I don't think the upside is larger than the downside.
* Why not use subcollections for "Data"? There are some limitations with subcollections and Firestore does not yet support an API to create needed indexes dynamically. That makes it impractical to use when users are generating the collections dynamically.
* What are the limitations imposed by Firestore? https://cloud.google.com/firestore/quotas#limits
* Is CSS database collection really necessary? No. It probably should not exist the way it does now.
* Is lastUpdate collection really necessary? Yes. It helps prevent re-installing Polyapp into the same Google Cloud Project. When you re-install, it causes temporary, rolling outages across all of Polyapp's functionality, which is obviously bad.
* Is publicmap really necessary? No. I implemented it to support the website builder since I needed to be able to redirect arbirary urls, like "polyapp.tech" to a specific bit of HTML stored in the UX collection. I still think it is helpful for creating easy-to-remember URLs.
* Why use a Role-based access control system? Because it is an OK solution to a sometimes complex problem. I know there are better solutions, but I didn't have time to implement them and I didn't want to make installation / startup more complex by adding a new database.
* Where is the caching? I would love to add caching using Redis with all App Engine instances hitting 1 Redis cluster. I even implemented some caching functionality in my private repo. But you'll need to be able to turn caching on and off since it isn't free in Google Cloud Platform and I don't have a way to do that yet.

## Why Server Design?
* Why have Bots and Actions? Why not just Actions? This causes additional complexity, but also additional configurability. I think a 2-tier system should be OK into the forseeable future, but a 1-tier system would leave something to be desired for more complex systems.
* Why use Polyapp to edit Polyapp? You could create a form which directly edits Task or Schema or UX? Yes. But this lets us save your work automatically & decouples that saving from actually updating the Task, Schema, and UX in the database.
* If you are so big on using Polyapp to create Polyapp, why are some Tasks hard-coded? Initially all tasks were hard-coded. I've been migrating them when I need to make significant edits. There will always be some hard-coded tasks which have a special purpose, like the log in screen (you don't want to auto-save someone's password as they are typing it).
* Why not just use a common design pattern like MVC? Polyapp _is_ MVC. Model -> Schema. Task -> Controller. View -> UX. We allow embedding views within views (embedded Tasks), and hoist the embedded Task's controller into the parent (sort of), and keep saving the Data to its own Schema.

### Why Services and Routing?
* Why defaultservice? That is what App Engine calls their default service. Why not do that with Polyapp too?
* Why Echo? Because it is a common Go framework which works. Gin would be another good option. I wanted built-in security middleware so the default server and muxes weren't the best choice.
* Why not just put all the code into cmd/defaultservice/*? The code in that file is supposed to run on the server. The code in handlers/* is supposed to be able to run in WASM on the client (although this feature is only partially completed).

### Why packages?
* Go best practices are to have as few packages as possible. Why are there so many? Well, not all of the packages are supposed to be able to work in the browser after compilation to WASM. Some package splits are entirely for organization, like the CRUD packages.
* Why isn't elasticsearchCRUD used in the code? Because Elasticsearch isn't free to run on Google Cloud. It is also complicated to deploy and you can get by without it.
* Why public? Public isn't a package. It's a public folder.

## Why Client Design?
* Why not use a web framework? I tried. It did not go well. Look back at the History section of the DESGIN.md to learn more.
### Why General Principles?
* Why write as little Javascript as possible? Because Javascript is a terrible language. It is hard to write good asynchronous code and there are no compile-time checks to ensure things make sense. Typescript is a patch, not a true fix.
* Why use a Progressive Web App structure? Because it is our eventual path to Polyapp running offline.
* Why authenticate with Firebase Auth? Because it works and it was the easiest option with Google Cloud.
### Why Data-related Code?
* Why auto-save? Because auto-save is the best save.
* Why have all these god functions like handleEvent() and setAllData()? There are a constant, relatively small number of different types of HTML tags and elements. There are a huge number of ways those elements are used. By listening directly to the elements themselves we limit the amount of code we will have to write in the future (I hope).
* Why allow arbitrary DOM modifications from the server? You should trust the server more. Besides, with server-side rendering of partial views I don't have to write my UI templating code in Javascript or Typescript.
### Why UI-related Code?
* A sidebar? Yes, back in the day I wanted a sidebar which would host all of the help text for the currently open Task.
* Why Summernote? It worked relatively painlessly. You're welcome to add support for another type of text editor.
* Why enlarging images? A holdover from using Polyapp as a website builder.
* Why DataTables.net? Free. Open-source. Well tested. Lots of features.
* Why Bootstrap? Free. Open-source. Well tested. Lots of features.
* Static assets? Haven't gotten around to removing them yet.
### Why Data Structures?
* AllData? Really? Yep. It's the simplest solution I could come up with and frankly it works great.
