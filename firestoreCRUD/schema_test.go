package firestoreCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateSchema(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionSchema).Doc("TestCreateSchema").Delete(ctx)
	err = CreateSchema(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.Schema{
		FirestoreID:  "TestCreateSchema",
		NextSchemaID: common.String("TestCreateSchema"),
		IndustryID:   "TestCreateSchema",
		DomainID:     "TestCreateSchema",
		SchemaID:     "TestCreateSchema",
		DataTypes:    make(map[string]string),
		DataKeys:     make([]string, 0),
		DataHelpText: make(map[string]string, 0),
	}
	err = CreateSchema(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = CreateSchema(ctx, client, &d)
	if err != nil {
		t.Error("should not have errored when creating that document. Error: " + err.Error())
	}
	_, err = client.Collection(common.CollectionSchema).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Errorf("document should have been created: " + err.Error())
	}
}

func TestReadSchema(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	s := common.Schema{
		FirestoreID:  "TestReadSchema",
		NextSchemaID: common.String("TestReadSchema"),
		IndustryID:   "TestReadSchema",
		DomainID:     "TestReadSchema",
		SchemaID:     "TestReadSchema",
		DataTypes:    make(map[string]string),
	}
	s.DataTypes["a"] = "S"
	s.DataKeys = []string{"a"}
	_, err = client.Collection(common.CollectionSchema).Doc("TestReadSchema").Set(ctx, &s)
	if err != nil {
		t.Fatal(err)
	}

	_, err = ReadSchema(ctx, nil, "TestReadSchema")
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	o, err := ReadSchema(ctx, client, "TestReadSchema")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if o.FirestoreID != "TestReadSchema" {
		t.Error("should have set FirestoreID")
	}
	if o.DataTypes["a"] != "S" {
		t.Error("did not populate the value")
	}
	if len(o.DataTypes) != 1 {
		t.Error("set too many data types")
	}
}

func TestUpdateSchema(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionSchema).Doc("TestUpdateSchema").Delete(ctx)
	s := common.Schema{
		FirestoreID:  "TestUpdateSchema",
		NextSchemaID: common.String("TestUpdateSchema"),
		IndustryID:   "TestUpdateSchema",
		DomainID:     "TestUpdateSchema",
		SchemaID:     "TestUpdateSchema",
		DataTypes:    make(map[string]string),
		DataHelpText: make(map[string]string),
	}
	s.DataTypes["a"] = "S"
	s.DataKeys = []string{"a"}
	s.DataHelpText["a"] = "generic"
	err = UpdateSchema(ctx, client, &s)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, _ = client.Collection(common.CollectionSchema).Doc("TestUpdateSchema").Set(ctx, map[string]string{
		common.PolyappIndustryID: "TestUpdateSchema",
		common.PolyappDomainID:   "TestUpdateSchema",
		common.PolyappSchemaID:   "TestUpdateSchema",
	})
	err = UpdateSchema(ctx, client, &s)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteSchema(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionSchema).Doc("TestDeleteSchema").Set(ctx, make(map[string]interface{}))
	err = DeleteSchema(ctx, nil, "TestDeleteSchema")
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = DeleteSchema(ctx, client, "TestDeleteSchema")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateSchema(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionSchema).Doc("TestDeprecateSchema").Set(ctx, make(map[string]interface{}))
	err = DeprecateSchema(ctx, nil, "TestDeprecateSchema")
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = DeprecateSchema(ctx, client, "TestDeprecateSchema")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionSchema).Doc("TestDeprecateSchema").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt("polyappDeprecated")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}
