package firestoreCRUD

import (
	"context"
	"errors"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

// CreateSchema creates a new Schema document and sets it. It DOES require that you've already generated an ID.
func CreateSchema(ctx context.Context, client *firestore.Client, schema *common.Schema) error {
	return Create(ctx, client, schema)
}

// ReadSchema reads from Firestore into a Schema object.
func ReadSchema(ctx context.Context, client *firestore.Client, firestoreID string) (common.Schema, error) {
	q := common.Schema{
		FirestoreID: firestoreID,
	}
	err := Read(ctx, client, &q)
	return q, err
}

// UpdateSchema in Firestore. You must already know the document ID. Does not allow updating documents which do not exist.
// Unlike other Update functions, UpdateSchema does not MergeAll - it just replaces. This means your input Schema must
// be valid.
//
// This function exists because it is using Merge which is not easy to support with interfaces.
func UpdateSchema(ctx context.Context, client *firestore.Client, schema *common.Schema) error {
	if client == nil {
		return errors.New("not all inputs were populated for UpdateSchema")
	}
	err := schema.Validate()
	if err != nil {
		return fmt.Errorf("schema invalid: %w", err)
	}

	// exists because Update should not succeed if the document does not exist
	err = client.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		oldDoc, err := tx.Get(&firestore.DocumentRef{
			Path: client.Collection(common.CollectionSchema).Doc(schema.FirestoreID).Path,
			ID:   schema.FirestoreID,
		})
		if codes.NotFound == status.Code(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("error getting document to update: %w", err)
		}

		mergedSchema := &common.Schema{
			FirestoreID: oldDoc.Ref.ID,
			SchemaID:    oldDoc.Ref.ID,
		}
		err = mergedSchema.Init(oldDoc.Data())
		if err != nil {
			return fmt.Errorf("mergedSchema init: %w", err)
		}
		err = mergedSchema.Merge(schema)
		if err != nil {
			return fmt.Errorf("merge: %w", err)
		}
		mergedFirestoreData, err := mergedSchema.Simplify()
		if err != nil {
			return fmt.Errorf("data could not be simplified: %w", err)
		}

		// only set non-nil fields since nil fields are by definition unknown values
		err = tx.Set(client.Collection(common.CollectionSchema).Doc(schema.FirestoreID), mergedFirestoreData)
		if err != nil {
			// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
			// but I would rather pass the error up so we could potentially utilize other resources like GCP Buckets
			return err
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction failed: %w", err)
	}

	return nil
}

// DeleteSchema removes a document. Allows you to delete documents which do not exist.
func DeleteSchema(ctx context.Context, client *firestore.Client, firestoreID string) error {
	q := common.Schema{
		FirestoreID: firestoreID,
	}
	return Delete(ctx, client, &q)
}

// DeprecateSchema sets the 'polyappDeprecated' property.
func DeprecateSchema(ctx context.Context, client *firestore.Client, firestoreID string) error {
	q := common.Schema{
		FirestoreID: firestoreID,
	}
	return Deprecate(ctx, client, &q)
}
