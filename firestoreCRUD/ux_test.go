package firestoreCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateUX(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUX).Doc("TestCreateUX").Delete(ctx)
	err = Create(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.UX{
		FirestoreID:  "TestCreateUX",
		IndustryID:   "TestCreateUX",
		DomainID:     "TestCreateUX",
		SchemaID:     "TestCreateUX",
		HTML:         common.String("<p>whatever</p>"),
		SelectFields: map[string]string{},
	}
	err = Create(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = Create(ctx, client, &d)
	if err != nil {
		t.Fatal("should not have errored when creating that document. Error: " + err.Error())
	}
	_, err = client.Collection(common.CollectionUX).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Errorf("document should have been created: " + err.Error())
	}
}

func TestReadUX(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	s := common.UX{
		FirestoreID: "TestReadUX",
		HTML:        common.String("TestReadUX"),
	}
	_, err = client.Collection(common.CollectionUX).Doc("TestReadUX").Set(ctx, &s)
	if err != nil {
		t.Fatal(err)
	}

	err = Read(ctx, nil, &s)
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	o := common.UX{
		FirestoreID: "TestReadUX",
	}
	err = Read(ctx, client, &o)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if o.FirestoreID != "TestReadUX" {
		t.Error("should have set FirestoreID")
	}
}

func TestUpdateUX(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUX).Doc("TestUpdateUX").Delete(ctx)
	s := common.UX{
		FirestoreID:  "TestUpdateUX",
		IndustryID:   "TestUpdateUX",
		DomainID:     "TestUpdateUX",
		SchemaID:     "TestUpdateUX",
		HTML:         common.String("TestUpdateUX"),
		SelectFields: map[string]string{},
	}
	err = Update(ctx, client, &s)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, _ = client.Collection(common.CollectionUX).Doc("TestUpdateUX").Set(ctx, make(map[string]string))
	err = Update(ctx, client, &s)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteUX(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	ux := common.UX{
		FirestoreID: "TestDeleteUX",
	}
	_, _ = client.Collection(common.CollectionUX).Doc("TestDeleteUX").Set(ctx, make(map[string]interface{}))
	err = Delete(ctx, nil, &ux)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Delete(ctx, client, &ux)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateUX(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	ux := common.UX{
		FirestoreID: "TestDeprecateUX",
	}
	_, _ = client.Collection(common.CollectionUX).Doc("TestDeprecateUX").Set(ctx, make(map[string]interface{}))
	err = Deprecate(ctx, nil, &ux)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Deprecate(ctx, client, &ux)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionUX).Doc("TestDeprecateUX").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt("polyappDeprecated")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}
