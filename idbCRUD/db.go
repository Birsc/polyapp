// +build js

package idbCRUD

import (
	"errors"
	"syscall/js"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// Create calls the JS API for IDB and handles converting between JS and Go objects.
func Create(collection string, firestoreID string, obj map[string]interface{}) error {
	self := js.Global().Get("self")
	if !self.Truthy() {
		return errors.New("couldn't get self")
	}

	// TODO handle return value
	common.ConvertPointers(obj)
	JSObj := js.ValueOf(obj)
	err := self.Call("createDB", collection, firestoreID, JSObj)
	errString := err.String()
	if errString != "" && errString != "<null>" {
		js.Global().Get("console").Call("log", err)
		return errors.New(errString)
	}
	return nil
}

// Read calls the JS API for IDB and handles converting between JS and Go objects.
func Read(collection string, firestoreID string) (map[string]interface{}, error) {
	self := js.Global().Get("self")
	if !self.Truthy() {
		return nil, errors.New("couldn't get self")
	}

	// TODO handle return value
	out := self.Call("readDB", collection, firestoreID)
	if out.Type() != js.TypeObject {
		return nil, errors.New("readDB did not return a JS Object")
	}
	if !out.Index(0).Truthy() {
		return nil, errors.New("not found in collection " + collection + " with id: " + firestoreID)
	}
	errString := out.Index(1).String()
	if errString != "" && errString != "<null>" {
		js.Global().Get("console").Call("log", out)
		return nil, errors.New(errString)
	}
	returnValue := out.Index(0)

	return common.ValueToMap(returnValue), nil
}

// Update calls the JS API for IDB and handles converting between JS and Go objects.
func Update(collection string, firestoreID string, obj map[string]interface{}) error {
	self := js.Global().Get("self")
	if !self.Truthy() {
		return errors.New("couldn't get self")
	}

	// TODO handle return value
	common.ConvertPointers(obj)
	JSObj := js.ValueOf(obj)
	err := self.Call("updateDB", collection, firestoreID, JSObj)
	errString := err.String()
	if errString != "" && errString != "<null>" {
		js.Global().Get("console").Call("log", err)
		return errors.New(errString)
	}
	return nil
}

// Delete calls the JS API for IDB and handles converting between JS and Go objects.
func Delete(collection string, firestoreID string) error {
	self := js.Global().Get("self")
	if !self.Truthy() {
		return errors.New("couldn't get self")
	}

	// TODO handle return value
	err := self.Call("deleteDB", collection, firestoreID)
	errString := err.String()
	if errString != "" && errString != "<null>" {
		js.Global().Get("console").Call("log", err)
		return errors.New(errString)
	}
	return nil
}

// Deprecate calls the JS API for IDB and handles converting between JS and Go objects.
func Deprecate(collection string, firestoreID string) error {
	self := js.Global().Get("self")
	if !self.Truthy() {
		return errors.New("couldn't get self")
	}

	// TODO handle return value
	err := self.Call("deprecateDB", collection, firestoreID)
	errString := err.String()
	if errString != "" && errString != "<null>" {
		js.Global().Get("console").Call("log", err)
		return errors.New(errString)
	}
	return nil
}
