// Package idbCRUD is a JS-dependent package which performs CRUD operations on the IndexedDB.
//
// To learn more about IndexedDB, Read this overview: https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API
//
// This package uses a library to interact with IndexedDB which is documented here: https://github.com/jakearchibald/idb
package idbCRUD
