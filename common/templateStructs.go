package common

// PublicSiteTemplate includes the inputs to the template at genPublic/index.html
type PublicSiteTemplate struct {
	IsPublic        bool
	AllContent      string
	TopBar          string
	LeftBar         string
	RightBar        string
	BottomBar       string
	Title           string
	MetaDescription string
	MetaAuthor      string
	MetaKeywords    string
	HeadTag         string // same as HeadTag in UX.
	IconPath        string
	CSSPath         string
	FullAuthPage    bool // True and this will be treated as a page requiring full Auth.
}
