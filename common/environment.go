package common

import (
	"os"
)

// GetCloudProvider returns "" for no cloud or "GOOGLE" for Google Cloud.
func GetCloudProvider() string {
	if os.Getenv("GAE_ENV") != "" {
		return "GOOGLE"
	}
	return ""
}

// GetDeploymentID return "LocalDeployment" for no cloud or the ID of the deployment.
func GetDeploymentID() string {
	switch GetCloudProvider() {
	case "GOOGLE":
		return os.Getenv("GAE_DEPLOYMENT_ID")
	default:
		return "LocalDeployment"
	}
}

// GetBlobBucket returns the name of the Google Cloud Bucket / S3 container / etc. into which to store blobs.
// This should be the same across all environments except for localhost, which won't have one.
func GetBlobBucket() string {
	switch GetCloudProvider() {
	case "GOOGLE":
		return os.Getenv("BUCKET")
	default:
		return os.Getenv("BUCKET")
	}
}

// GetLastBlobBucket returns the name of the Google Cloud Bucket / S3 container / etc. which was used to store blobs
// in the last environment. Ex. 1: Dev bucket is returned when in Staging. Ex. 2: Staging bucket is returned in Prod.
//
// This is used during environment installation. Blobs in the last environment may be copied over into the new environment.
func GetLastBlobBucket() string {
	switch GetCloudProvider() {
	case "GOOGLE":
		return os.Getenv("LAST_BUCKET")
	default:
		return os.Getenv("BUCKET")
	}
}

// GetElasticsearchClientAddresses returns an HTTPS endpoint for an Elasticsearch client.
func GetElasticsearchClientAddresses() []string {
	switch GetCloudProvider() {
	default:
		return []string{os.Getenv("ELASTICSEARCH_CLIENT_ENDPOINT")}
	}
}

// GetElasticsearchClientPassword returns the Elasticsearch Client Password.
func GetElasticsearchClientPassword() string {
	switch GetCloudProvider() {
	default:
		return os.Getenv("ELASTICSEARCH_CLIENT_PASSWORD")
	}
}

// GetElasticsearchClientUsername returns the Elasticsearch Client Username.
func GetElasticsearchClientUsername() string {
	switch GetCloudProvider() {
	default:
		return os.Getenv("ELASTICSEARCH_CLIENT_USERNAME")
	}
}

// GetGoogleProjectID returns the GOOGLE_PROJECT_ID environment variable.
//
// This exists to confine os.Getenv calls to this file.
func GetGoogleProjectID() string {
	return os.Getenv("GOOGLE_CLOUD_PROJECT")
}

// GetGoogleApplicationCredentials returns the GOOGLE_APPLICATION_CREDENTIALS variable.
//
// This exists to confine os.Getenv calls to this file.
func GetGoogleApplicationCredentials() string {
	return os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

func GetHubURL() string {
	fromEnv := os.Getenv("HUB_URL")
	if fromEnv == "" {
		return "https://hub.polyapp.tech"
	}
	return fromEnv
}
