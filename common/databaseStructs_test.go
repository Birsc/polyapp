package common

import (
	"encoding/json"
	"testing"
)

func TestUser_Validate(t *testing.T) {
	u := User{
		FirestoreID:   "",
		UID:           String(""),
		FullName:      String(""),
		PhotoURL:      String("Dave"),
		EmailVerified: Bool(false),
		PhoneNumber:   String(""),
		Email:         String(""),
	}
	err := u.Validate()
	if err == nil {
		t.Error("Should definitely fail if there are no IDs input")
	}
	u.FirestoreID = "lkjsdkljsdjkl"
	err = u.Validate()
	if err != nil {
		t.Error("didn't expect a fail. Error: " + err.Error())
	}
}

func TestData_Init(t *testing.T) {
	d := Data{}
	d.Init(nil)
	if d.Ref == nil {
		t.Error("Should have initialized the structures")
	}
}

func TestData_Validate(t *testing.T) {
	d := Data{}
	err := d.Validate()
	if err == nil {
		t.Error("should fail to validate if DataTypes isn't populated")
	}
	d.Init(nil)
	err = d.Validate()
	if err == nil {
		t.Error("should fail to validate if there is no ID")
	}
	d.FirestoreID = "ljkadsjlkfjlksdajklasd"
	d.SchemaID = "asdljkas"
	d.DomainID = "alskjdasljk"
	d.IndustryID = "asdljkas"
	err = d.Validate()
	if err != nil {
		t.Error("should have succeeded. Error: " + err.Error())
	}
}

func TestSchema_Validate(t *testing.T) {
	s := Schema{}
	err := s.Validate()
	if err == nil {
		t.Error("should fail to validate if Schema isn't populated")
	}
	s.FirestoreID = "kljdasjkldsjakl"
	err = s.Validate()
	if err == nil {
		t.Error("should fail to validate if the context of the schema is empty")
	}
	s.SchemaID = "ljksdajklasdjk"
	s.DomainID = "asdkljsdklj"
	s.IndustryID = "asdljksdajklasd"
	s.DataKeys = []string{}
	s.DataHelpText = make(map[string]string)
	s.DataKeys = []string{}
	err = s.Validate()
	if err == nil {
		t.Error("should fail if map is nil")
	}
	s.DataTypes = make(map[string]string)
	err = s.Validate()
	if err != nil {
		t.Error("expected this to succeed. Error: " + err.Error())
	}
}

func TestDataSatisfiesSchema(t *testing.T) {
	err := DataSatisfiesSchema(&Data{}, &Schema{})
	if err == nil {
		t.Error("shouldn't accept structures which are obviously invalid")
	}
	s := Schema{
		FirestoreID:  "dskjlasdjkl",
		IndustryID:   "TestDataSatisfiesSchema",
		DomainID:     "TestDataSatisfiesSchema",
		SchemaID:     "TestDataSatisfiesSchema",
		DataTypes:    make(map[string]string),
		DataKeys:     []string{},
		DataHelpText: map[string]string{},
	}
	err = DataSatisfiesSchema(&Data{}, &s)
	if err == nil {
		t.Error("shouldn't accept an empty DataTypes")
	}
	d := Data{
		FirestoreID: "lkslkjslks",
		IndustryID:  "TestDataSatisfiesSchema",
		DomainID:    "TestDataSatisfiesSchema",
		SchemaID:    "TestDataSatisfiesSchema",
	}
	d.Init(nil)
	err = DataSatisfiesSchema(&d, &s)
	if err != nil {
		t.Error("expected that with fully initialized (but empty) data structures the DataTypes would be validated. Error: " + err.Error())
	}
	d.F["a"] = Float64(44.33)
	d.Ref["b"] = String("/t/a/b/c?data=a&ux=a&schema=a")
	d.S["c"] = String("jklsdjlksd")
	d.I["d"] = Int64(99)
	d.B["e"] = Bool(false)
	err = DataSatisfiesSchema(&d, &s)
	if err == nil {
		t.Error("should be error if the schema is empty")
	}
	s.DataTypes["a"] = "F"
	s.DataTypes["c"] = "S"
	s.DataTypes["d"] = "I"
	s.DataTypes["e"] = "B"
	s.DataKeys = []string{"a", "c", "d", "e"}
	s.DataHelpText = map[string]string{
		"a": "help",
		"c": "help",
		"d": "help",
		"e": "help",
	}
	err = DataSatisfiesSchema(&d, &s)
	if err == nil {
		t.Error("should be error if schema does not fully cover the input")
	}
	s.DataTypes["b"] = "S"
	s.DataKeys = append(s.DataKeys, "b")
	s.DataHelpText["b"] = "help added"
	err = DataSatisfiesSchema(&d, &s)
	if err == nil {
		t.Error("may have all entries, but b is not a String it's a reference")
	}
	delete(s.DataTypes, "b")
	delete(s.DataHelpText, "b")
	s.DataKeys = s.DataKeys[:len(s.DataKeys)-1]
	s.DataTypes["_b"] = "Ref"
	s.DataKeys = append(s.DataKeys, "_b")
	s.DataHelpText["_b"] = "help added"
	s.DataTypes["f"] = "Q"
	s.DataKeys = append(s.DataKeys, "f")
	s.DataHelpText["f"] = "help added"
	err = DataSatisfiesSchema(&d, &s)
	if err == nil {
		t.Error("should fail validation of Schema if a provided Key's value is not a real type")
	}
	delete(s.DataTypes, "f")
	delete(s.DataHelpText, "f")
	s.DataKeys = s.DataKeys[:len(s.DataKeys)-1]
	err = DataSatisfiesSchema(&d, &s)
	if err != nil {
		t.Error("should have done everything right. Error: " + err.Error())
	}
}

func TestSchemaSatisfiesTask(t *testing.T) {
	type args struct {
		s *Schema
		t *Task
	}
	tgValid := taskGoal{
		Name:         "ya and ye string comparison",
		HelpText:     "comparies ya and ye and sees if they are the same",
		Operation:    "==",
		Op1Variable:  "ya",
		Op2Variable:  "ye",
		Op2Constant:  nil,
		ErrorMessage: "ya and ye must be the same",
	}
	tgMarshalled, err := json.Marshal(tgValid)
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil",
			args: args{
				s: nil,
				t: nil,
			},
			wantErr: true,
		},
		{
			name: "empty",
			args: args{
				s: &Schema{},
				t: &Task{},
			},
			wantErr: true,
		},
		{
			name: "empty data types but populated goals",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesTask",
					NextSchemaID: nil,
					IndustryID:   "TestSchemaSatisfiesTask",
					DomainID:     "TestSchemaSatisfiesTask",
					SchemaID:     "TestSchemaSatisfiesTask",
					DataTypes:    map[string]string{},
					Deprecated:   nil,
				},
				t: &Task{
					FirestoreID: "TestSchemaSatisfiesTask",
					IndustryID:  "TestSchemaSatisfiesTask",
					DomainID:    "TestSchemaSatisfiesTask",
					Name:        "Name",
					HelpText:    "HelpText",
					TaskGoals:   map[string]string{"a": "something"},
					Deprecated:  Bool(false),
				},
			},
			wantErr: true,
		},
		{
			name: "empty goals and data types",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesTask",
					NextSchemaID: nil,
					IndustryID:   "TestSchemaSatisfiesTask",
					DomainID:     "TestSchemaSatisfiesTask",
					SchemaID:     "TestSchemaSatisfiesTask",
					DataTypes:    map[string]string{},
					DataKeys:     []string{},
					DataHelpText: make(map[string]string),
					Deprecated:   nil,
				},
				t: &Task{
					FirestoreID: "TestSchemaSatisfiesTask",
					IndustryID:  "TestSchemaSatisfiesTask",
					DomainID:    "TestSchemaSatisfiesTask",
					Name:        "Name",
					HelpText:    "HelpText",
					TaskGoals:   map[string]string{},
					Deprecated:  Bool(false),
				},
			},
			wantErr: false,
		},
		{
			name: "empty goals and full data types",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesTask",
					NextSchemaID: nil,
					IndustryID:   "TestSchemaSatisfiesTask",
					DomainID:     "TestSchemaSatisfiesTask",
					SchemaID:     "TestSchemaSatisfiesTask",
					DataTypes:    map[string]string{"ya": "S"},
					DataKeys:     []string{"ya"},
					DataHelpText: map[string]string{"ya": "ya"},
					Deprecated:   nil,
				},
				t: &Task{
					FirestoreID: "TestSchemaSatisfiesTask",
					IndustryID:  "TestSchemaSatisfiesTask",
					DomainID:    "TestSchemaSatisfiesTask",
					Name:        "Name",
					HelpText:    "HelpText",
					TaskGoals:   map[string]string{},
					Deprecated:  Bool(false),
				},
			},
			wantErr: false,
		},
		{
			name: "goals and data types but one is missing",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesTask",
					NextSchemaID: nil,
					IndustryID:   "TestSchemaSatisfiesTask",
					DomainID:     "TestSchemaSatisfiesTask",
					SchemaID:     "TestSchemaSatisfiesTask",
					DataTypes:    map[string]string{"ya": "S"},
					DataKeys:     []string{"ya"},
					DataHelpText: map[string]string{"ya": "help text"},
					Deprecated:   nil,
				},
				t: &Task{
					FirestoreID: "TestSchemaSatisfiesTask",
					IndustryID:  "TestSchemaSatisfiesTask",
					DomainID:    "TestSchemaSatisfiesTask",
					Name:        "Name",
					HelpText:    "HelpText",
					TaskGoals:   map[string]string{"ya": "B", "ye": "C"},
					Deprecated:  Bool(false),
				},
			},
			wantErr: true,
		},
		{
			name: "goals and data types and all are present",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesTask",
					NextSchemaID: nil,
					IndustryID:   "TestSchemaSatisfiesTask",
					DomainID:     "TestSchemaSatisfiesTask",
					SchemaID:     "TestSchemaSatisfiesTask",
					DataKeys:     []string{"ya", "ye", "q"},
					DataTypes:    map[string]string{"ya": "S", "ye": "S", "q": "F"},
					DataHelpText: map[string]string{"ya": "help text", "ye": "hs", "q": "sdklj"},
					Deprecated:   nil,
				},
				t: &Task{
					FirestoreID: "TestSchemaSatisfiesTask",
					IndustryID:  "TestSchemaSatisfiesTask",
					DomainID:    "TestSchemaSatisfiesTask",
					Name:        "Name",
					HelpText:    "HelpText",
					TaskGoals:   map[string]string{"ya": string(tgMarshalled)},
					Deprecated:  Bool(false),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SchemaSatisfiesTask(tt.args.s, tt.args.t); (err != nil) != tt.wantErr {
				t.Errorf("SchemaSatisfiesTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSchemaSatisfiesUX(t *testing.T) {
	type args struct {
		s  *Schema
		ux *UX
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil",
			args: args{
				s:  nil,
				ux: nil,
			},
			wantErr: true,
		},
		{
			name: "empty",
			args: args{
				s:  &Schema{},
				ux: &UX{},
			},
			wantErr: true,
		},
		{
			name: "empty and invalid schema",
			args: args{
				s: &Schema{},
				ux: &UX{
					FirestoreID:  "TestSchemaSatisfiesUX",
					IndustryID:   "TestSchemaSatisfiesUX",
					DomainID:     "TestSchemaSatisfiesUX",
					SchemaID:     "TestSchemaSatisfiesUX",
					HTML:         String("TestSchemaSatisfiesUX"),
					SelectFields: map[string]string{},
					Deprecated:   nil,
				},
			},
			wantErr: true,
		},
		{
			name: "HTML without inputs",
			args: args{
				s: &Schema{
					FirestoreID:  "TestSchemaSatisfiesUX",
					NextSchemaID: String("TestSchemaSatisfiesUX"),
					IndustryID:   "TestSchemaSatisfiesUX",
					DomainID:     "TestSchemaSatisfiesUX",
					SchemaID:     "TestSchemaSatisfiesUX",
					DataTypes:    map[string]string{"a": "S", "b": "S"},
					DataKeys:     []string{"a", "b"},
					DataHelpText: map[string]string{"a": "help", "b": "text"},
					Deprecated:   Bool(false),
				},
				ux: &UX{
					FirestoreID:  "TestSchemaSatisfiesUX",
					IndustryID:   "TestSchemaSatisfiesUX",
					DomainID:     "TestSchemaSatisfiesUX",
					SchemaID:     "TestSchemaSatisfiesUX",
					HTML:         String("TestSchemaSatisfiesUX"),
					SelectFields: map[string]string{},
					Deprecated:   nil,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SchemaSatisfiesUX(tt.args.s, tt.args.ux); (err != nil) != tt.wantErr {
				t.Errorf("SchemaSatisfiesUX() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_taskGoal_Validate(t1 *testing.T) {
	type fields struct {
		Name         string
		HelpText     string
		Operation    string
		Op1Variable  string
		Op2Variable  string
		Op2Constant  interface{}
		ErrorMessage string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "nil",
			fields:  fields{},
			wantErr: true,
		},
		{
			name: "empty Name",
			fields: fields{
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "avar2",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "empty Operation",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "",
				Op1Variable:  "avar",
				Op2Variable:  "avar2",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "empty Op1",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "",
				Op2Variable:  "avar2",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "empty Op2",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "empty ErrorMessage",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "as",
				ErrorMessage: "",
			},
			wantErr: true,
		},
		{
			name: "bad operation",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "<<",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  "hello there",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "working with Op2Variable",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "avar",
				ErrorMessage: "some error",
			},
			wantErr: false,
		},
		{
			name: "working with Op2Constant",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  int64(15),
				ErrorMessage: "some error",
			},
			wantErr: false,
		},
		{
			name: "working with no help text",
			fields: fields{
				Name:         "some name",
				HelpText:     "",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "aVar3",
				ErrorMessage: "some error",
			},
			wantErr: false,
		},
		{
			name: "working with a constant string",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "==",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  "hello there",
				ErrorMessage: "some error",
			},
			wantErr: false,
		},
		{
			name: "string constant failing with bad comparison",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "<=",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  "hello there",
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "failing with a bad bool comparison",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "<",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  true,
				ErrorMessage: "some error",
			},
			wantErr: true,
		},
		{
			name: "boolean succeeding with a valid comparison",
			fields: fields{
				Name:         "some name",
				HelpText:     "some help text",
				Operation:    "!=",
				Op1Variable:  "avar",
				Op2Variable:  "",
				Op2Constant:  false,
				ErrorMessage: "some error",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &taskGoal{
				Name:         tt.fields.Name,
				HelpText:     tt.fields.HelpText,
				Operation:    tt.fields.Operation,
				Op1Variable:  tt.fields.Op1Variable,
				Op2Variable:  tt.fields.Op2Variable,
				Op2Constant:  tt.fields.Op2Constant,
				ErrorMessage: tt.fields.ErrorMessage,
			}
			if err := t.Validate(); (err != nil) != tt.wantErr {
				t1.Errorf("Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTask_TaskGoalAdd(t1 *testing.T) {
	type fields struct {
		FirestoreID string
		IndustryID  string
		DomainID    string
		ContextID   string
		TaskGoals   map[string]string
		Deprecated  *bool
	}
	type args struct {
		associatedField string
		name            string
		helpText        string
		op1Variable     string
		operation       string
		op2Variable     string
		op2Constant     interface{}
		errorMessage    string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "failing validation because no name",
			fields: fields{
				TaskGoals: map[string]string{},
			},
			args: args{
				associatedField: "something",
				name:            "",
				helpText:        "help text",
				op1Variable:     "jksdfljksd",
				operation:       "==",
				op2Variable:     "something",
				op2Constant:     nil,
				errorMessage:    "some error message",
			},
			wantErr: true,
		},
		{
			name: "failing validation because bad operation",
			fields: fields{
				TaskGoals: map[string]string{},
			},
			args: args{
				associatedField: "",
				name:            "hello there",
				helpText:        "help text",
				op1Variable:     "jksdfljksd",
				operation:       "|=",
				op2Variable:     "something",
				op2Constant:     nil,
				errorMessage:    "some error message",
			},
			wantErr: true,
		},
		{
			name: "succeeding with adding with key: polyappErrorhello there",
			fields: fields{
				TaskGoals: map[string]string{},
			},
			args: args{
				associatedField: "hello there",
				name:            "hello there",
				helpText:        "help text",
				op1Variable:     "jksdfljksd",
				operation:       "==",
				op2Variable:     "something",
				op2Constant:     nil,
				errorMessage:    "some error message",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Task{
				FirestoreID: tt.fields.FirestoreID,
				IndustryID:  tt.fields.IndustryID,
				DomainID:    tt.fields.DomainID,
				TaskGoals:   tt.fields.TaskGoals,
				Deprecated:  tt.fields.Deprecated,
			}
			err := t.TaskGoalAdd(tt.args.associatedField, tt.args.name, tt.args.helpText, tt.args.op1Variable, tt.args.operation, tt.args.op2Variable, tt.args.op2Constant, tt.args.errorMessage)
			if (err != nil) != tt.wantErr {
				t1.Errorf("TaskGoalAdd() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil {
				// check if we put something into the map
				v, ok := t.TaskGoals["polyappError"+tt.args.associatedField]
				if !ok {
					t1.Error("did not add to TaskGoals")
				}
				if v == "" || len(v) < 20 {
					t1.Error("probably did not marshall the TaskGoal correctly")
				}
			}
		})
	}
}

func TestTaskGoalEvaluate(t *testing.T) {
	type args struct {
		name string
		task *Task
		d    *Data
	}
	marshalledStringTaskGoal, err := json.Marshal(taskGoal{
		Name:         "hello there",
		HelpText:     "help text",
		Op1Variable:  "var1",
		Operation:    "==",
		Op2Variable:  "var2",
		Op2Constant:  nil,
		ErrorMessage: "some error message",
	})
	if err != nil {
		t.Fatal(err)
	}
	marshalledBoolTaskGoal, err := json.Marshal(taskGoal{
		Name:         "dos",
		HelpText:     "help text",
		Op1Variable:  "var1",
		Operation:    "==",
		Op2Constant:  true,
		ErrorMessage: "error message 2",
	})
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name             string
		args             args
		wantErrorMessage string
		wantErr          bool
	}{
		{
			name: "nil",
			args: args{
				name: "",
				task: nil,
				d:    nil,
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "empty name",
			args: args{
				name: "",
				task: &Task{
					TaskGoals: map[string]string{"hi": "there"},
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           nil,
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "empty task",
			args: args{
				name: "hi",
				task: nil,
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           nil,
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "empty Data",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: map[string]string{"hi": "there"},
				},
				d: nil,
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "TaskGoals as nil",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: nil,
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           nil,
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "Data uninitialized",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: map[string]string{"hi": "there"},
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           nil,
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "passing with no user error",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: map[string]string{"hi": string(marshalledStringTaskGoal)},
					Name:      "Name",
					HelpText:  "HelpText",
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           map[string]*string{"var1": String("alpha"), "var2": String("alpha")},
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          false,
			wantErrorMessage: "",
		},
		{
			name: "passing with a user error because values do not match",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: map[string]string{"hi": string(marshalledStringTaskGoal)},
					Name:      "Name",
					HelpText:  "HelpText",
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           map[string]*string{"var1": String("alpha"), "var2": String("beta")},
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          false,
			wantErrorMessage: "some error message",
		},
		{
			name: "failing because one of the needed inputs was not provided",
			args: args{
				name: "hi",
				task: &Task{
					TaskGoals: map[string]string{"hi": string(marshalledStringTaskGoal)},
					Name:      "Name",
					HelpText:  "HelpText",
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					S:           map[string]*string{"var1": String("alpha"), "var10010102": String("alpha")},
					I:           nil,
					B:           nil,
					F:           nil,
				},
			},
			wantErr:          true,
			wantErrorMessage: "",
		},
		{
			name: "passing with the constant code path",
			args: args{
				name: "two",
				task: &Task{
					TaskGoals: map[string]string{"two": string(marshalledBoolTaskGoal)},
					Name:      "Name",
					HelpText:  "HelpText",
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					B:           map[string]*bool{"var1": Bool(true)},
				},
			},
			wantErr:          false,
			wantErrorMessage: "",
		},
		{
			name: "failing with the constant code path",
			args: args{
				name: "two",
				task: &Task{
					TaskGoals: map[string]string{"two": string(marshalledBoolTaskGoal)},
					Name:      "Name",
					HelpText:  "HelpText",
				},
				d: &Data{
					FirestoreID: "TestTaskGoalEvaluate",
					IndustryID:  "TestTaskGoalEvaluate",
					DomainID:    "TestTaskGoalEvaluate",
					SchemaID:    "TestTaskGoalEvaluate",
					B:           map[string]*bool{"var1": Bool(false)},
				},
			},
			wantErr:          false,
			wantErrorMessage: "error message 2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotErrorMessage, err := TaskGoalEvaluate(tt.args.name, tt.args.task, tt.args.d)
			if (err != nil) != tt.wantErr {
				t.Errorf("TaskGoalEvaluate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotErrorMessage != tt.wantErrorMessage {
				t.Errorf("TaskGoalEvaluate() gotErrorMessage = %v, want %v", gotErrorMessage, tt.wantErrorMessage)
			}
		})
	}
}

func Test_evalBool(t *testing.T) {
	type args struct {
		op1          bool
		operation    string
		op2          bool
		errorMessage string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "== success",
			args: args{
				op1:          false,
				operation:    "==",
				op2:          false,
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "== fail",
			args: args{
				op1:          true,
				operation:    "==",
				op2:          false,
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
		{
			name: "!= success",
			args: args{
				op1:          true,
				operation:    "!=",
				op2:          false,
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "!= fail",
			args: args{
				op1:          true,
				operation:    "!=",
				op2:          true,
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := evalBool(tt.args.op1, tt.args.operation, tt.args.op2, tt.args.errorMessage)
			if (err != nil) != tt.wantErr {
				t.Errorf("evalBool() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("evalBool() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_evalFloat(t *testing.T) {
	type args struct {
		op1          float64
		operation    string
		op2          float64
		errorMessage string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "== success",
			args: args{
				op1:          float64(20),
				operation:    "==",
				op2:          float64(20),
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "== fail",
			args: args{
				op1:          float64(12),
				operation:    "==",
				op2:          float64(20),
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
		{
			name: "!= success",
			args: args{
				op1:          float64(-20),
				operation:    "!=",
				op2:          float64(0),
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "!= fail",
			args: args{
				op1:          float64(0),
				operation:    "!=",
				op2:          float64(0),
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := evalFloat(tt.args.op1, tt.args.operation, tt.args.op2, tt.args.errorMessage)
			if (err != nil) != tt.wantErr {
				t.Errorf("evalFloat() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("evalFloat() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_evalInt(t *testing.T) {
	type args struct {
		op1          int64
		operation    string
		op2          int64
		errorMessage string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "== success",
			args: args{
				op1:          int64(20),
				operation:    "==",
				op2:          int64(20),
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "== fail",
			args: args{
				op1:          int64(12),
				operation:    "==",
				op2:          int64(20),
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
		{
			name: "!= success",
			args: args{
				op1:          int64(-20),
				operation:    "!=",
				op2:          int64(0),
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "!= fail",
			args: args{
				op1:          int64(0),
				operation:    "!=",
				op2:          int64(0),
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := evalInt(tt.args.op1, tt.args.operation, tt.args.op2, tt.args.errorMessage)
			if (err != nil) != tt.wantErr {
				t.Errorf("evalInt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("evalInt() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_evalString(t *testing.T) {
	type args struct {
		op1          string
		operation    string
		op2          string
		errorMessage string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "== success",
			args: args{
				op1:          "false",
				operation:    "==",
				op2:          "false",
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "== fail",
			args: args{
				op1:          "true",
				operation:    "==",
				op2:          "false",
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
		{
			name: "!= success",
			args: args{
				op1:          "true",
				operation:    "!=",
				op2:          "false",
				errorMessage: "some message",
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "!= fail",
			args: args{
				op1:          "true",
				operation:    "!=",
				op2:          "true",
				errorMessage: "some message",
			},
			want:    "some message",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := evalString(tt.args.op1, tt.args.operation, tt.args.op2, tt.args.errorMessage)
			if (err != nil) != tt.wantErr {
				t.Errorf("evalString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("evalString() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateRef(t *testing.T) {
	type args struct {
		ref string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "passing with a task override",
			args: args{
				ref: "/t/industry/domain/task?schema=s&task=t&data=d&ux=ux",
			},
			wantErr: false,
		},
		{
			name: "no task",
			args: args{
				ref: "/t/industry/domain?schema=s&task=t&data=d",
			},
			wantErr: true,
		},
		{
			name: "passing with common.CreateRef",
			args: args{
				ref: CreateRef("industry", "domain", "task", "ux", "schema", "data"),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateRef(tt.args.ref); (err != nil) != tt.wantErr {
				t.Errorf("ValidateRef() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
