package common

import (
	cryptoRand "crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"math/rand"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"time"
	"unicode"
	"unsafe"
)

// CSSEscape takes a sequence of characters which could form a valid HTML5 ID and translates them to a sequence of characters
// which forms a valid HTML4 ID. It does this primarily by escaping characters like leading numbers and !"@#(*)$%, etc.
//
// Reference documentation: https://mathiasbynens.be/notes/css-escapes
//
// Please remember that this does NOT convert invalid characters like ' ' into something like %20. So typically you would
// call url.PathEscape(id) and then CSSEscape(id).
func CSSEscape(id string) string {
	if len(id) == 0 {
		return id
	}
	id = strings.ReplaceAll(id, " ", "%20")
	firstChar := rune(id[0])
	if '0' <= firstChar && firstChar <= '9' {
		id = `\3` + string(id[0]) + ` ` + id[1:]
	}
	replacer := strings.NewReplacer(`!`, `\!`, `"`, `\"`, `#`, `\#`, `$`, `\$`, `%`, `\%`, `&`, `\&`, `'`, `\'`,
		`(`, `\(`, `)`, `\)`, `*`, `\*`, `+`, `\+`, `,`, `\,`, `-`, `\-`, `.`, `\.`, `/`, `\/`, `:`, `\:`, `;`, `\;`,
		`<`, `\<`, `=`, `\=`, `>`, `\>`, `?`, `\?`, `@`, `\@`, `[`, `\[`, `\`, `\\`, `]`, `\]`, `^`, `\^`, `{`, `\{`,
		`|`, `\|`, `}`, `\}`, `~`, `\~`, "`", "\\`")
	id = replacer.Replace(id)
	return id
}

// RemoveNonAlphaNumeric removes non-alphanumeric characters from the string passed in
// it leaves in spaces in the middle but removes them at the edges with strings.TrimSpace
func RemoveNonAlphaNumeric(s string) string {
	s = strings.TrimSpace(s)
	var sBuilder strings.Builder
	var err error
	for _, c := range s {
		// find bad chars
		if (!unicode.IsLetter(c)) && (!unicode.IsNumber(c)) && c != ' ' {
			continue
		}
		_, err = sBuilder.WriteRune(c)
		if err != nil {
			return ""
		}
	}
	return sBuilder.String()
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var randStringMux sync.Mutex
var src rand.Source = nil

// GetRandString returns a random string of length n with an intermediate level of entropy.
// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go#31832326
func GetRandString(n int) string {
	randStringMux.Lock()
	defer randStringMux.Unlock()
	if src == nil {
		src = rand.NewSource(time.Now().UnixNano())
	}
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

// GetCryptographicallyRandString always returns a cryptographically random string of length >= size.
//
// For a faster version which guarantees the string length, use GetRandString(n int).
func GetCryptographicallyRandString(size int) (string, error) {
	b := make([]byte, size)
	_, err := cryptoRand.Read(b)
	if err != nil {
		return "", fmt.Errorf("cryptoRand.Read: %w", err)
	}
	var result strings.Builder
	for {
		if result.Len() >= size {
			return result.String(), nil
		}
		num, err := cryptoRand.Int(cryptoRand.Reader, big.NewInt(int64(127)))
		if err != nil {
			return "", fmt.Errorf("cryptoRand.Int: %w", err)
		}
		if (num.Int64() > 64 && num.Int64() < 91) ||
			(num.Int64() > 96 && num.Int64() < 123) ||
			(num.Int64() > 47 && num.Int64() < 58) {
			result.WriteString(string(num.Int64()))
		}
	}
}

// RemoveDataKeyUnderscore converts a key of the format: "_industry_context_schema_key" to the format: "industry_context_schema_key".
func RemoveDataKeyUnderscore(k string) string {
	if len(k) > 0 && []rune(k)[0] == '_' {
		return k[1:]
	}
	return k
}

// RemoveFieldPrefix converts a key of the format: "industry_context_schema_key" to the format: "key".
func RemoveFieldPrefix(k string) string {
	split := strings.Split(k, "_")
	return split[len(split)-1]
}

// AddFieldPrefix converts a key of the format: "key" to the format: "industry_context_schema_key".
func AddFieldPrefix(industryID string, domainID string, schemaID string, k string) string {
	return industryID + "_" + domainID + "_" + schemaID + "_" + k
}

// Dereference dereferences pointer values. For instance, it will turn *string values into string.
func Dereference(simple map[string]interface{}) {
	for k, v := range simple {
		switch vTyped := v.(type) {
		case *bool:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *uint:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *uint8:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *uint16:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *uint32:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *uint64:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *int:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *int8:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *int16:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *int32:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *int64:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *float32:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *float64:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *complex64:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *complex128:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		case *string:
			if vTyped == nil {
				simple[k] = nil
			} else {
				simple[k] = *vTyped
			}
		}
	}
}

var (
	publicPath = ""
)

// GetPublicPath returns the path to the 'public' directory in this git repo.
func GetPublicPath() string {
	if publicPath != "" {
		return publicPath
	}
	_, isAppEngine := os.LookupEnv("GAE_APPLICATION")
	goPath, pathExists := os.LookupEnv("GOPATH")
	if len(goPath) > 0 && goPath[len(goPath)-1] == ';' {
		goPath = goPath[:len(goPath)-1]
	}
	if pathExists {
		publicPath = filepath.Join(filepath.ToSlash(goPath), "src", "gitlab.com", "polyapp-open-source", "polyapp", "public")
	} else if isAppEngine {
		// App Engine
		publicPath = "/workspace/public"
	} else {
		// Run
		publicPath = "/polyapp/public"
	}
	return publicPath
}

// BlobURL returns the URL of a blob asset like an image or audio file given that asset's ID in Firestore.
// An example ID would be: industry_domain_schema_field%20name
//
// Note: this is coupled to BlobHandler in defaultservice/main.go
func BlobURL(firestoreID string, field string) string {
	return "/blob/assets/" + firestoreID + "/" + field
}

// GetDataRef is the first key in AllData. This func should stay in sync with getDataRef on the client.
func GetDataRef(request POSTRequest) string {
	industryS := ""
	if request.OverrideIndustryID != "polyappNone" {
		industryS = "industry=" + url.PathEscape(request.OverrideIndustryID)
	}
	domainS := ""
	if request.OverrideDomainID != "polyappNone" {
		domainS = "domain=" + url.PathEscape(request.OverrideDomainID)
	}
	taskS := ""
	if request.OverrideTaskID != "polyappNone" {
		taskS = "task=" + request.OverrideTaskID
	}
	dataIndex := "/t/" + request.IndustryID + "/" + request.DomainID + "/" + request.TaskID + "?"
	if industryS != "" {
		dataIndex += industryS + "&"
	}
	if domainS != "" {
		dataIndex += domainS + "&"
	}
	if taskS != "" {
		dataIndex += taskS + "&"
	}
	dataIndex += "ux=" + request.UXID + "&schema=" + request.SchemaID + "&data=" + request.DataID
	return url.PathEscape(dataIndex)
}

// CreateDataFromSchema is very helpful if you are trying to load a new page with a new Data. It is used when you
// take an action in the UI which creates a new Data document which users then immediately navigate to.
//
// overrideData does NOT copy the override's Industry, Domain or Schema IDs. It only copies over the actual data and
// only if that data's Field is present in the Schema which is also being passed in.
//
// Side effects: This function creates the Data in the DB.
func CreateDataFromSchema(schema *Schema, overrideData *Data) (*Data, error) {
	newData := &Data{
		FirestoreID: GetRandString(25),
	}
	toInit := map[string]interface{}{
		PolyappIndustryID: schema.IndustryID,
		PolyappDomainID:   schema.DomainID,
		PolyappSchemaID:   schema.FirestoreID,
	}
	// why add this? Well if we do not add any information /polyappChangeData/ will not find this existing Data
	// document because its query, /polyappDataTable/, will not find the field it is trying to order by.
	for k, dataType := range schema.DataTypes {
		switch dataType {
		case "S":
			v, ok := overrideData.S[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = ""
			}
		case "I":
			v, ok := overrideData.I[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = int64(0)
			}
		case "F":
			v, ok := overrideData.F[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = float64(0)
			}
		case "B":
			v, ok := overrideData.B[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = false
			}
		case "Ref":
			v, ok := overrideData.Ref[k[1:]] // schema keys include _, but refs do not. We need to remove the _.
			if ok {
				toInit[k] = v
				continue
			}
		case "AS":
			v, ok := overrideData.AS[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = []string{}
			}
		case "AI":
			v, ok := overrideData.AI[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = []int64{}
			}
		case "AF":
			v, ok := overrideData.AF[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = []float64{}
			}
		case "AB":
			v, ok := overrideData.AB[k]
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = []bool{}
			}
		case "ARef":
			v, ok := overrideData.ARef[k[1:]] // schema keys include _, but refs do not. We need to remove the _.
			if ok {
				toInit[k] = v
				continue
			} else {
				toInit[k] = []string{}
			}
		}
	}
	err := newData.Init(toInit)
	if err != nil {
		return nil, fmt.Errorf("createData could not init: %w", err)
	}
	return newData, nil
}

// RoleHasAccess returns true if the role has access to both the industry and domain.
//
// accessType must be one of: 'c', 'r', 'u', 'd'
func RoleHasAccess(role *Role, industryID string, domainID string, schemaID string, accessType byte) (bool, error) {
	if role.Deprecated != nil && *role.Deprecated {
		return false, nil
	}
	switch accessType {
	case 'c', 'r', 'u', 'd':
	default:
		return false, fmt.Errorf("accessType (%v) was not one of: c, r, u, d", accessType)
	}
	for i := range role.Access {
		if (role.Access[i].Industry == industryID || role.Access[i].Industry == "polyapp") &&
			(role.Access[i].Domain == domainID || role.Access[i].Domain == "polyapp") &&
			(role.Access[i].Schema == "" || role.Access[i].Schema == schemaID || role.Access[i].Schema == "polyapp") {
			// This Role's Access applies
			switch accessType {
			case 'c':
				if role.Access[i].Create {
					return true, nil
				}
			case 'r':
				if role.Access[i].Read {
					return true, nil
				}
			case 'u':
				if role.Access[i].Update {
					return true, nil
				}
			case 'd':
				if role.Access[i].Delete {
					return true, nil
				}
			}
		}
	}
	return false, nil
}

// RoleHasAccessKeyValues checks if a role has the requested cast-insensitive value at the given case-sensitive key for
// an Access which includes the Industry and Domain and (optionally) Schema. Returns true, nil if access is allowed.
func RoleHasAccessKeyValues(role *Role, industryID string, domainID string, schemaID string, key string, value string) (bool, error) {
	if role.Deprecated != nil && *role.Deprecated {
		return false, nil
	}
	for i := range role.Access {
		if (role.Access[i].Industry == industryID || role.Access[i].Industry == "polyapp") &&
			(role.Access[i].Domain == domainID || role.Access[i].Domain == "polyapp") &&
			(role.Access[i].Schema == "" || role.Access[i].Schema == schemaID || role.Access[i].Schema == "polyapp") {
			if role.Access[i].KeyValue[key] == strings.ToLower(value) {
				return true, nil
			}
		}
	}
	return false, nil
}

// SchemaConvertSchema converts a Schema into a new Schema. For example, convert schema with ID "A" to one with ID "B".
//
// The new Schema is returned.
func SchemaConvertSchema(oldSchema *Schema, newFirestoreID string) (Schema, error) {
	if oldSchema == nil {
		return Schema{}, fmt.Errorf("newSchema or oldSchema were nil")
	}
	if newFirestoreID == "" {
		return Schema{}, errors.New("FirestoreID was empty")
	}

	newSchema := Schema{
		FirestoreID:  newFirestoreID,
		NextSchemaID: nil,
		IndustryID:   oldSchema.IndustryID,
		DomainID:     oldSchema.DomainID,
		SchemaID:     newFirestoreID,
		DataTypes:    map[string]string{},
		DataHelpText: map[string]string{},
		DataKeys:     make([]string, len(oldSchema.DataKeys)),
		IsPublic:     oldSchema.IsPublic,
		Deprecated:   oldSchema.Deprecated,
	}
	for k, v := range oldSchema.DataTypes {
		newKey := k
		if k[0] != '_' {
			newKey = ReplaceIndustryDomainSchemaInKey(k, oldSchema.IndustryID, oldSchema.DomainID, newFirestoreID)
		}
		newSchema.DataTypes[ReplaceIndustryDomainSchemaInKey(newKey, oldSchema.IndustryID, oldSchema.DomainID, newFirestoreID)] = v
	}
	for k, v := range oldSchema.DataHelpText {
		newKey := k
		if k[0] != '_' {
			newKey = ReplaceIndustryDomainSchemaInKey(k, oldSchema.IndustryID, oldSchema.DomainID, newFirestoreID)
		}
		newSchema.DataHelpText[newKey] = v
	}
	for i := range oldSchema.DataKeys {
		newKey := oldSchema.DataKeys[i]
		if oldSchema.DataKeys[i][0] != '_' {
			newKey = ReplaceIndustryDomainSchemaInKey(oldSchema.DataKeys[i], oldSchema.IndustryID, oldSchema.DomainID, newFirestoreID)
		}
		newSchema.DataKeys[i] = ReplaceIndustryDomainSchemaInKey(newKey, oldSchema.IndustryID, oldSchema.DomainID, newFirestoreID)
	}
	return newSchema, nil
}

func SchemaConvertTask(task *Task, newSchemaID string) (Task, error) {
	if task == nil {
		return Task{}, fmt.Errorf("Task == nil")
	}
	if newSchemaID == "" {
		return Task{}, fmt.Errorf("newSchemaID was empty")
	}
	newTaskGoals := make(map[string]string)
	for k, v := range task.TaskGoals {
		newTaskGoals[ReplaceIndustryDomainSchemaInKey(k, task.IndustryID, task.DomainID, newSchemaID)] = v
	}
	newFieldSecurity := make(map[string]*FieldSecurityOptions)
	for k, v := range task.FieldSecurity {
		newFieldSecurity[ReplaceIndustryDomainSchemaInKey(k, task.IndustryID, task.DomainID, newSchemaID)] = v
	}
	newTask := Task{
		FirestoreID:               task.FirestoreID,
		IndustryID:                task.IndustryID,
		DomainID:                  task.DomainID,
		Name:                      task.Name,
		HelpText:                  task.HelpText,
		TaskGoals:                 newTaskGoals,
		FieldSecurity:             newFieldSecurity,
		BotsTriggeredAtLoad:       task.BotsTriggeredAtLoad,
		BotsTriggeredContinuously: task.BotsTriggeredContinuously,
		BotsTriggeredAtDone:       task.BotsTriggeredAtDone,
		BotStaticData:             task.BotStaticData,
		IsPublic:                  task.IsPublic,
		Deprecated:                task.Deprecated,
	}
	return newTask, nil
}

func SchemaConvertUX(oldUX *UX, newSchemaID string) (UX, error) {
	if oldUX == nil {
		return UX{}, fmt.Errorf("UX == nil")
	}
	if newSchemaID == "" {
		return UX{}, fmt.Errorf("newSchemaID was empty")
	}
	newHTML := *oldUX.HTML
	// We need a way to replace all instances of the old Schema ID with the new Schema ID in the HTML.
	// We want to replace the ID in 'for', 'ID', and possibly other tags, like in some 'data'.
	// Our 2 options are to ID all of these tags, parse the HTML, and then convert the data.
	// Or do the dumb thing which is sometimes wrong and use strings.ReplaceAll. I don't have a lot of time and I'll do the dumb thing.
	newHTML = strings.ReplaceAll(newHTML, oldUX.IndustryID+"_"+oldUX.DomainID+"_"+oldUX.SchemaID+"_", oldUX.IndustryID+"_"+oldUX.DomainID+"_"+newSchemaID+"_")
	newUX := UX{
		FirestoreID:     oldUX.FirestoreID,
		IndustryID:      oldUX.IndustryID,
		DomainID:        oldUX.DomainID,
		SchemaID:        newSchemaID,
		HTML:            String(newHTML),
		Navbar:          oldUX.Navbar,
		Title:           oldUX.Title,
		MetaDescription: oldUX.MetaDescription,
		MetaKeywords:    oldUX.MetaKeywords,
		MetaAuthor:      oldUX.MetaAuthor,
		HeadTag:         oldUX.HeadTag,
		IconPath:        oldUX.IconPath,
		CSSPath:         oldUX.CSSPath,
		SelectFields:    oldUX.SelectFields, // TODO fix this after re-implementing SelectFields & re-testing it.
		IsPublic:        oldUX.IsPublic,
		FullAuthPage:    oldUX.FullAuthPage,
		Deprecated:      oldUX.Deprecated,
	}
	return newUX, nil
}

func ReplaceIndustryDomainSchemaInKey(key string, newIndustry, newDomain, newSchema string) string {
	splitKey := strings.Split(key, "_")
	if splitKey[0] == "" {
		if len(splitKey) != 5 {
			return ""
		}
		// "", "industry", "domain", "schema", "field name" happens for Refs.
		return "_" + newIndustry + "_" + newDomain + "_" + newSchema + "_" + splitKey[4]
	}
	if len(splitKey) != 4 {
		return ""
	}
	// industry, domain, schema, field name
	return newIndustry + "_" + newDomain + "_" + newSchema + "_" + splitKey[3]
}

func SplitField(field string) (industry, domain, schema, key string) {
	splitKey := strings.Split(field, "_")
	if splitKey[0] == "" {
		if len(splitKey) != 5 {
			return
		}
		// "", "industry", "domain", "schema", "field name" happens for Refs.
		return splitKey[1], splitKey[2], splitKey[3], splitKey[4]
	}
	if len(splitKey) != 4 {
		return
	}
	// industry, domain, schema, field name
	return splitKey[0], splitKey[1], splitKey[2], splitKey[3]
}

// SchemaConvert translates a Data document which satisfies oldSchema into a Data document which satisfies NewSchema and
// returns the converted Data document *without a FirestoreID*. The original Data document is untouched.
//
// There are 3 possibilities for Fields in the Data document. (1) The field is present in the new schema. In this case
// the field is copied into the new schema.
// (2) The field is not present in the new schema at the correct DataType. In this case the "convertKey" map is checked.
// If the field is present as a key & the value of the key is present at convertKey[field] & the the type of the old and new keys match then the
// field is added to the new Data.
// (3) The field is invalid or can't be converted. In this case the field is not included in the new Data.
func SchemaConvert(newSchema Schema, data Data, convertKey map[string]string) (Data, error) {
	newSchemaID := newSchema.SchemaID
	if newSchemaID == "" {
		newSchemaID = newSchema.FirestoreID
	}
	if newSchema.SchemaID == data.SchemaID {
		// already in the correct Schema. We haven't tested this case.
		return data, nil
	}
	var newData Data
	err := newData.Init(nil)
	if err != nil {
		return Data{}, fmt.Errorf("newData.Init with nil: %w", err)
	}
	for k, v := range data.S {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "S" {
				newData.S[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "S" {
					// the converted key is present in newSchema & the right type
					newData.S[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.F {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "F" {
				newData.F[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "F" {
					// the converted key is present in newSchema & the right type
					newData.F[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.I {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "I" {
				newData.I[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "I" {
					// the converted key is present in newSchema & the right type
					newData.I[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.B {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "B" {
				newData.B[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "B" {
					// the converted key is present in newSchema & the right type
					newData.B[convertKey[k]] = v
				}
			}
		}
	}
	// Ref and ARef can have a completely different Industry, Domain, and Schema. That makes it
	// harder to force a strict conversion from A:B since the only thing which must be the same in the old schema
	// and the new schema is the field name.
	// We "solve" this problem by keeping the old schema's Industry, Domain, and Schema ID. In other words, we don't
	// convert these into the new Schema at all.
	for k, v := range data.Ref {
		keySplit := strings.Split(k, "_")
		keySuffix := "_" + keySplit[len(keySplit)-1]
		for schemaK, schemaV := range newSchema.DataTypes {
			if schemaV == "ARef" && strings.HasSuffix(schemaK, keySuffix) {
				newData.Ref[schemaK] = v
				break
			}
		}
	}
	for k, v := range data.ARef {
		keySplit := strings.Split(k, "_")
		keySuffix := "_" + keySplit[len(keySplit)-1]
		for schemaK, schemaV := range newSchema.DataTypes {
			if schemaV == "ARef" && strings.HasSuffix(schemaK, keySuffix) {
				newData.ARef[schemaK] = v
				break
			}
		}
	}
	for k, v := range data.AS {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "AS" {
				newData.AS[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "AS" {
					// the converted key is present in newSchema & the right type
					newData.AS[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.AI {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "AI" {
				newData.AI[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "AI" {
					// the converted key is present in newSchema & the right type
					newData.AI[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.AB {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "AB" {
				newData.AB[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "AB" {
					// the converted key is present in newSchema & the right type
					newData.AB[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.AF {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "AF" {
				newData.AF[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "AF" {
					// the converted key is present in newSchema & the right type
					newData.AF[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.Bytes {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "Bytes" {
				newData.Bytes[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "Bytes" {
					// the converted key is present in newSchema & the right type
					newData.Bytes[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.ABytes {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "ABytes" {
				newData.ABytes[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "ABytes" {
					// the converted key is present in newSchema & the right type
					newData.ABytes[convertKey[k]] = v
				}
			}
		}
	}
	for k, v := range data.Nils {
		newKey := ReplaceIndustryDomainSchemaInKey(k, newSchema.IndustryID, newSchema.DomainID, newSchemaID)
		if newKey != "" {
			if newSchema.DataTypes[newKey] == "Nils" {
				newData.Nils[newKey] = v
			} else if newSchema.DataTypes[newKey] == "" {
				// not present in newSchema at the current key
				if convertKey[k] != "" && newSchema.DataTypes[convertKey[k]] == "Nils" {
					// the converted key is present in newSchema & the right type
					newData.Nils[convertKey[k]] = v
				}
			}
		}
	}
	newData.IndustryID = newSchema.IndustryID
	newData.DomainID = newSchema.DomainID
	newData.SchemaID = newSchemaID
	return newData, nil
}

// StructureIntoData is the reverse of DataIntoStructure: it takes data in a structure and places it into *common.Data.
// Fields in Data need to be prefixed with industry_domain_schema_ so those arguments to this function are required.
//
// structure must be a pointer to a structure where we can get the data to retrieve.
//
// data must have already called Init().
func StructureIntoData(industry string, domain string, schema string, ptrToStructure interface{}, data *Data) error {
	if data == nil {
		return errors.New("data was nil")
	}
	if industry == "" || domain == "" || schema == "" {
		return fmt.Errorf("Industry (%v), Domain (%v) or Schema (%v) was empty", industry, domain, schema)
	}
	if reflect.TypeOf(ptrToStructure).Kind() != reflect.Ptr {
		return errors.New("ptrToStructure was not a pointer")
	}
	data.IndustryID = industry
	data.DomainID = domain
	data.SchemaID = schema
	structure := reflect.ValueOf(ptrToStructure).Elem()
	for i := 0; i < structure.NumField(); i++ {
		field := structure.Type().Field(i)
		suffix := field.Tag.Get("suffix")
		if suffix == "" {
			suffix = field.Name
		}
		// dataType solves the problem where there are 2 potential destinations for a field in Data.
		// For example: ARef and AS are both []string. Without knowing the type as ARef or AS we have to guess if it is
		// an ARef or AS (and we will always guess AS). If you supply the dataType we know which one to use.
		dataType := field.Tag.Get("dataType")
		key := AddFieldPrefix(industry, domain, schema, suffix)
		switch field.Type.Kind() {
		case reflect.String:
			if dataType == "Ref" {
				// Ref can have a Schema which != the schema of their parent.
				ref := structure.Field(i).String()
				getReq, err := ParseRef(ref)
				if err != nil {
					return fmt.Errorf("common.ParseRef for (%v): %w", ref, err)
				}
				key = AddFieldPrefix(getReq.IndustryID, getReq.DomainID, getReq.SchemaID, suffix)
				data.Ref[key] = String(structure.Field(i).String())
			} else {
				data.S[key] = String(structure.Field(i).String())
			}
		case reflect.Bool:
			data.B[key] = Bool(structure.Field(i).Bool())
		case reflect.Int64:
			data.I[key] = Int64(structure.Field(i).Int())
		case reflect.Float64:
			data.F[key] = Float64(structure.Field(i).Float())
		case reflect.Array, reflect.Slice:
			switch field.Type.Elem().Kind() {
			case reflect.String:
				if dataType == "ARef" {
					// Ref can have a Schema which != the schema of their parent.
					refs := structure.Field(i).Interface().([]string)
					if len(refs) < 1 {
						wasLucky := false
						// If we are lucky this common.Data will contain our field already and we can steal the industry, domain, and schema IDs from there
						for k, _ := range data.ARef {
							if strings.HasSuffix(k, "_"+suffix) {
								// we are lucky
								key = k
								wasLucky = true
								break
							}
						}
						if !wasLucky {
							return fmt.Errorf("len(refs) < 1 so I don't know what key to set this field at since the schema is variable. Key: %v ; Keep in mind that when a Data is created it is supposed to contain all the fields set to empty values. So this error only happens if the Schema was modified after this Data was created.", key)
						}
					} else {
						getReq, err := ParseRef(refs[0])
						if err != nil {
							return fmt.Errorf("common.ParseRef for (%v): %w", refs[0], err)
						}
						key = AddFieldPrefix(getReq.IndustryID, getReq.DomainID, getReq.SchemaID, suffix)
					}
					data.ARef[key] = structure.Field(i).Interface().([]string)
				} else {
					data.AS[key] = structure.Field(i).Interface().([]string)
				}
			case reflect.Bool:
				data.AB[key] = structure.Field(i).Interface().([]bool)
			case reflect.Int64:
				data.AI[key] = structure.Field(i).Interface().([]int64)
			case reflect.Float64:
				data.AF[key] = structure.Field(i).Interface().([]float64)
			case reflect.Uint8:
				data.Bytes[key] = structure.Field(i).Interface().([]byte)
			case reflect.Array, reflect.Slice:
				data.ABytes[key] = structure.Field(i).Interface().([][]byte)
			}
		}
	}
	return nil
}

// DataIntoStructure tries to find data elements which have attributes matching tags in the provided structure.
//
// Relevant tags should be formatted like this: `suffix:"Name"`
//
// If no tag is provided, it is assumed the field suffix the exact value so there is no accounting for spaces.
//
// String types will turn data.S and data.Ref into string; and data.AS and data.ARef into []string.
// Therefore if you have an embedded structure, like a Subtask, you must use the raw ref to allDB.ReadData and do something
// with it outside this function. This exclusion improves this function's performance and simplifies its purpose.
//
// Special cases: any field with tag PolyappFirestoreID gets data.FirestoreID set into its value. Similar cases:
// PolyappSchemaID, PolyappDomain, PolyappIndustry
func DataIntoStructure(data *Data, ptrToStructure interface{}) error {
	if reflect.TypeOf(ptrToStructure).Kind() != reflect.Ptr {
		return errors.New("ptrToStructure was not a pointer")
	}
	structure := reflect.ValueOf(ptrToStructure).Elem()
	for i := 0; i < structure.NumField(); i++ {
		field := structure.Type().Field(i)
		tag := "_" + field.Tag.Get("suffix")
		if tag == "_" {
			tag = tag + field.Name
		}
		switch field.Type.Kind() {
		case reflect.String:
			wasSet := false
			for k, v := range data.S {
				if strings.HasSuffix(k, tag) {
					structure.Field(i).SetString(*v)
					wasSet = true
					break
				}
			}
			if wasSet {
				continue
			}
			if tag == "_PolyappFirestoreID" {
				structure.Field(i).SetString(data.FirestoreID)
			} else if tag == "_PolyappSchemaID" {
				structure.Field(i).SetString(data.SchemaID)
			} else if tag == "_PolyappDomainID" {
				structure.Field(i).SetString(data.DomainID)
			} else if tag == "_PolyappIndustryID" {
				structure.Field(i).SetString(data.IndustryID)
			}
			for k, v := range data.Ref {
				if strings.HasSuffix(k, tag) {
					structure.Field(i).SetString(*v)
					break
				}
			}
		case reflect.Bool:
			for k, v := range data.B {
				if strings.HasSuffix(k, tag) {
					structure.Field(i).SetBool(*v)
					break
				}
			}
		case reflect.Int64:
			for k, v := range data.I {
				if strings.HasSuffix(k, tag) {
					structure.Field(i).SetInt(*v)
					break
				}
			}
		case reflect.Float64:
			for k, v := range data.F {
				if strings.HasSuffix(k, tag) {
					structure.Field(i).SetFloat(*v)
					break
				}
			}
		case reflect.Array, reflect.Slice:
			switch field.Type.Elem().Kind() {
			case reflect.String:
				wasSet := false
				for k, v := range data.AS {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).Set(reflect.ValueOf(v))
						wasSet = true
						break
					}
				}
				if wasSet {
					continue
				}
				for k, v := range data.ARef {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).Set(reflect.ValueOf(v))
						wasSet = true
						break
					}
				}
			case reflect.Bool:
				for k, v := range data.AB {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).Set(reflect.ValueOf(v))
					}
				}
			case reflect.Int64:
				for k, v := range data.AI {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).Set(reflect.ValueOf(v))
					}
				}
			case reflect.Float64:
				for k, v := range data.AF {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).Set(reflect.ValueOf(v))
					}
				}
			case reflect.Uint8:
				for k, v := range data.Bytes {
					if strings.HasSuffix(k, tag) {
						structure.Field(i).SetBytes(v)
					}
				}
			case reflect.Array, reflect.Slice:
				for k, v := range data.ABytes {
					if strings.HasSuffix(k, tag) {
						aWrapped := reflect.ValueOf(v)
						structure.Field(i).Set(aWrapped)
					}
				}
			case reflect.Struct:
				// Role gets here during the Edit Role bot.
				// But this function does not support embedded structures yet.
			}
		}
	}
	return nil
}
