// Package unique is at the very bottom of the dependency tree. It's used for contants
// and debugging

package common

import "errors"

const (
	// CollectionData holds all customer-owned data
	CollectionData = "data"
	// CollectionSchema holds all data defining CollectionData's schema
	CollectionSchema = "schema"
	// CollectionUser holds all data for particular users
	CollectionUser = "user"
	// CollectionRole holds document with authorization information
	CollectionRole = "role"
	// CollectionUX holds data which helps define a piece of UI and how users interact with it
	CollectionUX = "ux"
	// CollectionTask holds all data which helps bind together Schemed Data for a User in a UX to accomplish some goals
	CollectionTask = "task"
	// CollectionBot is where Bot definitions are stored.
	CollectionBot = "bot"
	// CollectionAction is where Action definitions are stored.
	CollectionAction = "action"
	// CollectionPublicMap contains information which helps route and respond to incoming requests from other domains.
	CollectionPublicMap = "publicmap"
	// CollectionCSS contains information related to CSS on websites hosted by Polyapp.
	CollectionCSS = "css"
)

const (
	// PolyappIndustryID is a field which holds the Industry
	PolyappIndustryID = "polyappIndustryID"
	// PolyappDomainID is a field which holds the Domain
	PolyappDomainID = "polyappDomainID"
	// PolyappSchemaID is a field which holds the this grouping of information under the Domain; aka a Schema.
	PolyappSchemaID = "polyappSchemaID"
	// PolyappDeprecated is a field which holds whether or not this Document is Deprecated
	PolyappDeprecated = "polyappDeprecated"
	// PolyappFirestoreID is a field in non-Firestore databases which holds the FirestoreID.
	PolyappFirestoreID = "polyappFirestoreID"
)

// ValidateCollection validates that the collection string passed in is one of the known collections
func ValidateCollection(c string) (err error) {
	switch c {
	case CollectionData:
	case CollectionSchema:
	case CollectionUser:
	case CollectionTask:
	case CollectionUX:
	case CollectionRole:
	case CollectionBot:
	case CollectionAction:
	case CollectionPublicMap:
	case CollectionCSS:
	default:
		return errors.New("collection string was not in the list of collections")
	}

	return
}

const (
	TemplateUserDataID = "EtCKtxdfmz8UmmiGjgaO"
	TemplateUserUserID = "p3gP9Lrox8pBxqQdeFUH"
	HomeTaskID         = "NrCEAuHUenfkPKXrwafnbwPKx"
	HomeUXID           = "GfzLPQfRAFMPjKgiRCQmMoCDK"
	HomeSchemaID       = "BJVHCgzBvaahJFBMOGTZSniwK"
	HomeDataID         = "lBDjYHsEaKDJAbuWSotncPIPb"
)
