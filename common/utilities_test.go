package common

import (
	"reflect"
	"testing"
)

func TestSchemaConvert(t *testing.T) {
	type args struct {
		newSchema  Schema
		data       Data
		convertKey map[string]string
	}
	tests := []struct {
		name    string
		args    args
		want    Data
		wantErr bool
	}{
		{
			name: "completely incompatible",
			args: args{
				newSchema: Schema{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertI",
					DomainID:    "TestSchemaConvertD",
					SchemaID:    "TestSchemaConvertC",
					DataTypes: map[string]string{
						"ind_dom_sch_one":   "S",
						"ind_dom_sch_two":   "B",
						"ind_dom_sch_three": "AS",
					},
					DataHelpText: map[string]string{
						"ind_dom_sch_one":   "help",
						"ind_dom_sch_two":   "help",
						"ind_dom_sch_three": "help",
					},
					DataKeys: []string{
						"ind_dom_sch_one", "ind_dom_sch_two", "ind_dom_sch_three",
					},
					IsPublic:   false,
					Deprecated: nil,
				},
				data: Data{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertOld",
					DomainID:    "TestSchemaConvertOld",
					SchemaID:    "TestSchemaConvertOld",
					Deprecated:  nil,
					Ref:         map[string]*string{},
					S: map[string]*string{
						"ind_dom_sch_a": String("uno"),
						"ind_dom_sch_b": String("dos"),
					},
					I: map[string]*int64{},
					B: map[string]*bool{},
					F: map[string]*float64{
						"ind_dom_sch_c": Float64(1.0),
					},
					ARef:   map[string][]string{},
					AS:     map[string][]string{},
					AI:     map[string][]int64{},
					AB:     map[string][]bool{},
					AF:     map[string][]float64{},
					ABytes: map[string][][]byte{},
					Bytes:  map[string][]byte{},
					Nils:   map[string]bool{},
				},
				convertKey: map[string]string{},
			},
			want: Data{
				FirestoreID: "",
				IndustryID:  "TestSchemaConvertI",
				DomainID:    "TestSchemaConvertD",
				SchemaID:    "TestSchemaConvertC",
				Deprecated:  nil,
				Ref:         map[string]*string{},
				S:           map[string]*string{},
				I:           map[string]*int64{},
				B:           map[string]*bool{},
				F:           map[string]*float64{},
				ARef:        map[string][]string{},
				AS:          map[string][]string{},
				AI:          map[string][]int64{},
				AB:          map[string][]bool{},
				AF:          map[string][]float64{},
				ABytes:      map[string][][]byte{},
				Bytes:       map[string][]byte{},
				Nils:        map[string]bool{},
			},
			wantErr: false,
		},
		{
			name: "newSchema is subset of old",
			args: args{
				newSchema: Schema{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertI",
					DomainID:    "TestSchemaConvertD",
					SchemaID:    "TestSchemaConvertC",
					DataTypes: map[string]string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one":   "S",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two":   "B",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three": "AS",
					},
					DataHelpText: map[string]string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one":   "help",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two":   "help",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three": "help",
					},
					DataKeys: []string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one", "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two", "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three",
					},
					IsPublic:   false,
					Deprecated: nil,
				},
				data: Data{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertOld",
					DomainID:    "TestSchemaConvertOld",
					SchemaID:    "TestSchemaConvertOld",
					Deprecated:  nil,
					Ref:         map[string]*string{},
					S: map[string]*string{
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_one": String("uno"),
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_two": String("dos"),
					},
					I: map[string]*int64{},
					B: map[string]*bool{},
					F: map[string]*float64{
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_c": Float64(1.0),
					},
					ARef:   map[string][]string{},
					AS:     map[string][]string{},
					AI:     map[string][]int64{},
					AB:     map[string][]bool{},
					AF:     map[string][]float64{},
					Bytes:  map[string][]byte{},
					ABytes: map[string][][]byte{},
					Nils:   map[string]bool{},
				},
				convertKey: map[string]string{},
			},
			want: Data{
				FirestoreID: "",
				IndustryID:  "TestSchemaConvertI",
				DomainID:    "TestSchemaConvertD",
				SchemaID:    "TestSchemaConvertC",
				Deprecated:  nil,
				Ref:         map[string]*string{},
				S: map[string]*string{
					"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one": String("uno"),
				},
				I:      map[string]*int64{},
				B:      map[string]*bool{},
				F:      map[string]*float64{},
				ARef:   map[string][]string{},
				AS:     map[string][]string{},
				AI:     map[string][]int64{},
				AB:     map[string][]bool{},
				AF:     map[string][]float64{},
				Bytes:  map[string][]byte{},
				ABytes: map[string][][]byte{},
				Nils:   map[string]bool{},
			},
			wantErr: false,
		},
		{
			name: "newSchema is subset of old with some convertKey",
			args: args{
				newSchema: Schema{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertI",
					DomainID:    "TestSchemaConvertD",
					SchemaID:    "TestSchemaConvertC",
					DataTypes: map[string]string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one":   "S",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two":   "B",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three": "AS",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_four":  "F",
					},
					DataHelpText: map[string]string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one":   "help",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two":   "help",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three": "help",
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_four":  "help",
					},
					DataKeys: []string{
						"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one", "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_two", "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_three", "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_four",
					},
					IsPublic:   false,
					Deprecated: nil,
				},
				data: Data{
					FirestoreID: "TestSchemaConvert",
					IndustryID:  "TestSchemaConvertOld",
					DomainID:    "TestSchemaConvertOld",
					SchemaID:    "TestSchemaConvertOld",
					Deprecated:  nil,
					Ref:         map[string]*string{},
					S: map[string]*string{
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_one": String("uno"),
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_two": String("dos"),
					},
					I: map[string]*int64{},
					B: map[string]*bool{},
					F: map[string]*float64{
						"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_c": Float64(1.0),
					},
					ARef:   map[string][]string{},
					AS:     map[string][]string{},
					AI:     map[string][]int64{},
					AB:     map[string][]bool{},
					AF:     map[string][]float64{},
					Bytes:  map[string][]byte{},
					ABytes: map[string][][]byte{},
					Nils:   map[string]bool{},
				},
				convertKey: map[string]string{
					"TestSchemaConvertOld_TestSchemaConvertOld_TestSchemaConvertOld_c": "TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_four",
				},
			},
			want: Data{
				FirestoreID: "",
				IndustryID:  "TestSchemaConvertI",
				DomainID:    "TestSchemaConvertD",
				SchemaID:    "TestSchemaConvertC",
				Deprecated:  nil,
				Ref:         map[string]*string{},
				S: map[string]*string{
					"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_one": String("uno"),
				},
				I: map[string]*int64{},
				B: map[string]*bool{},
				F: map[string]*float64{
					"TestSchemaConvertI_TestSchemaConvertD_TestSchemaConvertC_four": Float64(1.0),
				},
				ARef:   map[string][]string{},
				AS:     map[string][]string{},
				AI:     map[string][]int64{},
				AB:     map[string][]bool{},
				AF:     map[string][]float64{},
				Bytes:  map[string][]byte{},
				ABytes: map[string][][]byte{},
				Nils:   map[string]bool{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := SchemaConvert(tt.args.newSchema, tt.args.data, tt.args.convertKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("SchemaConvert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SchemaConvert() got\n%v\n\nwant\n%v", got, tt.want)
			}
		})
	}
}
