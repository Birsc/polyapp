// Package common holds structures and data which are common across all databases and architectures (JS / Server).
// This exists because it is often helpful to translate information from several sources into one intermediate
// form and then translate it back out again.
//
// Note: pointerConvert is needed so we know if a field is the zero value (like "") or missing (like nil).
// This is the same strategy used by some google data transport libraries written in Go, and also used by Stripe to know
// if a field is supposed to be the zero value or nil.
package common
