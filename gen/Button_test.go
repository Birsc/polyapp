package gen

import (
	"bytes"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestGenTextButton(t *testing.T) {
	var w bytes.Buffer
	err := GenTextButton(TextButton{}, &w)
	if err == nil {
		t.Error("Expected an error if TextButton is empty because you shouldn't call a function expecting it to do nothing")
	}

	button := TextButton{
		ID:      "asdfasd",
		Text:    "b",
		Styling: "red",
	}
	w.Reset()
	err = GenTextButton(button, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	button.Styling = "dark"
	w.Reset()
	err = GenTextButton(button, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	if !strings.Contains(w.String(), `type="button"`) {
		t.Error(`should have added an input with type="text"`)
	}
	if !strings.Contains(w.String(), `dark`) {
		t.Error(`should have used 'dark' as part of a CSS class`)
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}

func TestGenIconButton(t *testing.T) {
	var w bytes.Buffer
	err := GenIconButton(IconButton{}, &w)
	if err == nil {
		t.Error("Expected an error if IconButton is empty because you shouldn't call a function expecting it to do nothing")
	}

	button := IconButton{
		ID:        "asdfasd",
		ImagePath: "assets",
	}
	w.Reset()
	err = GenIconButton(button, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	button = IconButton{
		ID:        "asdfasd",
		ImagePath: "a",
		AltText:   "some alt text",
	}
	w.Reset()
	err = GenIconButton(button, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	button.ImagePath = "/assets/icons/edit-24px.svg"
	w.Reset()
	err = GenIconButton(button, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	if !strings.Contains(w.String(), `type="button"`) {
		t.Error(`should have added an input with type="text"`)
	}
	if !strings.Contains(w.String(), button.Text) {
		t.Error(`should have used 'dark' as part of a CSS class`)
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}
