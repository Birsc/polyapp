package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"math/rand"
	"net/url"
	"strconv"
	textTemplate "text/template"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// Breadcrumbs is used to show the current position in the application, like "Photos / Summer 2017 / Italy / Rome"
// Breadcrumbs OUGHT to be convertible to URLs, like polyapp.tech/task/polyapp/bot/create
// Breadcrumbs assumes that the last element in the breadcrumb is active.
type Breadcrumbs struct {
	// ID of the Breadcrumbs class
	ID string
	// NavigationReferences are items in the breadcrumb tree
	NavigationReferences []NavigationRef
}

// Validate ensures the correct fields are populated without validating the output.
func (b Breadcrumbs) Validate() error {
	if b.ID == "" {
		return errors.New("ID must not be empty")
	}
	if b.NavigationReferences == nil || len(b.NavigationReferences) < 1 {
		return errors.New("NavigationReferences must be included in Breadcrumbs")
	}
	for i := range b.NavigationReferences {
		err := b.NavigationReferences[i].Validate()
		if err != nil {
			return fmt.Errorf("error in NavigationReferences in Breadcrumbs at index "+string(i)+" : %w", err)
		}
	}
	return nil
}

// NavigationRef creates an 'a' tag.
type NavigationRef struct {
	// LinkPath to wherever you're trying to navigate
	LinkPath string
	// Text is the text of the link
	Text string
	// ID is optional.
	ID string
}

// Validate ensures the correct fields are populated without validating the output.
func (navigationRef NavigationRef) Validate() error {
	if navigationRef.LinkPath == "" {
		return errors.New("LinkPath must not be empty")
	}
	if navigationRef.Text == "" {
		return errors.New("Text must not be empty")
	}
	return nil
}

// GenBreadcrumbs generates a URL-like list of links.
func GenBreadcrumbs(breadcrumbs Breadcrumbs, writer io.Writer) error {
	err := breadcrumbs.Validate()
	if err != nil {
		return fmt.Errorf("breadcrumbs failed validation: %w", err)
	}
	breadcrumbs.ID = url.PathEscape(breadcrumbs.ID)
	breadcrumbsTemplate := `{{/* . is Breadcrumbs  */}}
{{define "breadcrumbsTemplate"}}
<ul class="breadcrumb" id="{{.ID}}">
	{{range .NavigationReferences}}<li class="breadcrumb-item"><a href="{{.LinkPath}}">{{.Text}}</a></li>{{end}}
</ul>
{{- end}}
`
	t, err := template.New("breadcrumbsTemplate").Parse(breadcrumbsTemplate)
	if err != nil {
		return fmt.Errorf("error parsing: %w", err)
	}
	err = t.Execute(writer, breadcrumbs)
	if err != nil {
		return fmt.Errorf("error executing: %w", err)
	}
	return nil
}

// GiantLinks are giant links buttons. This should be the only thing on the page.
// WrapperID is auto-generated.
type GiantLinks struct {
	// Label for the links
	Label string
	// Links are the locations you wish to navigate to. I strongly recommend only using partial paths, ie /design
	Links []NavigationRef
}

func (g GiantLinks) Validate() error {
	if g.Label == "" {
		return errors.New("must include Label")
	}
	if g.Links == nil || len(g.Links) == 0 {
		return errors.New("must include Links")
	}
	for i, ref := range g.Links {
		err := ref.Validate()
		if err != nil {
			return fmt.Errorf("link index: "+strconv.Itoa(i)+" had err: %w", err)
		}
	}
	return nil
}

// GenGiantLinks generates giant buttons which are links to different pages.
func GenGiantLinks(input GiantLinks, writer io.Writer) error {
	err := input.Validate()
	if err != nil {
		return fmt.Errorf("GenGiantLinks failed validation: %w", err)
	}
	wrapperID := "a" + strconv.FormatUint(rand.Uint64(), 10)
	giantLabelID := "a" + strconv.FormatUint(rand.Uint64(), 10)
	selectTemplate := `{{/* . is SelectInput  */}}
{{define "giantLinks"}}
<div id="` + wrapperID + `" class="giantOptionWrapper">
	<label class="giantOptionLabel" id="` + giantLabelID + `">{{.Label}}</label>
	{{range .Links}}
	<div class="giantOption">
		<a href="{{.LinkPath}}">
			<label tabindex="0" class="maxWidthHeight">
				<span class="centerElement">{{.Text}}</span>
			</label>
		</a>
	</div>
	{{end}}
</div>
{{- end}}
`
	t, err := template.New("giantLinks").Parse(selectTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, input)
	if err != nil {
		return err
	}
	return nil
}

// GenLink generates a simple link.
func GenLink(navRef NavigationRef, writer io.Writer) error {
	err := navRef.Validate()
	if err != nil {
		return fmt.Errorf("Link failed validation: %w", err)
	}
	navRef.ID = url.PathEscape(navRef.ID)
	linkTemplate := `{{define "link"}}
<a href="{{.LinkPath}}" id="{{.ID}}">{{.Text}}</a>
{{- end}}
`
	t, err := textTemplate.New("link").Parse(linkTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, navRef)
	if err != nil {
		return err
	}
	return nil
}

// NavigationBar represents the bar at the top of the screen which holds navigation links.
type NavigationBar struct {
	// LogoSrc is an image which can be placed on the far left hand side of the navigation bar.
	LogoSrc string
	// Theme is one of: "light" or "dark"
	Theme string
	// NavigationRefs are the links which are part of the navigation bar.
	NavigationRefs []NavigationRef
}

// Validate returns an error if the NavigationBar will not render correctly.
func (n NavigationBar) Validate() error {
	if n.NavigationRefs == nil {
		return errors.New("NavigationRefs was nil")
	}
	if n.Theme != "light" && n.Theme != "dark" {
		return errors.New("Theme must be set to 'light' or 'dark'.")
	}
	return nil
}

// GenNavigationBar generates a Navigation Bar which should be placed at the top of the body of an HTML document.
func GenNavigationBar(navBar NavigationBar, writer io.Writer) error {
	err := navBar.Validate()
	if err != nil {
		return fmt.Errorf("navBar.Validate: %w", err)
	}
	collapsableNavbarID := "polyappJSNavbar" + common.GetRandString(20)
	navTemplate := `{{define "navbar"}}
<nav class="navbar navbar-expand-lg {{if eq .Theme "light"}}navbar-light bg-light{{else}}navbar-dark bg-dark{{end}}">
  	{{if .LogoSrc -}}
	<a class="navbar-brand" href="/">
		<img src="{{.LogoSrc}}" width="30" height="30" alt="Site Logo" />
	</a>
	{{- end}}
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#` + collapsableNavbarID + `" aria-controls="` + collapsableNavbarID + `" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="` + collapsableNavbarID + `">
		<ul class="navbar-nav">
			{{range .NavigationRefs}}<li class="nav-item"><a class="nav-link" href="{{.LinkPath}}">{{.Text}}</a></li>{{end}}
		</ul>
	</div>
</nav> 
{{- end}}`
	t, err := textTemplate.New("navbar").Parse(navTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, navBar)
	if err != nil {
		return err
	}
	return nil
}

// WorkflowNavigation is a vertical navigation bar.
type WorkflowNavigation struct {
	Title          string          // Optional. Shown at the top of the Navbar.
	NavigationRefs []NavigationRef // Links within the navbar.
	Theme          string          // is either "light" or "dark"
}

func (s WorkflowNavigation) Validate() error {
	if s.NavigationRefs == nil || len(s.NavigationRefs) == 0 {
		return errors.New("NavigationRefs nil or list is empty")
	}
	return nil
}

// GenWorkflowNavigation creates a vertical navigation bar.
func GenWorkflowNavigation(workflowNavigation WorkflowNavigation, w io.Writer) error {
	err := workflowNavigation.Validate()
	if err != nil {
		return fmt.Errorf("navBar.Validate: %w", err)
	}
	sidenavTemplate := `{{define "sidenav"}}
<nav class="navbar navbar-expand {{if eq .Theme "light"}}navbar-light bg-light{{else}}navbar-dark bg-dark{{end}} m-1">
	{{if .Title}}<a class="{{if eq .Theme "light"}}text-dark{{else}}text-light{{end}} navbar-brand">{{.Title}}</a>{{end}}
	<ul class="nav nav-pills">
		{{range .NavigationRefs}}<li class="nav-item"><a class="nav-link" href="{{.LinkPath}}">{{.Text}}</a></li>{{end}}
	</ul>
</nav>
{{- end}}`
	t, err := textTemplate.New("sidenav").Parse(sidenavTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, workflowNavigation)
	if err != nil {
		return err
	}
	return nil
}
