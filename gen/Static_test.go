package gen

import (
	"bytes"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestGenAlert(t *testing.T) {
	alert := Alert{}
	var w bytes.Buffer
	err := GenAlert(alert, &w)
	if err == nil {
		t.Error("Expected an error if alert is empty because you shouldn't call a function expecting it to do nothing")
	}
	w.Reset()
	alert = Alert{
		Text:     "test text",
		Severity: "blegh",
	}
	err = GenAlert(alert, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	w.Reset()
	alert.Severity = "warning"
	err = GenAlert(alert, &w)
	if err != nil {
		t.Error("An unexpected error occurred in GenAlert: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have generated some output")
	}
	if !strings.Contains(w.String(), "alert alert-warning") {
		t.Error("Should be generating a type warning with this severity level")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}

func TestGenStaticContent(t *testing.T) {
	staticContent := StaticContent{}
	var w bytes.Buffer
	err := GenStaticContent(staticContent, &w)
	if err == nil {
		t.Error("expected an error if alert is empty because you shouldn't call a function expecting it to do nothing")
	}
	w.Reset()
	staticContent = StaticContent{
		ID:      "sdksd",
		Content: "hello world",
		Tag:     "bl",
	}
	err = GenStaticContent(staticContent, &w)
	if err == nil {
		t.Error("just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	w.Reset()
	staticContent.Tag = "h6"
	err = GenStaticContent(staticContent, &w)
	if err != nil {
		t.Error("An unexpected error occurred in GenAlert: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have generated some output")
	}
	if !strings.Contains(w.String(), "</h6>") || !strings.Contains(w.String(), staticContent.Content) {
		t.Error("Should be generating a header tag with the given content. Output seen: " + w.String())
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}
