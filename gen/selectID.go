package gen

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html/template"
	"io"
	"net/url"
)

type SelectID struct {
	Label             string
	HelpText          string
	ID                string
	InitialHumanValue string
	InitialIDValue    string
	Collection        string
	Field             string // Field includes prefixes. Examples: "polyappIndustryID" or "ind_dom_sch_field"
	Readonly          bool
}

func (s SelectID) Validate() error {
	if s.Label == "" {
		return errors.New("Label was empty")
	}
	if s.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if s.ID == "" {
		return errors.New("s.ID was empty")
	}
	switch s.Collection {
	case common.CollectionAction, common.CollectionData, common.CollectionBot, common.CollectionTask, common.CollectionSchema,
		common.CollectionUX, common.CollectionUser, common.CollectionRole:
	default:
		return fmt.Errorf("s.Collection (%v) was not one of the acceptable collection strings", s.Collection)
	}
	if s.Field == "" {
		return errors.New("s.Field was empty")
	}
	if s.InitialHumanValue != "" && s.InitialIDValue == "" {
		return errors.New("if setting InitialHumanValue must also set InitialIDValue")
	}
	if s.InitialIDValue != "" && s.InitialHumanValue == "" {
		return errors.New("if setting InitialIDValue must also set InitialHumanValue")
	}
	return nil
}

// GenSelectID an ID Selection control which allows you to choose a Data, Task, Schema, or other collection's ID
// as those this were a <select>.
//
// The person typing searches a particular field in a particular collection. The <select> is populated on the server
// with up to 100 values when a GET request is made. If the number of options exceeds 100,
func GenSelectID(s SelectID, w io.Writer) error {
	err := s.Validate()
	if err != nil {
		return fmt.Errorf("StaticContent failed validation: %w", err)
	}
	s.ID = url.PathEscape(s.ID)
	staticContentTemplate := `
<div class="mb-4">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
{{if .Readonly}}<input type="text" class="form-control" id="{{.ID}}" value="{{.InitialIDValue}}" readonly />
{{else}}<input type="text" class="form-control" id="{{.ID}}" data-polyappJS-select-value="{{.InitialIDValue}}" value="{{.InitialHumanValue}}"
			data-polyappJS-select-collection="{{.Collection}}" data-polyappJS-select-field="{{.Field}}" aria-autocomplete="list"
			data-polyappJS-select-field-initial-human-value="{{.InitialHumanValue}}" />{{end}}
		<div class="input-group-append">
			<span class="input-group-text">
				<img src="/assets/icons/search-24px.svg" aria-hidden="true" height="24px" width="24px" />
			</span>
		</div>
	</div>
	<div id="polyappSearchSelectResults{{.ID}}" class="position-relative p-2">
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
`
	t, err := template.New("s").Parse(staticContentTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, s)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

type SelectIDResult struct {
	IDValue    string
	HumanValue string
	Context    string
}

// GenSelectIDResultHTML generates some HTML which can be appended to the end of a GenSelectID. When one of the results
// is clicked, it updates the GenSelectID appropriately.
func GenSelectIDResultHTML(results []SelectIDResult, w io.Writer) error {
	t := `<div class="modal-content position-absolute" style="z-index:100;max-width:500px">
<div class="modal-header">
	<h5>Search Results</h5>
	<button type="button" class="close" aria-label="Close" onclick="this.parentNode.parentNode.remove()">
	  <span aria-hidden="true">×</span>
	</button>
</div>
<div class="modal-body">
	<ul class="list-group">{{range .}}
	  <li class="list-group-item pointer" data-polyappJS-select-option-value="{{.IDValue}}">{{.HumanValue}}{{if .Context}}<br><span class="small">↳ in {{.Context}}</span>{{end}}</li>{{end}}
	</ul>
</div>
</div>`
	parsed, err := template.New("t").Parse(t)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = parsed.Execute(w, results)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

// GenSelectIDNoResultsHTML generates some HTML which can be appended to the end of a GenSelectID.
func GenSelectIDNoResultsHTML() string {
	return `<div class="modal-content position-absolute" style="z-index:100;max-width:500px">
<div class="modal-header">
	<h5>Search Results</h5>
	<button type="button" class="close" aria-label="Close" onclick="this.parentNode.parentNode.remove()">
	  <span aria-hidden="true">×</span>
	</button>
</div>
<div class="modal-body">
	<p>No results found. Searches are case-sensitive and must be exact matches.</p>
</div>
</div>`
}
