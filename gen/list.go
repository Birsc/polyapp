package gen

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html/template"
	"io"
	"net/url"
)

// List allows generating a static, read-only list control attached to an array of booleans. When an element in this
// control is clicked the boolean index in Data is set.
type List struct {
	ID        string
	ListItems []ListItem
}

// ListItem controls the information contained in the list. Its elements mirror those in Bootstrap's List Group
// Custom Content example: https://getbootstrap.com/docs/4.6/components/list-group/#custom-content
type ListItem struct {
	Heading       string // Heading (optional) sets a h5 element at the top of the list item.
	SmallTopRight string // SmallTopRight (optional) sets a <small> element to the right of the Heading.
	Content       string // Content (optional) sets a p element in the middle of the list item.
	SmallBottom   string // SmallBottom (optional) sets a <small> and muted text on the bottom of the list item.
	ID            string // ID (do not set). This is set based on the parent List's ID.
}

// Validate returns an error if the List object is not valid.
func (l List) Validate() error {
	if l.ListItems == nil {
		return errors.New("ListItems was nil")
	}
	if l.ID == "" {
		return errors.New("ID was empty")
	}
	return nil
}

// GenList allows generating a static, read-only list control attached to an array of booleans. When an element in this
// control is clicked the boolean index in Data is set.
func GenList(l List, w io.Writer) error {
	err := l.Validate()
	if err != nil {
		return fmt.Errorf("l.Validate: %w", err)
	}
	for i := range l.ListItems {
		l.ListItems[i].ID = url.PathEscape(l.ID + "_" + common.GetRandString(15))
	}
	l.ID = url.PathEscape(l.ID)
	genListTemplate := `{{define "GenList"}}
<div class="list-group" id="{{.ID}}">
{{range .ListItems}}
  <a class="list-group-item list-group-item-action" id="{{.ID}}">
    <div class="d-flex w-100 justify-content-between" data-polyappJSTriggerEventOnParent="1">
      <h5 class="mb-1" data-polyappJSTriggerEventOnParent="2">{{.Heading}}</h5>
      <small class="text-muted" data-polyappJSTriggerEventOnParent="2">{{.SmallTopRight}}</small>
    </div>
    <p class="mb-1" data-polyappJSTriggerEventOnParent="1">{{.Content}}</p>
    <small class="text-muted" data-polyappJSTriggerEventOnParent="1">{{.SmallBottom}}</small>
  </a>
{{end}}
</div>
{{end}}`
	t, err := template.New("GenList").Parse(genListTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, l)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

// TaskListItem is a card which can be used as a list item. It displays information about a Task - its Name, Help Text, etc -
// and links to important pages for the Task like "Change Data", "New Data", and "Edit Task".
type TaskListItem struct {
	// ID is optional.
	ID             string
	Name           string
	HelpText       string
	ChangeDataLink string
	NewDataLink    string
	EditTaskLink   string
}

// Validate returns an error if the object is not valid.
func (l TaskListItem) Validate() error {
	return nil
}

// GenTaskList is a card which can be
func GenTaskList(taskListItem []TaskListItem, w io.Writer) error {
	var wrapTaskList struct {
		T []TaskListItem
	}
	wrapTaskList.T = taskListItem
	for _, l := range wrapTaskList.T {
		err := l.Validate()
		if err != nil {
			return fmt.Errorf("l.Validate: %w", err)
		}
		if l.ID != "" {
			l.ID = url.PathEscape(l.ID)
		}
	}
	t := `
<div class="m-1 p-1 container-fluid">
	<div class="row">{{range .T}}
		<div class="col-xl-4 col-lg-6 col-xs-12 d-flex align-items-stretch">
			<div class="card my-2">
				<div class="card-body d-flex flex-column justify-content-between">
					<h5>{{.Name}}</h5>
					<p class="card-text">{{.HelpText}}</p>
					<div>
						<a href="{{.ChangeDataLink}}" class="btn btn-primary mt-1">Change Data</a>
						<a href="{{.NewDataLink}}" class="btn btn-primary mt-1">New Data</a>
						<a href="{{.EditTaskLink}}" class="btn btn-primary mt-1">Edit Task</a>
					</div>
				</div>
			</div>
		</div>{{end}}
	</div>
</div>`
	temp, err := template.New("").Parse(t)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = temp.Execute(w, wrapTaskList)
	if err != nil {
		return fmt.Errorf("temp.Execute: %w", err)
	}
	return nil
}
