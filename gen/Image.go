package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

type Image struct {
	// ID of the element which contains the image src.
	ID string
	// URL is the image's src property.
	URL string
	// AltText is the text which describes what is in the image. It is used primarily by the blind.
	AltText string
	// Packed is true if you wish this image to not have any padding. False if you want about 15px of padding on the sides.
	// This setting is ignored for images which take up the full width of a container such as GenImage() calls.
	Packed bool
	// Layout is one of: "Title and Caption Below","Title on Top; Caption Below", "Show Title in Middle on Hover", "Show Title and Caption in Middle on Hover",
	// "Show Title and Caption on Bottom on Hover", "Show Title on Bottom on Hover".
	Layout string
	// Width (optional; used in responsive) is the width of the image
	Width string
	// Height (optional; used in responsive) is the height of the image
	Height string
	// Title (optional) is the title of the image.
	Title string
	// Caption (optional) is the text underneath the image in "captioned" Type.
	Caption string
	// Link (optional) is a URL to send the user to if the image is clicked on. By default there is no link applied to the image.
	Link string
}

func (i *Image) Validate() error {
	if i.ID == "" {
		return errors.New("ID empty")
	}
	if i.URL == "" {
		return errors.New("URL empty")
	}
	if i.AltText == "" {
		return errors.New("AltText empty")
	}
	if i.Layout == "" {
		return errors.New("Layout empty")
	}
	switch i.Layout {
	case "Title and Caption Below":
	case "Title on Top; Caption Below":
	case "Show Title in Middle on Hover":
	case "Show Title and Caption in Middle on Hover":
	case "Show Title and Caption on Bottom on Hover":
	case "Show Title on Bottom on Hover":
	case "No Title or Caption":
	default:
		return errors.New("Unrecognized Layout: " + i.Layout)
	}
	return nil
}

// GenImage displays images as part of a gallery with captions or simply in a responsive container.
func GenImage(image Image, w io.Writer) error {
	err := image.Validate()
	if err != nil {
		return fmt.Errorf("GenImage failed validation: %w", err)
	}
	ImageTemplate := ""
	switch image.Layout {
	case "Title and Caption Below":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="figure-img img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}" 
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
{{if .Link}}	</a>{{end}}
	{{/* px-1 px-md-0 because .row has -15 padding when in .container and yet we need padding for captions to avoid hitting the sides of the screen. Larger breakpoints introduce margins which fix the problem. */}}
	<figcaption class="figure-caption px-1 px-md-0{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		{{if .Title}}<h3>{{.Title}}</h3>{{end}}
		{{if .Caption}}<p class="m-0">{{.Caption}}</p>{{end}}
	</figcaption>
</figure>
{{- end}}
`
	case "Title on Top; Caption Below":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden">
	{{if .Title}}<h3 class="">{{.Title}}</h3>{{end}}
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="figure-img img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}"
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
{{if .Link}}	</a>{{end}}
	{{/* px-1 px-md-0 because .row has -15 padding when in .container and yet we need padding for captions to avoid hitting the sides of the screen. Larger breakpoints introduce margins which fix the problem. */}}
	<figcaption class="figure-caption px-1 px-md-0{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		{{if .Caption}}<p class="m-0">{{.Caption}}</p>{{end}}
	</figcaption>
</figure>
{{- end}}
`
	case "Show Title in Middle on Hover":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden position-relative">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}" 
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
	{{if .Title}}<figcaption class="parentOverlay text-center text-light{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		<h3 class="centerElement">{{.Title}}</h3>
	</figcaption>{{end}}
{{if .Link}}	</a>{{end}}
</figure>
{{- end}}
`
	case "Show Title and Caption in Middle on Hover":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden position-relative">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}" 
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
	<figcaption class="parentOverlay text-center text-light{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		<div class="centerElement">
			{{if .Title}}<h3>{{.Title}}</h3>{{end}}
			{{if .Caption}}<p class="m-0">{{.Caption}}</p>{{end}}
		</div>
	</figcaption>
{{if .Link}}	</a>{{end}}
</figure>
{{- end}}
`
	case "Show Title and Caption on Bottom on Hover":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden position-relative">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}"
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
	{{if .Title}}<figcaption class="parentOverlayPartial text-light{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		<div class="parentOverlayPartialMiddleman px-1">
			<h3>{{.Title}}</h3>
			{{if .Caption}}<p class="m-0">{{.Caption}}</p>{{end}}
		</div>
	</figcaption>{{end}}
{{if .Link}}	</a>{{end}}
</figure>
{{- end}}
`
	case "Show Title on Bottom on Hover":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden position-relative">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}"
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
	<figcaption class="parentOverlayPartial text-light px-1{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}">
		<div class="parentOverlayPartialMiddleman">
			{{if .Title}}<h3>{{.Title}}</h3>{{end}}
		</div>
	</figcaption>
{{if .Link}}	</a>{{end}}
</figure>
{{- end}}
`
	case "No Title or Caption":
		ImageTemplate = `{{define "ImageTemplate"}}
{{/* mw-100 and overflow-hidden to deal with images which are bigger than needed & captions which overflow */}}
<figure class="figure mw-100 overflow-hidden">
{{if .Link}}	<a {{if ne .Link "polyappEnlarge"}}href="{{.Link}}"{{else}}class="pointer"{{end}}>{{end}}
	<img src="{{.URL}}" id="{{.ID}}" alt="{{.AltText}}" class="figure-img img-fluid{{if eq .Link "polyappEnlarge"}} polyappEnlargeImage{{end}}" 
		{{if .Width}}width="{{.Width}}"{{end}} {{if .Height}}height="{{.Height}}"{{end}}
		data-polyappJS-image-title="{{.Title}}" data-polyappJS-image-caption="{{.Caption}}" />
{{if .Link}}	</a>{{end}}
</figure>
{{- end}}
`
	}
	t, err := template.New("ImageTemplate").Parse(ImageTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, image)
	if err != nil {
		return err
	}
	return nil
}

// GenImageMedium wraps GenImage and ensures that the image does not exceed a certain size.
// In general, the image size in GenImageMedium is set up to take up the full width on small
// screens like phones and then expand to up to 2 images wide on 1980 x 1080 screens.
func GenImageMedium(image Image, w io.Writer) error {
	var err error
	if image.Packed {
		_, _ = w.Write([]byte(`<div class="col-12 col-lg-6 px-0">`))
	} else {
		_, _ = w.Write([]byte(`<div class="col-12 col-lg-6">`))
	}
	err = GenImage(image, w)
	_, _ = w.Write([]byte(`</div>`))
	return err
}

// GenImageSmall wraps GenImage and ensures that the image does not exceed a certain size.
// In general, the image size in GenImageSmall is set up to take up the full width on small screens like phones,
// has 2 columns on iPads, and 4 images wide on large screens.
func GenImageSmall(image Image, w io.Writer) error {
	var err error
	if image.Packed {
		_, _ = w.Write([]byte(`<div class="col-12 col-md-6 col-lg-3 px-0">`))
	} else {
		_, _ = w.Write([]byte(`<div class="col-12 col-md-6 col-lg-3">`))
	}
	err = GenImage(image, w)
	_, _ = w.Write([]byte(`</div>`))
	return err
}

type ImageUpload struct {
	// ID of the input element.
	ID string
	// Label above the input element. It's not actually a label, it's a <p>
	Label string
	// HelpText for this field.
	HelpText string
	// AllowMultiple (optional) allows multiple images to be uploaded with this control.
	AllowMultiple bool
}

func (i *ImageUpload) Validate() error {
	if i.ID == "" {
		return errors.New("ID empty")
	}
	return nil
}

// GenImageUpload for a control which allows uploading only images.
func GenImageUpload(upload ImageUpload, w io.Writer) error {
	err := upload.Validate()
	if err != nil {
		return fmt.Errorf("GenImageUpload failed validation: %w", err)
	}
	randName := "a" + common.GetRandString(15)
	upload.ID = url.PathEscape(upload.ID)
	// Note to self: the only thing you need to do to make this a video uploader or whatever is to adjust the "accept"
	// property in the input to: audio/* or video/*
	ImageUploadTemplate := `{{define "ImageUpload"}}
{{if .Label}}<div>
	<p class="d-inline">{{.Label}}</p>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
</div>{{end}}
<div class="d-flex">
  <div class="custom-file mb-3 d-block" style="max-width: 600px;">
    <input type="file" accept='image/*' class="custom-file-input pointer" id="{{.ID}}" name="` + randName + `" {{if .AllowMultiple}}multiple{{end}} />
    <label class="custom-file-label" for="` + randName + `">Choose image file</label>
  </div>
  <div>
    <button class="btn btn-sm btn-link mx-4" style="padding: .50rem .75rem;border: 1px solid;">
      <a href="/blob/assets/polyappShouldBeOverwritten/{{.ID}}">View Files</a>
    </button>
  </div>
</div>
{{if not .AllowMultiple}}
<div class="container">
	<img id="polyappJSImagePreview{{.ID}}" class="rounded mx-auto d-block">
</div>
{{end}}
{{- end}}`
	t, err := template.New("ImageUpload").Parse(ImageUploadTemplate)
	if err != nil {
		return fmt.Errorf("ImageUploadTemplate parsing: %w", err)
	}
	err = t.Execute(w, upload)
	if err != nil {
		return fmt.Errorf("ImageUploadTemplate Execute: %w", err)
	}
	return nil
}
