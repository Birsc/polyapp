package gen

import (
	"bytes"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestGenSimpleContainer(t *testing.T) {
	var w bytes.Buffer
	err := GenSimpleContainer(SimpleContainer{}, &w)
	if err == nil {
		t.Error("Expected an error if LabeledInput is empty because you shouldn't call a function expecting it to do nothing")
	}

	simpleContainer := SimpleContainer{
		ID:          "asfdjlkjkldfsaasdf",
		Type:        "blegh",
		AlignCenter: false,
		Template:    "<p>hello world</p>",
	}
	w.Reset()
	err = GenSimpleContainer(simpleContainer, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	simpleContainer.Type = "fluid"
	w.Reset()
	err = GenSimpleContainer(simpleContainer, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	if !strings.Contains(w.String(), `div`) {
		t.Error(`should have added a div`)
	}
	if !strings.Contains(w.String(), `<p>hello world</p>`) {
		t.Error(`should have added the inner HTML to the output`)
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}

	simpleContainer.AlignCenter = true
	w.Reset()
	err = GenSimpleContainer(simpleContainer, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	if !strings.Contains(w.String(), `align-items-center`) {
		t.Error(`should have used align-items-center class`)
	}
	if !strings.Contains(w.String(), `<p>hello world</p>`) {
		t.Error(`should have added the inner HTML to the output`)
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}
