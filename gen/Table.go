package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"
)

type Column struct {
	Header string // Header shown for this column.
	ID     string // ID is the Field displayed in this column like: industry_domain_schema_some field
}

// Table which has all of the table features you may ever need or want.
type Table struct {
	// ID of the table. If an element is selected, its value is stored at this ID.
	ID string
	// Industry of the table. Used when making the request to get the table's rows.
	Industry string
	// Domain of the table.
	Domain string
	// Schema of the table, such as "polyappTask".
	Schema string
	// Columns in the table
	Columns []Column
	// InitialSearch (optional) is the initial value to apply to the search bar when loading the table.
	// Overwritten by the table-initial-search URL parameter.
	InitialSearch string
	// InitialOrderColumn (optional) is the column to order in 'asc' when the table loads. The int is the column number (0 is first).
	// Overwritten by the table-initial-order-column URL parameter.
	InitialOrderColumn int
	// InitialLastDocumentID (optional) is the initial value to being sorting after. By setting this you can seek to a different
	// part of the table. Because Firestore isn't very clear about what your index is in the overall list, you can't
	// set the initial list # another way.
	// Overwritten by the table-initial-last-document-ID URL parameter.
	InitialLastDocumentID string
}

// Validate ensures the correct fields are populated without validating the output.
func (t Table) Validate() error {
	if t.ID == "" {
		return errors.New("ID is required")
	}
	if t.Columns == nil {
		return errors.New("no columns provided")
	}
	for _, column := range t.Columns {
		if column.Header == "" {
			return errors.New("Header in column was empty")
		}
		if column.ID == "" {
			return errors.New("ID in column " + column.Header + " was empty")
		}
	}
	if t.Industry == "" {
		return errors.New("Industry empty")
	}
	if t.Domain == "" {
		return errors.New("Domain empty")
	}
	if t.Schema == "" {
		return errors.New("Schema empty")
	}
	return nil
}

// GenTable generates a TextButton in Template.
func GenTable(table Table, writer io.Writer) error {
	err := table.Validate()
	if err != nil {
		return fmt.Errorf("GenTable failed validation: %w", err)
	}
	table.ID = url.PathEscape(table.ID) // TODO change to Schema not Schema here and in the JS
	TableTemplate := `{{define "Table"}}
<table id="{{.ID}}" class='polyappDataTable table table-striped table-bordered table-hover' data-industry="{{.Industry}}" 
	data-domain="{{.Domain}}" data-schema="{{.Schema}}" data-initial-search="{{.InitialSearch}}"
	data-initial-order-column="{{.InitialOrderColumn}}" data-initial-last-document-ID="{{.InitialLastDocumentID}}">
	<thead>
		<tr>
{{range .Columns}}
			<th id="{{.ID}}">{{.Header}}</th>
{{end}}
		</tr>
	</thead>
</table>
{{- end}}
`
	t, err := template.New("Table").Parse(TableTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, table)
	if err != nil {
		return err
	}
	return nil
}
