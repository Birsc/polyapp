package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"
	"strings"
)

// Video is used when displaying videos in the UI.
type Video struct {
	// ID of the element which contains the video and the video URL.
	ID string
	// VideoURL is the source VideoURL of the video.
	//
	// If it is a Youtube video, the "v" parameter is extracted and the URL is reformed as: https://www.youtube.com/embed/{{.ID}}
	VideoURL string
	// OverlayText (optional) is shown on top of the Video.
	OverlayText string
	// Width (default: 640) of the player.
	Width string
	// Height (default: 360) of the player.
	Height string
	// Autoplay (optional) autoplays the video.
	Autoplay bool
	// Fullscreen (optional) ignores Width and Height and tries to play the video in fullscreen mode.
	Fullscreen bool
}

func (v *Video) Validate() error {
	if v.ID == "" {
		return errors.New("ID empty")
	}
	if v.VideoURL == "" {
		return errors.New("VideoURL empty")
	}
	if strings.Contains(v.VideoURL, "youtube.com/watch") {
		// this is the form of the URL which is used on Youtube itself.
		parsedURL, err := url.Parse(v.VideoURL)
		if err != nil {
			return fmt.Errorf("url.Parse %v: %w", v.VideoURL, err)
		}
		// Docs: https://developers.google.com/youtube/player_parameters
		v.VideoURL = "https://www.youtube.com/embed/" + parsedURL.Query().Get("v") + "?"
		if v.Autoplay {
			v.VideoURL += "autoplay=1&"
		}
		v.VideoURL += "controls=0&disablekb=1&loop=1&modestbranding=1&enablejsapi=1&playlist=" + parsedURL.Query().Get("v")
	}
	if v.Width == "" && !v.Fullscreen {
		v.Width = "100vw"
	}
	if v.Height == "" && !v.Fullscreen {
		v.Height = "56.25vw"
	}
	return nil
}

// GenVideo generates a new control which hosts a video.
func GenVideo(video Video, w io.Writer) error {
	err := video.Validate()
	if err != nil {
		return fmt.Errorf("Validation: %w", err)
	}
	videoTemplate := `{{define "VideoTemplate"}}
{{if .OverlayText}}<div class="w-100 text-center align-items-center" style="height: 56.25vw; position: absolute; background-color: rgba(0,0,0,0.60);">
<h1 style="top: 50%; left: 50%; position: absolute; -webkit-transform: translate(-50%,-50%); -ms-transform: translate(-50%,-50%); transform: translate(-50%,-50%); color:white;">{{.OverlayText}}</h1>
<button class="btn-primary btn-lg">Play Video</button>
</div>{{end}}
<iframe id="{{.ID}}" type="text/html" src="{{.VideoURL}}" frameborder="0" allow="{{if .Autoplay}}autoplay; {{end}}{{if .Fullscreen}}fullscreen{{end}}" allowfullscreen 
style="border: 0; {{if .Fullscreen}}width:100%; height:56.25vw{{else}}width: {{.Width}}; height: {{.Height}}{{end}}"></iframe>
{{- end}}
`
	t, err := template.New("VideoTemplate").Parse(videoTemplate)
	if err != nil {
		return fmt.Errorf("Parse: %w", err)
	}
	err = t.Execute(w, video)
	if err != nil {
		return fmt.Errorf("Execute: %w", err)
	}
	return nil
}
