package gen

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/url"
	"strconv"
	"strings"
	"text/template"
)

// SimpleContainer is a Div which wraps the provided Template
type SimpleContainer struct {
	// ID of the container
	ID string
	// Type is either "fixed" or "fluid"
	Type string
	// AlignCenter does what you think it does for the content inside the container
	// It overrides Type to use the "row" class
	AlignCenter bool
	// Template is the content inside the container
	Template string
}

// Validate ensures the correct fields are populated without validating the output.
func (s SimpleContainer) Validate() error {
	if s.ID == "" {
		return errors.New("ID not provided")
	}
	if s.Template == "" {
		return errors.New("Template not provided")
	}
	if s.Type == "" {
		return errors.New("Type not provided")
	}
	switch s.Type {
	case "fixed":
		return nil
	case "fluid":
		return nil
	default:
		return errors.New("unhandled SimpleContainer Type")
	}
}

// GenSimpleContainer generates a Div with a Container class
func GenSimpleContainer(simpleContainer SimpleContainer, writer io.Writer) error {
	err := simpleContainer.Validate()
	if err != nil {
		return fmt.Errorf("SimpleContainer failed validation: %w", err)
	}
	simpleContainer.ID = url.PathEscape(simpleContainer.ID)
	simpleContainerTemplate := `{{/* . is SimpleContainer  */}}
{{define "simpleContainerTemplate"}}
<div class="{{if .AlignCenter}}row justify-content-center align-items-center no-gutters{{else}}{{.Type}}{{end}}" id="{{.ID}}">
	{{if .AlignCenter}}<div>{{.Template}}</div>{{else}}{{.Template}}{{end}}
</div>
{{- end}}
`
	t, err := template.New("simpleContainerTemplate").Parse(simpleContainerTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, simpleContainer)
	if err != nil {
		return err
	}
	return nil
}

// ListAccordion is a component which contains at its highest level a list. This component is designed to hold extremely
// generic elements so that it can hold a list of SubTasks. Because it is so generic, you must provide your own
// DisplayNoneRenderedSubTask which will play nice with main.js.
type ListAccordion struct {
	// ID of the container. Must start with "_" for the convenience of the UI.
	ID string
	// Label is shown to users as the header.
	Label string
	// HelpText of this list.
	HelpText string
	// Deprecated. RenderedSubTasks are the already-populated templates of the subtasks.
	// Deprecated because this barely works if someone is loading information from a real field. You could argue this is OK
	// for static lists which can not be removed from / added to, but that's now how this control is currently set up.
	RenderedSubTasks []string
	// Deprecated. SubTaskNames has 1 entry for every RenderedSubTasks and is shown to users when the subtask is collapsed.
	// Deprecated because this barely works if someone is loading information from a real field. You could argue this is OK
	// for static lists which can not be removed from / added to, but that's now how this control is currently set up.
	SubTaskNames []string
	// Deprecated. SubTaskRefs are Refs. For more information about Ref, see common.Data.
	SubTaskRefs []string
	// DisplayNoneRenderedSubTask is a model list item. You probably need to examine func GenListLabeledInput to know what this means.
	// Deprecated because this barely works if someone is loading information from a real field. You could argue this is OK
	// for static lists which can not be removed from / added to, but that's now how this control is currently set up.
	DisplayNoneRenderedSubTask string
	// DisplayNoneTitleField is a field like "industry_domain_schema_some field"
	DisplayNoneTitleField string
	// DisplayNoneSubTaskRef is a Ref used when adding an element to the list. Its Data will be replaced, but not its UX or Schema.
	//
	// This should be constructed using this format: common.CreateRef("industry", "domain", "task", "ux", "schema", "polyappShouldBeOverwritten")
	//
	// For more information about Ref, see common.Data.
	DisplayNoneSubTaskRef string
}

// Validate ensures the correct fields are populated without validating the output.
func (l *ListAccordion) Validate() error {
	if l.ID == "" {
		return errors.New("ID not provided")
	}
	if l.ID[0] != '_' {
		return errors.New("ListAccordion are embedded tasks, which means the IDs must start with _")
	}
	if l.Label == "" {
		return errors.New("Label not provided")
	}
	if l.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if l.DisplayNoneRenderedSubTask == "" {
		return errors.New("DisplayNoneRenderedSubTask must exist; else a javascript error will be thrown when adding a new item")
	}
	if l.DisplayNoneTitleField == "" {
		return errors.New("DisplayNoneTitleField must exist so there is a title for the task")
	}

	var escapeWriter bytes.Buffer
	template.HTMLEscape(&escapeWriter, []byte(l.ID))
	l.ID = escapeWriter.String()
	escapeWriter.Reset()
	template.HTMLEscape(&escapeWriter, []byte(l.Label))
	l.Label = escapeWriter.String()
	escapeWriter.Reset()
	template.HTMLEscape(&escapeWriter, []byte(l.HelpText))
	l.HelpText = escapeWriter.String()
	escapeWriter.Reset()
	return nil
}

func genListAccordionItem(renderedSubTask string, subTaskName string, subTaskRef string, currentListID int) (string, error) {
	var removeButton bytes.Buffer
	var nestedAndRenderedSubtask bytes.Buffer
	err := GenIconButton(IconButton{
		ID:        "polyappJShtml5sortable-remove" + strconv.Itoa(currentListID),
		ImagePath: "/assets/icons/red_minus.png",
		Text:      "",
		AltText:   "Remove List Item",
	}, &removeButton)
	if err != nil {
		return "", fmt.Errorf("GenIconButton err: %w", err)
	}
	err = GenNestedAccordion([]NestedAccordion{
		{
			TitleField:   subTaskName,
			InnerContent: renderedSubTask,
			Ref:          subTaskRef,
		},
	}, &nestedAndRenderedSubtask)
	if err != nil {
		return "", fmt.Errorf("GenNestedAccordion: %w", err)
	}

	return `<div class="list-group-item list-group-item-dark list-group-item-action grabbable">
	<div class="d-inline-block col-11 p-0">
		` + nestedAndRenderedSubtask.String() + `
	</div>
	<div class="d-inline-block col-1 px-0 py-1 float-right">
		` + removeButton.String() + `
	</div>
</div>`, nil
}

// GenListAccordion is of a list of Accordion elements which can be expanded to reveal additional content.
// Each Accordion element inside the list is part of a separate Task and is created with GenNestedAccordion.
func GenListAccordion(listGeneric ListAccordion, w io.Writer) error {
	err := listGeneric.Validate()
	if err != nil {
		return fmt.Errorf("validation failure: %w", err)
	}
	listItems := ""
	currentListID := 1
	for i, renderedSubTask := range listGeneric.RenderedSubTasks {
		newListItem, err := genListAccordionItem(renderedSubTask, listGeneric.SubTaskNames[i], listGeneric.SubTaskRefs[i], currentListID)
		if err != nil {
			return fmt.Errorf("genListAccordionItem: %w", err)
		}
		currentListID++
		listItems += newListItem
	}

	var addImgButton bytes.Buffer
	err = GenIconButton(IconButton{
		ID:        "polyappJShtml5sortable-add" + listGeneric.ID,
		ImagePath: "/assets/icons/add-24px.svg",
		Text:      listGeneric.Label,
		AltText:   "Add",
	}, &addImgButton)
	if err != nil {
		return fmt.Errorf("err in GenIconButton for adding purposes")
	}

	displayNoneEl, err := genListAccordionItem(listGeneric.DisplayNoneRenderedSubTask, listGeneric.DisplayNoneTitleField, listGeneric.DisplayNoneSubTaskRef, rand.Int())
	if err != nil {
		return fmt.Errorf("genListAccordionItem: %w", err)
	}

	// do this last to avoid double-escaping in genListAccordionItem or another generator which escapes key names
	listGeneric.ID = url.PathEscape(listGeneric.ID)

	listGenericTemplate := `{{define "listGeneric"}}
<div class="form-group" id="{{.ID}}">
	<label>{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="list-group html5sortable">
	` + listItems + `
	</div>
	<div class="displayNone">` + displayNoneEl + `</div>
	` + addImgButton.String() + `
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
{{end}}
`
	t, err := template.New("listGeneric").Parse(listGenericTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, listGeneric)
	if err != nil {
		return err
	}
	return nil
}

// SectionStart defines options for Sections.
type SectionStart struct {
	// BackgroundColor is set to any valid CSS color.
	BackgroundColor string
	// SectionRow is the # of the section on the page. If this is even or odd it can change the styles used.
	SectionRow int
	// IsLastRow if true, this row expands to take up the remainder of the page.
	IsLastRow bool
	// Layout is one of: "Max Width", "2 Column", or "Standard" (default).
	Layout string
}

// GenSectionStart starts a new Section.
func GenSectionStart(start SectionStart, w io.StringWriter) {
	if strings.Contains(start.Layout, "Column") {
		columnLayoutSectionStart(start, w)
		return
	}
	w.WriteString(`<section class="py-3 d-flex flex-column `)
	if start.IsLastRow {
		w.WriteString(`flex-grow-1 `)
	}
	if start.BackgroundColor == "black" {
		w.WriteString(`bg-gray-9 text-light`)
	} else {
		w.WriteString(`text-dark`)
	}
	// https://stackoverflow.com/questions/32689686/overlapping-css-flexbox-items-in-safari
	w.WriteString(`" style="flex-shrink: 0; `) // close class
	if start.BackgroundColor != "" && start.BackgroundColor != "black" {
		w.WriteString(`background-color:`)
		w.WriteString(start.BackgroundColor)
		w.WriteString(`;"`)
	} else {
		w.WriteString(`"`)
	}
	w.WriteString(`>`) // close <section
	w.WriteString(`<div class="`)
	if start.Layout == "Max Width" {
		w.WriteString(`container-fluid`)
	} else {
		w.WriteString(`container`)
	}
	w.WriteString(`">
`)
}

// columnLayoutSectionStart starts a new Section.
//
// The plan is to wrap a Div around all of the content on a 2 Column page which
// takes care of Overflow for us and pads us from other Sections.
//
// After that it's a basic Bootstrap Grid layout.
func columnLayoutSectionStart(start SectionStart, w io.StringWriter) {
	if start.SectionRow == 0 {
		w.WriteString(`<div class="py-3 d-flex flex-column flex-grow-1`)
		if start.BackgroundColor == "black" {
			w.WriteString(`bg-gray-9 text-light`)
		}
		w.WriteString(`">
`)
	}

	// Information which is part of one column or another.
	if start.SectionRow%2 == 0 {
		w.WriteString(`<div class="row">
`)
	}
	w.WriteString(`<div class="col-md">
`)
}

// GenSectionTaskStart creates a subtask in the UX which contains this Section.
//
// sectionID MUST be of the 'polyappRef' format. This can be created with common.CreateRef. Do not escape it nor add the polyappRef prefix.
func GenSectionTaskStart(start SectionStart, polyappRef string, RefInParent string, w io.StringWriter) {
	escapedRef := url.PathEscape("polyappRef" + polyappRef)
	w.WriteString(`<section id="`)
	w.WriteString(escapedRef)
	w.WriteString(`" data-Ref-Field="`)
	w.WriteString(RefInParent)
	w.WriteString(`" class="py-3 d-flex flex-column `)
	if start.IsLastRow {
		w.WriteString(`flex-grow-1 `)
	}
	w.WriteString(`text-dark" style="flex-shrink: 0;`)
	if start.BackgroundColor != "" && start.BackgroundColor != "#000000" && start.BackgroundColor != "black" {
		w.WriteString(` background-color:`)
		w.WriteString(start.BackgroundColor)
		w.WriteString(`;"`)
	} else {
		w.WriteString(`"`)
	}
	w.WriteString(`>`)
	w.WriteString(`<div class="`)
	if start.Layout == "Max Width" {
		w.WriteString(`container-fluid`)
	} else {
		w.WriteString(`container`)
	}
	w.WriteString(`">
`)
}

func GenSectionEnd(start SectionStart, w io.StringWriter) {
	if strings.Contains(start.Layout, "Column") {
		columnLayoutSectionEnd(start, w)
		return
	}
	w.WriteString("</div></section>")
}

func columnLayoutSectionEnd(start SectionStart, w io.StringWriter) {
	if start.SectionRow%2 == 1 || start.IsLastRow {
		// close Row
		w.WriteString(`</div>
`)
	}
	if start.IsLastRow {
		// close overall Div which handles overflow and padding between sections
		w.WriteString(`</div>
`)
	}
	// Div close for the column
	w.WriteString(`</div>`)
}

func GenGalleryStart(w io.StringWriter) {
	w.WriteString(`<div class="row m-x-0 m-x-">`)
}

func GenGalleryEnd(w io.StringWriter) {
	w.WriteString(`</div>`)
}
