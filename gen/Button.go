package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/url"
	"strings"
)

// TextButton can be used to create a simple Button
type TextButton struct {
	// ID of the button
	ID string
	// Text on the button
	Text string
	// Styling is one of: "success", "info", "warning", "danger", "primary", "secondary", "dark", "light", "link"
	Styling string
	// Recaptcha (optional) if set to true, this button will be associated with a recaptcha. When a user presses the
	// button, the challenge will be initiated.
	Recaptcha        bool
	RecaptchaSiteKey string
	Disabled         bool // If true the button will be disabled in the UI
}

// Validate ensures the correct fields are populated without validating the output.
func (b TextButton) Validate() error {
	if b.ID == "" {
		return errors.New("no ID provided")
	}
	if b.Text == "" {
		return errors.New("no Text provided")
	}
	if b.Styling == "" {
		return errors.New("no Severity provided")
	}
	switch b.Styling {
	case "success":
		return nil
	case "info":
		return nil
	case "warning":
		return nil
	case "danger":
		return nil
	case "primary":
		return nil
	case "secondary":
		return nil
	case "dark":
		return nil
	case "light":
		return nil
	case "link":
		return nil
	default:
		return errors.New("styling was not recognized")
	}
}

// GenTextButton generates a TextButton in Template.
func GenTextButton(TextButton TextButton, writer io.Writer) error {
	err := TextButton.Validate()
	if err != nil {
		return fmt.Errorf("GenTextButton failed validation: %w", err)
	}
	TextButton.ID = url.PathEscape(TextButton.ID)
	IconButtonTemplate := `{{/* . is TextButton  */}}
{{define "IconButtonTemplate"}}
<button type="button" id="{{.ID}}" class="btn btn-{{.Styling}}{{if .Recaptcha}} g-recaptcha{{end}}"{{if .Recaptcha}} data-sitekey="{{.RecaptchaSiteKey}}" data-callback="recaptchaSuccess" data-expired-callback="recaptchaExpired"{{end}}{{if .Disabled}} disabled{{end}}>{{.Text}}</button>
{{- end}}
`
	t, err := template.New("IconButtonTemplate").Parse(IconButtonTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, TextButton)
	if err != nil {
		return err
	}
	return nil
}

// IconButton can be used to create a button composed solely of an Icon and the button. Aka an image button.
// A related concept is the IconRadioInput. TODO create IconRadioInput and use it for the side bar.
type IconButton struct {
	// ID of the button
	ID string
	// ImagePath is a path in the public folder like "/assets/icons/edit-24px.svg"
	// It should be a full path, not a partial path.
	ImagePath string
	// AltText is used by users with screen readers to identify the image and the button. It should be a set of capitalized words without a period.
	AltText string
	// Text (optional) is some text to show to the right of the icon. You can't show it to the left of the icon intentionally.
	Text string
	// Inline (optional) places the button with inline-block.
	Inline bool
	// Readonly (optional) disables this button
	Readonly bool
}

func (b IconButton) Validate() error {
	if b.ID == "" {
		return errors.New("no ID provided")
	}
	if b.ImagePath == "" {
		return errors.New("no ImagePath provided")
	}
	if len(b.ImagePath) < len("assets")+1 || (b.ImagePath[:len("assets")] != "assets" && b.ImagePath[:len("/assets")] != "/assets") {
		return errors.New("ImagePath must start with 'assets'")
	}
	s := strings.Split(b.ImagePath, "/")
	if len(s) < 2 {
		return errors.New("ImagePath must be a '/' separated path")
	}
	if b.AltText == "" {
		return errors.New("no alt text provided")
	}
	return nil
}

// GenIconButton generates a button containing an image in Template.
func GenIconButton(IconButton IconButton, writer io.Writer) error {
	err := IconButton.Validate()
	if err != nil {
		return fmt.Errorf("GenIconButton failed validation: %w", err)
	}
	IconButton.ID = url.PathEscape(IconButton.ID)
	IconButtonTemplate := `{{/* . is IconButton  */}}
{{define "IconButtonTemplate"}}
<button id="{{.ID}}" type="button" class="btn{{if .Inline}} d-inline{{end}}" {{if eq .Text ""}}aria-label="{{.AltText}}"{{end}} {{if .Readonly}}disabled{{end}}>
	<img src="{{.ImagePath}}" alt="{{.AltText}}" />{{.Text}}
	</button>
{{- end}}
`
	t, err := template.New("IconButtonTemplate").Parse(IconButtonTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(writer, IconButton)
	if err != nil {
		return err
	}
	return nil
}
