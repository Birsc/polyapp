package gen

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"sort"
)

// OneToOneFromTo is a simple component which shows a 'from' field and a 'to' field.
// It assumes we're mapping from a string to a string.
type OneToOneFromTo struct {
	// WrapperID of the outermost div
	WrapperID string
	// HelpText of this component
	HelpText string
	// FromID is the ID of the field hosting the 'From'
	FromID string
	// FromLabel is the label of the field hosting the 'From'
	FromLabel string
	// FromInitialValue is the initial value of the 'From' field
	FromInitialValue string
	// FromType is the type of the To field; "text" "textarea" "number" "checkbox"
	FromType string
	// ToID is the ID of the field hosting the 'To'.
	ToID string
	// ToLabel is the label of the field hosting the 'To'
	ToLabel string
	// ToInitialValue is the initial value of the 'To' field
	ToInitialValue string
	// ToType is the type of the To field; "text" "textarea" "number" "checkbox"
	ToType string
}

// Validate ensures the correct fields are populated without validating the output.
func (o OneToOneFromTo) Validate() error {
	if o.WrapperID == "" {
		return errors.New("must populate WrapperID")
	}
	if o.FromID == "" {
		return errors.New("must populate FromID")
	}
	if o.FromLabel == "" {
		return errors.New("must populate FromLabel")
	}
	if o.HelpText == "" {
		return errors.New("must populate HelpText")
	}
	if o.FromType == "" {
		return errors.New("must populate FromType")
	}
	if o.ToID == "" {
		return errors.New("must populate ToID")
	}
	if o.ToLabel == "" {
		return errors.New("must populate ToLabel")
	}
	if o.ToType == "" {
		return errors.New("must populate ToType")
	}
	return nil
}

// GenOneToOneFromTo creates Template out of an array of OneToOneFromTo.
func GenOneToOneFromTo(inputs []OneToOneFromTo, writer io.Writer) error {
	if len(inputs) < 1 {
		return errors.New("expected an array of LabeledInput to be passed")
	}
	for i := range inputs {
		err := inputs[i].Validate()
		if err != nil {
			return fmt.Errorf("GenOneToOneFromTo validation error at index "+string(i)+" : %w", err)
		}
	}

	var w bytes.Buffer
	for i := range inputs {
		w.WriteString(`<div id="` + inputs[i].WrapperID + `"><hr>`)
		err := GenLabeledInput([]LabeledInput{
			{
				WrapperID:    inputs[i].WrapperID + "FromInnerWrapper",
				Label:        inputs[i].FromLabel,
				HelpText:     inputs[i].HelpText,
				ID:           inputs[i].FromID,
				InitialValue: inputs[i].FromInitialValue,
				Type:         inputs[i].FromType,
			},
		}, &w)
		if err != nil {
			return fmt.Errorf("GenLabeledInput From error index: "+string(i)+"%w", err)
		}
		err = GenLabeledInput([]LabeledInput{
			{
				WrapperID:    inputs[i].WrapperID + "ToInnerWrapper",
				Label:        inputs[i].ToLabel,
				HelpText:     inputs[i].HelpText,
				ID:           inputs[i].ToID,
				InitialValue: inputs[i].ToInitialValue,
				Type:         inputs[i].ToType,
			},
		}, &w)
		if err != nil {
			return fmt.Errorf("GenLabeledInput To error index: "+string(i)+"%w", err)
		}
		w.WriteString(`<hr></div>`)
	}
	_, err := writer.Write(w.Bytes())
	return err
}

// OneToOneFromToSort sorts OneToOneFromTo by the label itself.
// It follows a pattern similar to the SortKeys example in https://golang.org/pkg/sort/
type OneToOneFromToBy func(li1 *OneToOneFromTo, li2 *OneToOneFromTo) bool

// Sort is a method on the function type that sorts the argument slice according to the function.
func (by OneToOneFromToBy) Sort(lis []OneToOneFromTo) {
	sorter := &OneToOneFromToSorter{
		lis: lis,
		by:  by,
	}
	sort.Sort(sorter)
}

// OneToOneFromToSorter helps with sorting.
type OneToOneFromToSorter struct {
	lis []OneToOneFromTo
	by  func(li1 *OneToOneFromTo, li2 *OneToOneFromTo) bool
}

// Len is part of sort.Interface.
func (s *OneToOneFromToSorter) Len() int {
	return len(s.lis)
}

// Swap is part of sort.Interface.
func (s *OneToOneFromToSorter) Swap(i, j int) {
	s.lis[i], s.lis[j] = s.lis[j], s.lis[i]
}

// Sort is a method on the function type, By, that sorts the argument slice.
func (s OneToOneFromToSorter) Less(i, j int) bool {
	return s.by(&s.lis[i], &s.lis[j])
}
