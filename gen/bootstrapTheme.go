package gen

import (
	"fmt"
	"io"
	"text/template"
)

type BootstrapTheme struct {
	// Primary is the primary color used in the template.
	Primary string
	// Secondary color
	Secondary string
	// Success color
	Success string
	// Info color
	Info string
	// Warning color
	Warning string
	// Danger color
	Danger string
	// Light color
	Light string
	// Dark color
	Dark string
	// BodyBackground is the color for the body's background.
	BodyBackground string
	// Body is the color of the body
	Body string
}

func (b *BootstrapTheme) Validate() error {
	if b.Primary == "" {
		b.Primary = "#007bff"
	}
	if b.Secondary == "" {
		b.Secondary = "$gray-600"
	}
	if b.Success == "" {
		b.Success = "$green"
	}
	if b.Info == "" {
		b.Info = "$cyan"
	}
	if b.Warning == "" {
		b.Warning = "$yellow"
	}
	if b.Danger == "" {
		b.Danger = "$red"
	}
	if b.Light == "" {
		b.Light = "$gray-100"
	}
	if b.Dark == "" {
		b.Dark = "$gray-800"
	}
	if b.BodyBackground == "" {
		b.BodyBackground = "#ffffff"
	}
	if b.Body == "" {
		b.Body = "$gray-900"
	}
	return nil
}

// GenBootstrapTheme uses the data in BootstrapTheme to create custom theme overrides for Bootstrap. It then runs an
// external program to turn those
func GenBootstrapTheme(b BootstrapTheme, w io.Writer) error {
	err := b.Validate()
	if err != nil {
		return fmt.Errorf("GenImage failed validation: %w", err)
	}
	t, err := template.New("BootstrapTheme").Parse(bootstrapThemeTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, b)
	if err != nil {
		return err
	}
	return nil
}

var bootstrapThemeTemplate = `{{define "BootstrapTheme"}}
// test.scss is a test theme in Bootstrap created solely for testing purposes.
// It contains ALL of the variables I am considering overriding.

// The variables I am modifying are located in node_modules/bootstrap/scss/_variables.scss
// To compile the newly themed Boostrap: sass public/bootstrap-themes/test.scss:public/bootstrap-themes/test.css --style compressed

// The website https://themestr.app/theme tends to just set the first 8 theme colors.

@import "bootstrap-scss/functions";
@import "bootstrap-scss/variables";
@import "bootstrap-scss/mixins";

// Theme Colors
$primary:       {{.Primary}};
$secondary:     {{.Secondary}};
$success:       {{.Success}};
$info:          {{.Info}};
$warning:       {{.Warning}};
$danger:        {{.Danger}};
$light:         {{.Light}};
$dark:          {{.Dark}};

// Body
//
// Settings for the <body> element.

$body-bg:                   {{.BodyBackground}};
$body-color:                {{.Body}};

// Components
//
// Define common padding and border radius sizes and more.
$border-color:                $gray-300;

$component-active-color:      $white;
$component-active-bg:         theme-color("primary");

// Typography
//
// Font, line-height, and color for body text, headings, and more.

// stylelint-disable value-keyword-case
$font-family-sans-serif:      -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
$font-family-monospace:       SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
$font-family-base:            $font-family-sans-serif;


$text-muted:                  $gray-600;

$blockquote-small-color:      $gray-600;

$mark-bg:                     #fcf8e3;


// Tables
//
// Customizes the .table component with basic values, each used across all table variations.

$table-color:                 $body-color;
$table-bg:                    null;
$table-accent-bg:             rgba($black, .05);
$table-hover-color:           $table-color;
$table-hover-bg:              rgba($black, .075);
$table-active-bg:             $table-hover-bg;

$table-border-color:          $border-color;

$table-head-bg:               $gray-200;
$table-head-color:            $gray-700;

$table-dark-color:            $white;
$table-dark-bg:               $gray-800;
$table-dark-accent-bg:        rgba($white, .05);
$table-dark-hover-color:      $table-dark-color;
$table-dark-hover-bg:         rgba($white, .075);
$table-dark-border-color:     lighten($table-dark-bg, 7.5%);

$table-caption-color:         $text-muted;

// Buttons + Forms
//
// Shared variables that are reassigned to $input- and $btn- specific variables.

$input-btn-font-family:       null;

$input-btn-focus-color:       rgba($component-active-bg, .25);


// Buttons
//
// For each of Bootstrap's buttons, define text, background, and border color.

$btn-font-family:             $input-btn-font-family;

$btn-link-disabled-color:     $gray-600;


// Forms

$input-font-family:                     $input-btn-font-family;

$input-bg:                              $white;
$input-disabled-bg:                     $gray-200;

$input-color:                           $gray-700;
$input-border-color:                    $gray-400;

$input-focus-bg:                        $input-bg;
$input-focus-border-color:              lighten($component-active-bg, 25%);
$input-focus-color:                     $input-color;

$input-placeholder-color:               $gray-600;
$input-plaintext-color:                 $body-color;

$input-group-addon-color:               $input-color;
$input-group-addon-bg:                  $gray-200;
$input-group-addon-border-color:        $input-border-color;

$custom-control-indicator-bg:           $input-bg;

$custom-control-indicator-box-shadow:   $input-box-shadow;
$custom-control-indicator-border-color: $gray-500;

$custom-control-label-color:            null;

$custom-control-indicator-disabled-bg:          $input-disabled-bg;
$custom-control-label-disabled-color:           $gray-600;

$custom-control-indicator-checked-color:        $component-active-color;
$custom-control-indicator-checked-bg:           $component-active-bg;
$custom-control-indicator-checked-disabled-bg:  rgba(theme-color("primary"), .5);
$custom-control-indicator-checked-box-shadow:   none;
$custom-control-indicator-checked-border-color: $custom-control-indicator-checked-bg;

$custom-control-indicator-focus-box-shadow:     $input-focus-box-shadow;
$custom-control-indicator-focus-border-color:   $input-focus-border-color;

$custom-control-indicator-active-color:         $component-active-color;
$custom-control-indicator-active-bg:            lighten($component-active-bg, 35%);
$custom-control-indicator-active-box-shadow:    none;
$custom-control-indicator-active-border-color:  $custom-control-indicator-active-bg;

$custom-checkbox-indicator-icon-checked:        url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'><path fill='#{$custom-control-indicator-checked-color}' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26l2.974 2.99L8 2.193z'/></svg>");

$custom-checkbox-indicator-indeterminate-bg:           $component-active-bg;
$custom-checkbox-indicator-indeterminate-color:        $custom-control-indicator-checked-color;
$custom-checkbox-indicator-icon-indeterminate:         url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='4' height='4' viewBox='0 0 4 4'><path stroke='#{$custom-checkbox-indicator-indeterminate-color}' d='M0 2h4'/></svg>");
$custom-checkbox-indicator-indeterminate-box-shadow:   none;
$custom-checkbox-indicator-indeterminate-border-color: $custom-checkbox-indicator-indeterminate-bg;

$custom-radio-indicator-icon-checked:           url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='12' height='12' viewBox='-4 -4 8 8'><circle r='3' fill='#{$custom-control-indicator-checked-color}'/></svg>");

$custom-select-font-family:         $input-font-family;
$custom-select-color:               $input-color;
$custom-select-disabled-color:      $gray-600;
$custom-select-bg:                  $input-bg;
$custom-select-disabled-bg:         $gray-200;
$custom-select-indicator-color:     $gray-800;
$custom-select-indicator:           url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='4' height='5' viewBox='0 0 4 5'><path fill='#{$custom-select-indicator-color}' d='M2 0L0 2h4zm0 5L0 3h4z'/></svg>");

$custom-select-border-color:        $input-border-color;

$custom-select-focus-border-color:  $input-focus-border-color;

$custom-range-track-cursor:         pointer;
$custom-range-track-bg:             $gray-300;

$custom-range-thumb-bg:                      $component-active-bg;
$custom-range-thumb-active-bg:               lighten($component-active-bg, 35%);
$custom-range-thumb-disabled-bg:             $gray-500;

$custom-file-focus-border-color:    $input-focus-border-color;
$custom-file-disabled-bg:           $input-disabled-bg;

$custom-file-font-family:           $input-font-family;
$custom-file-color:                 $input-color;
$custom-file-bg:                    $input-bg;
$custom-file-border-color:          $input-border-color;
$custom-file-button-color:          $custom-file-color;
$custom-file-button-bg:             $input-group-addon-bg;


// Form validation

$form-feedback-valid-color:         theme-color("success");
$form-feedback-invalid-color:       theme-color("danger");

$form-feedback-icon-valid-color:    $form-feedback-valid-color;
$form-feedback-icon-valid:          url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'><path fill='#{$form-feedback-icon-valid-color}' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/></svg>");
$form-feedback-icon-invalid-color:  $form-feedback-invalid-color;
$form-feedback-icon-invalid:        url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='#{$form-feedback-icon-invalid-color}' viewBox='0 0 12 12'><circle cx='6' cy='6' r='4.5'/><path stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/><circle cx='6' cy='8.2' r='.6' fill='#{$form-feedback-icon-invalid-color}' stroke='none'/></svg>");

// Navs

$nav-link-disabled-color:           $gray-600;

$nav-tabs-border-color:             $gray-300;
$nav-tabs-link-hover-border-color:  $gray-200 $gray-200 $nav-tabs-border-color;
$nav-tabs-link-active-color:        $gray-700;
$nav-tabs-link-active-bg:           $body-bg;
$nav-tabs-link-active-border-color: $gray-300 $gray-300 $nav-tabs-link-active-bg;

$nav-pills-link-active-color:       $component-active-color;
$nav-pills-link-active-bg:          $component-active-bg;

$nav-divider-color:                 $gray-200;


// Navbar

$navbar-dark-color:                 rgba($white, .5);
$navbar-dark-hover-color:           rgba($white, .75);
$navbar-dark-active-color:          $white;
$navbar-dark-disabled-color:        rgba($white, .25);
$navbar-dark-toggler-icon-bg:       url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path stroke='#{$navbar-dark-color}' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>");
$navbar-dark-toggler-border-color:  rgba($white, .1);

$navbar-light-color:                rgba($black, .5);
$navbar-light-hover-color:          rgba($black, .7);
$navbar-light-active-color:         rgba($black, .9);
$navbar-light-disabled-color:       rgba($black, .3);
$navbar-light-toggler-icon-bg:      url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path stroke='#{$navbar-light-color}' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/></svg>");
$navbar-light-toggler-border-color: rgba($black, .1);

$navbar-light-brand-color:                $navbar-light-active-color;
$navbar-light-brand-hover-color:          $navbar-light-active-color;
$navbar-dark-brand-color:                 $navbar-dark-active-color;
$navbar-dark-brand-hover-color:           $navbar-dark-active-color;


// Dropdowns
//
// Dropdown menu container and contents.

$dropdown-color:                    $body-color;
$dropdown-bg:                       $white;
$dropdown-border-color:             rgba($black, .15);
$dropdown-divider-bg:               $gray-200;
$dropdown-divider-margin-y:         $nav-divider-margin-y;

$dropdown-link-color:               $gray-900;
$dropdown-link-hover-color:         darken($gray-900, 5%);
$dropdown-link-hover-bg:            $gray-100;

$dropdown-link-active-color:        $component-active-color;
$dropdown-link-active-bg:           $component-active-bg;

$dropdown-link-disabled-color:      $gray-600;

$dropdown-header-color:             $gray-600;


// Pagination

$pagination-color:                  $link-color;
$pagination-bg:                     $white;
$pagination-border-color:           $gray-300;

$pagination-hover-color:            $link-hover-color;
$pagination-hover-bg:               $gray-200;
$pagination-hover-border-color:     $gray-300;

$pagination-active-color:           $component-active-color;
$pagination-active-bg:              $component-active-bg;
$pagination-active-border-color:    $pagination-active-bg;

$pagination-disabled-color:         $gray-600;
$pagination-disabled-bg:            $white;
$pagination-disabled-border-color:  $gray-300;


// Jumbotron

$jumbotron-color:                   null;
$jumbotron-bg:                      $gray-200;


// Cards

$card-border-color:                 rgba($black, .125);
$card-cap-bg:                       rgba($black, .03);
$card-cap-color:                    null;
$card-color:                        null;
$card-bg:                           $white;


// Tooltips

$tooltip-color:                     $white;
$tooltip-bg:                        $black;

$tooltip-arrow-color:               $tooltip-bg;

// Popovers

$popover-bg:                        $white;
$popover-border-color:              rgba($black, .2);

$popover-header-bg:                 darken($popover-bg, 3%);
$popover-header-color:              $headings-color;

$popover-body-color:                $body-color;

$popover-arrow-color:               $popover-bg;

$popover-arrow-outer-color:         fade-in($popover-border-color, .05);


// Toasts

$toast-color:                       null;
$toast-background-color:            rgba($white, .85);
$toast-border-color:                rgba(0, 0, 0, .1);

$toast-header-color:                $gray-600;
$toast-header-background-color:     rgba($white, .85);
$toast-header-border-color:         rgba(0, 0, 0, .05);


// Badges

$badge-font-weight:                 $font-weight-bold;

$badge-transition:                  $btn-transition;

// Modals

$modal-content-color:               null;
$modal-content-bg:                  $white;
$modal-content-border-color:        rgba($black, .2);

$modal-backdrop-bg:                 $black;
$modal-backdrop-opacity:            .5;
$modal-header-border-color:         $border-color;
$modal-footer-border-color:         $modal-header-border-color;


// Alerts
//
// Define alert colors, border radius, and padding.

// Progress bars

$progress-bg:                       $gray-200;
$progress-bar-color:                $white;
$progress-bar-bg:                   theme-color("primary");

// List group

$list-group-color:                  null;
$list-group-bg:                     $white;
$list-group-border-color:           rgba($black, .125);

$list-group-hover-bg:               $gray-100;
$list-group-active-color:           $component-active-color;
$list-group-active-bg:              $component-active-bg;
$list-group-active-border-color:    $list-group-active-bg;

$list-group-disabled-color:         $gray-600;
$list-group-disabled-bg:            $list-group-bg;

$list-group-action-color:           $gray-700;
$list-group-action-hover-color:     $list-group-action-color;

$list-group-action-active-color:    $body-color;
$list-group-action-active-bg:       $gray-200;


// Image thumbnails

$thumbnail-bg:                      $body-bg;
$thumbnail-border-color:            $gray-300;


// Figures

$figure-caption-color:              $gray-600;


// Breadcrumbs

$breadcrumb-bg:                     $gray-200;
$breadcrumb-divider-color:          $gray-600;
$breadcrumb-active-color:           $gray-600;
$breadcrumb-divider:                quote("/");

$breadcrumb-border-radius:          $border-radius;


// Carousel

$carousel-control-color:             $white;
$carousel-control-opacity:           .5;
$carousel-control-hover-opacity:     .9;

$carousel-indicator-active-bg:       $white;

$carousel-caption-color:             $white;

$carousel-control-prev-icon-bg:      url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' fill='#{$carousel-control-color}' width='8' height='8' viewBox='0 0 8 8'><path d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/></svg>");
$carousel-control-next-icon-bg:      url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' fill='#{$carousel-control-color}' width='8' height='8' viewBox='0 0 8 8'><path d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/></svg>");


// Spinners


// Close

$close-font-weight:                 $font-weight-bold;
$close-color:                       $black;


// Code

$code-color:                        $pink;

$kbd-color:                         $white;
$kbd-bg:                            $gray-900;

$pre-color:                         $gray-900;


// Utilities

// Printing

// This must be LAST. Otherwise the overrides (aka the theme) will not work.
@import "bootstrap-scss/bootstrap";
{{- end}}`
