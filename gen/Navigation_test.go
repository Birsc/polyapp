package gen

import (
	"bytes"
	"testing"

	"golang.org/x/net/html"
)

func TestGenBreadcrumbs(t *testing.T) {
	var w bytes.Buffer
	err := GenBreadcrumbs(Breadcrumbs{}, &w)
	if err == nil {
		t.Error("Expected an error if LabeledInput is empty because you shouldn't call a function expecting it to do nothing")
	}

	breadcrumbs := Breadcrumbs{
		ID:                   "asdasd",
		NavigationReferences: nil,
	}
	w.Reset()
	err = GenBreadcrumbs(breadcrumbs, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	breadcrumbs = Breadcrumbs{
		ID:                   "asdasd",
		NavigationReferences: []NavigationRef{},
	}
	w.Reset()
	err = GenBreadcrumbs(breadcrumbs, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	breadcrumbs.NavigationReferences = append(breadcrumbs.NavigationReferences, NavigationRef{
		LinkPath: "path/to/something",
		Text:     "",
	})
	w.Reset()
	err = GenBreadcrumbs(breadcrumbs, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	breadcrumbs.NavigationReferences[0].Text = "something or whatever"
	w.Reset()
	err = GenBreadcrumbs(breadcrumbs, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}

func TestGenGiantLinks(t *testing.T) {
	type args struct {
		input GiantLinks
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "empty Label",
			args: args{
				input: GiantLinks{
					Label: "something",
					Links: []NavigationRef{{
						Text:     "",
						LinkPath: "/something",
					}},
				},
			},
			wantErr: true,
		},
		{
			name: "nil Links",
			args: args{
				input: GiantLinks{
					Label: "something",
					Links: nil,
				},
			},
			wantErr: true,
		},
		{
			name: "single link",
			args: args{
				input: GiantLinks{
					Label: "something",
					Links: []NavigationRef{{
						Text:     "link 1",
						LinkPath: "/something",
					}},
				},
			},
			wantErr: false,
		},
		{
			name: "2 links",
			args: args{
				input: GiantLinks{
					Label: "something",
					Links: []NavigationRef{{
						Text:     "link 1",
						LinkPath: "/something",
					},
						{
							Text:     "link 2",
							LinkPath: "/polyapp/polyapp/polyapp/Test",
						}},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenGiantLinks(tt.args.input, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenGiantLinks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
