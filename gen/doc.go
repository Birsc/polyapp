// Package gen is a solution to the problem: How do you create HTML code dynamically?
//
// In a web framework like Angular or React
// you can create a number of components out of markup, Typescript, and other code and then place those components inside
// other components. When the framework is compiled, the references between components are results and the output
// "dist" or distributable contains regular HTML, CSS, and Javascript files. In some other frameworks the references
// are resolved at runtime, which allows for a lot more flexibility. We COULD use a framework which resolves references
// at runtime, and when a user builds a form we could insert components with configuration into a file or template.
// Then we would run the compilation step of React (or Angular or whatever the framework is). This would give us either
// HTML or something else which we can run in order to run a web server.
//
// In the case of "getting something else which we can run in order to run a web server" as with Angular, a user generating a new form
// would mean recompiling the entire project and then restarting the entire web server with the new components.
// That is too expensive of an operation, and would both disrupt other user's experience and cause tons of problems
// when many users are editing forms at the same time. Incidentally, this is why Angular was abandoned in this project.
//
// In the case of generating HTML after running it through the build process it SHOULD be possible to add more routes
// dynamically without restarting the web server. The biggest reason this is not done is the long compile time of many
// of these web frameworks. For instance, Angular's incremental compile time is over 1 second. I wanted a user experience
// which functioned as fast as possible, and although a developer might be willing to wait a couple of seconds to compile
// the UI, I am not.
//
// There is also a simpler argument against using a front-end framework (other than Bootstrap) to generate the components.
// These frameworks are designed to make a developer's life easier, and involve writing something simple, translating that
// into HTML, and then delivering that HTML to the client. Compare that to writing HTML directly and then sending
// it to the client. The difference? Not using a compilation step cuts out work. Compiling / using a framework introduces
// an extra step, and that's the opposite of keeping it simple, stupid.
//
// Putting the discussion aside, this package contains HTML code. The HTML is placed into strings which are parsed as templates
// from text/template or html/template. This allows sufficient flexibility to create any web form. Obviously forms do
// not exist in a vaccuum and these forms need quite a bit of javascript to work well. Therefore some components are
// coupled to specific Javascript functions in main.js or 'if' cases in that code. They also use data-??? properties
// sometimes to communicate necessary information to the client-side javascript.
package gen
