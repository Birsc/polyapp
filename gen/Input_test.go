package gen

import (
	"bytes"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func TestGenLabeledInput(t *testing.T) {
	inputs := make([]LabeledInput, 0)
	var w bytes.Buffer
	err := GenLabeledInput(inputs, &w)
	if err == nil {
		t.Error("Expected an error if LabeledInput is empty because you shouldn't call a function expecting it to do nothing")
	}

	inputs = make([]LabeledInput, 2)
	inputs[0] = LabeledInput{
		HelpText:  "a help text",
		Label:     "something",
		ID:        "sdklsd",
		Type:      "ksadfk afdsk asdf",
		WrapperID: "blegh",
	}
	inputs[1] = LabeledInput{
		HelpText:  "a help text",
		Label:     "something else",
		ID:        "asdfk",
		Type:      "text",
		WrapperID: "blegh",
	}
	w.Reset()
	err = GenLabeledInput(inputs, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	inputs[0].Type = "textarea"
	w.Reset()
	err = GenLabeledInput(inputs, &w)
	if err != nil {
		t.Error("I expected this to succeed. Error: " + err.Error())
	}
	if w.Len() < 1 {
		t.Error("should have written to output")
	}
	if !strings.Contains(w.String(), `type="text"`) {
		t.Error(`should have added an input with type="text"`)
	}
	if !strings.Contains(w.String(), `textarea`) {
		t.Error(`should have added a textarea`)
	}
	if strings.Index(w.String(), "textarea") > strings.Index(w.String(), "input") {
		t.Error("Expect that the first argument passed will be output first, and the second will be output second, etc.")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}
}

func TestGenSelectInput(t *testing.T) {
	selectInput := SelectInput{}
	var w bytes.Buffer
	err := GenSelectInput(selectInput, &w)
	if err == nil {
		t.Error("Expected an error if selectInput is empty because you shouldn't call a function expecting it to do nothing")
	}

	w.Reset()
	selectInput = SelectInput{
		ID:            "klasdkasdksda",
		Label:         "DOB",
		SelectOptions: []SelectOption{},
		HelpText:      "some help text",
		MultiSelect:   false,
	}
	err = GenSelectInput(selectInput, &w)
	if err == nil {
		t.Error("Just because you have a value doesn't mean the input is valid")
	}
	if w.Len() > 0 {
		t.Error("shouldn't be generating output if there's an error")
	}

	w.Reset()
	selectInput.SelectOptions = []SelectOption{
		{Value: "option 1", ID: "testID"},
		{Value: "option 2", ID: "testID2"},
	}
	err = GenSelectInput(selectInput, &w)
	if err != nil {
		t.Error("this should be valid. err: " + err.Error())
	}
	if strings.Contains(w.String(), "multiple") {
		t.Error("should not have 'multiple' in output if .MultiSelect == false")
	}
	if !strings.Contains(w.String(), "option 1") {
		t.Error("label should be getting used")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}

	w.Reset()
	selectInput.MultiSelect = true
	err = GenSelectInput(selectInput, &w)
	if err != nil {
		t.Error("this should also be valid")
	}
	if !strings.Contains(w.String(), "multiple") {
		t.Error("Should see 'multiple' if multiselect = true")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Error("HTML was not valid")
	}

	w.Reset()
	selectInput.SelectOptions[0].Selected = true
	err = GenSelectInput(selectInput, &w)
	if err != nil {
		t.Errorf("should be able to select one by default")
	}
	if !strings.Contains(w.String(), "selected") {
		t.Error("shoudl see 'selected' in result")
	}
	_, err = html.Parse(&w)
	if err != nil {
		t.Errorf("HTML was not valid")
	}
}

// TestGenListLabeledInput merely makes sure there is no error thrown; it does not truly
// test the component.
func TestGenListLabeledInput(t *testing.T) {
	type args struct {
		labeledInput ListLabeledInput
	}
	tests := []struct {
		name       string
		args       args
		wantWriter string
		wantErr    bool
	}{
		{
			name: "empty",
			args: args{
				labeledInput: ListLabeledInput{},
			},
			wantWriter: "",
			wantErr:    true,
		},
		{
			name: "no initial values",
			args: args{
				labeledInput: ListLabeledInput{
					Label:         "l",
					ID:            "llI",
					InitialValues: nil,
					DefaultValue:  "",
					Type:          "text",
				},
			},
			wantErr: false,
		},
		{
			name: "2 initial values",
			args: args{
				labeledInput: ListLabeledInput{
					Label:         "l",
					ID:            "llI",
					InitialValues: []string{"initial value", "initial value 2"},
					DefaultValue:  "",
					Type:          "text",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenListLabeledInput(tt.args.labeledInput, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenListLabeledInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

// TestGenSearchLabeledInput merely makes sure there is no error thrown; it does not truly
// test the component.
func TestGenSearchLabeledInput(t *testing.T) {
	type args struct {
		searchLabeledInput SearchLabeledInput
	}
	tests := []struct {
		name       string
		args       args
		wantWriter string
		wantErr    bool
	}{
		{
			name: "empty",
			args: args{
				searchLabeledInput: SearchLabeledInput{},
			},
			wantErr: true,
		},
		{
			name: "no initial value",
			args: args{
				searchLabeledInput: SearchLabeledInput{
					Label: "l",
					ID:    "llI",
				},
			},
			wantErr: false,
		},
		{
			name: "an initial value",
			args: args{
				searchLabeledInput: SearchLabeledInput{
					Label:        "l",
					ID:           "llI",
					InitialValue: "initial value",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenSearchLabeledInput(tt.args.searchLabeledInput, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenSearchLabeledInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

// TestGenSearchLabeledInputResults merely makes sure there is no error thrown; it does not truly
// test the component.
func TestGenSearchLabeledInputResults(t *testing.T) {
	type args struct {
		searchLabeledInputResult SearchLabeledInputResult
	}
	tests := []struct {
		name       string
		args       args
		wantWriter string
		wantErr    bool
	}{
		{
			name: "empty",
			args: args{
				searchLabeledInputResult: SearchLabeledInputResult{},
			},
			wantErr: true,
		},
		{
			name: "no initial value",
			args: args{
				searchLabeledInputResult: SearchLabeledInputResult{
					Results:              []string{"hello"},
					SearchLabeledInputID: "something",
				},
			},
			wantErr: false,
		},
		{
			name: "an initial value",
			args: args{
				searchLabeledInputResult: SearchLabeledInputResult{
					Results:              []string{"one", "two", "three"},
					SearchLabeledInputID: "something",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenSearchLabeledInputResults(tt.args.searchLabeledInputResult, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenSearchLabeledInputResults() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestGenGiantOptionSelect(t *testing.T) {
	type args struct {
		input GiantOptionSelect
	}
	tests := []struct {
		name       string
		args       args
		wantWriter string
		wantErr    bool
	}{
		{
			name: "empty",
			args: args{
				input: GiantOptionSelect{},
			},
			wantErr: true,
		},
		{
			name: "no initial value",
			args: args{
				input: GiantOptionSelect{
					WrapperID: "aWrapper",
					Name:      "a",
					Label:     "a",
					SelectOptions: []SelectOption{{
						Selected: false,
						Value:    "one",
					}},
				},
			},
			wantErr: false,
		},
		{
			name: "an initial value",
			args: args{
				input: GiantOptionSelect{
					WrapperID: "aWrapper",
					Name:      "a",
					Label:     "a",
					SelectOptions: []SelectOption{{
						Selected: true,
						Value:    "one",
					}},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenGiantOptionSelect(tt.args.input, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenGiantOptionSelect() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
