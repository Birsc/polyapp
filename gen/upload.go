package gen

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html/template"
	"io"
	"net/url"
)

// Upload is for an arbitrary upload component which accepts all types of data.
type Upload struct {
	ID       string
	Label    string
	HelpText string
}

// Validate returns an error if not all required inputs are populated.
func (a Upload) Validate() error {
	if a.ID == "" {
		return errors.New("ID not populated")
	}
	return nil
}

// GenUpload is for an arbitrary upload component which accepts all types of data.
func GenUpload(u Upload, w io.Writer) error {
	err := u.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	randName := "a" + common.GetRandString(15)
	u.ID = url.PathEscape(u.ID)
	audioUploadTemplate := `{{define "AudioUpload"}}
{{if .Label}}<div>
	<p class="d-inline">{{.Label}}</p>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
</div>{{end}}
<div class="d-flex">
  <div class="custom-file mb-3 d-block" style="max-width: 600px;">
    <input type="file" class="custom-file-input pointer" id="{{.ID}}" name="` + randName + `" />
    <label class="custom-file-label" for="` + randName + `">Choose file</label>
  </div>
  <div>
    <button class="btn btn-sm btn-link mx-4" style="padding: .50rem .75rem;border: 1px solid;">
      <a href="/blob/assets/polyappShouldBeOverwritten/{{.ID}}">View Files</a>
    </button>
  </div>
</div>
{{- end}}`
	t, err := template.New("AudioUpload").Parse(audioUploadTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, u)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
