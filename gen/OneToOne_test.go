package gen

import (
	"bytes"
	"testing"
)

// TestGenOneToOneFromTo merely verifies that the function finishes successfully.
func TestGenOneToOneFromTo(t *testing.T) {
	type args struct {
		inputs []OneToOneFromTo
	}
	tests := []struct {
		name       string
		args       args
		wantWriter string
		wantErr    bool
	}{
		{
			name: "nil",
			args: args{
				nil,
			},
			wantWriter: "",
			wantErr:    true,
		},
		{
			name: "single valid",
			args: args{
				[]OneToOneFromTo{
					{
						WrapperID:        "wrap",
						FromID:           "a",
						FromLabel:        "a label",
						FromInitialValue: "a",
						FromType:         "text",
						ToID:             "b",
						ToLabel:          "b label",
						ToInitialValue:   "b",
						ToType:           "text",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "double valid",
			args: args{
				[]OneToOneFromTo{
					{
						WrapperID:        "wrap",
						FromID:           "a",
						FromLabel:        "a label",
						FromInitialValue: "55",
						FromType:         "number",
						ToID:             "b",
						ToLabel:          "b label",
						ToInitialValue:   "true",
						ToType:           "checkbox",
					},
					{
						WrapperID:        "wrap2",
						FromID:           "a2",
						FromLabel:        "a label",
						FromInitialValue: "a",
						FromType:         "text",
						ToID:             "b2",
						ToLabel:          "b label",
						ToInitialValue:   "b",
						ToType:           "textarea",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenOneToOneFromTo(tt.args.inputs, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenOneToOneFromTo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
