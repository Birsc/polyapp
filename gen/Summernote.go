package gen

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html"
	"html/template"
	"io"
	"net/url"
	textTemplate "text/template"
)

type Summernote struct {
	ID string
}

func (summernote *Summernote) Validate() error {
	if summernote.ID == "" {
		return errors.New("ID was empty")
	}
	return nil
}

// GenSummernote generates a full-fledged text editor called Summernote.
func GenSummernote(summernote Summernote, w io.Writer) error {
	err := summernote.Validate()
	if err != nil {
		return fmt.Errorf("Failed validation: %w", err)
	}
	summernote.ID = url.PathEscape(summernote.ID)
	// Note: This template uses polyappSummernotePlaceholder as a way to avoid the UI moving when Summernote initializes post-load.
	SummernoteTemplate := `{{define "SummernoteTemplate"}}
	<div id="{{.ID}}" class="polyappJSsummernote polyappSummernotePlaceholder"></div>
{{- end}}
`
	t, err := template.New("SummernoteTemplate").Parse(SummernoteTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, summernote)
	if err != nil {
		return err
	}
	return nil
}

type DiscussionThreadComment struct {
	Name     string // Name of the person making the comment
	DateTime string // DateTime when the comment was posted
	Comment  string // HTML string which is the comment's body.
}

type DiscussionThread struct {
	Label           string
	ID              string
	InitialComments []DiscussionThreadComment
	Readonly        bool
}

func (d DiscussionThread) Validate() error {
	if d.ID == "" {
		return errors.New("ID was empty")
	}
	return nil
}

// GenDiscussionThread is an []string control where only the last part of the array is editable. After clicking the "Done"
// button the current time and date and the user's name are affixed to the output and the text is rendered as HTML.
//
// Summernote is used to edit the posts.
func GenDiscussionThread(d DiscussionThread, w io.Writer) error {
	err := d.Validate()
	if err != nil {
		return fmt.Errorf("Failed validation: %w", err)
	}
	d.ID = url.PathEscape(d.ID)
	d.Label = html.EscapeString(d.Label)
	buttonID := common.GetRandString(15)
	// Note: This template uses polyappSummernotePlaceholder as a way to avoid the UI moving when Summernote initializes post-load.
	SummernoteTemplate := `{{define "DiscussionThread"}}
<h5 class="py-2">{{.Label}}</h5>
<div id="{{.ID}}" data-polyappjs-discussion-thread="" {{if .Readonly}}readonly{{end}}>
{{range .InitialComments}}
<div class="p-2">
	<div class="card">
		<div class="card-body">{{.Comment}}</div>
		<div class="card-footer text-muted">
			<p class="d-inline-block m-0">{{.Name}}</p>
			<p class="d-inline-block m-0">commented on</p>
			<p class="d-inline-block m-0">{{.DateTime}}</p>
		</div>
	</div>
</div>
{{end}}
{{if eq .Readonly false}}
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#` + buttonID + `">
	Add Comment
</button>
<div class="modal" tabindex="-1" id="` + buttonID + `">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">New Comment</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="polyappJSsummernote polyappSummernotePlaceholder"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary polyappjs-discussion-thread-add-comment" data-dismiss="modal">Save and Close</button>
			</div>
		</div>
	</div>
</div>
{{end}}
</div>
{{- end}}
`
	t, err := textTemplate.New("DiscussionThread").Parse(SummernoteTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, d)
	if err != nil {
		return err
	}
	return nil
}
