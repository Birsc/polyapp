package elasticsearchCRUD

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/integrity"

	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

type Query struct {
	// equals is added to if AddEquals is called
	equals map[string]string
	// arrayContains is added to if AddArrayContains is called
	arrayContains map[string]string
	index         string
}

// Validate returns an error if the query is dangerous.
func (q *Query) Validate() error {
	for k := range q.equals {
		err := integrity.ValidateElasticsearchKey(k)
		if err != nil {
			return fmt.Errorf("ValidateElasticsearchKey in q.equals ("+k+"): %w", err)
		}
	}
	for k := range q.arrayContains {
		err := integrity.ValidateElasticsearchKey(k)
		if err != nil {
			return fmt.Errorf("ValidateElasticsearchKey in q.arrayContains ("+k+"): %w", err)
		}
	}
	return nil
}

// AddArrayContains is unimplemented.
func (q *Query) AddArrayContains(k string, v string) error {
	panic("implement me")
}

// Init this query. This should be called first. collectionID parameter is optional & SHOULD be used for collection Data.
func (q *Query) Init(industryID string, domainID string, collectionID string, collection string) {
	q.equals = make(map[string]string)
	q.arrayContains = make(map[string]string)
	q.AddEquals(common.PolyappIndustryID, industryID)
	q.AddEquals(common.PolyappDomainID, domainID)
	q.AddEquals(common.PolyappSchemaID, collectionID)
	q.index = collection
}

// AddEquals searches for any field name 'k' which includes all of the input values.
func (q *Query) AddEquals(k string, v string) {
	q.equals[k] = v
}

// QueryRead performs the created query and returns results. This is a Term query. Because Firestore already gives us
// (better) access to regular Querying, the QueryRead() function here tries fuzzy matching.
//
// While iterating through the results I highly recommend casting each result to the appropriate struct, like *common.Data.
func (q *Query) QueryRead() (common.Iter, error) {
	err := q.Validate()
	if err != nil {
		return &ESIter{}, fmt.Errorf("Validate: %w", err)
	}
	client, err := GetClient()
	if err != nil {
		return nil, fmt.Errorf("GetClient: %w", err)
	}
	var compoundMatch map[string]interface{}
	if len(q.equals) > 0 {
		compoundMatch = map[string]interface{}{
			"query": map[string]interface{}{
				"bool": map[string]interface{}{
					"must": make([]interface{}, len(q.equals)),
				},
			},
		}
	}
	i := 0
	for k, v := range q.equals {
		compoundMatch["query"].(map[string]interface{})["bool"].(map[string]interface{})["must"].([]interface{})[i] = map[string]interface{}{
			"match": map[string]interface{}{
				k: map[string]interface{}{
					"query":     v,
					"fuzziness": "AUTO",
					"operator":  "AND",
				},
			},
		}
		i++
	}
	docMap, docID, score, err := query(client, compoundMatch, q.index)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	iter := &ESIter{
		i:          0,
		collection: q.index,
		docMaps:    docMap,
		docIDs:     docID,
		scores:     score,
	}
	return iter, nil
}

// ESIter implements the common.Iter interface.
type ESIter struct {
	i          int
	collection string
	docMaps    []map[string]interface{}
	docIDs     []string
	scores     []float64
}

// Next returns the next search result or common.IterDone.
func (E *ESIter) Next() (common.Queryable, error) {
	if E.i >= len(E.docMaps) {
		return nil, common.IterDone
	}
	var out common.Queryable
	switch E.collection {
	case common.CollectionData:
		out = &common.Data{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("data collection init failure: %w", err)
		}
	case common.CollectionSchema:
		out = &common.Schema{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("schema collection init failure: %w", err)
		}
	case common.CollectionRole:
		out = &common.Role{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("role collection init failure: %w", err)
		}
	case common.CollectionUser:
		out = &common.User{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("user collection init failure: %w", err)
		}
	case common.CollectionTask:
		out = &common.Task{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("task collection init failure: %w", err)
		}
	case common.CollectionUX:
		out = &common.UX{
			FirestoreID: E.docIDs[E.i],
		}
		err := out.Init(E.docMaps[E.i])
		if err != nil {
			return nil, fmt.Errorf("ux collection init failure: %w", err)
		}

	}
	E.i++
	return out, nil
}

// Stop stops the iterator, freeing its resources.
func (E *ESIter) Stop() {
	E.i = 0
	E.collection = ""
	E.docIDs = nil
	E.docMaps = nil
	E.scores = nil
}

// Length of the results.
func (E *ESIter) Length() int {
	return len(E.docIDs)
}

// query runs the query and returns the results in a common format. It uses the Search API, which makes it synchronous.
func query(client *elasticsearch.Client, query map[string]interface{}, index string) (docMap []map[string]interface{}, docID []string, score []float64, err error) {
	var buf bytes.Buffer
	if err = json.NewEncoder(&buf).Encode(query); err != nil {
		err = errors.New("Error encoding query: " + err.Error())
		return
	}

	// Perform the search request.
	// This way of doing things lacks type safety, so in the future the struct construction + 'do' method will probably be used
	res, err := client.Search(
		client.Search.WithContext(context.Background()),
		client.Search.WithIndex(index),
		client.Search.WithBody(&buf),
	)
	if err != nil {
		err = errors.New("Error getting response: " + err.Error())
		return
	}
	defer res.Body.Close()

	var r map[string]interface{}
	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			err = errors.New("Error parsing the response body: " + err.Error())
			return docMap, docID, score, err
		}
		if strings.Contains(res.Status(), "403") {
			err = errors.New("Error in query response: " + res.Status() + "; set a breakpoint here for more info.")
			return
		}
		err = errors.New("Error in query response: " + res.Status())
		return
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		err = errors.New("Error parsing the response body: " + err.Error())
		return docMap, docID, score, err
	}
	// what a mess. This is from the docs: https://github.com/elastic/go-elasticsearch
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		// each hit is a document reference of sorts
		// docMap is all of the information in that document
		// whereas docID is the ID in elasticsearch
		docMap = append(docMap, hit.(map[string]interface{})["_source"].(map[string]interface{}))
		docID = append(docID, hit.(map[string]interface{})["_id"].(string))
		score = append(score, hit.(map[string]interface{})["_score"].(float64))
	}
	return
}

// rawSearchToQueryString converts a raw search string from the client to something we can query for
func rawSearchToQueryString(s string) string {
	s = common.RemoveNonAlphaNumeric(s)
	if s == "" {
		return ""
	}

	var sBuilder strings.Builder
	var err error
	first := true
	sBuilder.WriteString("(")
	for _, splitString := range strings.Split(s, " ") {
		if !first {
			_, err = sBuilder.WriteString(" OR ")
			if err != nil {
				return ""
			}
		}
		_, err = sBuilder.WriteString(splitString)
		if err != nil {
			return ""
		}
		first = false
	}
	sBuilder.WriteString(")")

	return sBuilder.String()
}
