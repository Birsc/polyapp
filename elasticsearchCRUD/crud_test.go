package elasticsearchCRUD

import (
	"reflect"
	"strings"
	"testing"

	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreate(t *testing.T) {
	type args struct {
		client         *elasticsearch.Client
		i              common.Queryable
		collectionName string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil client",
			args: args{
				client:         nil,
				i:              &common.User{},
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "nil i",
			args: args{
				client:         client,
				i:              nil,
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "bad collection name",
			args: args{
				client:         client,
				i:              &common.User{},
				collectionName: "blah",
			},
			wantErr: true,
		},
		{
			name: "invalid user",
			args: args{
				client:         client,
				i:              &common.User{},
				collectionName: "user",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Create(tt.args.client, tt.args.i, tt.args.collectionName); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
	err = Create(client, &common.User{
		FirestoreID:   "TestCreate",
		UID:           common.String("TestCreate"),
		FullName:      nil,
		PhotoURL:      nil,
		EmailVerified: nil,
		PhoneNumber:   nil,
		Email:         nil,
		Roles:         nil,
		Deprecated:    nil,
	}, "user")
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Error(err)
	}
}

func TestRead(t *testing.T) {
	type args struct {
		client         *elasticsearch.Client
		firestoreID    string
		collectionName string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "nil client",
			args: args{
				client:         nil,
				firestoreID:    "TestRead",
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "empty Firestore ID",
			args: args{
				client:         client,
				firestoreID:    "",
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "invalid collection name",
			args: args{
				client:         client,
				firestoreID:    "TestRead",
				collectionName: "useasdadsr",
			},
			wantErr: true,
		},
		{
			name: "simple success",
			args: args{
				client:         client,
				firestoreID:    "TestCreate",
				collectionName: "user",
			},
			wantErr: false,
			want: common.User{
				FirestoreID:   "TestCreate",
				UID:           common.String("TestCreate"),
				FullName:      nil,
				PhotoURL:      nil,
				EmailVerified: nil,
				PhoneNumber:   nil,
				Email:         nil,
				Roles:         nil,
				Deprecated:    nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := Read(tt.args.client, tt.args.firestoreID, tt.args.collectionName)
			if (err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDelete(t *testing.T) {
	type args struct {
		client         *elasticsearch.Client
		firestoreID    string
		collectionName string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil client",
			args: args{
				client:         nil,
				firestoreID:    "TestDelete",
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "empty Firestore ID",
			args: args{
				client:         client,
				firestoreID:    "",
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "invalid collection name",
			args: args{
				client:         client,
				firestoreID:    "TestDelete",
				collectionName: "useasdadsr",
			},
			wantErr: true,
		},
		{
			name: "simple success",
			args: args{
				client:         client,
				firestoreID:    "TestDelete",
				collectionName: "user",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Delete(tt.args.client, tt.args.firestoreID, tt.args.collectionName); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	type args struct {
		client         *elasticsearch.Client
		i              common.Queryable
		collectionName string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	err = Create(client, &common.User{
		FirestoreID:   "TestUpdate",
		UID:           common.String("TestUpdate"),
		FullName:      nil,
		PhotoURL:      nil,
		EmailVerified: nil,
		PhoneNumber:   nil,
		Email:         nil,
		Roles:         nil,
		Deprecated:    nil,
	}, "user")
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil client",
			args: args{
				client:         nil,
				i:              &common.User{},
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "nil Queryable",
			args: args{
				client:         client,
				i:              nil,
				collectionName: "user",
			},
			wantErr: true,
		},
		{
			name: "invalid collection name",
			args: args{
				client:         client,
				i:              &common.User{},
				collectionName: "useasdadsr",
			},
			wantErr: true,
		},
		{
			name: "simple success",
			args: args{
				client: client,
				i: &common.User{
					FirestoreID:   "TestUpdate",
					UID:           common.String("TestUpdate"),
					FullName:      common.String("TestUpdate"),
					PhotoURL:      nil,
					EmailVerified: nil,
					PhoneNumber:   nil,
					Email:         nil,
					Roles:         nil,
					Deprecated:    nil,
				},
				collectionName: "user",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Update(tt.args.client, tt.args.i, tt.args.collectionName); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
