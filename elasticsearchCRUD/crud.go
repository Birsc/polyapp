package elasticsearchCRUD

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/common"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/elastic/go-elasticsearch/v8"
)

// Create creates a new Queryable document from the given collectionName.
// A query is performed to ensure duplicate FirestoreIDs are not being entered.
func Create(client *elasticsearch.Client, i common.Queryable, collectionName string) error {
	if client == nil {
		return errors.New("client was nil in indexForUser")
	}
	if i == nil {
		return errors.New("user was nil in indexForUser")
	}
	err := common.ValidateCollection(collectionName)
	if err != nil {
		return fmt.Errorf("invalid collection name: %w", err)
	}
	err = i.Validate()
	if err != nil {
		return fmt.Errorf("user invalid: %w", err)
	}

	simpleUser, err := i.Simplify()
	if err != nil {
		return fmt.Errorf("user was not simplified: %w", err)
	}
	simpleUser[common.PolyappFirestoreID] = i.GetFirestoreID()
	for k := range simpleUser {
		switch simpleUser[k].(type) {
		case []byte:
			// don't store blobs in Elasticsearch
			delete(simpleUser, k)
		}
	}
	esID := common.GetRandString(50)

	// check for existing duplicate
	q := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				common.PolyappFirestoreID: map[string]interface{}{
					"query": i.GetFirestoreID(),
				},
			},
		},
	}

	docMap, _, _, err := query(client, q, collectionName)
	if err != nil {
		return fmt.Errorf("error executing query: %w", err)
	}
	if len(docMap) > 0 {
		return status.Error(codes.AlreadyExists, "Elasticsearch already contains a document with this FirestoreID")
	}

	docToIndex, err := json.Marshal(simpleUser)
	if err != nil {
		return fmt.Errorf("error marshalling simpleUser: %w", err)
	}
	_, err = index(client, esID, docToIndex, collectionName)
	return nil
}

// Read a document from the given collectionName. A query is performed to do this.
func Read(client *elasticsearch.Client, firestoreID string, collectionName string) (commonObject interface{}, docID string, err error) {
	if client == nil {
		return nil, "", errors.New("client was nil in indexForUser")
	}
	err = common.ValidateCollection(collectionName)
	if err != nil {
		return nil, "", fmt.Errorf("invalid collection name: %w", err)
	}
	if firestoreID == "" {
		return nil, "", errors.New("firestoreID not provided")
	}

	q := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				common.PolyappFirestoreID: map[string]interface{}{
					"query": firestoreID,
				},
			},
		},
	}

	var docMapAll []map[string]interface{}
	var docIDAll []string
	docMapAll, docIDAll, _, err = query(client, q, collectionName)
	if err != nil {
		return nil, "", fmt.Errorf("error executing query: %w", err)
	}
	if len(docMapAll) > 1 {
		// why return the docIDAll joined with periods? Sometimes we don't care that we found more than one document - like
		// when we are trying to delete a document at a particular Firestore ID.
		return nil, strings.Join(docIDAll, "."), errors.New("found more than one document")
	}
	if len(docMapAll) < 1 {
		return nil, "", status.Error(codes.NotFound, "FirestoreID not found in query")
	}
	docMap := docMapAll[0]
	docID = docIDAll[0]
	var outInterface interface{}
	switch collectionName {
	case common.CollectionUser:
		user := common.User{}
		err = user.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("error setting up the User: %w", err)
		}
		outInterface = user
	case common.CollectionRole:
		role := common.Role{}
		err = role.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("error setting up the Role: %w", err)
		}
		outInterface = role
	case common.CollectionTask:
		task := common.Task{}
		err = task.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("error setting up the Task: %w", err)
		}
		outInterface = task
	case common.CollectionSchema:
		schema := common.Schema{}
		err = schema.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("error setting up the Schema: %w", err)
		}
		outInterface = schema
	case common.CollectionUX:
		ux := common.UX{}
		err = ux.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("error setting up the UX: %w", err)
		}
		outInterface = ux
	case common.CollectionData:
		data := common.Data{}
		err = data.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("setting up Data: %w", err)
		}
		outInterface = data
	case common.CollectionBot:
		bot := common.Bot{}
		err = bot.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("setting up Bot: %w", err)
		}
		outInterface = bot
	case common.CollectionAction:
		action := common.Action{}
		err = action.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("setting up Action: %w", err)
		}
		outInterface = action
	case common.CollectionPublicMap:
		publicMap := common.PublicMap{}
		err = publicMap.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("setting up PublicMap: %w", err)
		}
		outInterface = publicMap
	case common.CollectionCSS:
		CSS := common.CSS{}
		err = CSS.Init(docMap)
		if err != nil {
			return nil, "", fmt.Errorf("setting up CSS: %w", err)
		}
		outInterface = CSS
	default:
		return nil, "", errors.New("unhandled collection")
	}
	return outInterface, docID, nil
}

// Update will deleteByQuery all documents at a specified FirestoreID via a query.
// It does NOT use the standard Merge method, so it's very possible the information in Elasticsearch
// will not be the same as the information in other databases if this function is used.
func Update(client *elasticsearch.Client, i common.Queryable, collectionName string) error {
	if client == nil {
		return errors.New("client was nil")
	}
	if i == nil {
		return errors.New("Queryable was nil")
	}
	err := common.ValidateCollection(collectionName)
	if err != nil {
		return fmt.Errorf("invalid collection name: %w", err)
	}

	_, esID, err := Read(client, i.GetFirestoreID(), collectionName)
	if err != nil {
		return fmt.Errorf("failed to figure out what document to update: %w", err)
	}
	// if Queryable had a Merge method on it, we'd use it here and then just call 'index' instead of using 'update'

	updateData, err := i.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	for k := range updateData {
		switch updateData[k].(type) {
		case []byte:
			// don't store blobs in Elasticsearch
			delete(updateData, k)
		}
	}
	_, err = update(client, esID, updateData, collectionName)
	if err != nil {
		return fmt.Errorf("failure to update: %w", err)
	}
	return nil
}

// Delete will deleteByQuery any provided FirestoreID via a query. If no document is found an error is NOT thrown.
func Delete(client *elasticsearch.Client, firestoreID string, collectionName string) error {
	if client == nil {
		return errors.New("client was nil in indexForUser")
	}
	err := common.ValidateCollection(collectionName)
	if err != nil {
		return fmt.Errorf("invalid collection name: %w", err)
	}
	if firestoreID == "" {
		return errors.New("firestoreID not provided")
	}

	_, esID, err := Read(client, firestoreID, collectionName)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		// already deleted
		return nil
	}
	if err != nil && strings.Contains(err.Error(), "found more than one document") {
		allIDsToDelete := strings.Split(esID, ".")
		for _, id := range allIDsToDelete {
			_, err = deleteES(client, id, collectionName)
			if err != nil {
				return fmt.Errorf("deleteES in allIDsToDelete (%v): %w", id, err)
			}
		}
	}
	if err != nil {
		return fmt.Errorf("failed to figure out what document to delete: %w", err)
	}

	_, err = deleteES(client, esID, collectionName)
	if err != nil {
		return fmt.Errorf("error executing query: %w", err)
	}
	return nil
}
