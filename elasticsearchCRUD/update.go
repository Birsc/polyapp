package elasticsearchCRUD

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

// update follows the Elasticsearch Update API. It will use Elasticsearch's algorithm for merging. It does not allow
// updating documents which do not exist.
func update(client *elasticsearch.Client, esID string, update map[string]interface{}, index string) (r map[string]interface{}, err error) {
	if client == nil || esID == "" || update == nil || index == "" || index != strings.ToLower(index) {
		return nil, errors.New("invalid inputs")
	}

	bodyUnmarshalled := map[string]interface{}{
		"doc": update,
	}
	var bodyBuf bytes.Buffer
	if err = json.NewEncoder(&bodyBuf).Encode(bodyUnmarshalled); err != nil {
		err = errors.New("Error encoding query: " + err.Error())
		return
	}

	// Set up the request object.
	req := esapi.UpdateRequest{
		Index:      index,
		DocumentID: esID,
		Body:       &bodyBuf,
		Refresh:    "true",
	}

	// Perform the request with the client.
	res, err := req.Do(context.Background(), client)
	if err != nil {
		return nil, errors.New("Error getting response: " + err.Error())
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, errors.New(res.String() + " Error indexing document ID=" + esID + " index: " + index)
	}
	// Deserialize the response into a map.
	if err = json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, fmt.Errorf("Error decoding JSON in index: %w", err)
	}

	return r, err
}
