package elasticsearchCRUD

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
)

var (
	clientCache *elasticsearch.Client
	clientMux   sync.Mutex
)

// GetClient gets a client.
// User Role documentation (since apparently there's nowhere else to put this?)
// apm_system Role to enable the Health Check test to run.
// monitoring_user because we may wish to monitor our cluster at runtime.
// Web_Server gives it access to the indices for this app and a test index, /test.
func GetClient() (*elasticsearch.Client, error) {
	clientMux.Lock()
	defer clientMux.Unlock()
	if clientCache != nil {
		return clientCache, nil
	}
	cfg := elasticsearch.Config{
		// I wanted to use the API key which would replace the username and password above.
		// First I tried copy-pasting APIKey from elasticsearch.
		// Then I tried using a base64 encoding of the API Key from elasticsearch & restarted Goland and it did not work.
		// Both times I received a 401 Unauthorized security_exception: missing authentication credentials for a REST request.
		// Then I tried encoding api_key:[key] as confusingly described here: https://www.elastic.co/guide/en/elasticsearch/reference/current/security-api-create-api-key.html
		// Then I tried encoding the actual ID of the key :[key] like polyapp-webserver-etc:[key]
		// Then I gave up and tried using a Transport hack which just adds the proper Authorization header (based on the last method described above).
		// Then the proper auth header also did not seem to work with the same error. At that point I gave up and created a new
		// user with a username and password which this access now flows through. The user you create should not be
		// your default user and it should have its access ability restricted as much as possible.
		Addresses: common.GetElasticsearchClientAddresses(),
		Username:  common.GetElasticsearchClientUsername(),
		Password:  common.GetElasticsearchClientPassword(),
		Transport: &http.Transport{
			MaxIdleConnsPerHost:   10,
			ResponseHeaderTimeout: time.Second,
			DialContext:           (&net.Dialer{Timeout: time.Second * 5}).DialContext,
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS11,
			},
		},
	}

	var err error
	clientCache, err = elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("Failed to create client: %v", err)
	}

	return clientCache, nil
}

// PrintResponseError prints out some error information from a es response
// Call this if res.IsError()
func PrintResponseError(res *esapi.Response) {
	var e map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
		common.LogError(fmt.Errorf("parsing the response body: %v", err))
	} else {
		// Print the response status and error information.
		common.LogError(fmt.Errorf("[%s] %s: %s",
			res.Status(),
			e["error"].(map[string]interface{})["type"],
			e["error"].(map[string]interface{})["reason"],
		))
	}
}
