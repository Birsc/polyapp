package elasticsearchCRUD

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"cloud.google.com/go/firestore"
	"github.com/elastic/go-elasticsearch/v8"

	"github.com/elastic/go-elasticsearch/v8/esapi"
)

// index adds a document to an index. Recommended: Call another function instead.
// Document IDs are auto-generated, so if you want to add to an existing document
// docID is the document ID, which must be common. data is the data in JSON format
// index is the index. Typically the index is key in firestore
// r is the deserialized response from elasticsearch
func index(client *elasticsearch.Client, docID string, data []byte, index string) (r map[string]interface{}, err error) {
	if client == nil || docID == "" || data == nil || index == "" || index != strings.ToLower(index) {
		return nil, errors.New("Invalid inputs - docID, index: " + docID + index)
	}

	// Set up the request object.
	req := esapi.IndexRequest{
		Index:      index,
		DocumentID: docID,
		Body:       bytes.NewReader(data),
		Refresh:    "true",
	}

	// Perform the request with the client.
	res, err := req.Do(context.Background(), client)
	if err != nil {
		return nil, errors.New("Error getting response: " + err.Error())
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, errors.New(res.String() + " Error indexing document ID=" + docID + " index: " + index)
	}
	// Deserialize the response into a map.
	if err = json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, fmt.Errorf("Error decoding JSON in index: %w", err)
	}

	return r, err
}

// ArrayOfRefsAsJSON converts and array of DocumentRefs into something which can be indexed in elasticsearch
func ArrayOfRefsAsJSON(refs []*firestore.DocumentRef) string {
	if len(refs) < 1 {
		return "[]"
	}
	var outBuilder strings.Builder
	outBuilder.WriteString("[")
	for i, k := range refs {
		if i > 0 {
			outBuilder.WriteString(", ")
		}
		outBuilder.WriteString("\"")
		outBuilder.WriteString(k.ID)
		outBuilder.WriteString("\"")
	}
	outBuilder.WriteString("]")
	return outBuilder.String()
}

// converts the array of keywords to JSON
func keywordsAsJSON(keywords []string) string {
	if len(keywords) < 1 {
		return "[]"
	}
	var outBuilder strings.Builder
	outBuilder.WriteString("[")
	for i, k := range keywords {
		if i > 0 {
			outBuilder.WriteString(", ")
		}
		outBuilder.WriteString("\"")
		outBuilder.WriteString(k)
		outBuilder.WriteString("\"")
	}
	outBuilder.WriteString("]")
	return outBuilder.String()
}
