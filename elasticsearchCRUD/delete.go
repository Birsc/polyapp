package elasticsearchCRUD

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/elastic/go-elasticsearch/v8"
)

// deleteES deletes the specified esID and is named such to prevent a naming collision with the built-in func delete.
// It is not this function's responsibility to make sure the query safe and effective.
// A previous version of this function used DeleteByQuery, but since queries return before completion that was scrapped.
func deleteES(client *elasticsearch.Client, esID string, index string) (response map[string]interface{}, err error) {
	if client == nil || esID == "" || index == "" {
		return nil, errors.New("not all inputs were properly populated")
	}

	res, err := client.Delete(index, esID, client.Delete.WithRefresh("true"))
	if err != nil {
		err = errors.New("Error getting response: " + err.Error())
		return
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, errors.New(res.String() + " Error deleting documents in index: " + index)
	}
	// Deserialize the response into a map.
	if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
		return nil, fmt.Errorf("Error decoding JSON in index: %w", err)
	}

	return response, err
}

func deleteByQuery(client *elasticsearch.Client, query map[string]interface{}, index string) (response map[string]interface{}, err error) {
	if client == nil || query == nil || index == "" {
		return nil, errors.New("not all inputs were properly populated")
	}

	var buf bytes.Buffer
	if err = json.NewEncoder(&buf).Encode(query); err != nil {
		err = errors.New("Error encoding query: " + err.Error())
		return
	}

	res, err := client.DeleteByQuery([]string{index}, &buf,
		client.DeleteByQuery.WithMaxDocs(1),
		client.DeleteByQuery.WithConflicts("proceed"), // allows deletion if the document does not exist (tested)
	)
	if err != nil {
		err = errors.New("Error getting response: " + err.Error())
		return
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, errors.New(res.String() + " Error deleting documents in index: " + index)
	}
	// Deserialize the response into a map.
	if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
		return nil, fmt.Errorf("Error decoding JSON in index: %w", err)
	}

	return response, err
}
