package elasticsearchCRUD

import (
	"strings"
	"testing"
)

func TestGetClient(t *testing.T) {
	client, err := GetClient()
	if client == nil || err != nil {
		t.Errorf("Error seen: %s", err.Error())
	}

	res, err := client.Info()
	if err != nil {
		t.Fatalf("error seen: %s", err.Error())
	}
	if res == nil {
		t.Fatalf("err no response")
	}
	if res.IsError() {
		PrintResponseError(res)
		t.Error("response had an error. Response: " + res.String())
	}
}

func TestGetHealth(t *testing.T) {
	client, err := GetClient()
	if client == nil || err != nil {
		t.Errorf("Error seen when getting the client: %s", err.Error())
	}

	res, err := client.Cat.Health(
		client.Cat.Health.WithV(true),
	)
	if err != nil {
		t.Fatalf("Error when getting health: %s", err.Error())
	}
	if res.IsError() {
		PrintResponseError(res)
		t.Error("Error when getting response")
	}
	// pretty stupid, but why make it complicated?
	if !strings.Contains(res.String(), "green") && !strings.Contains(res.String(), "yellow") {
		t.Error("Node status may be something other than green. Please investigate the debug output from this test")
	}
}
