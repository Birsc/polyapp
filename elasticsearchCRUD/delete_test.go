package elasticsearchCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"

	"github.com/elastic/go-elasticsearch/v8"
)

func Test_deleteByQuery(t *testing.T) {
	type args struct {
		client *elasticsearch.Client
		query  map[string]interface{}
		index  string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	q := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				common.PolyappFirestoreID: map[string]interface{}{
					"query": "Test_deleteByQuery",
				},
			},
		},
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil",
			args: args{
				client: nil,
				query:  nil,
				index:  "user",
			},
			wantErr: true,
		},
		{
			name: "nil query",
			args: args{
				client: client,
				query:  nil,
				index:  "user",
			},
			wantErr: true,
		},
		{
			name: "empty index",
			args: args{
				client: client,
				query:  q,
				index:  "",
			},
			wantErr: true,
		},
		{
			name: "trying to deleteByQuery something which does not exist",
			args: args{
				client: client,
				query:  q,
				index:  "user",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := deleteByQuery(tt.args.client, tt.args.query, tt.args.index)
			if (err != nil) != tt.wantErr {
				t.Errorf("deleteByQuery() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
