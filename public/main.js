// ----------------- IMPORTANT! CONFIGURATION INFORMATION IS BELOW! ----------------
let firebaseConfigDev = {
    apiKey: "AIzaSyBSpSv7JdvWAle9sPwTNT8zItaVmtiiEQ4",
    authDomain: "high-sunlight-232214.firebaseapp.com",
    databaseURL: "https://high-sunlight-232214.firebaseio.com",
    projectId: "high-sunlight-232214",
    storageBucket: "high-sunlight-232214.appspot.com",
    messagingSenderId: "897117818320",
    appId: "1:897117818320:web:e692cd28de93610465fdd5"
};
let firebaseConfigStaging = {
    apiKey: "AIzaSyBHRyj9r5ocjbenymPYy8n_NJpMLCX64Pc",
    authDomain: "polyapp-staging.firebaseapp.com",
    databaseURL: "https://polyapp-staging.firebaseio.com",
    projectId: "polyapp-staging",
    storageBucket: "polyapp-staging.appspot.com",
    messagingSenderId: "126309489837",
    appId: "1:126309489837:web:5f6946b0a2799b92567a24"
};
let firebaseConfigProd = {
    apiKey: "AIzaSyAkZytrZ1nwEU3qT4vTOlY1YNlpLgnU88w",
    authDomain: "polyapp-production.firebaseapp.com",
    databaseURL: "https://polyapp-production.firebaseio.com",
    projectId: "polyapp-production",
    storageBucket: "polyapp-production.appspot.com",
    messagingSenderId: "600835525955",
    appId: "1:600835525955:web:e0a1b9fb4dbeb9433ef3f6"
};
// --------------------------- END CONFIGURATION -----------------------------------

// TODO consider using some sort of 'universal polyfill' like this: https://github.com/Financial-Times/polyfill-service
// the page I saw suggested using this: <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
// TODO redirect console messages into some sort of debugging service

// Justification: This is needed to start the service worker
var swCheckedForUpdates = false;
if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js').then(function(registration) {
            // Registration was successful
            // if (!swCheckedForUpdates) {
            //     // this is not really necessary because the browser will check for updates automatically.
            //     // However, since it only does so once every 24 hours and I test a lot more than that, let's force an update.
            //     swCheckedForUpdates = true;
            //     registration.update();
            // }
        }, function(err) {
            // registration failed :(
            console.error('ServiceWorker registration failed: ', err);
        });
    });
}

// All outgoing messages match the formats described in serviceWorkerAPIs.go
// Listen to messages from service workers.
// In diagrams of 'AJAX architectures' this is the AJAX engine's other function.
// Justification: This should be in JS because it interacts heavily with browser APIs.
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.addEventListener('message', function(event) {
        handleResponse(event.data);
    });
}

// handleResponse is for handling POST request responses. Its input is the body of the POST response.
// This function reads the response and applies its orders to the DOM.
// Justification: This should be in JS because its interacts heavily with the DOM.
function handleResponse(body) {
    if (body == null) {
        console.error('message was null');
        return;
    }
    if (body.MessageID === "") {
        console.error('no messageID');
        return;
    }

    // using 'truthy' comparison for body.PathName
    // because server can send down '', which means null in Go but not in JS.
    if (body.NewURL && body.NewURL !== location.href && body.NewURL.includes('blob/assets/downloads/')) {
        // Downloads should not be navigated to but we do need to provide auth information for the request to work.
        // fileName assumes the URL does not end in '/' & is the file name, like xyz.xml.
        let fileName = body.NewURL.substring(body.NewURL.lastIndexOf('/') + 1);
        let csrf = document.cookie.replace(/(?:(?:^|.*;\s*)_csrf\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        if (csrf == null) {
            console.error('csrf token not set');
            return;
        }
        let authorizationHeader = 'Bearer ';
        let currentUser = firebase.auth().currentUser;
        if (currentUser == null) {
            alert('Authentication has temporarily failed. Please refresh this page.');
            return;
        }
        currentUser.getIdToken(false).then((token) => {
            authorizationHeader += token;
            fetch(body.NewURL, {
                method: 'GET',
                credentials: 'same-origin',
                headers: {
                    'X-CSRF-TOKEN': csrf,
                    'Content-Type': 'application/json',
                    'Authorization': authorizationHeader
                }
            }).then(function (r) {
                return r.blob().then((b) => {
                    let link = document.createElement('a');
                    link.setAttribute('download', fileName);
                    link.href = URL.createObjectURL(b);
                    link.click();
                });
            });
        });
    }
    else if (body.NewURL && body.NewURL !== location.href) {
        if (!body.NewURL.startsWith('https://')) {
            body.NewURL = 'https://' + body.NewURL;
        }
        if (!body.NewURL.startsWith('https://' + location.host)) {
            // https:// length = 8
            body.NewURL = 'https://' + location.host + body.NewURL.substring(8);
        }
        DoPOSTAtUnload = false;
        location.href = body.NewURL;
        return;
    }
    if (body.BrowserActions != null && Array.isArray(body.BrowserActions)) {
        for (let i=0;i<body.BrowserActions.length;i++) {
            switch (body.BrowserActions[i].Name) {
                case "Open New Tab":
                    if (body.BrowserActions[i].Data != null && body.BrowserActions[i].Data !== "") {
                        window.open(body.BrowserActions[i].Data, '_blank');
                    }
            }
        }
    }

    if (body.ModDOMs == null) {
        // ModDOMs is empty - OK in most circumstances.
        return;
    }

    // do the work of modifying the DOM based on the received message
    // keep this code in sync with 'innerHTML' in handle.go where it's ranging over modDOMs
    body.ModDOMs.forEach((AjaxDOM) => {
        if (AjaxDOM.DeleteSelector !== "") {
            let oldEl = document.querySelectorAll(AjaxDOM.DeleteSelector);
            if (oldEl != null) {
                for (let i = 0; i<oldEl.length; i++) {
                    // Note: If you are interested in transitioning elements in/out
                    // you will need to use the 'removing' and 'adding' classes in index.css
                    // Right now I'm only using the 'removing' class.
                    if (oldEl[i].classList.contains('removing')) {
                        continue;
                    }
                    oldEl[i].classList.add('removing');
                }
                for (let i = 0; i < oldEl.length; i++) {
                    oldEl[i].remove();
                }
            }
        }
        if (AjaxDOM.InsertSelector !== "" && AjaxDOM.HTML !== "" && AjaxDOM.Action !== "") {
            let el = document.querySelector(AjaxDOM.InsertSelector);
            if (el != null) {
                el.insertAdjacentHTML(AjaxDOM.Action, AjaxDOM.HTML);
                if (AjaxDOM.InsertSelector === '#polyappJSToastContainer') {
                    $('.toast').toast('show');
                    // we will try to re-save momentarily.
                    setTimeout(transmitUpdateNow, 3000);
                } else {
                    // only add event listeners again if the request response was not an error
                    addAllEventListeners(el.parentNode); // afterend would need the parentNode
                }
            }
        }
        if (AjaxDOM.AddClassSelector !== "" && AjaxDOM.AddClasses != null && AjaxDOM.AddClasses.length != null && AjaxDOM.AddClasses.length > 0) {
            let els = document.querySelectorAll(AjaxDOM.AddClassSelector);
            if (els != null) {
                for (let i=0;i<els.length;i++) {
                    els[i].classList.add(AjaxDOM.AddClasses);
                }
            }
        }
        if (AjaxDOM.RemoveClassSelector !== "" && AjaxDOM.RemoveClasses != null && AjaxDOM.RemoveClasses.length != null && AjaxDOM.RemoveClasses.length > 0) {
            let els = document.querySelectorAll(AjaxDOM.RemoveClassSelector);
            if (els != null) {
                for (let i=0;i<els.length;i++) {
                    els[i].classList.remove(AjaxDOM.RemoveClasses);
                }
            }
        }
    });
}

// windowResize sends a message to a service worker which will ultimately handle the event for us.
// In diagrams of 'AJAX architectures' this is the 'AJAX engine'. Funny story: it's an engine with only 2 functions.
// Justification: if our web designs are done correctly they should be responsive without
// leaving the main thread. That means we need JS to respond to show/hide content.
var windowResize = debounce(windowResizeNow, 250);

function windowResizeNow(event) {
    if (event.type === 'resize' || event.type === 'DOMContentLoaded') {
        // handle resize events without going to the service worker
        let main = document.getElementById('polyappJSMain');
        let sidebar = document.getElementById('polyappJSSidebar');
        let expandSidebarFooter = document.getElementById('polyappJSExpandSidebarFooter');
        let hideSidebarFooter = document.getElementById('polyappJSHideSidebarFooter');
        if (main == null || sidebar == null || expandSidebarFooter == null || hideSidebarFooter == null) {
            return;
        }
        else if (window.innerWidth <= 700) {
            switch (main.getAttribute('class')) {
                case 'main': // Old: big screen w/ sidebar. New: small screen w/ hidden sidebar.
                case 'mainHiddenSidebar': // Old: sidebar contracted, wide screen. New: small screen w/ hidden sidebar.
                    main.setAttribute('class', 'mainSmallHiddenSidebar');
                    sidebar.setAttribute('class', 'sidebarSmallHiddenSidebar');
                    // messing with the display property did not work despite several attempts
                    expandSidebarFooter.hidden = false;
                    hideSidebarFooter.hidden = true;
                    break;
                case 'mainSmallSidebarExpanded': // Old: sidebar expanded, small screen. New: same.
                case 'mainSmallHiddenSidebar': // Old: sidebar hidden, small screen. New: same.
                    break;
            }
        } else {
            switch (main.getAttribute('class')) {
                case 'main': // Old: big screen w/ sidebar. New: same.
                case 'mainHiddenSidebar': // Old: sidebar contracted, wide screen. New: same.
                    break;
                case 'mainSmallSidebarExpanded': // Old: sidebar expanded, small screen. New: Sidebar expanded, big screen.
                case 'mainSmallHiddenSidebar': // Old: sidebar hidden, small screen. New: Sidebar expanded, big screen.
                    main.setAttribute('class', 'main');
                    sidebar.setAttribute('class', 'sidebar');
                    expandSidebarFooter.hidden = true;
                    hideSidebarFooter.hidden = false;
                    break;
            }
        }
    }
}
// example of using windowResize:
// document.querySelector('#someID').addEventListener('input', windowResize);

// sidebarToggle is UI JS - it's expanding or collapsing the sidebar. Although the HTML of the sidebar
// could be reloaded every time you click it the expansion / contraction is also based on window size, which makes this
// some code we want to have in JS and not WASM.
// Justification: same as windowResize
function sidebarToggle(event) {
    if (event.type !== 'input' || (event.target.id !== 'polyappJSExpandSidebarToggle' && event.target.id !== 'polyappJSHideSidebarToggle')) {
        // should only call this with elements with id matching the sidebar
        return;
    }

    if (event.target.id === 'polyappJSExpandSidebarToggle') {
        // trying to expand the sidebar
        let main = document.getElementById('polyappJSMain');
        let sidebar = document.getElementById('polyappJSSidebar');
        let expandSidebarFooter = document.getElementById('polyappJSExpandSidebarFooter');
        let hideSidebarFooter = document.getElementById('polyappJSHideSidebarFooter');
        if (window.innerWidth <= 700) {
            main.setAttribute('class', 'mainSmallSidebarExpanded');
            sidebar.setAttribute('class', 'sidebarSmallSidebarExpanded');
            expandSidebarFooter.hidden = true;
            hideSidebarFooter.hidden = false;
        } else {
            main.setAttribute('class', 'main');
            sidebar.setAttribute('class', 'sidebar');
            expandSidebarFooter.hidden = true;
            hideSidebarFooter.hidden = false;
        }
    }
    else if (event.target.id === 'polyappJSHideSidebarToggle') {
        // trying to hide the sidebar
        let main = document.getElementById('polyappJSMain');
        let sidebar = document.getElementById('polyappJSSidebar');
        let expandSidebarFooter = document.getElementById('polyappJSExpandSidebarFooter');
        let hideSidebarFooter = document.getElementById('polyappJSHideSidebarFooter');
        if (window.innerWidth <= 700) {
            main.setAttribute('class', 'mainSmallHiddenSidebar');
            sidebar.setAttribute('class', 'sidebarSmallHiddenSidebar');
            expandSidebarFooter.hidden = false;
            hideSidebarFooter.hidden = true;
        } else {
            main.setAttribute('class', 'mainHiddenSidebar');
            sidebar.setAttribute('class', 'displayNone');
            expandSidebarFooter.hidden = false;
            hideSidebarFooter.hidden = true;
        }
    }
}

// Add all window event listeners
// Justification: obviously this must be in JS.
window.addEventListener('DOMContentLoaded', windowResizeNow); // before paint
window.addEventListener('load', listenToLoad); // after paint
window.addEventListener('resize', windowResize);

// listenToLoad is called when the window 'loads'.
// Justification: heavily interacts with the DOM and runs a lot so it's not that inefficient.
function listenToLoad(event) {
    let headElement = document.getElementsByTagName('body')[0];
    addAllEventListeners(headElement);
    // It's possible the browser has populated some fields, like when you click the back button. We may not have the
    // data for those fields set on the server yet. Therefore whenever the page loads we should also trigger events
    // those listeners are listening for, if those events could potentially have data we haven't saved yet.
    //
    // The browser is probably not populating fields in nested accordions (hidden fields) so we can ignore those (mostly).
    let triggerEventsParent = document.getElementById('polyappJSMain').firstChild;
    while (triggerEventsParent != null) {
        if (triggerEventsParent.id == null) {
            // happens with text nodes
        } else if (triggerEventsParent.id.startsWith('_')) {
            // Accordion Lists go here (ARefs).
            // We want to trigger the first child under the first child under all IDs which start with polyappRef.
            // Those elements, like "Name", need to populate the value in the accordion's header.
            let polyappRefElements = triggerEventsParent.querySelectorAll('[id^=polyappRef]');
            for (let rI=0;rI<polyappRefElements.length;rI++) {
                triggerEvents(polyappRefElements[rI].firstElementChild.firstElementChild, ['input', 'textarea'], 'input');
            }
        } else {
            triggerEvents(triggerEventsParent, ['input', 'textarea'], 'input');
        }
        triggerEventsParent = triggerEventsParent.nextSibling;
    }
    // Bug note: Adding in 'change' for 'select' here causes problems. Essentially, if bootstrap-select has not yet initialized
    // (as in the case of SELECT within Accordions) and the change event is triggered, the 'value property' has not been set from the
    // 'value attribute' yet (which we do manually when boostrap-select is initialized). Consequently in the first POST after
    // initialization, the 'value' sent up is always ''. It's possible to fix this, but it would require us to have
    // some handling in triggerEvents which somehow recognizes that 'select' may or may not need to push the value from the
    // 'value attribute' into the 'value property', depending on if the control has initialized yet.
    // This problem only started when I started to delay bootstrap-select initialization, which was to fix a visual Bug.

    // TopBar listeners
    let signInEl = document.getElementById('polyappJSSignIn');
    if (signInEl != null) {
        signInEl.addEventListener('click', function() {
            location.href = '/signIn';
        });
    }
    let signOutEl = document.getElementById('polyappJSSignOut');
    if (signOutEl != null) {
        signOutEl.addEventListener('click', signOut);
    }

    // this block of code attempts to add the listeners. It is duplicated elsewhere.
    let e = document.getElementById('polyappJSExpandSidebarToggle');
    if (e != null) {
        e.addEventListener('input', sidebarToggle);
    }
    let h = document.getElementById('polyappJSHideSidebarToggle');
    if (h != null) {
        h.addEventListener('input', sidebarToggle);
    }

    let errorMessage = document.getElementById('polyappJSServerErrorMessage')
    if (errorMessage != null) {
        console.error(errorMessage);
        setTimeout(() => {
            window.location.reload();
        }, 1000);
    }
}

var lastDocumentID = '';
let visibleSelectObserver = new IntersectionObserver(function(entries, observer) {
    entries.forEach(entry => {
        selectPickerLoad(entry.target);
    });
});
let visibleDateTimeObserver = new IntersectionObserver(function(entries, observer) {
    entries.forEach(entry => {
        if ($(entry.target).is(':visible') && !$(entry.target).attr('polyappDateTimeInitialized')) {
            initDateTime($(entry.target));
        }
    });
});
let visibleSummernoteObserver = new IntersectionObserver(function(entries, observer) {
    entries.forEach(entry => {
        if ($(entry.target).is(':visible') && !$(entry.target).attr('data-summernote-loaded')) {
            loadSummernote(entry.target);
        }
    })
})

function InsertSocialButton(id) {
    let socialInput = document.getElementById('polyappSummernoteSocialDialogInput');
    let targetLink = document.getElementById(id);
    targetLink.href = socialInput.value;
}

var SocialButton = function(context) {
    var ui = $.summernote.ui;
    return ui.buttonGroup([
        ui.button({
            className: 'dropdown-toggle',
            contents: '<span class="caret">Social</span>',
            tooltip: 'Insert Link to Social Media',
            data: {
                toggle: 'dropdown'
            }
        }),
        ui.dropdown({
            className: 'dropdown-style',
            contents: "<ul class='list-group'><li class='list-group-item'>Facebook</li><li class='list-group-item'>Instagram</li><li class='list-group-item'>Youtube</li><li class='list-group-item'>Twitter</li><li class='list-group-item'>Tumblr</li><li class='list-group-item'>Pinterest</li><li class='list-group-item'>LinkedIn</li><li class='list-group-item'>BandsInTown</li></ul>",
            callback: function($dropdown) {
                $dropdown.find('li').each(function() {
                    $(this).click(function() {
                        let randomID = 'l' + randString(20);
                        switch($(this).html()) {
                            case 'Facebook':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Facebook" href="https://www.facebook.com/PLACEHOLDER"><img alt="Facebook logo" src="https://c866088.ssl.cf3.rackcdn.com/assets/facebook30x30.png" height="30px" width="30px"></a>');
                                break;
                            case 'Instagram':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Instagram" href="https://www.instagram.com/PLACEHOLDER"><img alt="Instagram logo" src="https://c866088.ssl.cf3.rackcdn.com/assets/instagram30x30.png" height="30px" width="30px"></a>');
                                break;
                            case 'Youtube':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Youtube" href="https://www.youtube.com/PLACEHOLDER"><img alt="Youtube logo" src="https://c866088.ssl.cf3.rackcdn.com/assets/youtube30x30.png" height="30px" width="30px"></a>');
                                break;
                            case 'Twitter':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Twitter" href="https://www.twitter.com/PLACEHOLDER"><img alt="Twitter logo" src="https://c866088.ssl.cf3.rackcdn.com/assets/twitter30x30.png" height="30px" width="30px"></a>');
                                break;
                            case 'Pinterest':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Pinterest" href="https://www.pinterest.com/PLACEHOLDER"><img alt="Pinterest logo" src="https://c866088.ssl.cf3.rackcdn.com/assets/pinterest30x30.png" height="30px" width="30px"></a>');
                                break;
                            case 'LinkedIn':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on LinkedIn" href="https://www.LinkedIn.com/PLACEHOLDER"><img alt="LinkedIn logo" src="/assets/LinkedIn-Bug.png" height="30px" width="35px"></a>');
                                break;
                            case 'BandsInTown':
                                context.invoke('editor.pasteHTML', '<a id="'+randomID+'" target="_blank" title="follow on Bands In Town" href="https://www.bandsintown.com/PLACEHOLDER"><img alt="Bands In Town logo" src="/assets/bandsintown.png" height="30px" width="30px"></a>');
                                break;
                        }
                        let modalHTML = '<div class="modal" tabindex="-1" role="dialog">\n' +
                            '  <div class="modal-dialog" role="document">\n' +
                            '    <div class="modal-content">\n' +
                            '      <div class="modal-header">\n' +
                            '        <h5 class="modal-title">Insert Link to '+$(this).html()+'</h5>\n' +
                            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                            '          <span aria-hidden="true">&times;</span>\n' +
                            '        </button>\n' +
                            '      </div>\n' +
                            '      <div class="modal-body">\n' +
                            '        <div class="form-group">\n' +
                            '          <label for="polyappSummernoteSocialDialogInput">Link</label>\n' +
                            '          <input type="text" id="polyappSummernoteSocialDialogInput" class="form-control"/>\n' +
                            '        </div>\n' +
                            '      </div>\n' +
                            '      <div class="modal-footer">\n' +
                            '        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="InsertSocialButton(\''+randomID+'\')">Insert Social Button</button>\n' +
                            '      </div>\n' +
                            '    </div>\n' +
                            '  </div>\n' +
                            '</div>';
                        $(modalHTML).modal('show');
                    });
                });
            }
        })
    ]).render();
}

// link is a string. If set to "polyappEnlarge" then a modal is opened which enlarges this image. If set to any other
// value, the user is redirected to the value of that webpage.
function polyappEnlargeImage(targetImg) {
    let modalHTML = '<div class="modal" tabindex="-1" role="dialog">\n' +
        '<div class="modal-dialog modal-lg" role="document">\n' +
        '<div class="modal-content">\n' +
    '<div class="modal-body">\n' +
    '    <figure class="mw-100 overflow-hidden">\n'+
    '    <img src="'+targetImg.src+'" alt="'+targetImg.alt+'" class="img img-fluid"/>\n' +
    '    <figcaption class="figure-caption">\n' +
        '   <h3>'+targetImg.getAttribute("data-polyappJS-image-title")+'</h3>\n' +
        '   <p class="m-0">'+targetImg.getAttribute("data-polyappJS-image-caption")+'</p>\n' +
    '   </figcaption>\n' +
    '    </figure>\n' +
    '</div>\n';
    modalHTML += '</div>\n' +
    '</div>\n' +
    '</div>';
    $(modalHTML).modal('show');
}

// addAllEventListeners adds event listeners to all elements under headElement.
// It also initializes things which exist outside of the app shell.
// Justification: same as listenToLoad.
function addAllEventListeners(headElement) {
    if (headElement == null) {
        headElement = document.getElementsByTagName('body')[0];
    }

    // EventListeners which are not part of the app shell go here

    // DataTable: https://datatables.net/examples/data_sources/server_side.html
    let postBody = createPOSTBody(location.pathname, location.search, {});
    // Note: because .is(':visible') is not being used here, the table will be initialized even if it is not visible due
    // to being a part of a subtask.
    let tableSelected = $(headElement).find('.polyappDataTable').not("[data-table-loaded=true]");
    let ajaxPath = '/polyappDataTable?industry=' + tableSelected.attr('data-industry') + '&domain=' + tableSelected.attr('data-domain') +
        '&schema=' + tableSelected.attr('data-schema') + '&role=' + postBody.RoleID + '&user=' + postBody.UserID + '&schema=' + postBody.SchemaID;
    let allHeaders = tableSelected.find('th');
    let columns = [];
    let requiredColumns = '';
    for (let i = 0; i < allHeaders.length; i++) {
        // the 'name' is passed to the server, NOT the id. Therefore set the 'name' so that we can extract the field name later.
        columns.push({"name": allHeaders[i].id});
    }
    ajaxPath += '&requiredColumns=' + requiredColumns;
    let initialSearch = '';
    if (postBody.TableInitialSearch !== '') {
        initialSearch = postBody.TableInitialSearch;
    } else if (tableSelected.attr('data-initial-search') != null) {
        initialSearch = tableSelected.attr('data-initial-search');
    }
    let initialOrderColumn = null;
    if (postBody.TableInitialOrderColumn !== '') {
        initialOrderColumn = [[postBody.TableInitialOrderColumn, 'asc']];
    } else if (tableSelected.attr('data-initial-order-column') != null) {
        initialOrderColumn = [[tableSelected.attr('data-initial-order-column'), 'asc']];
    }
    if (postBody.TableInitialLastDocumentID !== '') {
        lastDocumentID = postBody.TableInitialLastDocumentID;
    } else if (tableSelected.attr('data-initial-last-document-ID') != null) {
        lastDocumentID = tableSelected.attr('data-initial-last-document-ID');
    }

    // https://datatables.net/reference/event/error
    $.fn.dataTable.ext.errMode = 'none';
    // options from: https://datatables.net/reference/option/
    let table = $(tableSelected).on('error.dt', function (e, settings, techNote, message) {
        console.log('Error from DataTables: ', message);
        if (techNote === 7) {
            // ajax error. This is either an internal server error or an Authorization error.
        }
        alert("Error. Try refreshing this page. If this error persists, you may not be authorized to access the data stored in the table on this page." +
            " In that case, you can ask an admin to grant you access to this Industry and Domain.");
        document.getElementById('polyappJSSpinner').style.display = 'none';
    }).DataTable({
        // feature options
        "deferRender": true,
        "info": true,
        "lengthChange": true,
        "pageLength": 50, // because I hate having to change it every time. But it does not seem to do anything right now??
        "ordering": true,
        "paging": true,
        "processing": false,
        "scrollX": true,
        "scrollY": true,
        "searching": true,
        "search": {
            "caseInsensitive": false,
            "regex": false,
            "smart": false,
            "search": initialSearch,
        },
        "searchDelay": 800, // 800 ms between searches
        "serverSide": true,
        "stateSave": false,
        // general options
        "deferLoading": true, // We will trigger loading manually below. For some reason the automatic loading is not triggering right now.
        "orderMulti": false, // server can't handle multiple column ordering
        "order": initialOrderColumn,
        "pagingType": "simple", // it's expensive to jump to the end
        "retrieve": true, // do not re-initialize if this code runs multiple times.
        // UI Layout options
        // This is the default for Bootstrap 4, with these changes:
        // I removed the 'i' so that it won't incorrectly say, "Showing 1 to 32 of 32 entries" when there are many more entries.
        "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'><'col-sm-12 col-md-7'p>>",
        // data options
        "ajax": {
            "url": ajaxPath,
            "data": function (d) {
                // TODO allow for multiple tables on one page by indexing lastDocumentID
                d.LastDocumentID = lastDocumentID;
                // need to block input until the table loads.
                document.getElementById('polyappJSSpinner').style.display = 'block';
                return d;
            },
            "dataFilter": function (data) {
                var json = JSON.parse(data);
                lastDocumentID = json.lastDocumentID;
                // unblock input
                document.getElementById('polyappJSSpinner').style.display = 'none';
                return data;
            }
        },
        "columns": columns,
        // select options
        select: {
            style: 'single'
        },
        // APIs
        initComplete: function(t) {
            console.log('datatable init complete');
        }
    });
    // TODO this causes the table to 'jump' & re-paint @ a larger size before re-painting again w/ data.
    // It would be better to modify the Bootstrap 4 styling provided by DataTables to add this class.
    $(table.table().container()).addClass('DataTable95vw');
    table.on('select', tableSelect);
    table.ajax.reload();
    $(tableSelected).attr('data-table-loaded', true);

    let discussionThreadSaveButtons = headElement.querySelectorAll('.polyappjs-discussion-thread-add-comment');
    for (let i=0;i<discussionThreadSaveButtons.length;i++) {
        discussionThreadSaveButtons[i].addEventListener('click', function(event) {
            let summernoteControl = $(event.target).parent().parent().parent().find('.polyappJSsummernote');
            let dataRef = getDataRef(summernoteControl[0]);
            let id = summernoteControl[0].parentNode.parentNode.parentNode.parentNode.parentNode.id;
            setAllData(dataRef, getAllDataID(id), summernoteControl.summernote('code'), id);
            transmitUpdateSlow();
        })
        discussionThreadSaveButtons[i].addEventListener('keypress', onKeypress);
    }

    // Summernote: https://summernote.org/getting-started/
    let summernotes = $(headElement).find('.polyappJSsummernote').not("[data-summernote-loaded=true]");
    $(summernotes).each(function() {
        // See the code for bootstrap-select below in this function for why this is necessary.
        if ($(this).is(':visible')) {
            if (!$(this).attr('data-summernote-loaded')) {
                loadSummernote(this);
            }
        } else {
            visibleSummernoteObserver.observe(this);
        }
    });

    // Popovers: https://getbootstrap.com/docs/4.0/components/popovers/
    $(headElement).find('[data-toggle="popover"]').popover({
        container: 'body',
        trigger: 'focus'
    });

    // Select picker for bootstrap-select
    $(headElement).find('.polyappSelect').each(function() {
        // Bug Note: When the polyappSelect is first created it might be created hidden, as in the hidden element in lists.
        // This is a major problem because it must be initialized after becoming visible / rendering.
        // If you do not initialize it after it renders, it can create 2 controls or 1 control which does not work.
        // To prevent this, only initialize if it is actually visible. To trigger an initialization under those circumstances
        // an event listener is attached which is activated when the control comes into view for a user.
        if ($(this).is(':visible')) {
            selectPickerLoad(this);
        } else {
            visibleSelectObserver.observe(this);
        }
    });

    // DateTime control
    $(headElement).find('.polyappDateTime').each(function() {
        if ($(this).is(':visible')) {
            initDateTime($(this));
        } else {
            visibleDateTimeObserver.observe(this);
        }
    });

    $(headElement).find('.polyappjsgeolocationrecord').each(function() {
        $(this).on('click', setGeolocation);
        $(this).on('keypress', onKeypress);
    });

    // for sortable lists with html5sortable
    let allSortable = sortable('.html5sortable', {
        forcePlaceholderSize: true,
        placeholderClass: 'list-group-item',
    });
    for (let i=0;i<allSortable.length;i++) {
        allSortable[i].addEventListener('sortupdate', handleEvent);
    }

    // required for bootstrap's file picker: https://www.w3schools.com/bootstrap4/tryit.asp?filename=trybs_form_custom_file&stacked=h
    $(headElement).find('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    // for images we want to be able to enlarge
    $(headElement).find('.polyappEnlargeImage').on('click', function() {
        if (this.tagName.toUpperCase() === 'IMG') {
            polyappEnlargeImage($(this)[0]);
        } else if (this.tagName.toUpperCase() === 'FIGCAPTION') {
            polyappEnlargeImage($(this).prev()[0]);
        }
    })

    // for videos which are supposed to autoplay we can try to play them onReady & if they're already playing there is no effect (I think?).
    // We also want to mess with polyappJSAutoplayButton
    let els = document.querySelectorAll('iframe')
    if (els != null && els.length > 0) {
        for (let i = 0; i < els.length; i++) {
            if (! els[i].allow.includes('autoplay')) {
                continue;
            }
            var player;
            let playButton = els[i].previousElementSibling;
            if (playButton) {
                playButton.addEventListener('click', function(event) {
                    let buttonNode = event.target;
                    if (buttonNode != null && buttonNode.tagName.toUpperCase() !== 'BUTTON') {
                        // must be the header instead of the button. Let's try using the next node.
                        buttonNode = event.target.nextElementSibling;
                    }
                    if (buttonNode != null && buttonNode.tagName.toUpperCase() === 'BUTTON' && player != null) {
                        player.playVideo();
                        buttonNode.classList.add('d-none');
                    }
                });
            }
            if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
                $.getScript('https://www.youtube.com/iframe_api', function() {
                    window.YT.ready(function() {
                        function enclosed() {
                            player = new YT.Player(els[i].id, {
                                events: {
                                    'onReady': function (event) {
                                        event.target.playVideo();
                                    },
                                    'onStateChange': function(event) {
                                        let buttonNode = els[i].previousElementSibling.children[1];
                                        if (buttonNode != null && buttonNode.tagName.toUpperCase() !== 'BUTTON') {
                                            // must be the header instead of the button. Let's try using the next node.
                                            buttonNode = event.target.nextElementSibling;
                                        }
                                        if (buttonNode != null && buttonNode.tagName.toUpperCase() === 'BUTTON' && event.data !== -1) {
                                            buttonNode.classList.add('d-none');
                                        }
                                    },
                                    'onError': function(event) {
                                        console.error(event);
                                    }
                                }
                            });
                        }
                        enclosed();
                    });
                })
            } else {
                function enclosed() {
                    player = new YT.Player(els[i].id, {
                        events: {
                            'onReady': function (event) {
                                event.target.playVideo();
                            },
                            'onStateChange': function(event) {
                                let buttonNode = els[i].previousElementSibling.children[1];
                                if (buttonNode != null && buttonNode.tagName.toUpperCase() !== 'BUTTON') {
                                    // must be the header instead of the button. Let's try using the next node.
                                    buttonNode = event.target.nextElementSibling;
                                }
                                if (buttonNode != null && buttonNode.tagName.toUpperCase() === 'BUTTON' && event.data !== -1) {
                                    buttonNode.classList.add('d-none');
                                }
                            },
                            'onError': function(event) {
                                console.error(event);
                            }
                        }
                    });
                }
                enclosed();
            }
        }
    }

    let accordionLinks = $(headElement).find('.polyappjs-accordion-link');
    accordionLinks.on('keypress', onKeypress);
    accordionLinks.on('click', accordionLinkClick);

    let refLinks = $(headElement).find('.polyappjs-ref-link');
    refLinks.on('keypress', onKeypress);
    refLinks.on('click', refLinkClick);

    // causes MouseEvent
    let onClickTagNames = ['button', 'a'];
    addListenersForHandleEvent(headElement, onClickTagNames, 'click', handleEvent);
    addListenersForHandleEvent(headElement, onClickTagNames, 'keypress', onKeypress);
    // causes InputEvent, Event
    let onInputTagNames = ['input', 'textarea'];
    addListenersForHandleEvent(headElement, onInputTagNames, 'input', handleEvent);
    let onChangeTagNames = ['select'];
    addListenersForHandleEvent(headElement, onChangeTagNames, 'change', handleEvent);
    // causes UiEvent, Event
    let onResizeTagNames = [];
    addListenersForHandleEvent(headElement, onResizeTagNames, 'resize', handleEvent);
}

function loadSummernote(t) {
    $(t).summernote({
        // https://summernote.org/deep-dive/#xss-protection-for-codeview
        codeviewFilter: false,
        codeviewIframeFilter: true,
        // a guesstimate.
        height: 350,
        // seems dangerous to allow if Summernote is inside drage and drop objects.
        disableDragAndDrop: true,
        dialogsInBody: true,
        callbacks: {
            onImageUpload: function(files) {
                if (files && files[0]) {
                    let dataRef = getDataRef(this);
                    let imgNode = BigObjectUpload(dataRef, getAllDataIDForSummernoteImage(this.id), files, false);
                    $(this).summernote('insertNode', imgNode);
                }
            }
        },
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['socialgroup', ['social']],
        ],
        buttons: {
            social: SocialButton
        }
    });
    $(t).attr('data-summernote-loaded', true);
    $(t).on('summernote.change', function(event, contents, $editable) {
        if (event.target.id == null || event.target.id === "") {
            // Happens with GenDiscussionThread
            return;
        }
        let dataRef = getDataRef(event.target);
        setAllData(dataRef, getAllDataID(event.target.id), contents, event.target.id);
        transmitUpdateSlow();
    });
}

function onKeypress(event) {
    let code = event.charCode || event.keyCode;
    if ((code === 32) || (code === 13) || (code === 40)) {
        event.preventDefault();
        document.getElementById(event.target.id).click();
    }
}

function selectPickerLoad(node) {
    if ($(node).is(':visible') && !$(node).attr('data-selectpicker-init')) {
        let newValue = $(node).attr('value');
        if (node.multiple && newValue != null && newValue.length > 1) {
            try {
                newValue = JSON.parse(newValue);
            }
            catch(err) {
                newValue = $(node).attr('value');
            }
        }
        $(node).selectpicker('refresh');
        $(node).selectpicker('val', newValue);
        $(node).attr('data-selectpicker-init', true);
    }
}

function initDateTime(node) {
    if (node.children()[0].classList.contains('polyappDateTimeInitialized')) {
        return;
    }
    let updatedDuringInit = false;
    if (node.children()[0].value === '' || moment(Number(node.children()[0].value)).format("X") === '0') {
        // The default date / time.
        if (node.children()[0].classList.contains('polyappDateTimeInitializeWithNow')) {
            node.children()[0].value = moment().format('X');
            updatedDuringInit = true;
        } else {
            node.children()[0].value = '';
        }
    }
    let format = false;
    if (node.attr('data-polyappjsdatetimeformat') != null && node.attr('data-polyappjsdatetimeformat').length > 0) {
        format = node.attr('data-polyappjsdatetimeformat');
    }
    node.datetimepicker({
        format: format,
        date: moment(Number(node.children()[0].value), 'X'),
    });
    node.on('change.datetimepicker', function(e) {
        let newValue = 0;
        if (e.date != null) {
            newValue = e.date.format("X");
        } else {
            newValue = moment(e.target.value).format("X");
            if (newValue === 'Invalid date') {
                let valueWithDate = new Date().toLocaleDateString() + ' ' + e.target.value;
                newValue = moment(valueWithDate).format("X");
            }
            e.target = e.target.parentNode;
        }
        // Note: The timestamp here is in GMT regardless of what the UI shows.
        setAllData(getDataRef(e.target), getAllDataID(e.target.id), Number(newValue), e.target.id);
        transmitUpdateSlow();
    });
    node.children()[0].classList.add('polyappDateTimeInitialized');
    if (updatedDuringInit) {
        let newValue = moment(node.children()[0].value).format("X");
        setAllData(getDataRef(node[0]), getAllDataID(node[0].id), Number(newValue), node[0].id);
        transmitUpdateSlow();
    }
}

// addListenersForHandleEvent adds event listeners to every instance of a tag name.
// For each tag name, it adds the 'windowResize' function as an event handler.
// Justification: same as listenToLoad.
function addListenersForHandleEvent(headEl, tagNames, eventListenerName, eventHandler) {
    for (let tagNameIndex=0; tagNameIndex < tagNames.length; tagNameIndex++) {
        let elements = headEl.getElementsByTagName(tagNames[tagNameIndex]);
        if (elements == null) {
            continue;
        }
        for (let elementIndex=0; elementIndex < elements.length; elementIndex++) {
            if (elements[elementIndex].id == null || elements[elementIndex].id === "") {
                // this happens for elements which are created by bootstrap-select and potentially other libraries.
                continue;
            }
            elements[elementIndex].addEventListener(eventListenerName, eventHandler);
        }
    }
}

// triggerEvents finds all tagNames under headEl and throws a new Event of type eventNameString
function triggerEvents(headEl, tagNames, eventNameString) {
    for (let tagNameIndex=0; tagNameIndex < tagNames.length; tagNameIndex++) {
        let elements = headEl.getElementsByTagName(tagNames[tagNameIndex]);
        if (elements == null) {
            continue;
        }
        for (let elementIndex=0; elementIndex < elements.length; elementIndex++) {
            if (elements[elementIndex].id == null || elements[elementIndex].id === "") {
                // this happens for elements which are created by bootstrap-select and potentially other libraries.
                continue;
            }
            elements[elementIndex].dispatchEvent(new Event(eventNameString, {bubbles: true}));
        }
    }
}

// AllData is all of the data in the data model. It is a key:value where each key is the ID of a Data document and each
// value is an object representing that Data document. Inside each Data document is a key: value set of data put into an Object.
// Each key is formatted like it would be on the database: "industry_domain_schema_field name".
// 2 special cases exists. Keys in arrays have a number indicating their index in the array: "industry_domain_schema_field name_0"
// Keys which are references are appended with an underscore. Ex: "_industry_domain_schema_reference field name"
// Each value is the current value at that variable name. Values are updated in setAllData().
// Justification: this must be in JS to ensure the client can function without WASM.
var AllData = {};

// setAllData both updates AllData and then updates all other elements which share that dataRefKey / allDataID combination.
//
// This function is substantially similar to insertDataIntoDOM in handle.go
//
// dataRefKey is the first key in AllData
//
// allDataID is the second key in AllData
//
// newValue is the new value in AllData
//
// currentID is the ID of the element this came from.
function setAllData(dataRefKey, allDataID, newValue, currentID) {
    if (allDataID.length > 4 && allDataID.substring(allDataID.length - 4) === 'Done') {
        // This is a workaround for how Subtasks currently can't run their AtDone bots && we need the Done buttons
        // to be styled & grouped with the rest of the Section.
        dataRefKey = getMainDataRef();
    }
    AllData[dataRefKey][allDataID] = newValue;
    if (Array.isArray(newValue)) {
        // Select is made up of many, many inputs. When its value is set we never try to sync that value elsewhere
        // on screen to avoid a severe performance degredation.
        return;
    }
    let focusCache = document.activeElement;
    let allDataNodes;
    if (dataRefKey !== getMainDataRef()) {
        // Embedded Task. If the dataRefKey is not seen in AllData[mainDataRef] then the update is thrown out by the server
        // since there is no reference from the parent document to the child. Management of adding / removing the DataRef
        // is handled for arrays when the array element is added or removed. But for singleton subtasks, this is set by innerHTML() on the server.
        // See also: needToUpdateData variable in handlers/handle.go
        if (mainDataRef != null && mainDataRef.length > 0) {
            let pRef = document.getElementById('polyappRef' + dataRefKey);
            if (pRef != null) {
                let dataRefField = pRef.getAttribute('data-Ref-Field');
                if (dataRefField != null) {
                    if (AllData[mainDataRef][dataRefField] == null) {
                        // There is no entry for dataRefKey in AllData[mainDataRef] and if we do not create one this update will be
                        // thrown out by the server.
                        AllData[mainDataRef][dataRefField] = dataRefKey;
                    }
                }
            }
        }

        // is an embedded task. Such tasks require an extra layer of specificity before we can set their values:
        // we must make sure their fifth part of their name, which is _DataID, also matches.
        let splitID = currentID.split('_');
        if (splitID.length >= 5) {
            // very hacky
            allDataNodes = document.querySelectorAll('[id^='+CSS.escape(allDataID)+'_'+splitID[4]+']')
        } else {
            // skip propagating everywhere on-screen
            return;
        }
    } else {
        allDataNodes = document.querySelectorAll('[id^='+CSS.escape(allDataID)+']');
    }
    if (allDataNodes != null) {
        for (let i = 0; i<allDataNodes.length; i++) {
            if (currentID != null && allDataNodes[i].id === currentID) {
                continue;
            }
            if ((allDataNodes[i].id.length > currentID.length) && (allDataNodes[i].id[currentID.length] !== '_')) {
                // It's a trap! If there are 2 IDs, "file" and "file%20this", then the value for "file%20this" will match
                // twice - once for "file" since they share a prefix and once for "file%20this". If "file%20this" matches
                // second, it will overwrite whatever "file" put into the field.
                return true
            }
            let type = allDataNodes[i].getAttribute("type");
            if (allDataNodes[i].nodeName === 'INPUT' && type === 'checkbox') {
                // untested on 6/30/2020
                if (typeof newValue === 'boolean') {
                    if (newValue) {
                        allDataNodes[i].setAttribute("checked", "true");
                        allDataNodes[i].setAttribute("value", "true");
                    } else {
                        allDataNodes[i].removeAttribute("checked");
                        allDataNodes[i].setAttribute("value", "false")
                    }
                }
            } else if (allDataNodes[i].nodeName === 'INPUT' && type === 'file') {
                // do nothing
            } else if (allDataNodes[i].nodeName === 'INPUT') {
                if (typeof newValue === 'string' || typeof newValue === 'number') {
                    // do not allow setting booleans and other stuff into inputs
                    allDataNodes[i].value = newValue;
                }
            } else if (allDataNodes[i].nodeName === 'TEXTAREA') {
                if (typeof newValue === 'string' || typeof newValue === 'number') {
                    // do not allow setting booleans and other stuff into inputs
                    allDataNodes[i].value = newValue;
                }
            } else if (allDataNodes[i].nodeName === 'A' || allDataNodes[i].nodeName === 'BUTTON') {
                let newStringValue = '';
                switch (typeof newValue) {
                    case 'string':
                        newStringValue = newValue;
                        break;
                    case "boolean":
                    case "number":
                    case "bigint":
                        newStringValue = newValue.toString();
                        break;
                    default:
                        // do nothing
                }
                if (newStringValue !== '') {
                    let currentIDEl = document.getElementById(currentID);
                    if (currentIDEl.hasAttribute('data-polyappjs-select-value')) {
                        // ID Selection Control
                        allDataNodes[i].textContent = currentIDEl.value;
                    } else {
                        allDataNodes[i].textContent = newStringValue;
                    }
                }
                // do nothing if the value is not a string.
            } else if (allDataNodes[i].nodeName === 'SELECT') {
                $(allDataNodes[i]).selectpicker('val', newValue);
            } else if (allDataNodes[i].nodeName === 'DIV' && allDataNodes[i].classList.contains('polyappJSsummernote')) {
                $(allDataNodes[i]).summernote('code', newValue);
            } else if (allDataNodes[i].nodeName === 'DIV' && allDataNodes[i].classList.contains('form-group') && allDataNodes[i].children[0] != null && allDataNodes[i].children[0].nodeName === 'LABEL') {
                // list
                if (! Array.isArray(newValue)) {
                    // no idea how this happened
                    continue;
                }
                // at this point we know that we are trying to copy a list of information in newValue into another list.
                // The easiest way to handle that is to override all values in that new list.
                // The easiest way to do THAT is to rebuild the entire list.
                // The easiest way to rebuild the entire list is to delete it all and then re-create it using the
                // current creation methods.

                let wrappingDiv = allDataNodes[i];
                let ulRef = wrappingDiv.children[2];
                if (ulRef == null) {
                    // not sure what is happening here
                    continue;
                }

                // delete the current list
                while (ulRef.firstChild) {
                    ulRef.removeChild(ulRef.firstChild);
                }

                // Add to the new list
                for (let newValueIndex = 0; newValueIndex < newValue.length; newValueIndex++) {
                    html5SortableAdd(wrappingDiv, newValue[newValueIndex]);
                }

                // we should NOT need to trigger a list update.
                // After all, if the fields are bound to the same thing in AllData, why would we need to
                // set AllData at that field value twice?
            }
        }
    }
    focusCache.focus({preventScroll: true});
}

// KeysToBeRemoved are keys we wish to wipe out of AllData after they have been transmitted one time. This typically includes
// values generated when you click so that you do not, say, submit payments or start jobs twice.
// This is a map from AllData keys to arrays.
var KeysToBeRemoved = {};

// handleEvent takes any event and sends it to the server in a POST request. We expect events to be intercepted by WASM
// in the service worker after the service worker has finished loading.
// Justification: This makes a server call so it really should be in JS.
function handleEvent(event, isNested) {
    let dataRef = getDataRef(event.target);
    if (dataRef.includes('&data=polyappShouldBeOverwritten')) {
        // this is a hidden element which should not be transmitted to the server.
        return;
    }
    if (event.target.getAttribute('data-readonly')) {
        // readonly fields are ones we have explicitly said should never be saved.
        return;
    }
    // polyappJS prefixed IDs are handled on a case-by-case basis.
    // Occasionally users click on images or labels instead of the actual button / thing, and we need to check their parent, not themselves.
    // a perhaps better way to do this is to create a new eventConverted variable which switches the nodeName to the actual button or whatever
    // the user is trying to interact with.
    if (event.target.id && !event.target.id.startsWith('polyappJS') &&
        !((event.target.nodeName.toUpperCase() === 'IMG' || event.target.nodeName.toUpperCase() === 'LABEL') && (event.target.parentNode.id.startsWith('polyappJS')))
    ) {
        // if you're here, the data in the field is a candidate for sending up to the server.
        // Technically we only really want to send data if it is ALSO present in the Schema.
        // However right now the client does not have a copy of the schema so it cannot verify that for itself.
        // Instead we'll just add it to the list and hope the server is benevolent in its rejection.
        let splitID = event.target.id.split('_');
        if (((splitID.length === 5 && splitID[0] !== '') || (splitID.length > 5)) && isNested == null) {
            // should be an array item or an embedded task
            let potentialListGroup = event.target.parentNode.parentNode.parentNode;
            if (event.target.tagName === 'A') {
                staticListUpdate(event.target);
            } else if (potentialListGroup.className.includes('html5sortable')) {
                listUpdate(potentialListGroup);
            } else {
                // checkbox
                potentialListGroup = potentialListGroup.parentNode.parentNode;
                if (potentialListGroup.className.includes('html5sortable')) {
                    listUpdate(potentialListGroup);
                } else {
                    // In this case, you do NOT update the list. Instead, you update the embedded task's data.
                    // List updates should only be done when reordering the list and even then the 'update' is merely reordering
                    // the order of Refs in the parent Data in AllData.
                    let splitID = event.target.id.split('_');
                    if (splitID.length === 5 && splitID[0] !== '') {
                        // [ industry, domain, schema, ID, #### ]
                        return handleEvent.call(this, event, splitID[4]);
                    } else if (splitID.length === 6 && splitID[0] === '') {
                        // [ , industry, domain, schema, ID, #### ]
                        // This is untested.
                        return handleEvent.call(this, event, splitID[5]);
                    }
                }
            }
        } else if (splitID.length < 2 && ! location.pathname.startsWith('/polyapp') && !location.pathname.startsWith('/test/')) {
            // these elements are not bound to data in Polyapp. We should not save data about them since they are likely
            // embedded controls like Summernote.
        } else if (event.type === 'input' && event.target.type === 'checkbox') {
            setAllData(dataRef, getAllDataID(event.target.id), event.target.checked, event.target.id);
            transmitUpdateSlow();
            return;
        } else if (event.type === 'input' && event.target.type === 'file') {
            // image uploading
            if (this.files && this.files[0]) {
                BigObjectUpload(dataRef, getAllDataID(event.target.id), this.files, event.target.multiple);
                return;
            }
        } else if (event.type === 'input' && event.target.value != null && event.target.type === 'number') {
            setAllData(dataRef, getAllDataID(event.target.id), parseFloat(event.target.value), event.target.id);
            if (event.target.getAttribute('data-searchable')) {
                transmitUpdate();
                return;
            }
        } else if (event.type === 'input' && event.target.attributes.getNamedItem('data-polyappJS-select-collection') != null) {
            // select ID control. If we receive this event we should assume we are searching for a value.
            let selectCollection = event.target.getAttribute('data-polyappJS-select-collection');
            let selectField = event.target.getAttribute('data-polyappJS-select-field');
            let userValue = event.target.value;
            let startingValue = event.target.getAttribute('data-polyappJS-select-field-initial-human-value');
            if (userValue === startingValue) {
                let namedItem = document.createAttribute('data-polyappJS-select-field-initial-human-value');
                namedItem.value = "";
                event.target.attributes.setNamedItem(namedItem);
                setAllData(dataRef, getAllDataID(event.target.id), event.target.getAttribute('data-polyappJS-select-value'), event.target.id);
                return;
            }
            let selectSearch = document.getElementById("polyappSearchSelectResults"+event.target.id);
            while (selectSearch.firstChild) {
                selectSearch.firstChild.remove();
            }
            debounceSelectSearch(selectCollection, selectField, userValue, function(body) {
                let el = document.getElementById("polyappSearchSelectResults"+event.target.id);
                while (el.firstChild) {
                    el.firstChild.remove();
                }
                el.insertAdjacentHTML('afterbegin', body.ResultHTML);
                let options = el.querySelectorAll('[data-polyappJS-select-option-value]');
                for (let i=0;i<options.length;i++) {
                    options[i].addEventListener('click', function(e) {
                        let target = e.target;
                        while (target.tagName !== 'LI') {
                            target = target.parentNode;
                        }
                        let newID = target.getAttribute('data-polyappJS-select-option-value');
                        let newDisplayValue = target.firstChild.nodeValue;
                        let polyappSearchSelectResults = target.parentNode.parentNode.parentNode.parentNode;
                        let inputEl = document.getElementById(polyappSearchSelectResults.id.substring('polyappSearchSelectResults'.length));
                        inputEl.setAttribute('data-polyappJS-select-value', newID);
                        inputEl.value = newDisplayValue;
                        setAllData(dataRef, getAllDataID(inputEl.id), newID, inputEl.id);
                        while (polyappSearchSelectResults.firstChild) {
                            polyappSearchSelectResults.firstChild.remove();
                        }
                        transmitUpdateSlow();
                    });
                    options[i].addEventListener('keypress', onKeypress);
                }
            });
            return;
        } else if ((event.type === 'input' || event.target.tagName.toUpperCase() === 'SELECT') && event.target.value != null) {
            // input or textarea or select
            setAllData(dataRef, getAllDataID(event.target.id), event.target.value, event.target.id);
            if (event.target.getAttribute('data-searchable')) {
                transmitUpdate();
                return;
            }
        } else if (event.target.tagName.toUpperCase() === 'SELECT' && event.target.value != null) {
            if (event.target.multiple) {
                let newValue = [];
                for (let i=0;i<event.target.selectedOptions.length;i++) {
                    newValue.push(event.target.selectedOptions[i].value);
                }
                setAllData(dataRef, getAllDataID(event.target.id), newValue, event.target.id);
            } else {
                setAllData(dataRef, getAllDataID(event.target.id), event.target.value, event.target.id);
            }
            if (event.target.getAttribute('data-searchable')) {
                transmitUpdate();
                return;
            }
        } else if (event.type === 'sortupdate') {
            // Not sure if this code is reachable
            listUpdate(event.target);
            transmitUpdateSlow();
            return;
        } else if (event.type != null) {
            // taken by a multitude of UI elements like the settings dropdown button
            if (event.target.tagName.toUpperCase() === 'BUTTON' || event.target.tagName.toUpperCase() === 'A') {
                setAllData(dataRef, getAllDataID(event.target.id), true, event.target.id);
            } else {
                console.log('non-button and non-link took button path');
                console.log(event.target);
                setAllData(dataRef, getAllDataID(event.target.id), true, event.target.id);
            }
            if (KeysToBeRemoved[dataRef] == null) {
                KeysToBeRemoved[dataRef] = [];
            }
            KeysToBeRemoved[dataRef].push(getAllDataID(event.target.id));
            transmitUpdate();
            return;
        } else if (event.target) {
            console.error(event);
        }
    } else if (event.type === 'sortupdate') {
        listUpdate(event.target);
        transmitUpdateSlow();
        return;
    } else if (event.target.parentNode.id.startsWith('polyappJShtml5sortable-remove')) {
        // GenNestedAccordion
        if (event.target.parentNode.parentNode.parentElement.classList.contains('list-group-item')) {
            let ulLikeRef = event.target.parentNode.parentNode.parentNode.parentNode;
            let liLikeRef = event.target.parentNode.parentNode.parentNode;
            liLikeRef.remove();
            listUpdate(ulLikeRef);
        } else {
            // regular lists which contain just 1 element
            let ulLikeRef = event.target.parentNode.parentNode.parentNode.parentNode.parentNode;
            let liLikeRef = event.target.parentNode.parentNode.parentNode.parentNode;
            liLikeRef.remove();
            listUpdate(ulLikeRef);
        }
    } else if (event.target.id.startsWith('polyappJShtml5sortable-remove')) {
        if (event.target.parentNode.parentElement.classList.contains('list-group-item')) {
            let ulRef = event.target.parentNode.parentNode.parentNode;
            // [copied from above]
            let liLikeRef = event.target.parentNode.parentNode;
            liLikeRef.remove();
            listUpdate(ulRef);
        } else {
            let ulRef = event.target.parentNode.parentNode.parentNode.parentNode;
            // [copied from above]
            let liLikeRef = event.target.parentNode.parentNode.parentNode;
            liLikeRef.remove();
            listUpdate(ulRef);
        }
    } else if (event.target.parentNode.id.startsWith('polyappJShtml5sortable-add')) {
        // Note: this code is similar to how the server inserts new DOM nodes when a list is loaded in a GET request in handle.go.
        let wrappingDiv = event.target.parentNode.parentNode;
        let returnValues = html5SortableAdd(wrappingDiv);
        let ulRef = returnValues[0];
        let hiddenContentClone = returnValues[1];
        listUpdate(ulRef);
        hiddenContentClone.children[0].children[0].focus();
    } else if (event.target.id.startsWith('polyappJShtml5sortable-add')) {
        // Note: this code is similar to how the server inserts new DOM nodes when a list is loaded in a GET request in handle.go.
        let wrappingDiv = event.target.parentNode;
        let returnValues = html5SortableAdd(wrappingDiv);
        let ulRef = returnValues[0];
        let hiddenContentClone = returnValues[1];
        listUpdate(ulRef);
        hiddenContentClone.children[0].children[0].focus();
    } else if (event.target.id.startsWith('polyappJSSearchableItem')) {
        // should be an 'a' element
        let localInput = event.target.parentNode.parentNode.children[2].children[0];
        if (localInput.tagName.toUpperCase() !== 'INPUT') {
            console.error('unexpected value near polyappJSSearchableItem');
        } else {
            localInput.value = event.target.innerText;
            setAllData(dataRef, getAllDataID(localInput.id), event.target.innerText, event.target.id);
            transmitUpdate();
            return;
        }
    } else if (event.target.id.startsWith('polyappJSRadioID')) {
        // radio button was pressed
        setAllData(dataRef, getAllDataID(event.target.name), event.target.value, event.target.id);
        transmitUpdateSlow();
        return;
    } else if (event.target.id === 'polyappJSNavigateChangeDomain' || event.target.parentNode.id === 'polyappJSNavigateChangeDomain') {
        let newURL = location.origin + '/polyappChangeContext';
        let postBody = createPOSTBody(location.pathname, location.search, null);
        newURL += '?industry=' + getIndustry(postBody) + '&domain=' + getDomain(postBody);
        window.location.href = newURL;
        return;
    } else if (event.target.id === 'polyappJSNavigateChangeTask' || event.target.parentNode.id === 'polyappJSNavigateChangeTask') {
        let newURL = location.origin + '/polyappChangeTask';
        let postBody = createPOSTBody(location.pathname, location.search, null);
        newURL += '?industry=' + getIndustry(postBody) + '&domain=' + getDomain(postBody) + '&task=' + getTask(postBody) + '&schema=polyappTask';
        window.location.href = newURL;
        return;
    } else if (event.target.id === 'polyappJSNavigateChangeData' || event.target.parentNode.id === 'polyappJSNavigateChangeData') {
        let newURL = location.origin + '/polyappChangeData?';
        let postBody = createPOSTBody(location.pathname, location.search, null);
        newURL += 'industry=' + getIndustry(postBody) + '&domain=' + getDomain(postBody) + '&task=' + getTask(postBody) + '&schema=' + postBody.SchemaID +
            '&ux=' + postBody.UXID;
        window.location.href = newURL;
        return;
    } else if (event.target.id === 'polyappJSSettingsDropdown' || event.target.parentNode.id === 'polyappJSSettingsDropdown') {
        return;
    } else if (event.target.getAttribute('data-polyappJSTriggerEventOnParent') != null) {
        // Events are not naturally bubbling up in Polyapp's javascript. So if we want an event to be
        // triggered on a parent item we need to dispatch the event on that item.
        let levelsToJump = event.target.getAttribute('data-polyappJSTriggerEventOnParent');
        if (levelsToJump === '1') {
            let newEvent = new Event(event.type);
            event.target.parentNode.dispatchEvent(newEvent);
            transmitUpdateNow();
            return;
        } else if (levelsToJump === '2') {
            let newEvent = new Event(event.type);
            event.target.parentNode.parentNode.dispatchEvent(newEvent);
            transmitUpdateNow();
            return;
        }
    } else if (event.target.parentNode && (event.target.id == null || !event.target.id.startsWith('polyapp')) && event.target.nodeName.toUpperCase() === 'IMG') {
        // we weren't able to figure out what this element was doing. Perhaps if we check the parent element?
        // This could happen a LOT if we're not careful, so it's limited to images for right now with the use case of
        // images within buttons, where users wanted to click the button and not the image.
        let newEvent = new Event(event.type);
        event.target.parentNode.dispatchEvent(newEvent);
        return;
    }

    if (event.target.classList.contains('polyappJSTransmitUpdateNow')) {
        transmitUpdateNow();
    } else {
        transmitUpdateSlow();
    }
}

// html5SortableAdd adds a new item to an html5Sortable list.
//
// newValue parameter is optional. It sets the value of the new element without triggering an update.
// It can do this because event listeners have not been added to the newly cloned element yet.
function html5SortableAdd(wrappingDiv, newValue) {
    let ulRef = wrappingDiv.children[2];
    let hiddenContentClone = wrappingDiv.children[3].children[0].cloneNode(true);
    // the main problem with this strategy is we must adjust the list item IDs and we don't really know what the current
    // IDs are unless we read them all. Instead of doing that let's just generate a large number to make the ID unique in the list.
    let childrenWithBadIDs = hiddenContentClone.querySelectorAll('[id]');
    let nestedDataID = randString(20);
    Array.prototype.forEach.call(childrenWithBadIDs, function(el, i) {
        if (el.id.startsWith('polyappJSDisplayNone')) {
            // simple list functionality
            let splitID = el.id.split('_');

            let newID = splitID[0].substring(('polyappJSDisplayNone').length) + '_'
            // toLength's default would cut off the last bit of the existing string. This is good if the last bit is a random #
            // and we are trying to replace that random number with our own random number.
            let toLength = splitID.length - 1;
            if (isNaN(parseInt(splitID[toLength], 10))) {
                // if it is not a number, then we need to add our own random number.
                toLength+=1;
            }
            for (let idSection = 1; idSection < toLength; idSection++ ) {
                newID += splitID[idSection] + '_';
            }
            newID += Math.floor(Math.random() * 1048576);
            el.id = newID;

            if (newValue != null) {
                el.value = newValue;
            }
        } else {
            // nested components functionality
            // we need to update this list item's Data ID and any references to the data ID.
            // Note: the content of data-target and data-parent has been CSS escaped which is why escaping is applied to the ID.
            // We are also querying this escaped value, so we need to perform a second escaping.
            let linkedDataTarget = hiddenContentClone.querySelectorAll('[data-target="'+CSS.escape('#'+CSS.escape(el.id))+'"]');
            let linkedDataParent = hiddenContentClone.querySelectorAll('[data-parent="'+CSS.escape('#'+CSS.escape(el.id))+'"]');
            let linkedFor = hiddenContentClone.querySelectorAll('[for="'+CSS.escape(el.id)+'"]')
            let linkedAriaControls = hiddenContentClone.querySelectorAll('[aria-controls="'+el.id+'"]')
            let indexOfDataToOverwrite = el.id.indexOf('&data=polyappShouldBeOverwritten');
            if (indexOfDataToOverwrite < 0) {
                // most cases probably go here?
                // industry_domain_schema_field%20name_randomthingaddedtopreventcollisions
                let newID = '';
                let splitID = el.id.split('_');
                if (splitID.length === 5) {
                    newID = splitID[0] + '_' + splitID[1] + '_' + splitID[2] + '_' + splitID[3] + '_' + nestedDataID + '_' + splitID[4];
                } else {
                    newID = el.id + '_' + nestedDataID;
                }
                Array.prototype.forEach.call(linkedDataTarget, function(linkedDataTargetEl, i) {
                    linkedDataTargetEl.setAttribute('data-target', '#'+CSS.escape(newID));
                });
                Array.prototype.forEach.call(linkedDataParent, function(linkedDataParentEl, i) {
                    linkedDataParentEl.setAttribute('data-parent', '#'+CSS.escape(newID));
                });
                Array.prototype.forEach.call(linkedFor, function(linkedForEl, i) {
                    linkedForEl.setAttribute('for', newID);
                });
                Array.prototype.forEach.call(linkedAriaControls, function(linkedAriaControlsEl, i) {
                    linkedAriaControlsEl.setAttribute('for', newID);
                });
                el.id = newID;
                return;
            }
            let idPart1 = el.id.substring(0, indexOfDataToOverwrite);
            let idPart2 = el.id.substring(indexOfDataToOverwrite + ('&data=polyappShouldBeOverwritten').length);
            el.id =  idPart1 + '&data=' + nestedDataID + idPart2;
            el.id = el.id + '_' + nestedDataID;
            Array.prototype.forEach.call(linkedDataTarget, function(linkedDataTargetEl, i) {
                linkedDataTargetEl.setAttribute('data-target', '#' + CSS.escape(el.id));
            });
            Array.prototype.forEach.call(linkedDataParent, function(linkedDataTargetEl, i) {
                linkedDataTargetEl.setAttribute('data-parent', '#' + CSS.escape(el.id));
            });
            Array.prototype.forEach.call(linkedFor, function(linkedForEl, i) {
                linkedForEl.setAttribute('for', el.id);
            });
            Array.prototype.forEach.call(linkedAriaControls, function(linkedAriaControlsEl, i) {
                linkedAriaControlsEl.setAttribute('for', el.id);
            });
        }
    });
    ulRef.appendChild(hiddenContentClone);
    addAllEventListeners(hiddenContentClone);
    return [ulRef, hiddenContentClone];
}

// tableSelect is called when a datatable emits the 'select' event.
function tableSelect(event, dataTable, type, indexes) {
    let dataRef = getDataRef(event.target);
    if (type === 'row') {
        setAllData(dataRef, getAllDataID(event.target.id), dataTable.rows(indexes).data()[0], event.target.id);
        // right now DataTable allows users to 'click' on a row & send that information to a server, and after sending the
        // row still appears to be selected but that data is not re-sent to the server, much like when you click on a button.
        if (KeysToBeRemoved[dataRef] == null) {
            KeysToBeRemoved[dataRef] = [];
        }
        KeysToBeRemoved[dataRef].push(event.target.id);
        transmitUpdateNow();
    }
}

// listUpdate takes a reference to a list's ul element or ol element and updates the
// array of values in AllData. It can be used in conjunction with a drag and drop list.
function listUpdate(ulRef) {
    // this section is coupled how how list items are generated
    if (ulRef.children == null) {
        console.error('sortupdate children was null');
        return;
    }
    let arrayValues = [];
    for (let i = 0; i<ulRef.children.length;i++) {
        if (ulRef.children[i] == null || ulRef.children[i].lastChild == null) {
            ulRef.children[i].remove();
            console.error('sortupdate child lastChild was null');
            console.error(ulRef.children[i]);
            continue;
        }
        let inputValue = ulRef.children[i].children[0].children[0].value;
        let inputType = ulRef.children[i].children[0].children[0].type;
        if (inputValue != null) {
            if (inputType === 'number') {
                arrayValues.push(parseFloat(inputValue));
            } else {
                arrayValues.push(inputValue);
            }
            continue;
        }
        let checkboxValue = ulRef.children[i].children[0].children[0].children[0].children[0].checked;
        if (checkboxValue != null) {
            arrayValues.push(checkboxValue);
            continue;
        }
        let nestedRefValue = ulRef.children[i].children[0].children[0].children[0].children[1].id;
        if (nestedRefValue != null && nestedRefValue.startsWith('polyappRef')) {
            // push in the raw Ref value
            let rawRefValueArray = nestedRefValue.substr(('polyappRef').length).split('_');
            let rawRefValue = rawRefValueArray[0];
            arrayValues.push(rawRefValue);
            continue;
        }
        let refValue = ulRef.children[i].children[0].children[0].id;
        // TODO there's no way this is correct and working... right? Riiiight? What is this case even meant for?
        if (refValue != null) {
            arrayValues.push(refValue);
            continue;
        }
    }
    // when an array value is a Ref, it can also have an entry in AllData[doEscape(ref)]. If we remove that entry
    // from the array which is referencing it, we should also delete the entry in AllData.
    //
    // But what if we have another subtask pointing to the same Task? If we delete it here
    // then that subtask is now not working. It seems safer for now to leave that data in even though it may have
    // disappeared from the UI.
    let dataRef = getDataRef(ulRef);
    let fieldName = ulRef.parentNode.id;
    // let oldArrayValues = AllData[dataRef][getAllDataID(fieldName)];
    // if (oldArrayValues != null) {
    //     for (let i=0;i<oldArrayValues.length;i++) {
    //         let escapedDataRef = escapeDataRef(oldArrayValues[i]);
    //         if (!arrayValues.includes(escapedDataRef)) {
    //             delete AllData[escapedDataRef];
    //         }
    //     }
    // }
    setAllData(dataRef, getAllDataID(fieldName), arrayValues, ulRef.parentNode.id);
}

// staticListUpdate works with gen.List to update a list which is backed by an []boolean. When an element is clicked the
// list element's ID is examined and the appropriate boolean is set in that parent.
function staticListUpdate(listItemRef) {
    let dataRef = getDataRef(listItemRef.parentNode);
    let fieldName = listItemRef.parentNode.id;
    let arrayValues = [];
    for (let i = 0; i<listItemRef.parentNode.children.length;i++) {
        if (listItemRef.parentNode.children[i].id === listItemRef.id) {
            arrayValues.push(true);
        } else {
            arrayValues.push(false);
        }
    }
    setAllData(dataRef, getAllDataID(fieldName), arrayValues, fieldName);
}

// DoPOSTAtUnload deals with what happens when you set a value via a Bot @ end of Task but those changes are not sent back down to the UI.
// In that case, when the page unloads and the 'beforeunload' POST is made, the old value will be stored in the database
// even though it was properly updated just a second ago by the Bot.
var DoPOSTAtUnload = true;
// this method will not always work.
window.addEventListener('beforeunload', ev => {
    if (DoPOSTAtUnload) {
        transmitUpdateNow();
    } else {
        DoPOSTAtUnload = true;
    }
});

// https://www.nngroup.com/articles/response-times-3-important-limits/
// 'instantaneous' (will only work if request is served from WASM on client)
var transmitUpdate = debounce(transmitUpdateNow, 100);
// 'easy flow of thought' (assumes server responds in <300 ms & RTT < 100ms).
var transmitUpdateSlow = debounce(transmitUpdateNow, 1500);

var debounceSelectSearch = debounce(selectSearch, 300);

// blockingCount is how many requests are in flight which want user input blocked.
var blockingCount = 0;

var updateInProgress = false;

var consecutiveErrors = 0;

// transmitUpdateNow should not be called directly. Instead call transmitUpdate or transmitUpdateSlow.
function transmitUpdateNow() {
    if (updateInProgress) {
        return transmitUpdate();
    }
    updateInProgress = true;
    let postKeyRemovalAllData = {};
    let haveBlocked = false;
    for (const d in AllData) {
        postKeyRemovalAllData[d] = {};
        for (const k in AllData[d]) {
            if (KeysToBeRemoved[d] != null && KeysToBeRemoved[d].includes(k)) {
                // events like button clicks or row selections can get here.
                // Events which arrive here aren't transmitted to the server so it's pretty rare for you to
                // want a button press or other thing the user does to take this code path.
                // Reviewing my code, I'm not sure what specific cases take this code path.
                if (!haveBlocked) {
                    document.getElementById('polyappJSSpinner').style.display = 'block';
                    haveBlocked = true;
                }
            } else {
                if (k.endsWith('_Done') && !haveBlocked) {
                    document.getElementById('polyappJSSpinner').style.display = 'block';
                    haveBlocked = true;
                }
                postKeyRemovalAllData[d][k] = AllData[d][k];
            }
        }
    }
    if (haveBlocked) {
        blockingCount += 1;
    }
    let POSTBody = createPOSTBody(location.pathname, location.search, AllData);
    let stringifiedBody = JSON.stringify(POSTBody);

    // we can remove the click value now that doing so won't remove it from the request.
    AllData = postKeyRemovalAllData;
    KeysToBeRemoved = {};

    let url = location.pathname + location.search;
    let csrf = document.cookie.replace(/(?:(?:^|.*;\s*)_csrf\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if (csrf == null) {
        console.error('csrf token not set');
        updateInProgress = false;
        return;
    }
    let setKeepalive = true;
    if (stringifiedBody.length * 4 > 64000) {
        // this ignores that most characters will be encoded with only one byte.
        // this is necessary because the keepalive is limited to 64kb on Chrome.
        setKeepalive = false;
    } else if (typeof stringifiedBody === ReadableStream) {
        // prohibited by the fetch spec.
        setKeepalive = false;
    }
    let authorizationHeader = 'Bearer ';
    let currentUser = firebase.auth().currentUser;
    if (currentUser == null) {
        alert('Authentication has temporarily failed. Please refresh this page.');
        return;
    }
    currentUser.getIdToken(false).then((token) => {
        savingNow();
        authorizationHeader += token;
        fetchWithRetry(url, 1000, 2, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'X-CSRF-TOKEN': csrf,
                'Content-Type': 'application/json',
                'Authorization': authorizationHeader
            },
            body: stringifiedBody,
            keepalive: setKeepalive, // should mimic 'sendBeacon' https://stackoverflow.com/questions/40523469/navigator-sendbeacon-to-pass-header-information
        }).then(
            (response) => {
                notSavingNow();
                if (response.status !== 200) {
                    consecutiveErrors++;
                    if (response.body != null && response.headers.get("Content-Type") != null &&
                        response.headers.get("Content-Type").includes('application/json') && consecutiveErrors < 5) {
                        // some errors are nice enough to include a header to show to users
                        response.json().then((body) => {
                            handleResponse(body);
                        });
                    } else if (consecutiveErrors >= 5) {
                        let toastContainer = document.getElementById('polyappJSToastContainer');
                        while (toastContainer.firstChild) {
                            toastContainer.firstChild.remove();
                        }
                        toastContainer.innerHTML = '<div class="toast ml-auto" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"><div class="toast-header">  <strong class="mr-auto">Fatal Error While Saving</strong>  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">    <span aria-hidden="true">×</span>  </button></div><div class="toast-body">An error occurred while saving and all retries failed. Refreshing this page may fix the problem.<br><br>If refreshing the page does not work, please contact support.</div>\n</div>'
                        $('.toast').toast('show');
                    }
                    if (haveBlocked) {
                        blockingCount -= 1;
                    }
                    if (blockingCount <= 0) {
                        document.getElementById('polyappJSSpinner').style.display = 'none';
                    }
                    updateInProgress = false;
                    return;
                }
                consecutiveErrors = 0;

                response.json().then((body) => {
                    handleResponse(body);
                    if (haveBlocked) {
                        blockingCount -= 1;
                    }
                    if (blockingCount <= 0) {
                        document.getElementById('polyappJSSpinner').style.display = 'none';
                    }
                    updateInProgress = false;
                });
            }
        ).catch((error) => {
            if (haveBlocked) {
                blockingCount -= 1;
            }
            if (blockingCount <= 0) {
                document.getElementById('polyappJSSpinner').style.display = 'none';
            }
            updateInProgress = false;
            console.error(error);
        });
    }).catch((err) => {
        if (haveBlocked) {
            blockingCount -= 1;
        }
        if (blockingCount <= 0) {
            document.getElementById('polyappJSSpinner').style.display = 'none';
        }
        updateInProgress = false;
        console.error(err);
    });
}

// BigObjectUpload takes files of any type and uploads them to Polyapp with a POST directly to the correct Data document.
// This is used for images.
//
// This function returns an img node you can use to access the image which was just uploaded.
//
// dataRefKey is the first key in AllData. It is the Ref for this image in the database.
//
// allDataID is the second key in AllData. It is the field in the database.
//
// files is an array of files which are being uploaded.
//
// multipleInput is true if the Input allows multiple files to be uploaded at once. False otherwise.
function BigObjectUpload(dataRefKey, allDataID, files, multipleInput) {
    if (dataRefKey == null || allDataID == null || files == null || files.length < 1) {
        return;
    }

    // Validation
    let maxSize = 100000000; // according to the database validator
    for (let i=0;i<files.length;i++) {
        if (files[i].size > maxSize) {
            alert('Image #'+String(i)+' too large. Images must be less than 100 million bytes (99 MB). Less than 10MB is recommended.');
            return;
        }
    }

    let url = "/u/" + dataRefKey.substring(('%2Fu%2F').length) + "&field=" + allDataID;
    // the URL will start escaped because that is standard for all dataRefKey.
    // We are just unescaping the URL.
    // TODO let url = '/u/' + decodeURI(dataRefKey.substring(('%2Fu%2F').length)) + "&field=" + allDataID; // instead of decoding individual parts.
    url = url.replace(/%2F/g, '/');
    url = url.replace(/%3F/g, '?');

    let csrf = document.cookie.replace(/(?:(?:^|.*;\s*)_csrf\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if (csrf == null) {
        console.error('csrf token not set');
        return;
    }
    // our request is being made to a different origin - one where we may not have a cookie set.
    // To deal with that, set the same cookie on this other origin we are making a request to.
    // I'm not sure this is the right thing to do.
    let d = new Date();
    d.setTime(d.getTime() + 60*1000); // 1 minute
    document.cookie = "_csrf="+csrf+"; expires="+d.toUTCString()+"; path="+url;

    let authorizationHeader = 'Bearer ';
    let currentUser = firebase.auth().currentUser;
    if (currentUser == null) {
        alert('Authentication has temporarily failed. Please refresh this page.');
        return;
    }
    currentUser.getIdToken(false).then((token) => {
        authorizationHeader += token;
        for (let i=0;i<files.length;i++) {
            let thisFileURL = url;
            if (multipleInput) {
                // multiple image uploads end in /#. See HandleUploads()
                thisFileURL = url + "%2F" + String(i); // it just so happens that &field is the last parameter
                thisFileURL += '&filesLength='+String(files.length);
                thisFileURL += '&Title='+encodeURIComponent(files[i].name.slice(0, files[i].name.indexOf('.'))); // https://developer.mozilla.org/en-US/docs/Web/API/File
            }
            savingNow();
            fetchWithRetry(thisFileURL, 1000, 2, {
                method: 'POST',
                credentials: 'same-origin',
                headers: {
                    'X-CSRF-TOKEN': csrf,
                    'Authorization': authorizationHeader,
                    // setting Content-Type to allow POSTing JSON files, which otherwise are not sent in the request.
                    'Content-Type': 'text/plain'
                },
                body: files[i],
                // keepalive is limited to 64kb. More than that and the request fails.
            }).then(response => {
                notSavingNow();
                if (response.status !== 200) {
                    console.log('file upload failed');
                    alert('file upload failed');
                    return;
                }

                // refresh all img tags on this page which are referenced by the correct URL. This makes sure you don't get those
                // broken image icons on the page.
                let allAssets = document.querySelectorAll('[src^='+CSS.escape('/blob/assets/' + dataID + '/' + allDataID) + ']');
                for (let i = 0; i < allAssets.length; i++) {
                    allAssets[i].src = '/blob/assets/' + dataID + '/' + allDataID + '?cacheBuster=' + new Date().getTime();
                }
            }).catch((error) => {
                alert('Uploading failed. Error: ' + error.toString());
                console.error(error);
            });
        }
    }).catch((err) => {
        alert('Uploading failed. Error: ' + error.toString());
        console.error(err);
    });

    // this code ought to execute before the fetch returns.
    let dataIDIndex = dataRefKey.indexOf('&data=');
    let dataIDIndexEnd = dataRefKey.indexOf('&', dataIDIndex + ('&data=').length);
    if (dataIDIndexEnd < 0) {
        dataIDIndexEnd = dataRefKey.length;
    }
    let dataID = dataRefKey.substring(dataIDIndex + ('&data=').length, dataIDIndexEnd);

    let imgNode = document.createElement('IMG');
    imgNode.setAttribute('id', 'polyappJSImagePreview' + allDataID)
    imgNode.setAttribute('src', '/blob/assets/' + dataID + '/' + allDataID);
    imgNode.classList.add('img-fluid');
    return imgNode;
}

// selectSearch makes a GET request when searching for an ID in the database.
//
// collection (such as "data"), field (such as "Name" or "ind_dom_sch_Name") and value (what the user has typed) are required inputs.
//
// responseHandler is also required. It should be a function with one argument which is called when the request responds.
function selectSearch(collection, field, value, responseHandler) {
    let url = '/polyappSearch?collection=' + collection + '&field=' + field + '&value=' + value;
    let csrf = document.cookie.replace(/(?:(?:^|.*;\s*)_csrf\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if (csrf == null) {
        console.error('csrf token not set');
        return;
    }
    let authorizationHeader = 'Bearer ';
    let currentUser = firebase.auth().currentUser;
    if (currentUser == null) {
        alert('Authentication has temporarily failed. Please refresh this page.');
        return;
    }
    currentUser.getIdToken(false).then((token) => {
        authorizationHeader += token;
        savingNow();
        fetchWithRetry(url, 1000, 2, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'X-CSRF-TOKEN': csrf,
                'Content-Type': 'application/json',
                'Authorization': authorizationHeader
            },
        }).then(
            (response) => {
                notSavingNow();
                if (response.status !== 200) {
                    return;
                }
                response.json().then((body) => {
                    if (body == null) {
                        console.error("body was null from selectSearch request");
                        return;
                    }
                    responseHandler(body);
                });
            }
        ).catch((error) => {
            console.error(error);
        });
    }).catch((err) => {
        console.error(err);
    });
}

function setGeolocation(event) {
    let targetElement = event.target;
    if (! targetElement.classList.contains('input-group-append')) {
        event.target = targetElement.parentElement;
        return setGeolocation(event);
    }
    let inputEl = targetElement.parentElement.children[0];
    navigator.geolocation.getCurrentPosition(function(pos) {
        let newValue = pos.coords.latitude.toString() + ', ' + pos.coords.longitude.toString();
        inputEl.value = newValue;
        let dataRef = getDataRef(inputEl);
        setAllData(dataRef, getAllDataID(inputEl.id), newValue, inputEl.id);
        transmitUpdateSlow();
    }, function(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
        if (err.code === 1) {
            alert('Setting location failed because this website does not have access to view your location. You can fix this. Help: https://www.lifewire.com/denying-access-to-your-location-4027789');
        } else {
            alert('Setting location failed because: ' + err.message);
        }
    });
}

function wait(delay) {
    return new Promise((resolve) => setTimeout(resolve, delay));
}

// fetchWithRetry performs an exponential backoff for "tries" attempts to fetch with the
// given URL and fetchOptions.
function fetchWithRetry(url, delay, tries, fetchOptions = {}) {
    return fetch(url, fetchOptions).catch((err) => {
        let triesLeft = tries - 1;
        if (triesLeft == null || triesLeft < 1) {
            throw err;
        }
        return wait(delay).then(() => fetchWithRetry(url, delay * 2, triesLeft, fetchOptions));
    });
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

// https://stackoverflow.com/a/2117523/12713117
function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function randString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// parseQuery returns an object with a URL query converted to key: value entries in the object.
// later entries could override earlier entries if for some reason there are duplicates in the URL.
function parseQuery(queryString) {
    if (queryString == null || queryString.length < 1) {
        return {};
    }
    let query = {};
    let parameters = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    if (parameters.length < 1) {
        return {};
    }
    for (let i=0; i<parameters.length; i++) {
        let keyValue = parameters[i].split('=');
        if (keyValue[0] !== '') {
            query[decodeURIComponent(keyValue[0])] = decodeURIComponent(keyValue[1] || '');
        }
    }
    return query;
}

// mainDataRef is the dataRef for the overall Task (NOT any subtask).
var mainDataRef = '';

// getDataRef determines the key into the object/map for AllData for a given event.target.
// If the key->object pair does not exist in AllData it is created there.
//
// target is the event.target
function getDataRef(target) {
    let dataRef = '';
    let targetParent = target;
    while (targetParent != null) {
        if (targetParent.id != null && targetParent.id.length > ('polyappRef').length && targetParent.id.startsWith('polyappRef')) {
            let splitTarget = [targetParent.id];
            if (targetParent.id.includes('_')) {
                // relevant if this is in an array.
                splitTarget = targetParent.id.split('_');
            }
            dataRef = splitTarget[0].substr(('polyappRef').length);
            break;
        }
        targetParent = targetParent.parentNode;
    }
    if (dataRef === '') {
        dataRef = getMainDataRef();
    }
    if (AllData[dataRef] == null) {
        AllData[dataRef] = {};
    }
    return dataRef;
}

// getMainDataRef gets the first key into AllData ref for the topmost document.
function getMainDataRef() {
    if (mainDataRef != null && mainDataRef.length > 0) {
        return mainDataRef;
    }
    let dataRef = location.pathname + location.search;

    // We should not include the user or role or finishURL in the Data index - it gets too messy.
    // These values ought to be at the end of the URL so cut off everything after that.
    // We know that the last part of the URL is one of these, in order: Data, Schema, Domain, Industry, ?
    // So if we just try to find the correct sequence in the URL, then we can cut off the bit after that sequence.
    let dataIndex = dataRef.indexOf("&data=");
    let schemaIndex = dataRef.indexOf("&schema=");
    let domainIndex = dataRef.indexOf("&domain=");
    let industryIndex = dataRef.indexOf("&industry=");
    let qIndex = dataRef.indexOf("?");
    let cutIndex = 0;
    if (dataIndex > 0) {
        cutIndex = dataRef.indexOf("&", dataIndex + 1);
    } else if (schemaIndex > 0) {
        cutIndex = dataRef.indexOf("&", schemaIndex + 1);
    } else if (domainIndex > 0) {
        cutIndex = dataRef.indexOf("&", domainIndex + 1);
    } else if (industryIndex > 0) {
        cutIndex = dataRef.indexOf("&", industryIndex + 1);
    } else if (qIndex > 0) {
        // well this is a very short URL. We will cut off every query parameter.
        cutIndex = qIndex;
    }
    if (cutIndex > 0) {
        dataRef = dataRef.substring(0, cutIndex);
    }
    dataRef = escapeDataRef(dataRef);
    mainDataRef = dataRef;
    if (AllData[mainDataRef] == null) {
        AllData[mainDataRef] = {};
    }
    return mainDataRef;
}

// getAllDataID is the second index into AllData. It represents a field, like "industry_domain_schema_field name"
function getAllDataID(potentialArrayID) {
    let split = potentialArrayID.split('_');
    let allDataID = '';

    if (split[0] === '') {
        allDataID += '_';
        split.shift();
    }
    // [ industry domain schema ID ]
    for (let i=0;i<split.length;i++) {
        if (i > 3) {
            return allDataID; // this path is taken by nested Tasks.
        }
        if (i !== 0) {
            allDataID += '_'
        }
        allDataID += split[i];
    }
    return allDataID;
}

// getAllDataIDForSummernoteImage is the second index into AllData. It represents an image associated with a field, like
// "industry_domain_schema_field name" for Summernote. Summernote
// might have a link to an image inside its content. Rather than make every Summernote field a subtask with a HTML field
// and an array of images associated with it, we can getAllDataIDForSummernoteImage with the summernote ID.
function getAllDataIDForSummernoteImage(potentialArrayID) {
    let split = potentialArrayID.split('_');
    let allDataID = '';

    if (split[0] === '') {
        allDataID += '_';
        split.shift();
    }
    // [ industry domain schema ID ]
    for (let i=0;i<split.length;i++) {
        if (i > 3) {
            return allDataID; // this path is taken by nested Tasks.
        }
        if (i !== 0) {
            allDataID += '_'
        }
        if (i === 3) {
            // The last part of the AllDataID. Based on how the cloud provider allows querying the prefixes of blob objects,
            // as long as the prefix is calculable from the Summernote ID, we should also be able to calculate all of Summernote's
            // associated object IDs.
            //
            // Therefore let's create a predictable ID by prefixing with the actual ID + "Blob" so that we can find this
            // object again by querying for all objects prefixed with the SummernoteID + "Blob" which then have 25 additional characters
            // in their Names.
            allDataID += split[i] + "Blob" + randString(25);
        } else {
            allDataID += split[i];
        }
    }
    return allDataID;
}

// escapeDataRef takes an unescaped OR already escaped /t/industry/domain/etc URL and escapes it similarly to url.PathEscape
// in Go EXCEPT it only escapes characters we expect to find unescaped in the URL, like "/" and "?".
//
// output should look something like this: %2Ft%2Fpolyapp%2FTaskSchemaUX%2FZVAzgqXzEizxAdFDLhLF%3Fux=Oo0T7RiiiymJvormn4pO&schema=0MPHNrhEqwAYtxGeiL59&data=ljcDLKslw49csakl2K
function escapeDataRef(ref) {
    ref = ref.replace(/\//g, '%2F');
    return ref.replace('?', '%3F');
}

// createPOSTBody is able to convert a myriad of information sources into a coherent request body.
//
// pathname is location.pathname or its equivalent
//
// search is location.search or its equivalent
//
// data is event.target.value or its equivalent
//
// FYI this is substantially similar to func CreateGETRequest but this one is for POST and that one is for GET.
//
// Justification: This must be in JS so we can properly formulate a request even when WASM has not loaded.
function createPOSTBody(pathname, search, data) {
    let industryID = 'polyappNone';
    let domainID = 'polyappNone';
    let taskID = 'polyappNone';
    let publicPath = '';
    let context = pathname.split('/');
    // if url is localhost:8080/t/test/test/test/idb
    // context = ["", "t", "test", "test", "test", "idb"]
    if (context.length > 2 && (context[1] === "t" || context[1] === "test" || context[1] === "u")) {
        if (context[2]) {
            industryID = context[2];
        }
        if (context.length > 3 && context[3]) {
            domainID = context[3];
        }
        if (context.length > 4 && context[4]) {
            taskID = context[4];
        }
    }
    else {
        publicPath = pathname
    }
    let query = parseQuery(search);
    let ux = 'polyappNone';
    let schema = 'polyappNone';
    let dataID = 'polyappNone';
    let userID = 'polyappNone';
    let roleID = 'polyappNone';
    let modifyID = 'polyappNone';
    let finishURL = '';
    let tableInitialSearch = '';
    let tableInitialOrderColumn = '';
    let tableInitialLastDocumentID = '';
    if (query['ux']) {
        ux = query['ux'];
    }
    if (query['schema']) {
        schema = query['schema'];
    }
    if (query['data']) {
        dataID = query['data'];
    }
    if (query['user']) {
        userID = query['user'];
    }
    if (query['role']) {
        roleID = query['user'];
    }
    if (query['modify']) {
        modifyID = query['modify'];
    }
    if (query['finishURL']) {
        finishURL = query['finishURL'];
    }
    if (query['table-initial-search']) {
        tableInitialSearch = query['table-initial-search'];
    }
    if (query['table-initial-order-column']) {
        tableInitialOrderColumn = query['table-initial-order-column'];
    }
    if (query['table-initial-last-document-ID']) {
        tableInitialLastDocumentID = query['table-initial-last-document-ID'];
    }
    // happens when changing Context / Task / Data
    // these function as overrides, so they could also be helpful for public pages or set for other Ajax requests.
    let overrideIndustryID = 'polyappNone';
    let overrideDomainID = 'polyappNone';
    let overrideTaskID = 'polyappNone';
    if (query['industry']) {
        overrideIndustryID = query['industry'];
    }
    if (query['domain']) {
        overrideDomainID = query['domain'];
    }
    if (query['task']) {
        overrideTaskID = query['task'];
    }

    // try to send a message to the service worker, else ask the server to deal with it.
    return {
        // MessageID identifies this message. This ensures 1 message gets 1 response.
        MessageID: uuidv4(),
        // IndustryID is part of the context of this request.
        IndustryID: industryID,
        // OverrideIndustryID is if the query includes an industry.
        OverrideIndustryID: overrideIndustryID,
        // DomainID is part of the context of this request.
        DomainID: domainID,
        // OverrideDomainID is if the query includes a domain.
        OverrideDomainID: overrideDomainID,
        // TaskID of the Task in the database is used to tie the User, Data, Schema, and UX together into a single package
        TaskID: taskID,
        // OverrideTaskID is if the query includes a task.
        OverrideTaskID: overrideTaskID,
        // Data is the marshalled information from the Task. Data is in an object of Data Document ID -> Data Object.
        // In each Data Object, there is a map of ElementID -> data.
        Data: data,
        // UXID is the ID of the UI data in the database.
        UXID: ux,
        // SchemaID is the ID of the Data's schema in the database.
        SchemaID: schema,
        // DataID of the data instance in the database.
        DataID: dataID,
        // UserID of the user in the request. Do not confuse this with the UID from Authentication.
        UserID: userID,
        // RoleID is the Role of the User in the request. This is NOT required.
        RoleID: roleID,
        // ModifyID is used in certain admin contexts
        ModifyID: modifyID,
        // PublicPath is the full path used for public pages
        PublicPath: publicPath,
        // FinishURL is where we will navigate after this Task
        FinishURL: finishURL,
        // TableInitialSearch is the initial value for the search bar for a Task containing a Table.
        TableInitialSearch: tableInitialSearch,
        // TableInitialOrderColumn is the initial column index (indexing starts at 0) to sort.
        TableInitialOrderColumn: tableInitialOrderColumn,
        // TableInitialLastDocumentID is the initial document ID to start after when searching the database.
        TableInitialLastDocumentID: tableInitialLastDocumentID,
        // WantDocuments are arrays of FirestoreIDs of documents the client wants.
        WantDocuments: {
            Data: [],
            Role: [],
            Schema: [],
            Task: [],
            User: [],
            UX: [],
        }
    }
}

function getIndustry(POSTBody) {
    if (POSTBody.OverrideIndustryID !== '' && POSTBody.OverrideIndustryID !== 'polyappNone') {
        return POSTBody.OverrideIndustryID;
    }
    return POSTBody.IndustryID;
}
function getDomain(POSTBody) {
    if (POSTBody.OverrideDomainID !== '' && POSTBody.OverrideDomainID !== 'polyappNone') {
        return POSTBody.OverrideDomainID;
    }
    return POSTBody.DomainID;
}
function getTask(POSTBody) {
    if (POSTBody.OverrideTaskID !== '' && POSTBody.OverrideTaskID !== 'polyappNone') {
        return POSTBody.OverrideTaskID;
    }
    return POSTBody.TaskID;
}

// accordionLinkClick is coupled to the exact structure of GenListAccordion.
function accordionLinkClick(event) {
    if (event.target.nodeName !== 'A') {
        event.target = event.target.parentNode;
        accordionLinkClick(event);
    }
    let accordionHeader = event.target.parentNode;
    let polyappRefNode = accordionHeader.nextElementSibling;
    let splitTarget = [polyappRefNode.id];
    if (polyappRefNode.id.includes('_')) {
        // relevant if this is in an array.
        splitTarget = polyappRefNode.id.split('_');
    }
    let url = splitTarget[0].substr(('polyappRef').length);
    url = url.replace(/%2F/g, '/');
    url = url.replace(/%3F/g, '?');
    location.href = url;
}

// refLinkClick is coupled to the exact structure of GenLabeledInput.
function refLinkClick(event) {
    if (event.target.nodeName !== 'A') {
        event.target = event.target.parentNode;
        refLinkClick(event);
    }
    let inputWithRefValue = event.target.parentNode.nextElementSibling;
    if (inputWithRefValue == null || inputWithRefValue.value == null) {
        console.error('could not follow link in child Ref');
    } else if (inputWithRefValue.value !== "") {
        location.href = inputWithRefValue.value;
    }
    // else the Ref is empty and there is nowhere to navigate to.
}

// TODO add a state changed listener to wrap this code so it only runs
// if the app is online.

// Initialize Firebase
var firebaseInitialized = false;
// This is the same as some code in sw.js
try {
    if (location.hostname === firebaseConfigDev.storageBucket || location.hostname === 'localhost') {
        firebase.initializeApp(firebaseConfigDev);
    } else if (location.hostname === firebaseConfigStaging.storageBucket) {
        firebase.initializeApp(firebaseConfigStaging);
    } else if (location.hostname === firebaseConfigProd.storageBucket) {
        firebase.initializeApp(firebaseConfigProd);
    } else {
        alert('location.hostname is not recognized. Did you configure main.js and sw.js to enable Firebase Authentication on your domain?');
    }
    firebaseInitialized = true;
} catch (err) {
    // it's unclear what would cause this to happen; firebase initializes OK when offline.
    console.log(err);
    alert('Firebase failed to start. Reloading this page.');
    location.reload();
}

// Use an observer to track if the user is signed in.
// This event will fire after firebase initializes even if the device is offline.
firebase.auth().onAuthStateChanged(function(user) {
    if (user && !user.isAnonymous) {
        userIsSignedIn();
    } else if (firebase.auth().currentUser != null) {
        userIsSignedIn();
    } else {
        // it is possible the user was just signed in via redirect
        firebase.auth().getRedirectResult().then(function(result) {
            if (result.user) {
                // they were signed in after all
                userIsSignedIn();
            } else {
                // user is not signed in
                // Run authentication code
                let authCanary = document.getElementById('polyappJSNeedFullAuthCanary');
                if (authCanary) {
                    // We want to force a full sign-in if you're accessing privacy-heavy sites or pages.
                    // If you didn't do this, the sites would reject your credentials or not send down key information.
                    location.href = '/signIn';
                } else {
                    // create an anonymous user instead of forcing everyone to sign in.
                    // If users choose to sign in, they can do so from the Options menu in the upper right hand corner.
                    //
                    // Now you are probably confused why this is even a supported piece of functionality. Why doesn't everyone
                    // always have to sign in? What happened was I added some public pages to Polyapp which anyone could
                    // access as part of building the website builder. And then I needed a way to let anyone view those
                    // pages. Now that I have the feature, I rather like it so I've decided to keep it.
                    firebase.auth().signInAnonymously().catch(function(error) {
                        console.error(error.code);
                        console.error(error.message);
                    });
                }
                userIsSignedOut();
            }
        });
    }
});

// signInGoogle requests that a user sign in.
//
// We are restricted to using the redirect sign in method for a variety of reasons. The most important one is the use
// of Service Worker Sessions in Firebase Auth: https://firebase.google.com/docs/auth/web/service-worker-sessions
//
// TODO use https://firebase.google.com/docs/auth/web/google-signin#expandable-2 instead so this sign-in works
// in Incognito mode.
function signInGoogle() {
    let user = firebase.auth().currentUser;
    if (user != null && user.isAnonymous != null && !user.isAnonymous) {
        // user is already signed in
        return;
    }
    // whenever we want to sign in, regardless of if an error is thrown fast or slow, we should always block user input.
    document.getElementById('polyappJSSpinner').style.display = 'block';
    // if we do not have any credentials from the redirect result then try signing in.
    let googleProvider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase.auth().signInWithRedirect(googleProvider);
    // at this point the page navigates away so there is no need to drop the spinner.
}

// signInOAuth requests that a user sign in with an arbitrary provider.
function signInOAuth(providerString) {
    let user = firebase.auth().currentUser;
    if (user != null && user.isAnonymous != null && !user.isAnonymous) {
        // user is already signed in
        return;
    }
    // whenever we want to sign in, regardless of if an error is thrown fast or slow, we should always block user input.
    document.getElementById('polyappJSSpinner').style.display = 'block';
    // if we do not have any credentials from the redirect result then try signing in.
    let provider = new firebase.auth.OAuthProvider(providerString);
    firebase.auth().useDeviceLanguage();
    firebase.auth().signInWithRedirect(provider);
    // at this point the page navigates away so there is no need to drop the spinner.
}

function signInLocal() {
    let user = firebase.auth().currentUser;
    if (user != null && user.isAnonymous != null && !user.isAnonymous) {
        // user is already signed in
        location.href = '/';
        return;
    }
    let email = document.getElementById('polyappSignInEmail');
    let password = document.getElementById('polyappSignInPassword');
    if (email == null || password == null) {
        return;
    }
    document.getElementById('polyappJSSpinner').style.display = 'block';
    firebase.auth().signInWithEmailAndPassword(email.value, password.value).then((userCredential) => {
        let user = userCredential.user;
        location.href = '/';
    }).catch((error) => {
        let errorCode = error.code;
        let errorMessage = error.message;
        document.getElementById('signInButton').insertAdjacentHTML('afterend', '<p>Sign in failed. Error code: ' + errorCode + ' Error message: ' + errorMessage);
    });
}

// userIsSignedIn can be called multiple times. It configures the UI after a sign-in is complete.
function userIsSignedIn() {
    let signInButton = document.getElementById('polyappJSSignIn');
    let signOutButton = document.getElementById('polyappJSSignOut');
    if (signInButton != null) {
        signInButton.setAttribute('class', 'displayNone');
    }
    if (signOutButton != null) {
        signOutButton.setAttribute('class', 'dropdown-item pointer');
    }
}

// userIsSignedOut can be called multiple times. It configures the UI after a sign-out occurs.
function userIsSignedOut() {
    let signInButton = document.getElementById('polyappJSSignIn');
    let signOutButton = document.getElementById('polyappJSSignOut');
    if (signInButton != null) {
        signInButton.setAttribute('class', 'dropdown-item pointer');
    }
    if (signOutButton != null) {
        signOutButton.setAttribute('class', 'displayNone');
    }
}

// signOut of the firebase user.
function signOut() {
    firebase.auth().signOut().then(function() {
        // Sign-out successful. We must clear the page content so navigate home.
        userIsSignedOut();
        location.href = '/signIn'
    }).catch(function(error) {
        console.error(error);
    });
}

// call savingNow when we are saving.
function savingNow() {
    // seems hacky to set the style this way
    // https://stackoverflow.com/questions/462537/overriding-important-style
    document.getElementById('polyappJSAutosaveSpinner').style.cssText='display:';
}

// call notSavingNow when we are no longer saving.
function notSavingNow() {
    document.getElementById('polyappJSAutosaveSpinner').style.cssText='display:none !important;';
}

