// ----------------- IMPORTANT! CONFIGURATION INFORMATION IS BELOW! ----------------
let firebaseConfigDev = {
    apiKey: "AIzaSyBSpSv7JdvWAle9sPwTNT8zItaVmtiiEQ4",
    authDomain: "high-sunlight-232214.firebaseapp.com",
    databaseURL: "https://high-sunlight-232214.firebaseio.com",
    projectId: "high-sunlight-232214",
    storageBucket: "high-sunlight-232214.appspot.com",
    messagingSenderId: "897117818320",
    appId: "1:897117818320:web:e692cd28de93610465fdd5"
};
let firebaseConfigStaging = {
    apiKey: "AIzaSyBHRyj9r5ocjbenymPYy8n_NJpMLCX64Pc",
    authDomain: "polyapp-staging.firebaseapp.com",
    databaseURL: "https://polyapp-staging.firebaseio.com",
    projectId: "polyapp-staging",
    storageBucket: "polyapp-staging.appspot.com",
    messagingSenderId: "126309489837",
    appId: "1:126309489837:web:5f6946b0a2799b92567a24"
};
let firebaseConfigProd = {
    apiKey: "AIzaSyAkZytrZ1nwEU3qT4vTOlY1YNlpLgnU88w",
    authDomain: "polyapp-production.firebaseapp.com",
    databaseURL: "https://polyapp-production.firebaseio.com",
    projectId: "polyapp-production",
    storageBucket: "polyapp-production.appspot.com",
    messagingSenderId: "600835525955",
    appId: "1:600835525955:web:e0a1b9fb4dbeb9433ef3f6"
};
// --------------------------- END CONFIGURATION -----------------------------------

if (typeof openDB == "undefined") {
    self.importScripts('/idb/index-min.js');
}

if (typeof firebase === 'undefined') {
    self.importScripts('/firebase/firebase-app.js');
    self.importScripts('/firebase/firebase-auth.js');
}

// CACHE_DO_NOT_PERSIST must not include files which require Authentication.
// These files do not persist when you load a new service worker.
var CACHE_DO_NOT_PERSIST = [
    '/manifest.json',
    // JS
    //'/sw.js' requests to service workers aren't intercepted by service workers.
    '/main.js',
    // Custom CSS
    '/index.css',
    // Things in the assets folder
    '/assets/giraffe192.jpg', // favicon.ico
    '/assets/icons/settings-20px.svg',
    '/assets/icons/help-24px.svg',
    '/assets/icons/search-24px.svg',
    '/assets/icons/chat-24px.svg',
    '/assets/icons/baseline-feedback-24px.svg',
    '/assets/icons/more_vert-24px.svg',
    '/assets/icons/expand_less-24px.svg',
    '/assets/icons/expand_more-24px.svg'
];

// CACHE_AUTH_DO_NOT_PERSIST are files which require Authorization to be set to cache but which we do want to cache.
// These files are saved after they are loaded instead of being precached by the service worker.
var CACHE_AUTH_DO_NOT_PERSIST = [
    '/serviceworker.wasm'
];

// CACHE_PERSIST are files not requiring authentication which are extremely unlikely to change when Polyapp is updated.
var CACHE_PERSIST = [
    '/robots.txt',
    '/idb/index-min.js',
    '/firebase/firebase-app.js',
    '/firebase/firebase-auth.js',
    '/firebaseui/firebaseui.js',
    '/firebaseui/firebaseui.css',
    // Bootstrap JS
    '/jquery/jquery.slim.min.js',
    '/bootstrap/bootstrap.bundle.min.js',
    // Bootstrap CSS
    '/bootstrap/bootstrap.min.css',
    '/bootstrap/bootstrap-grid.min.css',
    '/bootstrap/bootstrap-reboot.min.css',
    // Utilities
    '/html5sortable/html5sortable.min.js'
];

var CACHE_DO_NOT_PERSIST_NAME = 'required0.2';
var CACHE_AUTH_DO_NOT_PERSIST_NAME = 'auth0.1';
var CACHE_PERSIST_NAME = 'unimportant0.2';

// Install event activates as quickly as possible. This is done because if the service worker's fetch interceptor
// is not active the client won't be able to request anything which requires authentication.
self.addEventListener('install', (event) => {
    return self.skipWaiting();
});

// Activate event must clean up the older service worker's caches and WASM code.
self.addEventListener('activate', (event) => {
    const cacheKeepList = [CACHE_PERSIST_NAME];
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (cacheKeepList.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }))
        }).then(() => {
            // TODO reenable WASM when I figure out how to secure it.
            // initWASM();
            // set this worker as the active worker for all clients & trigger oncontrollerchange event
            // this means we're pushing out the old service worker ASAP
            return self.clients.claim();
        })
    );
});

// The 'fetch' event listener routes requests in a way which is supposed to get them replied to as quickly as possible.
self.addEventListener('fetch', function(event) {
    event.respondWith(caches.match(event.request).then((response) => {
        if (response) {
            return response;
        }
        if (event.request.url.startsWith('https://securetoken.googleapis.com') || event.request.url.startsWith('https://www.googleapis.com')) {
            // Firebase Auth. This is needed for getIdToken() to work so to prevent recursion we short-circuit and GET.
            return fetch(event.request);
        }

        // This is the main way we are servicing requests. We need to perform several tasks:
        // 1. Ensure the request is to our current origin (aka to Polaypp)
        // 2. Get an ID token and place it in the Authorization header only if auth is necessary for this request.
        // 3. If it's a GET request, try using the GET request handling WASM code. Else fetch to server.
        // 4. If it's a POST request, try using the POST request handling WASM code. Else fetch to server.
        let req = event.request.clone();
        let backupClone = null;
        return getIdToken().then((idToken) => {
            let processRequestPromise = Promise.resolve();

            // 1. Ensure the request is to our current origin. If not fetch normally.
            if (self.location.origin === getOriginFromUrl(event.request.url) &&
                (self.location.protocol === 'https:') && idToken) {

                // 2. Use the ID token to create the Authorization header only if auth is necessary for this request.
                // https://firebase.google.com/docs/auth/web/service-worker-sessions
                let url = new URL(req.url);
                if (!(CACHE_DO_NOT_PERSIST.includes(url.pathname)) && !(CACHE_PERSIST.includes(url.pathname))) {
                    // Clone headers as request headers are immutable.
                    const headers = new Headers();
                    for (let entry of req.headers.entries()) {
                        headers.append(entry[0], entry[1]);
                    }
                    // Add ID token to header.
                    headers.delete('Authorization');
                    headers.append('Authorization', 'Bearer ' + idToken);
                    processRequestPromise = getBodyContent(req).then((body) => {
                        try {
                            req = new Request(req.url, {
                                method: req.method,
                                headers: headers,
                                mode: 'same-origin',
                                credentials: req.credentials,
                                cache: req.cache,
                                redirect: req.redirect,
                                referrer: req.referrer,
                                body: body,
                                bodyUsed: req.bodyUsed,
                                context: req.context,
                                integrity: req.integrity, // may not be necessary
                                keepalive: req.keepalive, // may not be necessary
                                referrerPolicy: req.referrerPolicy, // may not be necessary
                                signal: req.signal, // may not be necessary
                                window: req.window, // may not be necessary
                            });
                            // re-clone the request in case processing fails while the request is still being worked on.
                            const headersBackupClone = new Headers();
                            for (let entry of req.headers.entries()) {
                                headersBackupClone.append(entry[0], entry[1]);
                            }
                            // Why are these next two lines necessary when we should by copying above? I have no idea.
                            headersBackupClone.delete('Authorization');
                            headersBackupClone.append('Authorization', 'Bearer ' + idToken);

                            backupClone = new Request(req.url, {
                                method: req.method,
                                headers: headersBackupClone,
                                mode: 'same-origin',
                                credentials: req.credentials,
                                cache: req.cache,
                                redirect: req.redirect,
                                referrer: req.referrer,
                                body: body,
                                bodyUsed: req.bodyUsed,
                                context: req.context,
                                integrity: req.integrity, // may not be necessary
                                keepalive: req.keepalive, // may not be necessary
                                referrerPolicy: req.referrerPolicy, // may not be necessary
                                signal: req.signal, // may not be necessary
                                window: req.window, // may not be necessary
                            });

                        } catch (e) {
                            // This will fail for CORS requests. We just continue with the
                            // fetch caching logic below and do not pass the ID token.
                        }
                    });
                }

                return processRequestPromise.then(() => {
                    // 3. If it's a GET request, try using the GET request handling WASM code. Else fetch to server.
                    if ((event.request.method === 'GET' || event.request.method === 'get') &&
                        self.GETHandler != null && !event.request.url.includes('.')) {

                        return Promise.all([caches.match('/'), new Promise((resolve, reject) => {
                            // This calls WASM.
                            let page = self.GETHandler(req);
                            if (page == null) {
                                reject('failure in GETHandler');
                                return;
                            }
                            if (!page.HTML) {
                                reject('HTML not set in GETHandler');
                                return;
                            }
                            resolve(page);
                        })]).then((results) => {
                            if (results == null || results.length !== 2) {
                                return Promise.reject(new Error('could not get dependencies'));
                            }
                            if (results[1].RedirectURL) {
                                // TODO verify this works
                                let headers = new Headers();
                                headers.set("access-control-allow-credentials", "true");
                                headers.set("content-length", '0');
                                headers.set("date", new Date().toUTCString());
                                headers.set("location", results[1].RedirectURL)
                                headers.set("strict-transport-security", "max-age=31536000");
                                // Set Origin for https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
                                headers.set("vary", "Accept-Encoding");
                                headers.set("x-content-type-options", "nosniff");
                                headers.set("x-frame-options", "SAMEORIGIN");
                                headers.set("x-xss-protection", "1; mode=block");
                                // Response.redirect does not have any mobile browser support
                                let response = new Response(null, {
                                    status: 307,
                                    statusText: "OK",
                                    headers: headers,
                                });
                                return Promise.resolve(response);
                            }
                            let combinedHTML = innerIntoAppShell(results[1].HTML, results[0]);
                            if (combinedHTML == null) {
                                return Promise.reject(new Error('failed innerIntoAppShell'));
                            }
                            return combinedHTML.then((finalHTML) => {
                                if (finalHTML == null) {
                                    return Promise.reject('finalHTML was null');
                                }
                                let headers = new Headers();
                                headers.set("access-control-allow-credentials", "true");
                                headers.set("content-length", finalHTML.length);
                                headers.set("content-type", "text/html; charset=UTF-8");
                                headers.set("Date", new Date().toUTCString());
                                headers.set("strict-transport-security", "max-age=31536000");
                                // Set Origin for https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
                                headers.set("vary", "Accept-Encoding, Origin");
                                headers.set("x-content-type-options", "nosniff");
                                headers.set("x-frame-options", "SAMEORIGIN");
                                headers.set("x-xss-protection", "1; mode=block");
                                let response = new Response(finalHTML, {
                                    status: 200,
                                    statusText: "OK",
                                    headers: headers,
                                });
                                return Promise.resolve(response);
                            });
                        }).catch((err) => {
                            console.log(err);
                            return fetch(req);
                        });
                    }
                    // 4. If it's a POST request, try using the POST request handling WASM code. Else fetch to server.
                    else if ((event.request.method === 'POST' || event.request.method === 'post') && self.POSTHandler != null) {
                        return req.json().then((json) => {
                            return new Promise((resolve, reject) => {
                                try {
                                    let body = self.POSTHandler(json);
                                    if (body == null) {
                                        reject('failure in POSTHandler');
                                        return;
                                    }
                                    let headers = new Headers();
                                    headers.set("Content-Type", "application/json");
                                    headers.set("Date", new Date().toUTCString());
                                    let response = new Response(JSON.stringify(body), {
                                        status: 200,
                                        statusText: "OK",
                                        headers: headers,
                                    });
                                    resolve(response);
                                } catch (err) {
                                    reject(err);
                                }
                            });
                        });
                    }
                    // This is the 'else' fallback for both GET and POST requests.
                    // CACHE_AUTH_DO_NOT_PERSIST requests always find their way here.
                    return fetch(req).then((response) => {
                        // we won't be able to populate the caches if we have no way to make the request.
                        if (backupClone != null) {
                            let url = new URL(req.url);
                            // These files must not be in a cache or else the code never gets this far.
                            if (CACHE_AUTH_DO_NOT_PERSIST.includes(url.pathname)) {
                                caches.open(CACHE_AUTH_DO_NOT_PERSIST_NAME).then((cache) => {
                                    return fetch(req).then((resp) => {
                                        return cache.put(req, resp);
                                    });
                                });
                            } else if (CACHE_DO_NOT_PERSIST.includes(url.pathname)) {
                                caches.open(CACHE_DO_NOT_PERSIST_NAME).then((cache) => {
                                    return fetch(req).then((resp) => {
                                        return cache.put(req, resp);
                                    });
                                });
                            } else if (CACHE_PERSIST.includes(url.pathname)) {
                                caches.open(CACHE_PERSIST).then((cache) => {
                                    return fetch(req).then((resp) => {
                                        return cache.put(req, resp);
                                    });
                                });
                            }
                        }
                        // server-side logic sometimes gives us documents we want.
                        // TODO put this logic into Go so I can use Go's Simplify and Init functions for these objects.
                        let clone = response.clone();
                        clone.json().then((body) => {
                            if (body == null) {
                                return response;
                            }
                            if (body.DataDocuments != null) {
                                body.DataDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("data", id, doc);
                                });
                            }
                            if (body.RoleDocuments != null) {
                                body.RoleDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("role", id, doc);
                                });
                            }
                            if (body.SchemaDocuments != null) {
                                body.SchemaDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("schema", id, doc);
                                });
                            }
                            if (body.TaskDocuments != null) {
                                body.TaskDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("task", id, doc);
                                });
                            }
                            if (body.UserDocuments != null) {
                                body.UserDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("user", id, doc);
                                });
                            }
                            if (body.UXDocuments != null) {
                                body.UXDocuments.forEach((doc) => {
                                    let id = doc.FirestoreID;
                                    delete doc.FirestoreID;
                                    createDB("ux", id, doc);
                                });
                            }
                        }).catch(() => {
                            // probably just a non-JSON body
                            return response;
                        });
                        return response;
                    });
                });
            } else if (idToken == null) {
                console.log('idToken was null; perhaps auth failed');
                return fetch(req);
            } else {
                // This is not a same-origin request so just reply with a regular fetch.
                return fetch(req);
            }
        }).catch((error) => {
            console.error(error);
            if (backupClone != null) {
                // If POSTHandler fails it falls back here.
                return fetch(backupClone);
            }
            // If there's a failure prior to backupClone being constructed there's a serious problem.
            // By sending the request to the server, we'll at least get a log on the server of this problem.
            return fetch(event.request);
        });
    }));
});

// innerIntoAppShell returns the string form of the DOM after placing innerHTML into the appShell's text at main.
// TODO write tests for this
async function innerIntoAppShell(innerHTML, appShell) {
    if (appShell != null && innerHTML != null) {
        return appShell.text().then((appShellText) => {
            let beginning = appShellText.toString().split('id="polyappJSMain">', 2);
            if (beginning.length !== 2) {
                return null;
            }
            let ending = appShellText.toString().split('</main>', 2);
            if (ending.length !== 2) {
                return null;
            }
            return beginning[0] + 'id="polyappJSMain">' + innerHTML + '</main>' + ending[1];
        });

    }
    return null;
}

// this is the same as some code in init-firebase.js
try {
    if (location.hostname === firebaseConfigDev.storageBucket || location.hostname === 'localhost') {
        firebase.initializeApp(firebaseConfigDev);
    } else if (location.hostname === firebaseConfigStaging.storageBucket) {
        firebase.initializeApp(firebaseConfigStaging);
    } else if (location.hostname === firebaseConfigProd.storageBucket) {
        firebase.initializeApp(firebaseConfigProd);
    } else {
        alert('location.hostname is not recognized. Did you configure main.js and sw.js to enable Firebase Authentication on your domain?');
    }
} catch (err) {
    // it's unclear what would cause this to happen; firebase initializes OK when offline.
    console.log(err);
}
const getIdToken = () => {
    return new Promise((resolve) => {
        const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            unsubscribe();
            if (user) {
                user.getIdToken().then((idToken) => {
                    resolve(idToken);
                }, (error) => {
                    console.log(error);
                    resolve(null);
                });
            } else {
                resolve(null);
            }
        });
    });
};

const getOriginFromUrl = (url) => {
    // https://stackoverflow.com/questions/1420881/how-to-extract-base-url-from-a-string-in-javascript
    const pathArray = url.split('/');
    const protocol = pathArray[0];
    const host = pathArray[2];
    return protocol + '//' + host;
};

// Get underlying body if available. Works for json, blob and text bodies.
// https://firebase.google.com/docs/auth/web/service-worker-sessions
const getBodyContent = (req) => {
    return Promise.resolve().then(() => {
        if (req.method !== 'GET') {
            let contentType = req.headers.get('Content-Type');
            if (contentType.indexOf('json') !== -1) {
                return req.json().then((json) => {
                    return JSON.stringify(json);
                });
            } else if (false) {
                return req.arrayBuffer().then((b) => {
                    return b;
                });
            } else if (contentType.indexOf('image') !== -1 || contentType.indexOf('application/') !== -1) {
                return req.blob().then((b) => {
                    return b;
                });
            } else if (contentType.indexOf('multipart/form-data') !== -1) {
                return req.formData().then((f) => {
                    return f;
                });
            } else {
                return req.text();
            }
        }
    }).catch((error) => {
        // Ignore error.
    });
};

// IDB APIs
// There is one DB per user since different users could get same access to IndexedDB on same computer.
// For each DB, a 'collection' in Firestore -> a 'StoreName' in IndexedDB.
// TODO when a user tries to access a DB, authenticate them to ensure they are
// trying to access their own DB.
self.allDBs = new Map();
async function initDB(user) {
    let db = self.allDBs.get(user);
    if (db != null) {
        return db;
    }
    db = await idb.openDB(user, 2, {
        upgrade(db) {
            // strings for object store names are coupled to unique/constants.go where 'data', etc. are defined.
            const storeData = db.createObjectStore("data", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            storeData.createIndex('polyappIndustryID', 'polyappIndustryID', {unique:false});
            storeData.createIndex('polyappDomainID', 'polyappDomainID', {unique:false});
            storeData.createIndex('polyappSchemaID', 'polyappSchemaID', {unique:false});
            db.createObjectStore("role", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            const storeSchema = db.createObjectStore("schema", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            storeSchema.createIndex('polyappIndustryID', 'polyappIndustryID', {unique:false});
            storeSchema.createIndex('polyappDomainID', 'polyappDomainID', {unique:false});
            storeSchema.createIndex('polyappSchemaID', 'polyappSchemaID', {unique:false});
            const storeTask = db.createObjectStore("task", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            storeTask.createIndex('polyappIndustryID', 'polyappIndustryID', {unique:false});
            storeTask.createIndex('polyappDomainID', 'polyappDomainID', {unique:false});
            storeTask.createIndex('polyappSchemaID', 'polyappSchemaID', {unique:false});
            db.createObjectStore("user", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            const storeUX = db.createObjectStore("ux", {
                keyPath: 'FirestoreID', // primary key
                autoIncrement: false, // must generate our own random IDs
            });
            storeUX.createIndex('polyappIndustryID', 'polyappIndustryID', {unique:false});
            storeUX.createIndex('polyappDomainID', 'polyappDomainID', {unique:false});
            storeUX.createIndex('polyappSchemaID', 'polyappSchemaID', {unique:false});
        },
    });
    self.allDBs.set(user, db);
    return db;
}

// TODO do not use 'a', use the ID of the signed in user instead. return null if no user is signed in.
// createDB creates an object @ primary key = firestoreID in the user's DB with storeName = collection
async function createDB(collection, firestoreID, data) {
    let idbRequest = (await initDB('a')).put(collection, data, firestoreID);
    let errOut = null;
    await idbRequest.catch((err) => {
        errOut = err.toString();
    });
    return errOut;
}
async function readDB(collection, firestoreID) {
    let idbRequest = (await initDB('a')).get(collection, firestoreID);
    let errOut = null;
    const value = await idbRequest.catch((err) => {
        errOut = err.toString();
    });
    errOut = await errOut;
    return [value, errOut];
}
async function updateDB(collection, firestoreID, data) {
    let originalData = (await initDB('a')).get(collection, firestoreID);
    if (!originalData) {
        return "code = NotFound"
    }
    // TODO this is broken for complex objects
    let mergedData = Object.assign(originalData, data);
    let idbRequest = (await initDB('a')).put(collection, mergedData, firestoreID);
    let errOut = null;
    await idbRequest.catch((err) => {
        errOut = err.toString();
    });
    errOut = await errOut;
    return errOut;
}
async function deleteDB(collection, firestoreID) {
    let idbRequest = (await initDB('a')).delete(collection, firestoreID);
    let errOut = null;
    await idbRequest.catch((err) => {
        errOut = err.toString();
    });
    return errOut;
}
async function deprecateDB(collection, firestoreID) {
    let data = {
        polyappDeprecated: true,
    };
    return updateDB(collection, firestoreID, data);
}

// Add the following polyfill for Microsoft Edge 17/18 support:
// <script src="https://cdn.jsdelivr.net/npm/text-encoding@0.7.0/lib/encoding.min.js"></script>
// (see https://caniuse.com/#feat=textencoder)
if (!WebAssembly.instantiateStreaming) { // polyfill
    WebAssembly.instantiateStreaming = async (resp, importObject) => {
        const source = await (await resp).arrayBuffer();
        return await WebAssembly.instantiate(source, importObject);
    };
}

let numInitWASMCalls = 0;
function initWASM() {
    let exitCode = 0;
    function goExit(code) {
        exitCode = code;
    }

    const init = (async() => {
        console.log('init WASM');
        const go = new Go();
        // pull WASM exit code into app logic
        go.exit = goExit;

        let mod, inst;
        let authorizationHeader = 'Bearer ';
        await getIdToken().then((idToken) => {
            authorizationHeader += idToken;
        });
        if (authorizationHeader === 'Bearer ') {
            // will automatically re-try in the loop below.
            return;
        }

        // No CSRF is OK here since requesting serviceworker.wasm does not cause an 'action' to be taken by the system
        // and no database state is updated.
        await WebAssembly.instantiateStreaming(fetch('/serviceworker.wasm', {
            method: 'GET',
            headers: {
                'Authorization': authorizationHeader
            }
        }), go.importObject).then(async (result) => {
            mod = result.module;
            inst = result.instance;
            await go.run(inst);
            console.log('go program terminated');
            // I expect go.run to run forever
            // if we reach here, the app has crashed or the test suite has finished
        }).catch((err) => {
            console.error(err);
        });
    });

    (async () => {
        await init();
        // if we get here, initialization failed so let's try again.
        // This does stack up callbacks, 2 per time.
        // We will tolerate a stack size of 2k, so about 1k iterations.
        // That works out to 2 seconds * 1k calls = 33 minutes.
        // info about limits: https://stackoverflow.com/questions/2805172/what-are-the-js-recursion-limits-for-firefox-chrome-safari-ie-etc
        setTimeout(() => {
            numInitWASMCalls++;
            if (numInitWASMCalls < 1000) {
                initWASM();
            } else {
                // unregister this service worker and force a page refresh.
                self.registration.unregister().then(() => {
                    return self.clients.matchAll();
                }).then((clients) => {
                    clients.forEach((client) => {
                        client.navigate(client.url);
                    });
                });
            }
        }, 2000);
    })();
}

// Copyright 2018 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Map multiple JavaScript environments to a single common API,
// preferring web standards over Node.js API.
//
// Environments considered:
// - Browsers
// - Node.js
// - Electron
// - Parcel

if (typeof global !== "undefined") {
    // global already exists
} else if (typeof window !== "undefined") {
    window.global = window;
} else if (typeof self !== "undefined") {
    self.global = self;
} else {
    throw new Error("cannot export Go (neither global, window nor self is defined)");
}

if (!global.require && typeof require !== "undefined") {
    global.require = require;
}

if (!global.fs && global.require) {
    global.fs = require("fs");
}

const enosys = () => {
    const err = new Error("not implemented");
    err.code = "ENOSYS";
    return err;
};

if (!global.fs) {
    let outputBuf = "";
    global.fs = {
        constants: { O_WRONLY: -1, O_RDWR: -1, O_CREAT: -1, O_TRUNC: -1, O_APPEND: -1, O_EXCL: -1 }, // unused
        writeSync(fd, buf) {
            outputBuf += decoder.decode(buf);
            const nl = outputBuf.lastIndexOf("\n");
            if (nl != -1) {
                console.log(outputBuf.substr(0, nl));
                outputBuf = outputBuf.substr(nl + 1);
            }
            return buf.length;
        },
        write(fd, buf, offset, length, position, callback) {
            if (offset !== 0 || length !== buf.length || position !== null) {
                callback(enosys());
                return;
            }
            const n = this.writeSync(fd, buf);
            callback(null, n);
        },
        chmod(path, mode, callback) { callback(enosys()); },
        chown(path, uid, gid, callback) { callback(enosys()); },
        close(fd, callback) { callback(enosys()); },
        fchmod(fd, mode, callback) { callback(enosys()); },
        fchown(fd, uid, gid, callback) { callback(enosys()); },
        fstat(fd, callback) { callback(enosys()); },
        fsync(fd, callback) { callback(null); },
        ftruncate(fd, length, callback) { callback(enosys()); },
        lchown(path, uid, gid, callback) { callback(enosys()); },
        link(path, link, callback) { callback(enosys()); },
        lstat(path, callback) { callback(enosys()); },
        mkdir(path, perm, callback) { callback(enosys()); },
        open(path, flags, mode, callback) { callback(enosys()); },
        read(fd, buffer, offset, length, position, callback) { callback(enosys()); },
        readdir(path, callback) { callback(enosys()); },
        readlink(path, callback) { callback(enosys()); },
        rename(from, to, callback) { callback(enosys()); },
        rmdir(path, callback) { callback(enosys()); },
        stat(path, callback) { callback(enosys()); },
        symlink(path, link, callback) { callback(enosys()); },
        truncate(path, length, callback) { callback(enosys()); },
        unlink(path, callback) { callback(enosys()); },
        utimes(path, atime, mtime, callback) { callback(enosys()); },
    };
}

if (!global.process) {
    global.process = {
        getuid() { return -1; },
        getgid() { return -1; },
        geteuid() { return -1; },
        getegid() { return -1; },
        getgroups() { throw enosys(); },
        pid: -1,
        ppid: -1,
        umask() { throw enosys(); },
        cwd() { throw enosys(); },
        chdir() { throw enosys(); },
    }
}

if (!global.crypto) {
    const nodeCrypto = require("crypto");
    global.crypto = {
        getRandomValues(b) {
            nodeCrypto.randomFillSync(b);
        },
    };
}

if (!global.performance) {
    global.performance = {
        now() {
            const [sec, nsec] = process.hrtime();
            return sec * 1000 + nsec / 1000000;
        },
    };
}

if (!global.TextEncoder) {
    global.TextEncoder = require("util").TextEncoder;
}

if (!global.TextDecoder) {
    global.TextDecoder = require("util").TextDecoder;
}

// End of polyfills for common API.

const encoder = new TextEncoder("utf-8");
const decoder = new TextDecoder("utf-8");

global.Go = class {
    constructor() {
        this.argv = ["js"];
        this.env = {};
        this.exit = (code) => {
            if (code !== 0) {
                console.warn("exit code:", code);
            }
        };
        this._exitPromise = new Promise((resolve) => {
            this._resolveExitPromise = resolve;
        });
        this._pendingEvent = null;
        this._scheduledTimeouts = new Map();
        this._nextCallbackTimeoutID = 1;

        const setInt64 = (addr, v) => {
            this.mem.setUint32(addr + 0, v, true);
            this.mem.setUint32(addr + 4, Math.floor(v / 4294967296), true);
        }

        const getInt64 = (addr) => {
            const low = this.mem.getUint32(addr + 0, true);
            const high = this.mem.getInt32(addr + 4, true);
            return low + high * 4294967296;
        }

        const loadValue = (addr) => {
            const f = this.mem.getFloat64(addr, true);
            if (f === 0) {
                return undefined;
            }
            if (!isNaN(f)) {
                return f;
            }

            const id = this.mem.getUint32(addr, true);
            return this._values[id];
        }

        const storeValue = (addr, v) => {
            const nanHead = 0x7FF80000;

            if (typeof v === "number") {
                if (isNaN(v)) {
                    this.mem.setUint32(addr + 4, nanHead, true);
                    this.mem.setUint32(addr, 0, true);
                    return;
                }
                if (v === 0) {
                    this.mem.setUint32(addr + 4, nanHead, true);
                    this.mem.setUint32(addr, 1, true);
                    return;
                }
                this.mem.setFloat64(addr, v, true);
                return;
            }

            switch (v) {
                case undefined:
                    this.mem.setFloat64(addr, 0, true);
                    return;
                case null:
                    this.mem.setUint32(addr + 4, nanHead, true);
                    this.mem.setUint32(addr, 2, true);
                    return;
                case true:
                    this.mem.setUint32(addr + 4, nanHead, true);
                    this.mem.setUint32(addr, 3, true);
                    return;
                case false:
                    this.mem.setUint32(addr + 4, nanHead, true);
                    this.mem.setUint32(addr, 4, true);
                    return;
            }

            let id = this._ids.get(v);
            if (id === undefined) {
                id = this._idPool.pop();
                if (id === undefined) {
                    id = this._values.length;
                }
                this._values[id] = v;
                this._goRefCounts[id] = 0;
                this._ids.set(v, id);
            }
            this._goRefCounts[id]++;
            let typeFlag = 1;
            switch (typeof v) {
                case "string":
                    typeFlag = 2;
                    break;
                case "symbol":
                    typeFlag = 3;
                    break;
                case "function":
                    typeFlag = 4;
                    break;
            }
            this.mem.setUint32(addr + 4, nanHead | typeFlag, true);
            this.mem.setUint32(addr, id, true);
        }

        const loadSlice = (addr) => {
            const array = getInt64(addr + 0);
            const len = getInt64(addr + 8);
            return new Uint8Array(this._inst.exports.mem.buffer, array, len);
        }

        const loadSliceOfValues = (addr) => {
            const array = getInt64(addr + 0);
            const len = getInt64(addr + 8);
            const a = new Array(len);
            for (let i = 0; i < len; i++) {
                a[i] = loadValue(array + i * 8);
            }
            return a;
        }

        const loadString = (addr) => {
            const saddr = getInt64(addr + 0);
            const len = getInt64(addr + 8);
            return decoder.decode(new DataView(this._inst.exports.mem.buffer, saddr, len));
        }

        const timeOrigin = Date.now() - performance.now();
        this.importObject = {
            go: {
                // Go's SP does not change as long as no Go code is running. Some operations (e.g. calls, getters and setters)
                // may synchronously trigger a Go event handler. This makes Go code get executed in the middle of the imported
                // function. A goroutine can switch to a new stack if the current stack is too small (see morestack function).
                // This changes the SP, thus we have to update the SP used by the imported function.

                // func wasmExit(code int32)
                "runtime.wasmExit": (sp) => {
                    const code = this.mem.getInt32(sp + 8, true);
                    this.exited = true;
                    delete this._inst;
                    delete this._values;
                    delete this._goRefCounts;
                    delete this._ids;
                    delete this._idPool;
                    this.exit(code);
                },

                // func wasmWrite(fd uintptr, p unsafe.Pointer, n int32)
                "runtime.wasmWrite": (sp) => {
                    const fd = getInt64(sp + 8);
                    const p = getInt64(sp + 16);
                    const n = this.mem.getInt32(sp + 24, true);
                    fs.writeSync(fd, new Uint8Array(this._inst.exports.mem.buffer, p, n));
                },

                // func resetMemoryDataView()
                "runtime.resetMemoryDataView": (sp) => {
                    this.mem = new DataView(this._inst.exports.mem.buffer);
                },

                // func nanotime1() int64
                "runtime.nanotime1": (sp) => {
                    setInt64(sp + 8, (timeOrigin + performance.now()) * 1000000);
                },

                // func walltime1() (sec int64, nsec int32)
                "runtime.walltime1": (sp) => {
                    const msec = (new Date).getTime();
                    setInt64(sp + 8, msec / 1000);
                    this.mem.setInt32(sp + 16, (msec % 1000) * 1000000, true);
                },

                // func scheduleTimeoutEvent(delay int64) int32
                "runtime.scheduleTimeoutEvent": (sp) => {
                    const id = this._nextCallbackTimeoutID;
                    this._nextCallbackTimeoutID++;
                    this._scheduledTimeouts.set(id, setTimeout(
                        () => {
                            this._resume();
                            while (this._scheduledTimeouts.has(id)) {
                                // for some reason Go failed to register the timeout event, log and try again
                                // (temporary workaround for https://github.com/golang/go/issues/28975)
                                console.warn("scheduleTimeoutEvent: missed timeout event");
                                this._resume();
                            }
                        },
                        getInt64(sp + 8) + 1, // setTimeout has been seen to fire up to 1 millisecond early
                    ));
                    this.mem.setInt32(sp + 16, id, true);
                },

                // func clearTimeoutEvent(id int32)
                "runtime.clearTimeoutEvent": (sp) => {
                    const id = this.mem.getInt32(sp + 8, true);
                    clearTimeout(this._scheduledTimeouts.get(id));
                    this._scheduledTimeouts.delete(id);
                },

                // func getRandomData(r []byte)
                "runtime.getRandomData": (sp) => {
                    crypto.getRandomValues(loadSlice(sp + 8));
                },

                // func finalizeRef(v ref)
                "syscall/js.finalizeRef": (sp) => {
                    const id = this.mem.getUint32(sp + 8, true);
                    this._goRefCounts[id]--;
                    if (this._goRefCounts[id] === 0) {
                        const v = this._values[id];
                        this._values[id] = null;
                        this._ids.delete(v);
                        this._idPool.push(id);
                    }
                },

                // func stringVal(value string) ref
                "syscall/js.stringVal": (sp) => {
                    storeValue(sp + 24, loadString(sp + 8));
                },

                // func valueGet(v ref, p string) ref
                "syscall/js.valueGet": (sp) => {
                    const result = Reflect.get(loadValue(sp + 8), loadString(sp + 16));
                    sp = this._inst.exports.getsp(); // see comment above
                    storeValue(sp + 32, result);
                },

                // func valueSet(v ref, p string, x ref)
                "syscall/js.valueSet": (sp) => {
                    Reflect.set(loadValue(sp + 8), loadString(sp + 16), loadValue(sp + 32));
                },

                // func valueDelete(v ref, p string)
                "syscall/js.valueDelete": (sp) => {
                    Reflect.deleteProperty(loadValue(sp + 8), loadString(sp + 16));
                },

                // func valueIndex(v ref, i int) ref
                "syscall/js.valueIndex": (sp) => {
                    storeValue(sp + 24, Reflect.get(loadValue(sp + 8), getInt64(sp + 16)));
                },

                // valueSetIndex(v ref, i int, x ref)
                "syscall/js.valueSetIndex": (sp) => {
                    Reflect.set(loadValue(sp + 8), getInt64(sp + 16), loadValue(sp + 24));
                },

                // func valueCall(v ref, m string, args []ref) (ref, bool)
                "syscall/js.valueCall": (sp) => {
                    try {
                        const v = loadValue(sp + 8);
                        const m = Reflect.get(v, loadString(sp + 16));
                        const args = loadSliceOfValues(sp + 32);
                        const result = Reflect.apply(m, v, args);
                        sp = this._inst.exports.getsp(); // see comment above
                        storeValue(sp + 56, result);
                        this.mem.setUint8(sp + 64, 1);
                    } catch (err) {
                        storeValue(sp + 56, err);
                        this.mem.setUint8(sp + 64, 0);
                    }
                },

                // func valueInvoke(v ref, args []ref) (ref, bool)
                "syscall/js.valueInvoke": (sp) => {
                    try {
                        const v = loadValue(sp + 8);
                        const args = loadSliceOfValues(sp + 16);
                        const result = Reflect.apply(v, undefined, args);
                        sp = this._inst.exports.getsp(); // see comment above
                        storeValue(sp + 40, result);
                        this.mem.setUint8(sp + 48, 1);
                    } catch (err) {
                        storeValue(sp + 40, err);
                        this.mem.setUint8(sp + 48, 0);
                    }
                },

                // func valueNew(v ref, args []ref) (ref, bool)
                "syscall/js.valueNew": (sp) => {
                    try {
                        const v = loadValue(sp + 8);
                        const args = loadSliceOfValues(sp + 16);
                        const result = Reflect.construct(v, args);
                        sp = this._inst.exports.getsp(); // see comment above
                        storeValue(sp + 40, result);
                        this.mem.setUint8(sp + 48, 1);
                    } catch (err) {
                        storeValue(sp + 40, err);
                        this.mem.setUint8(sp + 48, 0);
                    }
                },

                // func valueLength(v ref) int
                "syscall/js.valueLength": (sp) => {
                    setInt64(sp + 16, parseInt(loadValue(sp + 8).length));
                },

                // valuePrepareString(v ref) (ref, int)
                "syscall/js.valuePrepareString": (sp) => {
                    const str = encoder.encode(String(loadValue(sp + 8)));
                    storeValue(sp + 16, str);
                    setInt64(sp + 24, str.length);
                },

                // valueLoadString(v ref, b []byte)
                "syscall/js.valueLoadString": (sp) => {
                    const str = loadValue(sp + 8);
                    loadSlice(sp + 16).set(str);
                },

                // func valueInstanceOf(v ref, t ref) bool
                "syscall/js.valueInstanceOf": (sp) => {
                    this.mem.setUint8(sp + 24, loadValue(sp + 8) instanceof loadValue(sp + 16));
                },

                // func copyBytesToGo(dst []byte, src ref) (int, bool)
                "syscall/js.copyBytesToGo": (sp) => {
                    const dst = loadSlice(sp + 8);
                    const src = loadValue(sp + 32);
                    if (!(src instanceof Uint8Array)) {
                        this.mem.setUint8(sp + 48, 0);
                        return;
                    }
                    const toCopy = src.subarray(0, dst.length);
                    dst.set(toCopy);
                    setInt64(sp + 40, toCopy.length);
                    this.mem.setUint8(sp + 48, 1);
                },

                // func copyBytesToJS(dst ref, src []byte) (int, bool)
                "syscall/js.copyBytesToJS": (sp) => {
                    const dst = loadValue(sp + 8);
                    const src = loadSlice(sp + 16);
                    if (!(dst instanceof Uint8Array)) {
                        this.mem.setUint8(sp + 48, 0);
                        return;
                    }
                    const toCopy = src.subarray(0, dst.length);
                    dst.set(toCopy);
                    setInt64(sp + 40, toCopy.length);
                    this.mem.setUint8(sp + 48, 1);
                },

                "debug": (value) => {
                    console.log(value);
                },
            }
        };
    }

    async run(instance) {
        this._inst = instance;
        this.mem = new DataView(this._inst.exports.mem.buffer);
        this._values = [ // JS values that Go currently has references to, indexed by reference id
            NaN,
            0,
            null,
            true,
            false,
            global,
            this,
        ];
        this._goRefCounts = []; // number of references that Go has to a JS value, indexed by reference id
        this._ids = new Map();  // mapping from JS values to reference ids
        this._idPool = [];      // unused ids that have been garbage collected
        this.exited = false;    // whether the Go program has exited

        // Pass command line arguments and environment variables to WebAssembly by writing them to the linear memory.
        let offset = 4096;

        const strPtr = (str) => {
            const ptr = offset;
            const bytes = encoder.encode(str + "\0");
            new Uint8Array(this.mem.buffer, offset, bytes.length).set(bytes);
            offset += bytes.length;
            if (offset % 8 !== 0) {
                offset += 8 - (offset % 8);
            }
            return ptr;
        };

        const argc = this.argv.length;

        const argvPtrs = [];
        this.argv.forEach((arg) => {
            argvPtrs.push(strPtr(arg));
        });
        argvPtrs.push(0);

        const keys = Object.keys(this.env).sort();
        keys.forEach((key) => {
            argvPtrs.push(strPtr(`${key}=${this.env[key]}`));
        });
        argvPtrs.push(0);

        const argv = offset;
        argvPtrs.forEach((ptr) => {
            this.mem.setUint32(offset, ptr, true);
            this.mem.setUint32(offset + 4, 0, true);
            offset += 8;
        });

        this._inst.exports.run(argc, argv);
        if (this.exited) {
            this._resolveExitPromise();
        }
        await this._exitPromise;
    }

    _resume() {
        if (this.exited) {
            throw new Error("Go program has already exited");
        }
        this._inst.exports.resume();
        if (this.exited) {
            this._resolveExitPromise();
        }
    }

    _makeFuncWrapper(id) {
        const go = this;
        return function () {
            const event = { id: id, this: this, args: arguments };
            go._pendingEvent = event;
            go._resume();
            return event.result;
        };
    }
}

if (
    global.require &&
    global.require.main === module &&
    global.process &&
    global.process.versions &&
    !global.process.versions.electron
) {
    if (process.argv.length < 3) {
        console.error("usage: go_js_wasm_exec [wasm binary] [arguments]");
        process.exit(1);
    }

    const go = new Go();
    go.argv = process.argv.slice(2);
    go.env = Object.assign({ TMPDIR: require("os").tmpdir() }, process.env);
    go.exit = process.exit;
    WebAssembly.instantiate(fs.readFileSync(process.argv[2]), go.importObject).then((result) => {
        process.on("exit", (code) => { // Node.js exits if no event handler is pending
            if (code === 0 && !go.exited) {
                // deadlock, make Go print error and stack traces
                go._pendingEvent = { id: 0 };
                go._resume();
            }
        });
        return go.run(result.instance);
    }).catch((err) => {
        console.error(err);
        process.exit(1);
    });
}
