package handlers

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

// RoleAuthorize verifies that the objects in the request can be accessed by the User. This check is done based on the
// Roles assigned to a user. To improve lookup speed users can provide a "roleID". Access will first be checked in the roleID.
func RoleAuthorize(user *common.User, request common.GETRequest, roleID string, accessByte byte) error {
	// Some files are accessible to all users even if their Roles are not set up.
	if request.IndustryID == "polyappNone" && request.DomainID == "polyappNone" {
		return nil
	}

	if user == nil {
		return errors.New("User was nil in RoleAuthorize")
	}
	if user.Roles == nil {
		return fmt.Errorf("Roles was not populated for this user %v", user.FirestoreID)
	}
	if len(user.Roles) < 1 {

		return fmt.Errorf("User (%v) did not have any Roles", user.FirestoreID)
	}
	haveAccess := false

	// check the roleID cache. This is only faster if the Role provided actually has access & there are > 1 roles and the role
	// which has access is not the first in the list of Roles.
	if roleID != "" {
		for _, r := range user.Roles {
			if r == roleID {
				// roleID is valid for this user, but does it give the user the access they desire?
				role, err := allDB.ReadRole(roleID)
				if err != nil {
					return fmt.Errorf("cached role (%v) not readable for RoleAuthorize user (%v): %w", roleID, user.FirestoreID, err)
				}
				haveAccess, err = common.RoleHasAccess(&role, request.IndustryID, request.DomainID, request.SchemaID, accessByte)
				if err != nil {
					return fmt.Errorf("common.RoleHasAccess: %w", err)
				}
				if haveAccess {
					return nil
				}
				break
			}
		}
	}

	for i := range user.Roles {
		role, err := allDB.ReadRole(user.Roles[i])
		if err != nil {
			return fmt.Errorf("role (%v) not readable for authorization for user (%v): %w", user.Roles[i], user.FirestoreID, err)
		}
		haveAccess, err = common.RoleHasAccess(&role, request.IndustryID, request.DomainID, request.SchemaID, accessByte)
		if err != nil {
			return fmt.Errorf("common.RoleHasAccess: %w", err)
		}
		if haveAccess {
			return nil
		}
	}
	return fmt.Errorf("user (%v) did not have a role which was authorized for this request", user.FirestoreID)
}
