package handlers

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"strconv"

	"gitlab.com/polyapp-open-source/polyapp/allDB"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// RunBot executes a Bot with the given data as the "currentData" in the page. It does not do any substantial security checks.
func RunBot(bot *common.Bot, data *common.Data, task *common.Task, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	actionsToRun := make([]common.Action, len(bot.ActionIDs))
	for i, actionID := range bot.ActionIDs {
		actionsToRun[i], err = allDB.ReadAction(actionID)
		if err != nil {
			return fmt.Errorf("ReadAction: %w", err)
		}
	}
	for i, action := range actionsToRun {
		err = RunAction(action, data, task, request, response)
		if err != nil {
			return fmt.Errorf("RunAction index "+strconv.Itoa(i)+": %w", err)
		}
	}
	return nil
}

// RunAction executes a function if it is in the whitelist for action funcs.
//
// action is the Action being performed.
//
// actionData is a map from IDs of Data documents to the *common.Data. It should include all *common.Data needed by action.
//
// currentData is the Data from the DataID document passed in from the request; it is implied
// as being relevant to Actions as explained in the documentation for common.Action
//
// task is the Task which this currentData is a part of. This is mostly provided to allow access to task.BotStaticData
//
// request is from this request.
func RunAction(action common.Action, currentData *common.Data, task *common.Task, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	switch action.Name {
	case "Edit Task":
		err = actions.ActionEditTask(currentData, request)
		if err != nil {
			return fmt.Errorf("ActionEditTask: %w", err)
		}
	case "Edit Website Content":
		err = actions.ActionEditAWebsite(currentData, request)
		if err != nil {
			return fmt.Errorf("ActionEditAWebsite: %w", err)
		}
	case "Table":
		err = actions.ActionTable(currentData, task.BotStaticData, request)
		if err != nil {
			return fmt.Errorf("ActionTable: %w", err)
		}
	case "Edit Bot":
		err = actions.EditBot(currentData)
		if err != nil {
			return fmt.Errorf("EditBot: %w", err)
		}
	case "Next Data":
		err = actions.ActionNextData(currentData, request, response, task.BotStaticData)
		if err != nil {
			return fmt.Errorf("ActionNextData: %w", err)
		}
	case "Create Cover":
		err = actions.ActionCreateCover(currentData, response)
		if err != nil {
			return fmt.Errorf("ActionCreateCover: %w", err)
		}
	case "Browser Action Open URL":
		err = actions.ActionBrowserActionOpenURL(currentData, response)
		if err != nil {
			return fmt.Errorf("ActionBrowserActionOpenURL: %w", err)
		}
	case "Edit Role":
		err = actions.ActionEditRole(currentData)
		if err != nil {
			return fmt.Errorf("ActionEditRole: %w", err)
		}
	case "Edit User":
		err = actions.ActionEditUser(currentData)
		if err != nil {
			return fmt.Errorf("ActionEditUser: %w", err)
		}
	case "User Activity":
		err = actions.ActionUserActivity(request)
		if err != nil {
			return fmt.Errorf("ActionUserActivity: %w", err)
		}
	case "Home Load":
		err = actions.ActionHomeLoad(request, response)
		if err != nil {
			return fmt.Errorf("HomeLoad: %w", err)
		}
	case "Delete Data":
		err = actions.ActionDeleteData(currentData, request)
		if err != nil {
			return fmt.Errorf("ActionDeleteData: %w", err)
		}
	case "Delete Admin":
		err = actions.ActionDeleteAdmin(currentData)
		if err != nil {
			return fmt.Errorf("ActionDeleteAdmin: %w", err)
		}
	case "HTTPS Call":
		err = actions.ActionHTTPSCall(currentData, request, response, task.BotStaticData)
		if err != nil {
			return fmt.Errorf("ActionHTTPSCall: %w", err)
		}
	case "Import Data":
		err = actions.ActionImportData(currentData, request, response)
		if err != nil {
			return fmt.Errorf("Import: %w", err)
		}
	case "Import Data and Create Task":
		err = actions.ActionImportDataAndCreateTask(currentData, request, response)
		if err != nil {
			return fmt.Errorf("ActionImportDataAndCreateTask: %w", err)
		}
	case "Export Data":
		err = actions.ActionExportData(currentData, response)
		if err != nil {
			return fmt.Errorf("actions.ActionExportData: %w", err)
		}
	case "Load Chart Javascript":
		err = actions.ActionLoadChartJavascript(request, response)
		if err != nil {
			return fmt.Errorf("actions.ActionExportData: %w", err)
		}
	case "Create Chart from Data":
		err = actions.ActionCreateChartFromData(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.CreateChartFromData: %w", err)
		}
	case "Productivity Chart":
		err = actions.ActionProductivityChart(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.ActionProductivityChart: %w", err)
		}
	case "Create Table from Data":
		err = actions.ActionCreateTableFromData(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.ActionCreateTableFromData: %w", err)
		}
	case "New Data":
		err = actions.ActionNewData(request, response)
		if err != nil {
			return fmt.Errorf("actions.ActionNewData: %w", err)
		}
	case "Load Tracking":
		err = actions.ActionLoadTracking(currentData, request)
		if err != nil {
			return fmt.Errorf("actions.ActionLoadTracking: %w", err)
		}
	case "Done Tracking":
		err = actions.ActionDoneTracking(currentData, request)
		if err != nil {
			return fmt.Errorf("actions.ActionDoneTracking: %w", err)
		}
	case "Export App":
		err = actions.ActionExportApp(currentData, response)
		if err != nil {
			return fmt.Errorf("actions.ActionExportApp: %w", err)
		}
	case "Import App":
		err = actions.ActionImportApp(currentData, request)
		if err != nil {
			return fmt.Errorf("actions.ActionImportApp: %w", err)
		}
	case "Generate Bot Scaffold":
		err = actions.GenerateBotScaffold(currentData)
		if err != nil {
			return fmt.Errorf("actions.GenerateBotScaffold: %w", err)
		}
	case "Configure API Call":
		err = actions.ActionConfigureAPICall(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.ConfigureAPICall: %w", err)
		}
	case "API Call":
		err = actions.ActionAPICall(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.APICall: %w", err)
		}
	case "External API Ingress Data":
		err = actions.ActionExternalAPIIngressData(currentData, request, response, task.BotStaticData)
		if err != nil {
			return fmt.Errorf("actions.ActionExternalAPIIngressData: %w", err)
		}
	case "External API Egress Data":
		err = actions.ActionExternalAPIEgressData(currentData, request, response, task.BotStaticData)
		if err != nil {
			return fmt.Errorf("actions.ActionExternalAPIEgressData: %w", err)
		}
	case "Microservice Bot Generator":
		err = actions.ActionMicroserviceBotGenerator(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.MicroserviceBotGenerator: %w", err)
		}
	case "Populate From User":
		err = actions.ActionPopulateFromUser(currentData, request, response, task.BotStaticData)
		if err != nil {
			return fmt.Errorf("actions.ActionPopulateFromUser: %w", err)
		}
	case "Copy Task By Data ID":
		err = actions.ActionCopyTaskByDataID(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.CopyTaskByDataID: %w", err)
		}
	case "View Task Help Text":
		err = actions.ActionViewTaskHelpText(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.ViewTaskHelpText: %w", err)
		}
	default:
		return errors.New("unhandled action: " + action.Name)
	}
	return nil
}
