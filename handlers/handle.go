package handlers

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
	"gitlab.com/polyapp-open-source/polyapp/integrity"
	"io"
	"io/ioutil"
	"net/url"
	"runtime"
	"strconv"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/gen"

	"gitlab.com/polyapp-open-source/polyapp/allDB"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// HandlePOST routes the incoming POST request to the appropriate handler.
func HandlePOST(request common.POSTRequest) (common.POSTResponse, error) {
	if request.DomainID == "polyappNone" || request.IndustryID == "polyappNone" || request.PublicPath != "" {
		return hardCodedPOSTHandlers(request)
	}
	return handlePOST(request)
}

func hardCodedPOSTHandlers(request common.POSTRequest) (common.POSTResponse, error) {
	homeRouterMod, newURL, _, err := homeRouter(request)
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("homeRouter: %w", err)
	}
	postResponse := common.POSTResponse{}
	postResponse.Init(request)
	postResponse.NewURL = newURL
	postResponse.ModDOMs = append(homeRouterMod, postResponse.ModDOMs...)
	return postResponse, nil
}

// handlePOST updates the Data in the POST request & may initiate Bots if the Task is done (aka if "Done" is set).
// handlePOST assumes you are saving Data to/from request.IndustryID, request.DomainID, request.TaskID, NOT the override value.
//
// handlePOST DOES respect the override value for determining the Task, Bots, and Schema.
//
// This setup allows callers to use cross-industry & domain tasks which CRUD to some place else, as in CreateTask.
//
// TODO this function does not handle override industry or override domain. If they are set it won't set isDone correctly
// and won't find Data @ a valid index because GetDataRef only returns one possible DataRef, not all possible combinations.
func handlePOST(request common.POSTRequest) (common.POSTResponse, error) {
	if request.DataID == "" || request.DataID == "polyappNone" || request.TaskID == "" || request.TaskID == "polyappNone" ||
		request.IndustryID == "" || request.IndustryID == "polyappNone" || request.DomainID == "" || request.DomainID == "polyappNone" {
		// redirect for everything except for DataID
		modDOMs, newURL, err := changePageRedirects(request)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("changePageRedirects: %w", err)
		}
		if newURL != "" {
			return common.POSTResponse{
				MessageID: request.MessageID,
				NewURL:    newURL,
				ModDOMs:   modDOMs,
			}, nil
		}
		// redirect for DataID
		return common.POSTResponse{
			MessageID: request.MessageID,
			NewURL: "/polyappChangeData?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() +
				"&task=" + request.GetTask() + "&ux=" + request.UXID + "&schema=" + request.SchemaID + "&user=" + request.UserID +
				"&role=" + request.RoleID,
			ModDOMs: []common.ModDOM{{}},
		}, nil
	}

	var user common.User
	if request.UserCache == nil {
		var err error
		user, err = allDB.ReadUser(request.UserID)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("allDB.Readuser: %w", err)
		}
		request.UserCache = &user
	} else {
		user = *request.UserCache
	}

	// TODO communicate to users that "Done" is a special Field.
	isDone := false
	doneValue := request.Data[common.GetDataRef(request)][common.AddFieldPrefix(request.GetIndustry(), request.GetDomain(), request.SchemaID, "Done")]
	errorsExist := false
	if doneValue != nil {
		var ok bool
		isDone, ok = doneValue.(bool)
		if !ok {
			return common.POSTResponse{}, errors.New("Done field was not a bool")
		}
		// Delete it so that when the Task loads the next time it doesn't think it is already Done.
		// Could also do this check in GET, but we already have Done specific logic here so I'm putting it here for now.
		delete(request.Data[common.GetDataRef(request)], common.AddFieldPrefix(request.GetIndustry(), request.GetDomain(), request.SchemaID, "Done"))
		request.IsDone = true
	}

	response, hasUserErrors, err := POSTUpdate(&request, false)
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("POSTUpdate for main Data: %w", err)
	}
	if hasUserErrors {
		errorsExist = true
	}
	response.ModDOMs = append([]common.ModDOM{
		{
			DeleteSelector:      "[id^=polyappError] > *",
			RemoveClassSelector: ".is-invalid",
			RemoveClasses:       []string{"is-invalid"},
		},
		{
			DeleteSelector: "#" + common.CSSEscape("polyappErrorDone"),
			InsertSelector: "#" + common.CSSEscape("Done"),
			Action:         "beforeend",
			HTML:           `<div id="polyappErrorDone></div>`,
		},
	}, response.ModDOMs...)

	subTaskResponseChan := make(chan common.POSTResponse)
	hasUserErrorsChan := make(chan bool)
	errChan := make(chan error)
	loopLength := 0
	for ref := range request.Data {
		loopLength++
		go func(ref string, subTaskResponseChan chan common.POSTResponse, hasUserErrorsChan chan bool, errChan chan error) {
			// skip processing the head / main Data document since it has already been processed.
			if strings.Contains(ref, "&data="+request.DataID) {
				errChan <- nil
				subTaskResponseChan <- common.POSTResponse{MessageID: ""}
				hasUserErrorsChan <- false
				return
			}
			if strings.Contains(ref, "&data=polyappShouldBeOverwritten") {
				// special case where there is a Ref within a displayNone formField.
				errChan <- nil
				subTaskResponseChan <- common.POSTResponse{MessageID: ""}
				hasUserErrorsChan <- false
				return
			}
			if ref[0] != '/' {
				ref, err = url.PathUnescape(ref)
				if err != nil {
					errChan <- fmt.Errorf("PathUnescape for ref: %w", err)
					return
				}
			}
			URL, err := url.ParseRequestURI(ref)
			if err != nil {
				errChan <- fmt.Errorf("ParseRequestURI: %w", err)
				return
			}
			subTaskGETRequest, err := common.CreateGETRequest(*URL)
			if err != nil {
				errChan <- fmt.Errorf("CreateGETRequest: %w", err)
				return
			}
			subTaskPOSTRequest := common.POSTRequest{
				MessageID:          request.MessageID,
				IndustryID:         subTaskGETRequest.IndustryID,
				OverrideIndustryID: subTaskGETRequest.OverrideIndustryID,
				DomainID:           subTaskGETRequest.DomainID,
				OverrideDomainID:   subTaskGETRequest.OverrideDomainID,
				TaskID:             subTaskGETRequest.TaskID,
				OverrideTaskID:     subTaskGETRequest.OverrideTaskID,
				Data:               request.Data,
				UXID:               subTaskGETRequest.UXID,
				SchemaID:           subTaskGETRequest.SchemaID,
				DataID:             subTaskGETRequest.DataID,
				UserID:             subTaskGETRequest.UserID,
				RoleID:             subTaskGETRequest.RoleID,
				ModifyID:           subTaskGETRequest.ModifyID,
				PublicPath:         subTaskGETRequest.PublicPath,
				FinishURL:          subTaskGETRequest.FinishURL,
				WantDocuments:      common.WantDocuments{},
			}
			subTaskResponse, hasUserErrors, err := POSTUpdate(&subTaskPOSTRequest, true)
			if err != nil {
				errChan <- fmt.Errorf("POSTUpdate ref "+ref+" error: %w", err)
				return
			}
			errChan <- nil
			subTaskResponseChan <- subTaskResponse
			hasUserErrorsChan <- hasUserErrors
		}(ref, subTaskResponseChan, hasUserErrorsChan, errChan)
	}
	combinedErr := ""
	for i := 0; i < loopLength; i++ {
		err = <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
			continue
		}
		subTaskResponse := <-subTaskResponseChan
		hasUserErrors := <-hasUserErrorsChan
		if hasUserErrors {
			errorsExist = true
		}
		if subTaskResponse.MessageID != "" {
			response.ModDOMs = append(response.ModDOMs, subTaskResponse.ModDOMs...)
		}
	}
	if combinedErr != "" {
		return common.POSTResponse{}, fmt.Errorf("processing request.Data: " + combinedErr)
	}

	// Run bots continuously outside of any loop since we only want the top bot to run and we want to make sure all
	// Data has been saved prior to this Bot getting called.
	task, err := allDB.ReadTask(request.GetTask())
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("ReadTask for BotsTriggeredContinuously: %w", err)
	}
	botData, err := allDB.ReadData(request.DataID)
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("ReadData for botData: %w", err)
	}
	for _, botID := range task.BotsTriggeredContinuously {
		bot, err := allDB.ReadBot(botID)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("ReadBot ID ("+botID+"): %w", err)
		}
		err = RunBot(&bot, &botData, &task, &request, &response)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("RunBot ID ("+botID+"): %w", err)
		}
	}

	if isDone && !errorsExist {
		// This means the task is finished and did not experience any errors. Let's only run bots on the parent Task so that there's no
		// confusion about nested Tasks doing things in what order and when.
		data, err := allDB.ReadData(request.DataID)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("ReadData in isDone: %w", err)
		}
		for _, botID := range task.BotsTriggeredAtDone {
			bot, err := allDB.ReadBot(botID)
			if err != nil {
				return common.POSTResponse{}, fmt.Errorf("ReadBot with ID "+botID+": %w", err)
			}
			err = RunBot(&bot, &data, &task, &request, &response)
			if err != nil {
				return common.POSTResponse{}, fmt.Errorf("RunBot with ID "+botID+": %w", err)
			}
		}
		for _, botID := range user.BotsTriggeredAtDone {
			bot, err := allDB.ReadBot(botID)
			if err != nil {
				return common.POSTResponse{}, fmt.Errorf("ReadBot with ID "+botID+": %w", err)
			}
			err = RunBot(&bot, &data, &task, &request, &response)
			if err != nil {
				return common.POSTResponse{}, fmt.Errorf("RunBot with ID "+botID+": %w", err)
			}
		}
		if response.NewURL != "" {
			// if a Bot set request.FinishURL then that should override the request.FinishURL seen in the Task's URL.
		} else if request.FinishURL == "" || request.FinishURL == "polyappNone" {
			response.NewURL = "/polyappChangeData?industry=" + request.IndustryID + "&domain=" + request.DomainID + "&task=" + request.TaskID + "&schema=" + request.SchemaID + "&ux=" + request.UXID
		} else {
			response.NewURL = request.FinishURL
		}
	} else if errorsExist {
		var alert bytes.Buffer
		alert.WriteString(`<div id="polyappJSAfterDoneError">`)
		err = gen.GenAlert(gen.Alert{
			Text:     "Errors were found in Tasks or Subtasks.",
			Severity: "danger",
		}, &alert)
		if err != nil {
			return common.POSTResponse{}, fmt.Errorf("GenAlert for !isDone: %w", err)
		}
		alert.WriteString(`</div>`)
		response.ModDOMs = append(response.ModDOMs, common.ModDOM{
			DeleteSelector: "#polyappJSAfterDoneError",
			InsertSelector: "#" + common.CSSEscape(url.PathEscape(request.GetIndustry())+"_"+url.PathEscape(request.GetDomain())+"_"+url.PathEscape(request.SchemaID)+"_Done"),
			Action:         "afterend",
			HTML:           alert.String(),
		})
	} else if !errorsExist {
		response.ModDOMs = append(response.ModDOMs, common.ModDOM{
			DeleteSelector: "#polyappJSAfterDoneError",
		})
		if request.FinishURL != "" {
			response.NewURL = request.FinishURL
		}
	}

	if runtime.GOOS != "js" {
		// We do not have to re-check authorization because this User has already been checked for RoleAuthorize in the
		// request.IndustryID, request.DomainID, and request.SchemaID by middleware.
		err = populateWantDocuments(request, &response)
	}

	return response, err
}

// POSTUpdate performs POST-related actions for this dataID, given the full Data object from the request.
//
// IndustryID is this task (or sub-task's) Industry ID, probably taking into account overrides. Same for domainId, dataID, taskID.
//
// schemaID is probably the ID of the Schema.
//
// requestData is all of the data passed up from the client as part of this request.
//
// newURL is from the request, and is set to FinishURL if necessary.
//
// isSubTask is true if this "request" is not the parent Task.
func POSTUpdate(request *common.POSTRequest, isSubTask bool) (pr common.POSTResponse, hasUserErrors bool, err error) {
	// We can use cached data when we're in the parent tasks, but not in child tasks.
	var task common.Task
	if request.TaskCache == nil {
		task, err = allDB.ReadTask(request.GetTask())
		if err != nil {
			return common.POSTResponse{}, false, fmt.Errorf("ReadTask for BotsTriggeredContinuously: %w", err)
		}
	} else {
		task = *request.TaskCache
	}

	dataIndex := common.GetDataRef(*request)
	data := common.Data{
		FirestoreID: request.DataID,
		IndustryID:  request.GetIndustry(),
		DomainID:    request.GetDomain(),
		SchemaID:    request.SchemaID, // might change this later, but it makes sense for now.
	}

	// This seems like it's part of a class of security related checks. Like, instead of just having a formField 'Task.Readonly', there
	// could be field-level access controls where changes to the fields at those levels are rejected with an error message users can see.
	// For now though we only have Readonly fields at the Task level. and control the ability to change these values by controlling
	// who can design tasks.
	// Field-level access control is also the opposite of how I had originally envisioned empowering users to improve their own
	// workflows as long as they stay within their particular Domain. Unfortunately, since Tasks can now be edited by the public,
	// we have to assume some users will be malicious.
	for k := range request.Data[dataIndex] {
		if task.FieldSecurity[k] != nil && task.FieldSecurity[k].Readonly {
			delete(request.Data[dataIndex], k)
		}
	}

	err = data.Init(request.Data[dataIndex])
	if err != nil {
		return common.POSTResponse{}, false, fmt.Errorf("Init: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		err = allDB.CreateData(&data)
		if err != nil {
			return common.POSTResponse{}, false, fmt.Errorf("CreateData: %w", err)
		}
	}
	if err != nil {
		return common.POSTResponse{}, false, fmt.Errorf("could not update data for a POST request with FirestoreID %v because: %w", data.FirestoreID, err)
	}

	newData, err := allDB.ReadData(data.FirestoreID)
	if err != nil {
		return common.POSTResponse{}, false, fmt.Errorf("ReadData: %w", err)
	}

	if !isSubTask {
		// Remove all Data which are not referenced in the parent Task. Aka take out the garbage.
		// Why? Because if you do not remove an unreferenced Data, and that unreferenced Data is not valid, it can cause
		// the whole request to fail validation even though it is not part of the Task any more.
		// Doing things this particular way means you can only have 1 subTask layer.
		seenRefs := make(map[string]bool)
		seenRefs[dataIndex] = true
		for _, ref := range newData.Ref {
			seenRefs[url.PathEscape(*ref)] = true
		}
		for _, refs := range newData.ARef {
			for _, ref := range refs {
				seenRefs[url.PathEscape(ref)] = true
			}
		}
		for k := range request.Data {
			if !seenRefs[k] {
				delete(request.Data, k)
			}
		}
	}

	// evaluate validators and add their results to the response
	userErrors, err := CheckGoalsSatisfied(&task, &newData, isSubTask)
	if err != nil {
		return common.POSTResponse{}, false, fmt.Errorf("CheckGoalsSatisfied: %w", err)
	}
	modDOMs, err := UserErrorModDOMs("Done", userErrors, false)
	if err != nil {
		return common.POSTResponse{}, false, fmt.Errorf("UserErrorModDOMs: %w", err)
	}
	response := common.POSTResponse{}
	response.Init(*request)
	response.ModDOMs = append(response.ModDOMs, modDOMs...)
	return response, len(userErrors) > 0, nil
}

// populateWantDocuments will retrieve all documents requested in the 'request'.
func populateWantDocuments(request common.POSTRequest, response *common.POSTResponse) error {
	ct := 0
	limit := 10
	if request.WantDocuments.Data != nil {
		for _, id := range request.WantDocuments.Data {
			ct++
			if ct > limit {
				break
			}
			d, err := allDB.ReadData(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.DataDocuments = append(response.DataDocuments, &d)
		}
	}
	if request.WantDocuments.Role != nil {
		for _, id := range request.WantDocuments.Role {
			ct++
			if ct > limit {
				break
			}
			r, err := allDB.ReadRole(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.RoleDocuments = append(response.RoleDocuments, &r)
		}
	}
	if request.WantDocuments.Schema != nil {
		for _, id := range request.WantDocuments.Schema {
			ct++
			if ct > limit {
				break
			}
			s, err := allDB.ReadSchema(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.SchemaDocuments = append(response.SchemaDocuments, &s)
		}
	}
	if request.WantDocuments.Task != nil {
		for _, id := range request.WantDocuments.Task {
			ct++
			if ct > limit {
				break
			}
			t, err := allDB.ReadTask(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.TaskDocuments = append(response.TaskDocuments, &t)
		}
	}
	if request.WantDocuments.User != nil {
		for _, id := range request.WantDocuments.User {
			ct++
			if ct > limit {
				break
			}
			u, err := allDB.ReadUser(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.UserDocuments = append(response.UserDocuments, &u)
		}
	}
	if request.WantDocuments.UX != nil {
		for _, id := range request.WantDocuments.UX {
			ct++
			if ct > limit {
				break
			}
			ux, err := allDB.ReadUX(id)
			if err != nil {
				return fmt.Errorf("could not read WantDocument: %w", err)
			}
			response.UXDocuments = append(response.UXDocuments, &ux)
		}
	}
	return nil
}

// HandleGET takes a common.GETRequest and returns a common.GETResponse.
//
// This function has GOOS-specific handling since GOOS=js does not need the app shell
// packaged with the common.GETResponse. It is done like this because the alternative is for both JS and the
// server to have a copy of the app shell. This is difficult, but not impossible, for the client because
// it would have to store the directory structure of public/ in IndexedDB or a cache. Instead the
// client caches '/' responses (which contain the shell) and handles inserting the inner HTML into the shell in Javascript.
func HandleGET(request common.GETRequest) (common.GETResponse, error) {
	if request.DataID == "new" && request.GetTask() != "" && request.GetTask() != "polyappNone" && request.GetIndustry() != "" &&
		request.GetIndustry() != "polyappNone" && request.GetDomain() != "" && request.GetDomain() != "polyappNone" &&
		request.SchemaID != "" && request.SchemaID != "polyappNone" && request.UXID != "" && request.UXID != "polyappNone" {
		request.DataID = common.GetRandString(25)
		newData := &common.Data{}
		newData.Init(nil)
		newData.FirestoreID = request.DataID
		newData.IndustryID = request.GetIndustry()
		newData.DomainID = request.GetDomain()
		newData.SchemaID = request.SchemaID
		err := allDB.CreateData(newData)
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("allDB.CreateData: %w", err)
		}
		URL := common.CreateURL(request.GetIndustry(), request.GetDomain(), request.GetTask(), request.UXID, request.SchemaID,
			newData.FirestoreID, request.UserID, request.RoleID, "", "")
		return common.GETResponse{HTML: `<p>Redirecting to: <a href="` + URL + `">New</a></p>`, RedirectURL: URL}, nil
	}

	if runtime.GOOS == "js" {
		// client-side GET only needs to get the inner / Task HTML since the client already has the app shell
		ux, redirectURL, err := InnerHTML(request)
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("innerHTML: %w", err)
		}
		return common.GETResponse{HTML: *ux.HTML, RedirectURL: redirectURL}, nil
	}
	// server-side GET needs to get all of the HTML and place it in the common.GETResponse
	ux, redirectURL, err := InnerHTML(request)
	if err != nil {
		return common.GETResponse{}, fmt.Errorf("innerHTML: %w", err)
	}
	if redirectURL != "" {
		// if redirectURL is set, then ux.HTML may be nil. To avoid panicking on *ux.HTML return early.
		// Plus, if we're redirecting then who cares about the HTML?
		return common.GETResponse{HTML: "<p>We are redirecting you to a new URL. If you are not redirected in 5 seconds, please click here: <a href=\"" + redirectURL + "\">" + redirectURL + "</a></p>", RedirectURL: redirectURL}, nil
	}
	html, err := AppShellHTML(ux)
	if err != nil {
		return common.GETResponse{}, fmt.Errorf("error in appShellHTML: %w", err)
	}
	combinedHTML, err := InnerIntoAppShell([]byte(*ux.HTML), html)
	if err != nil {
		return common.GETResponse{}, fmt.Errorf("innerIntoAppShell: %w", err)
	}
	return common.GETResponse{HTML: combinedHTML, RedirectURL: redirectURL}, err
}

// HandleUploads handles a PUT request. This is typically a large file upload.
func HandleUploads(body io.ReadCloser, getRequest common.GETRequest, field string, filesLength string, metadata map[string]string) error {
	if runtime.GOOS == "js" {
		return errors.New("Client cannot accept and store PUT requests")
	}
	byteArray, err := ioutil.ReadAll(body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll: %w", err)
	}
	if len(byteArray) < 1 {
		return errors.New("HandleUploads file was length 0")
	}

	d := &common.Data{
		IndustryID:  getRequest.GetIndustry(),
		DomainID:    getRequest.GetDomain(),
		SchemaID:    getRequest.SchemaID,
		FirestoreID: getRequest.DataID,
	}
	err = d.Init(map[string]interface{}{})
	if err != nil {
		return fmt.Errorf("d.Init: %w", err)
	}
	if strings.Contains(field, "%2F") {
		// ABytes.
		splitField := strings.Split(field, "%2F")

		// Part of an Update is overwriting old data, but it can also be that the old was an array of length 4 and the new
		// has a length of 3. In that case we need to delete the object at the last position in the array.
		if filesLength == "" {
			return errors.New("filesLength was empty")
		}
		filesLengthInt, err := strconv.Atoi(filesLength)
		if err != nil {
			return errors.New("filesLength was empty")
		}
		objects, _ := firestoreCRUD.ListStorageObjects(common.GetBlobBucket(), firestoreCRUD.GetStorageObjectName(common.CollectionData, d.FirestoreID, splitField[0]))
		// ignoring the error because if there is an err it won't return an enumerable objects.
		// also I don't care if it returns code = NotFound since that just means this is a Create operation & not an Update.
		for _, objectName := range objects {
			splitObjectName := strings.Split(objectName, "/")
			if len(splitObjectName) != 2 {
				return errors.New("objectName should have split to 2 parts: " + objectName)
			}
			objectNameIndex, err := strconv.Atoi(splitObjectName[1])
			if err != nil {
				return errors.New("objectName should have had a 2nd part (split by /) with an int: " + objectName)
			}
			if objectNameIndex >= filesLengthInt {
				// Exterminate. Exterminate. Exterminate. bleep bloop bloop.
				err = firestoreCRUD.DeleteStorageObject(common.GetBlobBucket(), objectName)
				if err != nil && !strings.Contains(err.Error(), "object doesn't exist") {
					return fmt.Errorf("firestoreCRUD.DeleteStorageObject: %w", err)
				}
			}
		}

		// I'm doing the same security checks which are done by allDB here.
		d.Bytes[splitField[0]] = byteArray
		err = integrity.ValidateAllDBs(d)
		if err != nil {
			return fmt.Errorf("integrity.ValidateAllDBs: %w", err)
		}
		err = integrity.ValidateLanguages(d)
		if err != nil {
			return fmt.Errorf("integrity.ValidateLanguages: %w", err)
		}
		fieldIndex, _ := strconv.Atoi(splitField[1])
		// OK, if it passed all security checks let's bypass allDB package and set the field directly in Storage.
		// Why? Well if we go through allDB then it assumes we're handing it the complete array. But IRL we do not have
		// the complete array since each file is being uploaded and stored separately. So we have to bypass that assumption.
		objectName := firestoreCRUD.GetStorageObjectArrayName(common.CollectionData, d.FirestoreID, splitField[0], fieldIndex)
		err = firestoreCRUD.SetStorageObject(common.GetBlobBucket(), bytes.NewReader(byteArray), objectName, metadata)
		if err != nil {
			return fmt.Errorf("firestoreCRUD.SetStorageObject: %w", err)
		}
	} else {
		d.Bytes[field] = byteArray
		err = allDB.UpdateData(d)
		if err != nil {
			return fmt.Errorf("CreateData: %w", err)
		}
	}

	return nil
}

// AppShellHTML returns the app shell you could place additional content inside of.
func AppShellHTML(ux *common.UX) ([]byte, error) {
	buf := new(bytes.Buffer)
	templates, err := getTemplates()
	if err != nil {
		return []byte(""), fmt.Errorf("error getting templates: %w", err)
	}
	err = templates.ExecuteTemplate(buf, "index", ux)
	if err != nil {
		return []byte(""), fmt.Errorf("error executing templates: %w", err)
	}
	return buf.Bytes(), err
}

// InnerHTML returns the task - like HTML in a GET common.GETRequest.
//
// *common.UX contains the innerHTML of the webpage. string is a redirectURL. error != nil if there were any problems.
func InnerHTML(request common.GETRequest) (*common.UX, string, error) {
	var err error
	if (request.DomainID == "polyappNone" && request.IndustryID == "polyappNone") ||
		(request.PublicPath != "") {
		modDOMs, redirectURL, title, err := homeRouter(common.POSTRequest{
			MessageID:          "",
			IndustryID:         request.IndustryID,
			OverrideIndustryID: request.OverrideIndustryID,
			DomainID:           request.DomainID,
			OverrideDomainID:   request.OverrideDomainID,
			TaskID:             request.TaskID,
			OverrideTaskID:     request.OverrideTaskID,
			UXID:               request.UXID,
			SchemaID:           request.SchemaID,
			DataID:             request.DataID,
			UserID:             request.UserID,
			UserCache:          request.UserCache,
			RoleID:             request.RoleID,
			ModifyID:           request.ModifyID,
			PublicPath:         request.PublicPath,
			FinishURL:          request.FinishURL,
			IsDone:             false,
			WantDocuments:      common.WantDocuments{},
		})
		if err != nil {
			return nil, "", fmt.Errorf("homeRouter: %w", err)
		}
		if title == "" {
			title = "Polyapp"
		}
		return &common.UX{
			HTML:  common.String(modDOMs[0].HTML),
			Title: common.String(title),
		}, redirectURL, err
	}
	if request.DataID == "" || request.DataID == "polyappNone" || request.TaskID == "" || request.TaskID == "polyappNone" ||
		request.IndustryID == "" || request.IndustryID == "polyappNone" || request.DomainID == "" || request.DomainID == "polyappNone" {
		// redirect for everything except for DataID
		postReq := common.POSTRequest{
			MessageID:          "",
			IndustryID:         request.IndustryID,
			OverrideIndustryID: request.OverrideIndustryID,
			DomainID:           request.DomainID,
			OverrideDomainID:   request.OverrideDomainID,
			TaskID:             request.TaskID,
			OverrideTaskID:     request.OverrideTaskID,
			Data:               nil,
			UXID:               request.UXID,
			SchemaID:           request.SchemaID,
			DataID:             request.DataID,
			UserID:             request.UserID,
			RoleID:             request.RoleID,
			ModifyID:           request.ModifyID,
			PublicPath:         request.PublicPath,
			FinishURL:          request.FinishURL,
			WantDocuments:      common.WantDocuments{},
		}
		_, newURL, err := changePageRedirects(postReq)
		if err != nil {
			return nil, "", fmt.Errorf("changePageRedirects: %w", err)
		}
		if newURL != "" {
			return nil, newURL, nil
		}
		// redirect for DataID
		return nil,
			"/polyappChangeData?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() +
				"&task=" + request.GetTask() + "&schema=" + request.SchemaID + "&ux=" + request.UXID + "&user=" + request.UserID +
				"&role=" + request.RoleID,
			nil
	}

	// Task pages. We expect Task, Schema, UX and Data to be populated and error if they are not.
	// An alternative strategy would be to redirect users to the correct page they need to populate information for the Task.
	// For right now, users who type in a Task or somehow err here erroneously should see directions to fix the problem
	// in the resulting 404 or 500 page.
	errChan := make(chan error, 0)
	var task common.Task
	var data common.Data
	var ux common.UX
	var schema common.Schema
	var user common.User
	numGoRoutines := 4
	if request.UserCache == nil {
		numGoRoutines++
		go func(request common.GETRequest, errChan chan error) {
			var err error
			user, err = allDB.ReadUser(request.UserID)
			errChan <- err
		}(request, errChan)
	} else {
		user = *request.UserCache
	}
	go func(request common.GETRequest, errChan chan error) {
		var err error
		task, err = allDB.ReadTask(request.TaskID)
		errChan <- err
	}(request, errChan)
	go func(request common.GETRequest, errChan chan error) {
		var err error
		data, err = allDB.ReadData(request.DataID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			// handled later by CreateDataFromSchema
			errChan <- nil
			return
		}
		errChan <- err
	}(request, errChan)
	go func(request common.GETRequest, errChan chan error) {
		var err error
		ux, err = allDB.ReadUX(request.UXID)
		errChan <- err
	}(request, errChan)
	go func(request common.GETRequest, errChan chan error) {
		var err error
		schema, err = allDB.ReadSchema(request.SchemaID)
		errChan <- err
	}(request, errChan)
	totalErrString := ""
	for i := 0; i < numGoRoutines; i++ {
		err := <-errChan
		if err != nil {
			totalErrString += err.Error() + "; "
		}
	}
	if totalErrString != "" {
		return nil, "", errors.New("allDB.Read task/schema/ux/data: " + totalErrString)
	}

	if request.UserCache == nil {
		request.UserCache = &user
	}

	needToUpdateData := false
	if data.FirestoreID == "" {
		// The Read failed with code = NotFound. This is indicative of someone using a Task with a new Data.
		// We need to create a new Data.
		_ = data.Init(nil)
		newData, err := common.CreateDataFromSchema(&schema, &data)
		if err != nil {
			return nil, "", fmt.Errorf("CreateDataFromSchema: %w", err)
		}
		err = allDB.CreateData(newData)
		if err != nil {
			return nil, "", fmt.Errorf("CreateData: %w", err)
		}
		data = *newData
	}

	for k, v := range data.Ref {
		if v == nil || *v == "" {
			// This Ref does not exist. This is indicative of someone using a Task with a new Data.
			// We need to create a new Data for each Ref so that getDataAndTaskFromRawRef will succeed.
			refData := common.Data{}
			_ = refData.Init(nil)
			_, _, sch, _ := common.SplitField(k)
			refSchema, err := allDB.ReadSchema(sch)
			if err != nil {
				return nil, "", fmt.Errorf("allDB.ReadSchema for data.Ref k %v: %w", k, err)
			}
			newData, err := common.CreateDataFromSchema(&refSchema, &common.Data{})
			if err != nil {
				return nil, "", fmt.Errorf("common.CreateDataFromSchema for data.Ref k %v: %w", k, err)
			}
			err = allDB.CreateData(newData)
			if err != nil {
				return nil, "", fmt.Errorf("allDB.CreateData for data.Ref k %v: %w", k, err)
			}

			// TODO is there a better way to get this data? Looking at EditTask is not a good method.
			queryable, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), refSchema.FirestoreID)
			if err != nil {
				return nil, "", fmt.Errorf("allDB.QueryEquals: %w", err)
			}
			if queryable == nil {
				return nil, "", fmt.Errorf("QueryEquals returned nil when searching at key %v value %v", common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), refSchema.FirestoreID)
			}
			editTaskData := queryable.(*common.Data)
			data.Ref[k] = common.String(common.CreateRef(refSchema.IndustryID, refSchema.DomainID,
				*editTaskData.S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID")],
				*editTaskData.S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "UX ID")],
				refSchema.FirestoreID, newData.FirestoreID))

			needToUpdateData = true
		}
	}

	modDOMs := make([]common.ModDOM, 0)
	for _, botID := range task.BotsTriggeredAtLoad {
		bot, err := allDB.ReadBot(botID)
		if err != nil {
			return nil, "", fmt.Errorf("ReadBot ID ("+botID+"): %w", err)
		}
		POSTReq := common.POSTRequest{
			IndustryID:          request.IndustryID,
			OverrideIndustryID:  request.OverrideIndustryID,
			DomainID:            request.DomainID,
			OverrideDomainID:    request.OverrideDomainID,
			TaskID:              request.TaskID,
			TaskCache:           &task,
			OverrideTaskID:      request.OverrideTaskID,
			Data:                nil,
			UXID:                request.UXID,
			UXCache:             &ux,
			SchemaID:            request.SchemaID,
			SchemaCache:         &schema,
			DataID:              request.DataID,
			UserID:              request.UserID,
			PublicMapOwningUser: common.User{},
			RoleID:              request.RoleID,
			ModifyID:            request.ModifyID,
			PublicPath:          request.PublicPath,
			RecaptchaResponse:   "",
			FinishURL:           request.FinishURL,
			IsDone:              false,
			WantDocuments:       common.WantDocuments{},
		}
		POSTResp := common.POSTResponse{}
		POSTResp.Init(POSTReq)
		err = RunBot(&bot, &data, &task, &POSTReq, &POSTResp)
		if err != nil {
			return nil, "", fmt.Errorf("RunBot ID ("+botID+"): %w", err)
		}
		for _, m := range POSTResp.ModDOMs {
			modDOMs = append(modDOMs, m)
		}
	}
	for _, botID := range user.BotsTriggeredAtLoad {
		bot, err := allDB.ReadBot(botID)
		if err != nil {
			return nil, "", fmt.Errorf("ReadBot ID ("+botID+"): %w", err)
		}
		POSTReq := common.POSTRequest{
			IndustryID:          request.IndustryID,
			OverrideIndustryID:  request.OverrideIndustryID,
			DomainID:            request.DomainID,
			OverrideDomainID:    request.OverrideDomainID,
			TaskID:              request.TaskID,
			TaskCache:           &task,
			OverrideTaskID:      request.OverrideTaskID,
			Data:                nil,
			UXID:                request.UXID,
			UXCache:             &ux,
			SchemaID:            request.SchemaID,
			SchemaCache:         &schema,
			DataID:              request.DataID,
			UserID:              request.UserID,
			PublicMapOwningUser: common.User{},
			RoleID:              request.RoleID,
			ModifyID:            request.ModifyID,
			PublicPath:          request.PublicPath,
			RecaptchaResponse:   "",
			FinishURL:           request.FinishURL,
			IsDone:              false,
			WantDocuments:       common.WantDocuments{},
		}
		POSTResp := common.POSTResponse{}
		POSTResp.Init(POSTReq)
		err = RunBot(&bot, &data, &task, &POSTReq, &POSTResp)
		if err != nil {
			return nil, "", fmt.Errorf("RunBot ID ("+botID+"): %w", err)
		}
		for _, m := range POSTResp.ModDOMs {
			modDOMs = append(modDOMs, m)
		}
	}

	// integrity checks
	err = common.DataSatisfiesSchema(&data, &schema)
	if err != nil {
		return nil, "", fmt.Errorf("DataSatisfiesSchema: %w", err)
	}
	err = common.SchemaSatisfiesTask(&schema, &task)
	if err != nil {
		return nil, "", fmt.Errorf("SchemaSatisfiesTask: %w", err)
	}
	err = common.SchemaSatisfiesUX(&schema, &ux)
	if err != nil {
		return nil, "", fmt.Errorf("SchemaSatisfiesUX: %w", err)
	}

	// prepare the data
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(*ux.HTML))
	if err != nil {
		return nil, "", fmt.Errorf("NewDocumentFromReader: %w", err)
	}

	allDataForThisTask, allTasksForThisTask, allRefs, err := getAllDataAndTasksFromHeadData(data, task)
	if err != nil {
		return nil, "", fmt.Errorf("getAllDataAndTasksFromHeadData: %w", err)
	}

	for i := range allDataForThisTask {
		userErrors, err := CheckGoalsSatisfied(allTasksForThisTask[i], allDataForThisTask[i], i > 0)
		if err == nil {
			// we don't actually care, in GET, if the goals are not all satisfied except as a convenient time to place
			// errors under UI pieces. In the event of an error, maybe someone is just opening this Task
			// to fix the problem. Maybe they lost internet connectivity and had to reload the page. Who cares? Just ship it.
			additionalModDOMs, err := UserErrorModDOMs("Done", userErrors, false)
			if err != nil {
				return nil, "", fmt.Errorf("UserErrorModDOMs: %w", err)
			}
			modDOMs = append(modDOMs, additionalModDOMs...)
		}

		var d map[string]interface{}
		if allDataForThisTask[i].IndustryID == "" || allDataForThisTask[i].IndustryID == "polyappNone" {
			// this happens if it's a new array element which has not had information inserted into it, or a similar situation.
			parsedRef, err := url.ParseRequestURI(allRefs[i])
			if err != nil {
				return nil, "", fmt.Errorf("ParseRequestURI: %w", err)
			}
			GETRequestFromParsedRef, err := common.CreateGETRequest(*parsedRef)
			if err != nil {
				return nil, "", fmt.Errorf("CreateGETRequest: %w", err)
			}
			d = map[string]interface{}{
				common.PolyappIndustryID: GETRequestFromParsedRef.GetIndustry(),
				common.PolyappDomainID:   GETRequestFromParsedRef.GetDomain(),
				common.PolyappSchemaID:   GETRequestFromParsedRef.SchemaID,
			}
		} else {
			d, err = allDataForThisTask[i].Simplify()
			if err != nil {
				return nil, "", fmt.Errorf("could not simplify data (%v) before templating: %w", allDataForThisTask[i].FirestoreID, err)
			}
		}
		common.Dereference(d)
		var parentEl *goquery.Selection
		if allRefs[i] != "" {
			// parentEl should be limited to the scope of this particular nested Task. All others need to look under their Ref.
			refEscaped := allRefs[i]
			if allRefs[i][0] == '/' {
				refEscaped = url.PathEscape(allRefs[i])
			}
			parentEl = doc.Selection.Find("[id^=polyappRef" + common.CSSEscape(refEscaped) + "]")
		}
		if parentEl == nil || parentEl.Nodes == nil {
			parentEl = doc.Selection
		}
		err = insertDataIntoDOM(parentEl, d, allDataForThisTask[i].FirestoreID)
		if err != nil {
			return nil, "", fmt.Errorf("insertDataIntoDOM: %w", err)
		}
	}

	// The main difference between data.Ref and data.ARef is that data.ARef delays handling "polyappShouldBeOverwritten"
	// until after there is Data in the Task since there is always a "polyappShouldBeOverwritten" hidden on-page for use by main.js.
	//
	// Meanwhile, data.Ref needs to find that "polyappShouldBeOverwritten" immediately and give it a new value because
	// if it does not do so all writes to this Subtask will be ignored because main.js assumes writes under a data ID of
	// "polyappShouldBeOverwritten" should be ignored since those writes are to a hidden element.
	// Therefore, by overwriting "polyappShouldBeOverwritten" during the GET, when a value changes the data ID will be
	// recognized by main.js and the value will be added to AllData and transmitted up to the server.
	for k, v := range schema.DataTypes {
		// maybe I should be running this code for subtask Schemas too? Or just look in the Data themselves. for each data, for each ARef or Ref...
		if v == "Ref" {
			dataTypesSplit := strings.Split(k, "_")
			if len(dataTypesSplit) != 5 {
				return nil, "", fmt.Errorf("expected _industry_domain_schema_field name in dataTypesSplit. Got: " + k)
			}
			toOverwrite := doc.Find("[id^=polyappRef" + common.CSSEscape(url.PathEscape("/t/"+dataTypesSplit[1]+"/"+dataTypesSplit[2]+"/")) + "]")
			errToThrow := ""
			toOverwrite.EachWithBreak(func(i int, sel *goquery.Selection) bool {
				currentID, _ := sel.Attr("id")
				refField, _ := sel.Attr("data-ref-field")
				// First case is when the Ref is in an Accordion.
				// Second case is specifically for <section> or similar elements. Could probably remove the 1st case by adding
				// the field data-ref-field to all of the Accordions too.
				if (strings.Contains(currentID, dataTypesSplit[3]) && strings.Contains(currentID, "polyappShouldBeOverwritten")) ||
					strings.Contains(refField, dataTypesSplit[3]) && strings.Contains(currentID, "polyappShouldBeOverwritten") {
					// it's possible there are other things in this industry and domain, but it's unlikely one Task would
					// contain one Singleton Subtask which references a particular Industry/Domain/Schema and an array of
					// Tasks which references that same Industry/Domain/Schema. It's POSSIBLE, but I'm not going to handle it right now.
					newDataID := ""
					if data.Ref[k[1:]] != nil && *data.Ref[k[1:]] != "" {
						fullRefURL, _ := url.Parse(*data.Ref[k[1:]])
						getReq, err := common.CreateGETRequest(*fullRefURL)
						if err != nil {
							errToThrow = fmt.Sprintf("common.CreateGETRequest for *data.Ref[k] %v: %v", *data.Ref[k], err.Error())
							return false
						}
						newDataID = getReq.DataID
					} else {
						newDataID = common.GetRandString(25)
						needToUpdateData = true
					}
					newID := updateLinked(sel, currentID, sel.Parent().Parent(), newDataID)

					// Set up a reference to the new ID in the parent Data document. If you do not do this, then when the
					// child goes to save the value saved will be filtered out as being irrelevant garbage.
					// See also: POSTUpdate, near the bottom where the garbage is filtered if !isSubtask
					newIDAsRef, _ := url.PathUnescape(strings.Split(newID[len("polyappRef"):], "_")[0])
					data.Ref[k] = common.String(newIDAsRef)
				} // else this is some other Ref in this Industry and Domain. This could be a substantial number of DOM nodes.
				return true
			})
			if errToThrow != "" {
				return nil, "", errors.New(errToThrow)
			}
		}
	}
	if needToUpdateData {
		err = allDB.UpdateData(&data)
		if err != nil {
			return nil, "", fmt.Errorf("allDB.UpdateData (%v): %w", data.FirestoreID, err)
		}
	}

	// SelectFields are populated only the first time the page loads.
	for inputID, searchField := range ux.SelectFields {
		q := allDB.Query{}
		q.Init(request.GetIndustry(), request.GetDomain(), request.SchemaID, common.CollectionData)
		q.SetLength(10_000)
		err = q.AddSortBy(searchField, "asc")
		if err != nil {
			return nil, "", fmt.Errorf("q.AddEquals: %w", err)
		}
		iter, err := q.QueryRead()
		if err != nil {
			return nil, "", fmt.Errorf("q.QueryRead: %w", err)
		}
		results := make([]string, 0)
		numResults := 0
		for {
			doc, err := iter.Next()
			if err == common.IterDone {
				break
			}
			if err != nil {
				return nil, "", fmt.Errorf("iter.Next: %w", err)
			}
			numResults++
			docData := doc.(*common.Data)
			if docData.S[searchField] != nil {
				results = append(results, *docData.S[searchField])
			} else if docData.I[searchField] != nil {
				results = append(results, strconv.FormatInt(*docData.I[searchField], 10))
			} else if docData.F[searchField] != nil {
				results = append(results, strconv.FormatFloat(*docData.F[searchField], 'f', -1, 64))
			} else if docData.B[searchField] != nil {
				results = append(results, strconv.FormatBool(*docData.B[searchField]))
			} else if docData.Ref[searchField] != nil {
				results = append(results, *docData.S[searchField])
			} else if docData.AS[searchField] != nil {
				for _, f := range docData.AS[searchField] {
					results = append(results, f)
				}
			} else if docData.AI[searchField] != nil {
				for _, f := range docData.AI[searchField] {
					results = append(results, strconv.FormatInt(f, 10))
				}
			} else if docData.AF[searchField] != nil {
				for _, f := range docData.AF[searchField] {
					results = append(results, strconv.FormatFloat(f, 'f', -1, 64))
				}
			} else if docData.AB[searchField] != nil {
				for _, f := range docData.AB[searchField] {
					results = append(results, strconv.FormatBool(f))
				}
			} else if docData.ARef[searchField] != nil {
				for _, f := range docData.ARef[searchField] {
					results = append(results, f)
				}
			}
		}
		// construct results HTML
		var optionsBuilder strings.Builder
		// include empty option first so that the default option is not random.
		optionsBuilder.WriteString(`<option class="pointer"> </option>` + "\n")
		for _, result := range results {
			optionsBuilder.WriteString(`<option class="pointer">`)
			optionsBuilder.WriteString(result)
			optionsBuilder.WriteString("</option>\n")
		}
		modDOMs = append(modDOMs, common.ModDOM{
			DeleteSelector: "[id^=" + common.CSSEscape(inputID) + "] > *", // remove all existing OPTION
			InsertSelector: "[id^=" + common.CSSEscape(inputID) + "]",
			Action:         "beforeend",
			HTML:           optionsBuilder.String(),
		})
		if numResults >= 10_000 {
			common.LogError(errors.New("Search query for input ID (" + inputID + ") returned >= 10,000 results so not all " +
				"results were returned. This leads to unexpected behavior for users. Query Field: " + searchField))
		}
	}

	// Much like how we sometimes want to overwrite Data IDs with particular values, sometimes it's best to overwrite
	// User IDs with actual user IDs.
	doc.Find("*[id*=polyappOverwriteWithUserID]").Each(func(i int, selection *goquery.Selection) {
		idToOverwrite, ok := selection.Attr("id")
		if !ok {
			return
		}
		newID := strings.ReplaceAll(idToOverwrite, "polyappOverwriteWithUserID", request.UserID)
		selection.SetAttr("id", newID)
	})

	// keep this code in sync with 'handleResponse(body)' in main.js where it's placing the modDOMs into the DOM.
	for _, modDOM := range modDOMs {
		if modDOM.DeleteSelector != "" {
			doc.Find(modDOM.DeleteSelector).Remove()
		}
		if modDOM.InsertSelector != "" && modDOM.HTML != "" && modDOM.Action != "" {
			el := doc.Find(modDOM.InsertSelector)
			if el == nil || el.Nodes == nil || len(el.Nodes) < 1 {
				continue
			}
			if len(el.Nodes) > 1 && strings.Contains(modDOM.DeleteSelector, "polyappError") {
				return nil, "", fmt.Errorf("modDOM was invalid because the InsertSelector found more than one element and this seems to be a polyappError. " +
					"Although allowed in JS, in Go all such modDOMs should be for errors so this means we have duplicate IDs in the " +
					"goquery doc. Selector: " + modDOM.InsertSelector)
			}
			switch modDOM.Action {
			case "beforebegin":
				el.BeforeHtml(modDOM.HTML)
			case "afterbegin":
				el.PrependHtml(modDOM.HTML)
			case "beforeend":
				el.AppendHtml(modDOM.HTML)
			case "afterend":
				el.AfterHtml(modDOM.HTML)
			}
		}
		if modDOM.AddClassSelector != "" && modDOM.AddClasses != nil && len(modDOM.AddClasses) > 0 {
			doc.Find(modDOM.AddClassSelector).AddClass(modDOM.AddClasses...)
		}
		if modDOM.RemoveClassSelector != "" && modDOM.RemoveClasses != nil && len(modDOM.RemoveClasses) > 0 {
			doc.Find(modDOM.RemoveClassSelector).RemoveClass(modDOM.RemoveClasses...)
		}
	}
	htmlOut, err := doc.Html()
	if err != nil {
		return nil, "", fmt.Errorf("HTML: %w", err)
	}
	// remove parts of the HTML we don't want which are added by the renderer - ie, <html><head></head><body> and the closing tags.
	removeStart := strings.Index(htmlOut, "<body>") + len(`<body>`)
	removeEnd := strings.LastIndex(htmlOut, "</body>")
	if removeEnd > removeStart {
		ux.HTML = common.String(htmlOut[removeStart:removeEnd])
	} else {
		ux.HTML = common.String(htmlOut)
	}
	return &ux, "", nil
}

// getAllDataAndTasksFromHeadData goes through data.Ref and data.ARef and retrieves their common.Data and common.Task and
// Refs before returning them in bulk.
//
// It's important 'data' is first because if there are child documents inside a dynamic array, the space for those
// children to exist is created as part of inserting the Data for the parent document into the DOM during insertDataIntoDOM
func getAllDataAndTasksFromHeadData(data common.Data, task common.Task) ([]*common.Data, []*common.Task, []string, error) {
	allDataForThisTask := []*common.Data{
		&data,
	}
	allTasksForThisTask := []*common.Task{
		&task,
	}
	allRefs := []string{""}
	chanLength := 0
	errChan := make(chan error)
	dataChan := make(chan *common.Data)
	taskChan := make(chan *common.Task)
	refsChan := make(chan string)
	for k, v := range data.Ref {
		chanLength++
		go func(k string, v *string, errChan chan error, dataChan chan *common.Data, taskChan chan *common.Task, refsChan chan string) {
			data, task, err := getDataAndTaskFromRawRef(*v)
			if err != nil {
				errChan <- fmt.Errorf("getDataAndTaskFromRawRef at key "+k+": %w", err)
				dataChan <- data
				taskChan <- task
				refsChan <- *v
				return
			}
			errChan <- nil
			dataChan <- data
			taskChan <- task
			refsChan <- *v
		}(k, v, errChan, dataChan, taskChan, refsChan)
	}
	for kARef, v := range data.ARef {
		for _, unparsedRef := range v {
			chanLength++
			go func(k string, v string, errChan chan error, dataChan chan *common.Data, taskChan chan *common.Task, refsChan chan string) {
				data, task, err := getDataAndTaskFromRawRef(v)
				if err != nil {
					errChan <- fmt.Errorf("getDataAndTaskFromRawRef in range ARef at key %v and with v %v: %w", kARef, v, err)
					dataChan <- data
					taskChan <- task
					refsChan <- v
					return
				}
				errChan <- nil
				dataChan <- data
				taskChan <- task
				refsChan <- v
			}(kARef, unparsedRef, errChan, dataChan, taskChan, refsChan)
		}
	}
	combinedErr := ""
	for i := 0; i < chanLength; i++ {
		err := <-errChan
		allDataForThisTask = append(allDataForThisTask, <-dataChan)
		allTasksForThisTask = append(allTasksForThisTask, <-taskChan)
		allRefs = append(allRefs, <-refsChan)
		if err != nil {
			combinedErr += err.Error()
		}
	}
	if combinedErr != "" {
		return nil, nil, nil, fmt.Errorf("combinedErr: %v", combinedErr)
	}
	return allDataForThisTask, allTasksForThisTask, allRefs, nil
}

// getDataAndTaskFromRawRef is used during GET when you are trying to retrieve the subtasks within the parent Task.
func getDataAndTaskFromRawRef(rawRef string) (*common.Data, *common.Task, error) {
	var ref string
	var err error
	if len(rawRef) < 1 {
		return nil, nil, fmt.Errorf("rawRef (%v) was empty string so we can't extract its Data and Task", rawRef)
	}
	if (rawRef)[0] != '/' {
		ref, err = url.PathUnescape(rawRef)
		if err != nil {
			return nil, nil, fmt.Errorf("PathUnescape: %w", err)
		}
	} else {
		ref = rawRef
	}
	parsedRef, err := url.ParseRequestURI(ref)
	if err != nil {
		return nil, nil, fmt.Errorf("ParseRequestURI: %w", err)
	}
	refRequest, err := common.CreateGETRequest(*parsedRef)
	if err != nil {
		return nil, nil, fmt.Errorf("CreateGETRequest: %w", err)
	}
	refData, err := allDB.ReadData(refRequest.DataID)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		// Imagine there is a list of Refs and you add one to the list. But you do not change any of its values, and then close out of the Task.
		// In this case the Ref's value was never created by a POST request, only its reference.
		// We COULD create Data when the Data POST request is made, but then it would be messy since if it's a long list
		// we'd have to read every list element document
		refData := &common.Data{
			FirestoreID: refRequest.DataID,
			IndustryID:  refRequest.GetIndustry(),
			DomainID:    refRequest.GetDomain(),
			SchemaID:    refRequest.SchemaID,
		}
		_ = refData.Init(nil)
		err = allDB.CreateData(refData)
		if err != nil {
			return nil, nil, fmt.Errorf("CreateData: %w", err)
		}
	}
	if err != nil {
		return nil, nil, fmt.Errorf("ReadData: %w", err)
	}
	refTask, err := allDB.ReadTask(refRequest.GetTask())
	if err != nil {
		return nil, nil, fmt.Errorf("ReadTask: %w", err)
	}
	return &refData, &refTask, nil
}

// insertDataIntoDOM places all data from simplifiedData and puts it into the correct fields in the DOM.
//
// insertDataIntoDOM is scoped by its sel parameter so you only ever find fields associated with this dataID.
// If the sel parameter were to be set to the entire document all ARefs would match for every ARef Data ID & we would
// (incorrectly) overwrite Data in subsequent array indexes.
//
// This function is substantially similar to setAllData in main.js.
func insertDataIntoDOM(sel *goquery.Selection, simplifiedData map[string]interface{}, dataID string) error {
	for k, v := range simplifiedData {
		findableKey := url.PathEscape(k)
		// func Find in jquery was written for backwards compatibility so we need to escape our newfangled HTML5 IDs.
		findableKey = common.CSSEscape(findableKey)

		selected := sel.Find("[id^=" + findableKey + "]") // use "starts with operator "^=" b/c of ind_dom_coll_field_# where # is random
		if selected.Nodes == nil {
			// this should be common since Schema fields are a superset of the UX fields
			continue
		}
		// I used to have a check right here which tried to verify that none of the selected nodes have identical IDs
		// But I suppose the browser will do that work for us if we are catching and logging console errors in javascript.
		var err error
		simpleListIndex := 0
		selected.EachWithBreak(func(selectedIndex int, selection *goquery.Selection) bool {
			id, _ := selection.Attr("id")
			if len(id) > len(url.PathEscape(k)) && id[len(url.PathEscape(k))] != '_' {
				// It's a trap! If there are 2 IDs, "file" and "file%20this", then the value for "file%20this" will match
				// twice - once for "file" since they share a prefix and once for "file%20this". If "file%20this" matches
				// second, it will overwrite whatever "file" put into the field.
				return true
			}
			inputType, _ := selection.Attr("type")
			if selection.Is("input") && inputType == "checkbox" {
				switch concreteV := v.(type) {
				case bool:
					if concreteV {
						selection.SetAttr("checked", "true")
						selection.SetAttr("value", "true")
					} else {
						selection.RemoveAttr("checked")
						selection.SetAttr("value", "false")
					}
				default:
					err = errors.New("tried to set a checkbox with a non-boolean value at key: " + k)
					return false
				}
			} else if selection.Is("input") && inputType == "file" {
				inputID, _ := selection.Attr("id")
				switch v.(type) {
				case []byte:
					sel.Find("#"+common.CSSEscape("polyappJSImagePreview"+inputID)).SetAttr("src", common.BlobURL(dataID, inputID))
				case [][]byte:
					// No support for demoing multiple uploaded images in the UI.
				default:
					err = errors.New("File Input field (" + inputID + ") was not an []byte")
					return false
				}
			} else if selection.Is("input") && selection.AttrOr("data-polyappjs-select-collection", "") != "" {
				switch concreteV := v.(type) {
				case string:
					if concreteV == "" {
						return true
					}
					collection := selection.AttrOr("data-polyappjs-select-collection", "")
					humanValue := ""
					switch collection {
					case common.CollectionData:
						q, err2 := allDB.ReadData(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadData: %w", err2)
							return false
						}
						field := selection.AttrOr("data-polyappjs-select-field", common.PolyappFirestoreID)
						if q.S[field] != nil {
							humanValue = *q.S[field]
						}
					case common.CollectionTask:
						q, err2 := allDB.ReadTask(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadTask: %w", err)
							return false
						}
						humanValue = q.Name
					case common.CollectionSchema:
						q, err2 := allDB.ReadSchema(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadSchema: %w", err)
							return false
						}
						if q.Name != nil {
							humanValue = *q.Name
						}
					case common.CollectionUX:
						q, err2 := allDB.ReadUX(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadUX: %w", err)
							return false
						}
						if q.Title != nil {
							humanValue = *q.Title
						}
					case common.CollectionUser:
						q, err2 := allDB.ReadUser(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadUser: %w", err)
							return false
						}
						if q.FullName != nil {
							humanValue = *q.FullName
						}
					case common.CollectionRole:
						q, err2 := allDB.ReadRole(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadRole: %w", err)
							return false
						}
						humanValue = q.Name
					case common.CollectionBot:
						q, err2 := allDB.ReadBot(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadBot: %w", err)
							return false
						}
						humanValue = q.Name
					case common.CollectionAction:
						q, err2 := allDB.ReadAction(concreteV)
						if err2 != nil {
							err = fmt.Errorf("allDB.ReadAction: %w", err)
							return false
						}
						humanValue = q.Name
					}
					selection.SetAttr("value", humanValue)
					selection.SetAttr("data-polyappjs-select-value", concreteV)
					selection.SetAttr("data-polyappjs-select-field-initial-human-value", humanValue)
				default:
					err = errors.New("could not figure out how to set a Select ID input")
					return false
				}
			} else if selection.Is("input") || selection.Is("select") {
				switch concreteV := v.(type) {
				case string:
					if concreteV == "#ffff00" {
						common.LogError(fmt.Errorf("#ffff00 seen in dataID %v", dataID))
					}
					selection.SetAttr("value", concreteV)
				case bool:
					if concreteV {
						selection.SetAttr("value", "true")
					} else {
						selection.SetAttr("value", "false")
					}
				case float64:
					selection.SetAttr("value", strconv.FormatFloat(concreteV, 'f', -1, 64))
				case int64:
					selection.SetAttr("value", strconv.FormatInt(concreteV, 10))
				case []string:
					if len(concreteV) > 0 {
						var arrayValue strings.Builder
						arrayValue.WriteString("[")
						for i, s := range concreteV {
							if i > 0 {
								arrayValue.WriteString(",")
							}
							arrayValue.WriteString(`"`)
							arrayValue.WriteString(s)
							arrayValue.WriteString(`"`)
						}
						arrayValue.WriteString("]")
						selection.SetAttr("value", arrayValue.String())
					}
				default:
					err = errors.New("could not figure out how to set an input node for k: " + k)
					return false
				}
			} else if selection.Is("textarea") {
				switch concreteV := v.(type) {
				case string:
					selection.SetText(concreteV)
				case bool:
					if concreteV {
						selection.SetText("true")
					} else {
						selection.SetText("false")
					}
				case float64:
					selection.SetText(strconv.FormatFloat(concreteV, 'f', -1, 64))
				case int64:
					selection.SetText(strconv.FormatInt(concreteV, 10))
				case []bool:
					// currently unhandled. This should support a list of checkboxes.
					return true
				default:
					err = errors.New("could not figure out how to set an input node for k: " + k)
					return false
				}
			} else if selection.Is("a") || selection.Is("button") {
				switch concreteV := v.(type) {
				case string:
					// Note: this case is out of sync with javascript
					selection.SetText(concreteV)
				}
				// do nothing most of the time since a and button are supposed to only be set temporarily in the DB. See: main.js
				// and var KeysToBeRemoved
			} else if selection.Is("label") {
				// static content
				switch concreteV := v.(type) {
				case string:
					selection.SetHtml(concreteV)
				}
			} else if selection.Is("div") && selection.HasClass("form-group") && selection.Children().First().Is("label") {
				// This is a list. What is currently 'selection' is the div form-group which surrounds the label for the list and contains the list-group.
				// For each existing list member (ref in refs array) we can clone the displayNone element to make a new list element.
				var refs []string
				switch castV := v.(type) {
				case []int64:
					for castVIndex := range castV {
						refs = append(refs, strconv.FormatInt(castV[castVIndex], 10))
					}
				case []float64:
					for castVIndex := range castV {
						refs = append(refs, strconv.FormatFloat(castV[castVIndex], 'f', -1, 64))
					}
				case []string:
					refs = castV
				default:
					err = errors.New("this list-based pattern should be for a list of Refs or a list of single-line inputs")
					return false
				}
				for _, ref := range refs {
					var dataIDInRef string // only 1 per Ref, so why not cache it?
					// Note this code is very similar to how 'polyappJShtml5sortable-add' works, aka the handleEvent function.
					// the main difference is the Go code has to 'add' a list of new elements (thus, _, ref := range refs)
					// whereas the javascript code only has to add one new element.
					// Also, the javascript code rarely wants to set the value of the list, but we DO want to set the value
					// of the list IF and only if the data in simplifiedData is for this element. There are 2 options: either
					// the data is for this Task or Subtask, or the Data is for a parent Task & a child task will set the data later.
					displayNoneCloned := selection.ChildrenFiltered(".displayNone").Eq(0).Children().Eq(0).Clone()
					displayNoneCloned.Find("[id]").Each(func(i int, s *goquery.Selection) {
						currentID, exists := s.Attr("id")
						if !exists {
							common.LogError(fmt.Errorf("something is messed up with the goquery library if an element could be selection by ID but ID did not exist in node. Data ID: %v", dataID))
							return
						}
						currentIDBaseLength := strings.LastIndex(currentID, "_")
						if strings.HasPrefix(currentID, "polyappJShtml5sortable-remove") {
							// clearly we do not want to change the ID of this element. We only want to change how it is appended
							s.SetAttr("id", currentID+"_"+common.GetRandString(15))
						} else if strings.HasPrefix(currentID, "polyappJSDisplayNonepolyappJShtml5sortable-remove") {
							s.SetAttr("id", strings.TrimPrefix(currentID, "polyappJSDisplayNone")+"_"+common.GetRandString(15))
						} else if strings.HasPrefix(currentID, "polyappJSDisplayNone") && strings.HasPrefix(currentID[len("polyappJSDisplayNone"):currentIDBaseLength], url.PathEscape(k)) {
							// simple list functionality
							//
							// Typically a simple list is inside the main Data, which means the values at this key
							// are the actual values in the array & therefore we should SetAttr("value", arrayValues)
							//
							// However, it's possible this list is within a sub-Task & when we go to set the arrayValues,
							// we will incorrectly set Ref strings instead of the sub-Task's values for this array.
							// To avoid that, only set the value "v" if the ID of this element matches v's key.
							//
							// An alternative approach would be to reject any set whose key starts with "_" since that => Ref.
							newID := currentID[len("polyappJSDisplayNone"):currentIDBaseLength] + "_" + common.GetRandString(15)
							s.SetAttr("id", newID)

							// Set the value. There is a chance that this will fail because # of inputs in the control > 1
							// in which case we would hit this 2x or 3x or 4x times more than expected. But no simple lists
							// are currently implemented that way.
							if s.Is("input") {
								switch concreteV := v.(type) {
								case []string:
									s.SetAttr("value", concreteV[simpleListIndex])
								case []bool:
									if concreteV[simpleListIndex] {
										s.SetAttr("value", "true")
									} else {
										s.SetAttr("value", "false")
									}
								case []float64:
									s.SetAttr("value", strconv.FormatFloat(concreteV[simpleListIndex], 'f', -1, 64))
								case []int64:
									s.SetAttr("value", strconv.FormatInt(concreteV[simpleListIndex], 10))
								default:
									err = errors.New("simple list had unhandled type for k: " + k + " and newID: " + newID)
									return
								}
								simpleListIndex++
							}
						} else {
							if dataIDInRef == "" {
								unescapedRef, err := url.PathUnescape(ref)
								if err != nil {
									return
								}
								parsed, err := url.Parse(unescapedRef)
								if err != nil {
									return
								}
								dataIDInRef = parsed.Query().Get("data")
							}
							// nested components functionality
							// update this node's ID and the update any references to this node.
							// Note: the content of data-target and data-parent has been CSS escaped which is why escaping is applied to the ID.
							// We are also querying this escaped value, so we need to perform a second escaping.
							_ = updateLinked(s, currentID, displayNoneCloned, dataIDInRef)
						}
					})
					if err != nil {
						return false
					}
					selection.Children().Eq(2).AppendSelection(displayNoneCloned)
					// this is super helpful for debugging IMO
					//out, _ := selection.Html()
					//fmt.Println(out)
				}
			} else if selection.Is("div") && selection.HasClass("polyappJSsummernote") {
				switch concreteV := v.(type) {
				case string:
					selection.SetHtml(concreteV)
				default:
					err = errors.New("summernote control can not be set with something other than a string")
					return false
				}
			} else if selection.Is("div") && selection.HasClass("polyappDateTime") {
				switch concreteV := v.(type) {
				case int64:
					if concreteV != 0 {
						selection.Children().First().SetAttr("value", strconv.FormatInt(concreteV, 10))
					}
				default:
					err = errors.New("polyapp Date Time control can only be set with an int64")
					return false
				}
			} else if selection.Is("iframe") {
				switch concreteV := v.(type) {
				case string:
					// this is pattern-breaking. Here we're always directly applying the value. But iFrames are meaningless
					// if they point to nothing. Therefore I'm instituting this check to ensure we don't overwrite a real
					// value default which is being set by the UX document.
					if concreteV != "" {
						selection.SetAttr("src", concreteV)
					}
				case bool:
					// should not happen
				case float64:
					// should not happen
				case int64:
					// should not happen
				default:
					err = errors.New("could not figure out how to set an input node for k: " + k)
					return false
				}
			}
			return true
		})
		if err != nil {
			return err
		}
	}
	return nil
}

// updateLinked tries to find IDs which are linked to this ID and update them all when you update the ID. It updates the
// ID primarily by trying to find "polyappShouldBeOverwritten" within the ID and replacing it with "dataIDInRef" input. If that
// fails it may try to replace the ID in a couple of other ways, or else append dataIDInRef to the end of the ID.
//
// selectedIDNode: the node that you want to remove "polyappShouldBeOverwritten" from and replace with a random string.
//
// currentID: the current value of the ID. Used to find nodes which are dependent upon this ID.
//
// sel: the parent selection in which to search for IDs to replace / update. Typically this is the ID's parent's parent node.
//
// dataIDInRef: the new value for the ID.
//
// This function returns the new Ref.
func updateLinked(selectedIDNode *goquery.Selection, currentID string, sel *goquery.Selection, dataIDInRef string) string {
	linkedDataTarget := sel.Find(`[data-target="` + common.CSSEscape("#"+common.CSSEscape(currentID)) + `"]`)
	linkedDataParent := sel.Find(`[data-parent="` + common.CSSEscape("#"+common.CSSEscape(currentID)) + `"]`)
	linkedFor := sel.Find(`[for="` + common.CSSEscape(currentID) + `"]`)
	linkedAriaControls := sel.Find(`[aria-controls="` + currentID + `"]`)
	indexOfDataToOverwrite := strings.Index(currentID, "&data=polyappShouldBeOverwritten")
	if indexOfDataToOverwrite < 0 {
		// most cases probably go here?
		// industry_domain_schema_field%20name_randomthingaddedtopreventcollisions
		newID := ""
		currentIDSplit := strings.Split(currentID, "_")
		if len(currentIDSplit) == 5 {
			newID = currentIDSplit[0] + "_" + currentIDSplit[1] + "_" + currentIDSplit[2] +
				"_" + currentIDSplit[3] + "_" + dataIDInRef + "_" + currentIDSplit[4]
			selectedIDNode.SetAttr("data-polyappid", currentIDSplit[0]+"_"+currentIDSplit[1]+"_"+currentIDSplit[2]+
				"_"+currentIDSplit[3]+"_"+dataIDInRef)
		} else {
			newID = currentID + "_" + dataIDInRef
			selectedIDNode.SetAttr("data-polyappid", newID)
		}
		linkedDataTarget.SetAttr("data-target", "#"+common.CSSEscape(newID))
		linkedDataParent.SetAttr("data-parent", "#"+common.CSSEscape(newID))
		linkedFor.SetAttr("for", newID)
		linkedAriaControls.SetAttr("for", newID)
		selectedIDNode.SetAttr("id", newID)
		return newID
	}

	newID := currentID[:indexOfDataToOverwrite] + "&data=" + dataIDInRef +
		currentID[indexOfDataToOverwrite+len("&data=polyappShouldBeOverwritten"):]
	linkedDataTarget.SetAttr("data-target", "#"+common.CSSEscape(newID))
	linkedDataParent.SetAttr("data-parent", "#"+common.CSSEscape(newID))
	linkedFor.SetAttr("for", newID)
	linkedAriaControls.SetAttr("for", newID)
	selectedIDNode.SetAttr("id", newID)
	newIDSplit := strings.Split(newID, "_")
	if len(newIDSplit) == 5 {
		selectedIDNode.SetAttr("data-polyappid", newIDSplit[0]+"_"+newIDSplit[1]+"_"+newIDSplit[2]+
			"_"+newIDSplit[3]+"_"+dataIDInRef)
	} else {
		selectedIDNode.SetAttr("data-polyappid", newID)
	}
	return newID
}

// innerIntoAppShell places the inner HTML of a Task into the HTML of the App Shell
// This is substantially similar to the client function
func InnerIntoAppShell(innerHTML []byte, appShell []byte) (string, error) {
	beginning := bytes.SplitAfterN(appShell, []byte(`id="polyappJSMain">`), 2)
	if len(beginning) != 2 {
		return "", errors.New("wasn't able to bifurcate app shell for beginning")
	}
	ending := bytes.SplitAfterN(appShell, []byte(`</main>`), 2)
	if len(ending) != 2 {
		return "", errors.New("wasn't able to bifurcate app shell for ending")
	}
	firstPart := bytes.Join([][]byte{beginning[0], innerHTML}, []byte(``))
	allParts := bytes.Join([][]byte{firstPart, ending[1]}, []byte(`</main>`))
	return string(allParts), nil
}

// GETDataTable performs a GET for the DataTables.net data table request.
func GETDataTable(request common.DataTableRequest) (common.DataTableResponse, error) {
	var err error
	q := allDB.Query{}
	q.Init(request.IndustryID, request.DomainID, request.SchemaID, common.CollectionData)
	for i, colNumber := range request.OrderColumn {
		colFieldName, _ := url.PathUnescape(request.ColumnsName[colNumber])
		err = q.AddSortBy(colFieldName, request.OrderDir[i])
		if err != nil {
			return common.DataTableResponse{}, fmt.Errorf("AddSortBy for index "+strconv.Itoa(i)+": %w", err)
		}
	}
	// len(q.SortBy) and len(q.SortDirection) should always == 1 here because we're always sorting by a column.
	if request.SearchInput != "" && len(q.SortBy) == 1 {
		q.AddEquals(q.SortBy[0], request.SearchInput)
		// Without this, err: "code = InvalidArgument desc = order by clause cannot contain a field with an equality filter"
		q.SortBy = []string{}
		q.SortDirection = []string{}
	}
	q.SetStartAfterDocumentID(request.LastDocumentID)
	q.SetOffset(request.Start) // TODO create long-running queries or store refs to documents such as the last document
	// currently on screen, and store them in a map with keys as their index in the array, and use that data instead of SetOffset.
	q.SetLength(request.Length)
	iter, err := q.QueryRead()
	if err != nil {
		return common.DataTableResponse{}, fmt.Errorf("QueryRead: %w", err)
	}
	dataOut := make([][]string, 0)
	lastDocumentID := ""
	numSkipped := 0
	for {
		doc, err := iter.Next() // calling Next is often very fast
		if err == common.IterDone {
			break
		}
		if err != nil {
			return common.DataTableResponse{}, fmt.Errorf("error iterating during QueryRead: %w", err)
		}
		simple, err := doc.Simplify()
		if err != nil && err.Error() == "Industry, Domain, and Schema are required due to the need to prefix the fields." {
			// This should really never be necessary since it indicates bad data in the database,
			// but I want to be able to find bad data and fix it in the UI so this seems to be necessary.
			docEditable := doc.(*common.Data)
			docEditable.IndustryID = request.IndustryID
			docEditable.DomainID = request.DomainID
			docEditable.SchemaID = request.SchemaID
			simple, err = docEditable.Simplify()
		}
		if err != nil {
			return common.DataTableResponse{}, fmt.Errorf("doc.Simplify err: %w", err)
		}
		common.Dereference(simple)
		thisRow := make([]string, len(request.ColumnsName))
		for i, key := range request.ColumnsName {
			if key == "polyappFirestoreID" {
				thisRow[i] = doc.GetFirestoreID()
				continue
			}
			keyUnescaped, _ := url.PathUnescape(key)
			toSet := simple[keyUnescaped]
			// this feels wrong without a type switch but strings do be like that sometimes.
			thisRow[i] = fmt.Sprintf("%v", toSet)
		}
		dataOut = append(dataOut, thisRow)
		lastDocumentID = doc.GetFirestoreID()
	}
	numRecord := iter.Length() - numSkipped
	if numRecord < 0 {
		// set it so that you can paginate forwards. Of course maybe this isn't the true length & you can't paginate forwards
		// but TODO cross that bridge later
		numRecord = request.Length + request.Start + 1
	}
	return common.DataTableResponse{
		Draw:            request.Draw,
		RecordsTotal:    numRecord,
		RecordsFiltered: numRecord,
		Data:            dataOut,
		Error:           "",
		LastDocumentID:  lastDocumentID,
	}, nil
}

// GETSelectSearch performs a search among the specified collection and looks for a field which matches the input.
func GETSelectSearch(s common.SelectSearchRequest) (common.SelectSearchResponse, error) {
	r := common.SelectSearchResponse{}
	var err error
	results := make([]gen.SelectIDResult, 0)
	switch s.Collection {
	case common.CollectionData:
		industry, domain, schema, _ := common.SplitField(s.Field)
		q := allDB.Query{}
		q.Init(industry, domain, schema, common.CollectionData)
		q.AddEquals(s.Field, s.Value)
		iter, err := q.QueryRead()
		if err != nil {
			return common.SelectSearchResponse{}, fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			d, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return common.SelectSearchResponse{}, fmt.Errorf("iter.Next: %w", err)
			}
			data := d.(*common.Data)
			s := data.S[s.Field]
			if s != nil {
				results = append(results, gen.SelectIDResult{
					IDValue:    d.GetFirestoreID(),
					HumanValue: *s,
					Context:    data.IndustryID + " / " + data.DomainID + " / " + data.SchemaID,
				})
			}
		}
	case common.CollectionSchema, common.CollectionTask, common.CollectionUX, common.CollectionBot, common.CollectionAction,
		common.CollectionUser, common.CollectionRole:
		q := allDB.Query{}
		q.Init("", "", "", s.Collection)
		q.AddEquals(s.Field, s.Value)
		iter, err := q.QueryRead()
		if err != nil {
			return common.SelectSearchResponse{}, fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			queryable, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return common.SelectSearchResponse{}, fmt.Errorf("iter.Next: %w", err)
			}
			simplified, err := queryable.Simplify()
			if err != nil {
				return common.SelectSearchResponse{}, fmt.Errorf("queryable.Simplify: %w", err)
			}
			genSelectIDResult := gen.SelectIDResult{
				IDValue: queryable.GetFirestoreID(),
			}
			var ok bool
			switch s.Collection {
			case common.CollectionTask:
				genSelectIDResult.HumanValue, ok = simplified["Name"].(string)
				if !ok {
					return common.SelectSearchResponse{}, fmt.Errorf("humanFieldValue was not a string for %v's FirestoreID: %v", s.Collection, queryable.GetFirestoreID())
				}
				if simplified[common.PolyappIndustryID] != nil && simplified[common.PolyappDomainID] != nil {
					genSelectIDResult.Context = simplified[common.PolyappIndustryID].(string) + " / " + simplified[common.PolyappDomainID].(string)
				}
			case common.CollectionBot, common.CollectionAction, common.CollectionRole:
				genSelectIDResult.HumanValue, ok = simplified["Name"].(string)
				if !ok {
					return common.SelectSearchResponse{}, fmt.Errorf("humanFieldValue was not a string for %v's FirestoreID: %v", s.Collection, queryable.GetFirestoreID())
				}
			case common.CollectionUser:
				humanValuePtr, ok := simplified["FullName"].(*string)
				if !ok {
					return common.SelectSearchResponse{}, fmt.Errorf("humanFieldValue was not a *string for User's FirestoreID: %v", queryable.GetFirestoreID())
				}
				genSelectIDResult.HumanValue = *humanValuePtr
			case common.CollectionUX:
				humanValuePtr, ok := simplified["Title"].(*string)
				if !ok {
					return common.SelectSearchResponse{}, fmt.Errorf("humanFieldValue was not a *string for UX's FirestoreID: %v", queryable.GetFirestoreID())
				}
				genSelectIDResult.HumanValue = *humanValuePtr
				if simplified[common.PolyappIndustryID] != nil && simplified[common.PolyappDomainID] != nil {
					genSelectIDResult.Context = simplified[common.PolyappIndustryID].(string) + " / " + simplified[common.PolyappDomainID].(string)
				}
			case common.CollectionSchema:
				humanValuePtr, ok := simplified["Name"].(*string)
				if !ok {
					return common.SelectSearchResponse{}, fmt.Errorf("humanFieldValuePtr was not a *string for Schema's FirestoreID: %v", queryable.GetFirestoreID())
				}
				genSelectIDResult.HumanValue = *humanValuePtr
				if simplified[common.PolyappIndustryID] != nil && simplified[common.PolyappDomainID] != nil {
					genSelectIDResult.Context = simplified[common.PolyappIndustryID].(string) + " / " + simplified[common.PolyappDomainID].(string)
				}
			}

			results = append(results, genSelectIDResult)
		}
	}
	if len(results) > 0 {
		var resultHTMLBuffer bytes.Buffer
		err = gen.GenSelectIDResultHTML(results, &resultHTMLBuffer)
		if err != nil {
			return common.SelectSearchResponse{}, fmt.Errorf("GenSelectIDResultHTML: %w", err)
		}
		r.ResultHTML = resultHTMLBuffer.String()
	} else {
		r.ResultHTML = gen.GenSelectIDNoResultsHTML()
	}
	return r, nil
}
