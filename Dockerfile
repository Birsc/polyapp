# Dockerfile is for CI/CD pipelines which use Polyapp. It is NOT a production Dockerfile.
# If you want to deploy Polyapp on Docker please add an issue in Gitlab and I will migrate my private-repo Dockerfile over.
#
# How to deploy this to Gitlab:
# 1. Clone the repository and cd ./polyapp
# 2. docker login registry.gitlab.com
# 3. docker build -t registry.gitlab.com/polyapp-open-source/polyapp .
# 4. docker push registry.gitlab.com/polyapp-open-source/polyapp
FROM golang:1.14

# Install gcloud command line tool
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz
RUN mkdir -p /usr/local/gcloud && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz && /usr/local/gcloud/google-cloud-sdk/install.sh --quiet
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

# Install poly and golangci-lint
RUN go get -u gitlab.com/polyapp-open-source/poly
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.23.6

# Install npm
RUN apt-get update -yq && apt-get upgrade -yq && apt-get install -yq curl git nano
RUN apt install nodejs npm -yq
RUN npm install -g --unsafe-perm npm

# Install minifier (not used right now)
RUN npm install --global --unsafe-perm esbuild

# Get dependencies downloaded into the docker image
# Elastic isn't used right now but it may be used again in the future.
RUN if cd /go/src/github.com/elastic/go-elasticsearch/v8; then git pull; else git clone -v --branch master https://github.com/elastic/go-elasticsearch.git /go/src/github.com/elastic/go-elasticsearch/v8; fi
RUN go get cloud.google.com/go
RUN go get cloud.google.com/go/pubsub
RUN go get github.com/googleapis/gax-go
RUN go get cloud.google.com/go/firestore
RUN go get cloud.google.com/go/errorreporting
RUN go get cloud.google.com/go/logging
RUN go get cloud.google.com/go/storage
RUN go get github.com/rs/cors
RUN go get firebase.google.com/go
RUN go get github.com/labstack/echo
RUN go get github.com/PuerkitoBio/goquery
# Errs because no go files in directory but works on my machine RUN go get github.com/gomodule/redigo
