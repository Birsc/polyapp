package actions

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

// ActionExternalAPIIngressData calls an external API and puts the response into the current Task. Configuration is
// provided by Bot Static Data pointing to an "API Call"'s Bot Static Data To Use.
func ActionExternalAPIIngressData(data *common.Data, request *common.POSTRequest, response *common.POSTResponse,
	botStaticData map[string]string) error {

	mWithPrefix, err := externalAPICall(data, request, response, botStaticData)
	if err != nil {
		return err
	}
	if mWithPrefix == nil {
		// requested by externalAPICall's docs.
		return nil
	}

	dataIn := common.Data{}
	err = dataIn.Init(mWithPrefix)
	if err != nil {
		return fmt.Errorf("dataIn.Init: %w", err)
	}
	dataIn.FirestoreID = data.FirestoreID
	dataIn.IndustryID = data.IndustryID
	dataIn.DomainID = data.DomainID
	dataIn.SchemaID = data.SchemaID
	err = data.Merge(&dataIn)
	if err != nil {
		return fmt.Errorf("data.Merge: %w", err)
	}
	if data.SchemaCache.FirestoreID == "" {
		data.SchemaCache, err = allDB.ReadSchema(data.SchemaID)
		if err != nil {
			return fmt.Errorf("allDB.ReadSchema: %w", err)
		}
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	isAtLoad, _, isAtDone := WhereIsBotBeingRun(request)
	if isAtLoad || isAtDone {
		// we do not need to redirect the person to view the updated Data because it will be populated for them.
	} else {
		// if we do not redirect to view this Data we will not see the result of our API call
		response.NewURL = common.CreateURLFromRequest(*request)
	}

	return nil
}

// ActionExternalAPIEgressData calls an external API and puts the response into the current Task. Configuration is
// provided by Bot Static Data pointing to an "API Call"'s Bot Static Data To Use.
func ActionExternalAPIEgressData(data *common.Data, request *common.POSTRequest, response *common.POSTResponse,
	botStaticData map[string]string) error {
	if botStaticData["APICallData"] == "" {
		return errors.New("No APICallData in Bot Static Data")
	}

	mWithPrefix, err := externalAPICall(data, request, response, botStaticData)
	if err != nil {
		return err
	}
	if mWithPrefix == nil {
		// requested by externalAPICall's docs.
		return nil
	}

	isAtLoad, _, isAtDone := WhereIsBotBeingRun(request)
	if isAtLoad || isAtDone {
		// we do not need to redirect the person to view the updated Data because it will be populated for them.
	} else {
		// if we do not redirect to view this Data we will not see the result of our API call
		response.NewURL = common.CreateURLFromRequest(*request)
	}

	return nil
}

// WhereIsBotBeingRun helps you adjust your code's behavior based on how where in the Task life cycle the Bot is being run.
func WhereIsBotBeingRun(request *common.POSTRequest) (atLoad bool, continously bool, atDone bool) {
	if request.MessageID == "" {
		atLoad = true
	} else if request.IsDone {
		atDone = true
	} else {
		continously = true
	}
	return
}

type APICallResponseMapping struct {
	PolyappFieldName     string `suffix:"Polyapp Field Name"`
	ExternalAPIFieldName string `suffix:"External API Field Name"`
}

type APICallRequestQueryMapping struct {
	PolyappFieldName     string `suffix:"Polyapp Field Name"`
	ExternalAPIFieldName string `suffix:"External API Field Name"`
}

type APICallRequestBodyMapping struct {
	PolyappFieldName     string `suffix:"Polyapp Field Name"`
	ExternalAPIFieldName string `suffix:"External API Field Name"`
}

// externalAPICall returns a map of the response which is ready to be initialized into a common.Data.
// This may return a nil map, in which case you should return immediately without an error.
func externalAPICall(data *common.Data, request *common.POSTRequest, response *common.POSTResponse, botStaticData map[string]string) (map[string]interface{}, error) {
	var err error
	if botStaticData["APICallData"] == "" {
		return nil, errors.New("No APICallData in Bot Static Data")
	}

	_, isContinuously, _ := WhereIsBotBeingRun(request)

	continuousTrigger := botStaticData["APICallContinuousTrigger"]
	if isContinuously && continuousTrigger == "" {
		return nil, errors.New("isContinuously but haven't set up a trigger. This is not allowed to reduce network traffic. Trigger is in BotStaticData and looks like this: \"APICallContinuousTrigger Field Name\"")
	}

	if data.B[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, continuousTrigger)] != nil &&
		*data.B[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, continuousTrigger)] == true &&
		isContinuously {
		data.B[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, continuousTrigger)] = common.Bool(false)
	} else if isContinuously {
		return nil, nil
	}

	_, ConfigureAPICall, responseMapping, bodyMapping, queryMapping, err := getAPICallInfo(botStaticData["APICallData"])
	if err != nil {
		return nil, fmt.Errorf("getAPICallInfo: %w", err)
	}

	simplifiedData, err := data.Simplify()
	if err != nil {
		return nil, fmt.Errorf("data.Simplify: %w", err)
	}
	bodyMappingMap := make(map[string]string)
	for k := range simplifiedData {
		bodyMappingMap[common.RemoveFieldPrefix(k)] = castValue(data, k)
	}
	for i := range bodyMapping {
		bodyMappingMap[bodyMapping[i].ExternalAPIFieldName] = castValue(data, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, bodyMapping[i].PolyappFieldName))
	}
	queryMappingMap := make(map[string]string)
	for k := range simplifiedData {
		queryMappingMap[common.RemoveFieldPrefix(k)] = castValue(data, k)
	}
	for i := range queryMapping {
		queryMappingMap[queryMapping[i].ExternalAPIFieldName] = castValue(data, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, queryMapping[i].PolyappFieldName))
	}

	err = DoConfigureAPICallRequest(&ConfigureAPICall, bodyMappingMap, queryMappingMap)
	if err != nil {
		return nil, fmt.Errorf("DocConfigureAPICallRequest: %w", err)
	}

	if ConfigureAPICall.APICallStatusCode != "200 OK" {
		return nil, fmt.Errorf("APICallStatusCode != 200 OK. Was: %v", ConfigureAPICall.APICallStatusCode)
	}

	externalNameToPolyappName := make(map[string]string)
	for i := range responseMapping {
		externalNameToPolyappName[responseMapping[i].ExternalAPIFieldName] = responseMapping[i].PolyappFieldName
	}

	// We may want to store the response in a new Data under the APICall.APIResponseSchemaID
	// That way we could retain a record of our API Call request (temporarily) and retain the API Call Response
	// too. We could use this record for... what? I don't currently have a use case, so I haven't implemented this.

	m := make(map[string]interface{})
	err = json.Unmarshal([]byte(ConfigureAPICall.APICallResponse), &m)
	if err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}
	mWithPrefix := make(map[string]interface{})
	for k, v := range m {
		polyappName, ok := externalNameToPolyappName[k]
		if ok {
			k = polyappName
		}
		mWithPrefix[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, k)] = v
	}

	return mWithPrefix, nil
}

func getAPICallInfo(APICallDataID string) (APICall, ConfigureAPICall, []APICallResponseMapping, []APICallRequestBodyMapping, []APICallRequestQueryMapping, error) {
	APICallData, err := allDB.ReadData(APICallDataID)
	if err != nil {
		return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("allDB.ReadData %v: %w", APICallDataID, err)
	}
	A := APICall{}
	err = common.DataIntoStructure(&APICallData, &A)
	if err != nil {
		return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.DataIntoStructur: %w", err)
	}
	C := ConfigureAPICall{}
	var ConfigureAPICallData common.Data
	for _, ref := range APICallData.Ref {
		getRef, err := common.ParseRef(*ref)
		if err != nil {
			return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.ParseRef: %w", err)
		}
		ConfigureAPICallData, err = allDB.ReadData(getRef.DataID)
		if err != nil {
			return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("allDB.ReadData %v: %w", getRef.DataID, err)
		}
		err = common.DataIntoStructure(&ConfigureAPICallData, &C)
		if err != nil {
			return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.DataIntoStructure: %w", err)
		}
		C.FirestoreID = getRef.DataID
		break
	}
	responseMapping := make([]APICallResponseMapping, 0)
	queryMapping := make([]APICallRequestQueryMapping, 0)
	bodyMapping := make([]APICallRequestBodyMapping, 0)
	for _, ARef := range APICallData.ARef {
		for _, ref := range ARef {
			getRef, err := common.ParseRef(ref)
			if err != nil {
				return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.ParseRef %v: %w", ref, err)
			}
			d, err := allDB.ReadData(getRef.DataID)
			if err != nil {
				return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("allDB.ReadData %v: %w", getRef.DataID, err)
			}
			switch getRef.SchemaID {
			case "InzJHBVAyldrwhaCqmXNbYJCd":
				// query responseMapping
				queryMap := APICallRequestQueryMapping{}
				err = common.DataIntoStructure(&d, &queryMap)
				if err != nil {
					return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.DataIntoStructure: %w", err)
				}
				queryMapping = append(queryMapping, queryMap)
			case "NzmecQWttNNpOdAfqqyJKUYFb":
				// body responseMapping
				queryMap := APICallRequestBodyMapping{}
				err = common.DataIntoStructure(&d, &queryMap)
				if err != nil {
					return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.DataIntoStructure: %w", err)
				}
				bodyMapping = append(bodyMapping, queryMap)
			case "JOUclEYsyCPQLRcsiJVUdBzAt":
				// response responseMapping
				queryMap := APICallResponseMapping{}
				err = common.DataIntoStructure(&d, &queryMap)
				if err != nil {
					return APICall{}, ConfigureAPICall{}, nil, nil, nil, fmt.Errorf("common.DataIntoStructure: %w", err)
				}
				responseMapping = append(responseMapping, queryMap)
			}
		}
	}

	return A, C, responseMapping, bodyMapping, queryMapping, nil
}
