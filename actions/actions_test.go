package actions

import (
	"encoding/json"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"reflect"
	"strings"
	"testing"
)

// TestCreateEditTaskFromJSON does not really test if this function works; it merely ensures it runs.
func TestCreateEditTaskFromJSON(t *testing.T) {
	got, _, err := CreateEditTaskFromJSON("polyapp", "JSON2Form", "Test Name", map[string]interface{}{
		"field": "value",
		"array": []interface{}{
			"array value 1",
			"array value 2",
		},
	})
	if err != nil {
		t.Fatal("CreateEditTaskFromJSON: " + err.Error())
	}
	if len(got) != 1+2 {
		t.Error("1 Edit Task Data and 2 Fields Datas should have been generated")
	}
	if got[0].SchemaID != "gjGLMlcNApJyvRjmgQbopsXPI" {
		t.Error("got SchemaID was not expected for EditTask")
	}
	if got[1].SchemaID != got[2].SchemaID || got[1].SchemaID != "SszMuOVRaMIOUteoWiXClbpSC" {
		t.Error("got SchemaID was not expected for Fields")
	}

	J := []byte(`{
	"field": "value",
	"array": [
		55.5
	],
	"subtask": {
		"subtask field": "value",
		"subtask array": [
			60
		],
		"subsubtask": {
			"subsubtask field": "subsubtask value"
		},
		"subtask subtasks": [
			{
				"first": 1
			},
			{
				"first": 2
			}
		]
	}
}`)
	m := make(map[string]interface{})
	err = json.Unmarshal(J, &m)
	if err != nil {
		t.Fatal(err.Error())
	}
	got, tree, err := CreateEditTaskFromJSON("polyapp", "JSON2Form", "Test Name 2", m)
	if err != nil {
		t.Error(err.Error())
	}
	// head Edit Task, 3 fields, subtask Edit Task, 4 fields, subsubtask Edit Task, 1 field, subtask subtasks Edit Task, 1 field
	if len(got) != 1+3+1+4+1+1+1+1 {
		t.Error("incorrect number of Datas")
	}
	if tree == nil {
		t.Error("tree was nil")
	}
}

func TestImportArbitraryJSONRecursively(t *testing.T) {
	simpleSchemaData := common.Data{}
	simpleSchemaData.Init(map[string]interface{}{
		common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"): "TestImportArbitraryJSONRecursivelySimple",
		common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"):   "TestImportArbitraryJSONRecursivelySimple",
		common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "UX ID"):     "TestImportArbitraryJSONRecursivelySimple",
	})
	simpleSchemaData.IndustryID = "polyapp"
	simpleSchemaData.DomainID = "TaskSchemaUX"
	simpleSchemaData.SchemaID = "gjGLMlcNApJyvRjmgQbopsXPI"
	simpleSchemaData.FirestoreID = "TestImportArbitraryJSONRecursivelySimpleEditTaskData"
	var err error
	_ = allDB.DeleteData(simpleSchemaData.FirestoreID)
	err = allDB.CreateData(&simpleSchemaData)
	if err != nil {
		t.Fatal("allDB.CreateData(simpleSchemaData): " + err.Error())
	}

	simpleSchema := common.Schema{
		FirestoreID:  "TestImportArbitraryJSONRecursivelySimple",
		NextSchemaID: nil,
		Name:         common.String("TestImportArbitraryJSONRecursivelySimple"),
		IndustryID:   "TestImportArbitraryJSONRecursively",
		DomainID:     "TestImportArbitraryJSONRecursively",
		SchemaID:     "TestImportArbitraryJSONRecursivelySimple",
		DataTypes: map[string]string{
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_a": "S",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_b": "AS",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_c": "F",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_d": "I",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_e": "AI",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_f": "AF",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_g": "B",
		},
		DataHelpText: map[string]string{"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_a": "a", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_b": "b", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_c": "c", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_d": "d", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_e": "e", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_f": "f", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_g": "g"},
		DataKeys:     []string{"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_a", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_b", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_c", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_d", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_e", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_f", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_g"},
		IsPublic:     false,
		Deprecated:   nil,
	}
	_ = allDB.DeleteSchema(simpleSchema.FirestoreID)
	err = allDB.CreateSchema(&simpleSchema)
	if err != nil && strings.Contains(err.Error(), "code = AlreadyExists") {
		// ok
	} else if err != nil {
		t.Fatal("allDB.CreateSchema: " + err.Error())
	}
	m := map[string]interface{}{
		"a": "string",
		"b": []string{"b1", "b2"},
		"c": 44.4,
		"d": 50,
		"e": []int64{1, 2, 3},
		"f": []float64{1.1, 2.2, 3.3},
		"g": true,
	}
	datas, err := ImportArbitraryJSONRecursively(m, simpleSchema)
	if err != nil {
		t.Error("ImportArbitraryJSONRecursively should have succeeded: " + err.Error())
	} else {
		if len(datas) != 1 {
			t.Error("incorrect number of datas")
		} else if datas[0].SchemaID != simpleSchema.FirestoreID {
			t.Error("incorrect Schema ID")
		} else if *datas[0].S[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "a")] != "string" {
			t.Error("did not store S value")
		} else if len(datas[0].AS[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "b")]) != 2 {
			t.Error("did not store AS value")
		} else if *datas[0].F[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "c")] != 44.4 {
			t.Error("did not store F value")
		} else if *datas[0].I[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "d")] != 50 {
			t.Error("did not store I value")
		} else if len(datas[0].AI[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "e")]) != 3 {
			t.Error("did not store []int64 values")
		} else if len(datas[0].AF[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "f")]) != 3 {
			t.Error("did not store AF values")
		} else if *datas[0].B[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "g")] != true {
			t.Error("did not store B value")
		}
	}

	complexSchema := common.Schema{
		FirestoreID:  "TestImportArbitraryJSONRecursivelyComplex",
		NextSchemaID: nil,
		Name:         common.String("TestImportArbitraryJSONRecursivelyComplex"),
		IndustryID:   "TestImportArbitraryJSONRecursively",
		DomainID:     "TestImportArbitraryJSONRecursively",
		SchemaID:     "TestImportArbitraryJSONRecursivelyComplex",
		DataTypes: map[string]string{
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_a": "S",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_b":  "Ref",
			"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelySimple_c":  "ARef",
		},
		DataHelpText: map[string]string{"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_a": "a", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_b": "b", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_c": "c"},
		DataKeys:     []string{"TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_a", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_b", "TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursively_TestImportArbitraryJSONRecursivelyComplex_c"},
		IsPublic:     false,
		Deprecated:   nil,
	}
	_ = allDB.DeleteSchema(complexSchema.FirestoreID)
	err = allDB.CreateSchema(&complexSchema)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("allDB.CreateSchema: " + err.Error())
	}
	mComplex := map[string]interface{}{
		"a": "string",
		"b": map[string]interface{}{
			"a": "string",
			"b": []string{"b1", "b2"},
			"c": float64(44.4),
			"d": int64(50),
			"e": []int64{1, 2, 3},
			"f": []float64{1.1, 2.2, 3.3},
			"g": true,
		},
		"c": []interface{}{
			map[string]interface{}{
				"a": "string",
				"b": []string{"b1", "b2"},
				"c": float64(44.4),
				"d": int64(50),
				"e": []int64{1, 2, 3},
				"f": []float64{1.1, 2.2, 3.3},
				"g": true,
			},
			map[string]interface{}{
				"a": "string2",
				"b": []string{"b1", "b2"},
				"c": float64(44.4),
				"d": int64(50),
				"e": []int64{1, 2, 3},
				"f": []float64{1.1, 2.2, 3.3},
				"g": true,
			},
		},
	}
	datas, err = ImportArbitraryJSONRecursively(mComplex, complexSchema)
	if err != nil {
		t.Error("ImportArbitraryJSONRecursively should have succeeded: " + err.Error())
	} else {
		if len(datas) != 4 {
			t.Error("incorrect number of datas")
		}
		if datas[0].SchemaID != complexSchema.FirestoreID {
			t.Error("incorrect Schema ID")
		}
		if *datas[0].S[common.AddFieldPrefix(complexSchema.IndustryID, complexSchema.DomainID, complexSchema.SchemaID, "a")] != "string" {
			t.Error("did not store S value")
		}
		if len(datas[0].ARef[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "c")]) != 2 {
			t.Error("length of c was not 2")
		} else {
			if !strings.HasPrefix(datas[0].ARef[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "c")][0],
				"/t/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursivelySimple?ux=TestImportArbitraryJSONRecursivelySimple&schema=TestImportArbitraryJSONRecursivelySimple&data=") {
				t.Error("first ARef incorrect")
			}
			if !strings.HasPrefix(datas[0].ARef[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "c")][0],
				"/t/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursivelySimple?ux=TestImportArbitraryJSONRecursivelySimple&schema=TestImportArbitraryJSONRecursivelySimple&data=") {
				t.Error("second ARef incorrect")
			}
		}
		if datas[0].Ref[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "b")] == nil {
			t.Error("b ref was not found")
		} else if !strings.HasPrefix(*datas[0].Ref[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, "b")],
			"/t/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursively/TestImportArbitraryJSONRecursivelySimple?ux=TestImportArbitraryJSONRecursivelySimple&schema=TestImportArbitraryJSONRecursivelySimple&data=") {
			t.Error("Ref incorrect")
		}
		if datas[1].SchemaID != datas[2].SchemaID || datas[2].SchemaID != datas[3].SchemaID {
			t.Error("datas 1, 2, 3 should all be from the simple schema")
		}

		simplifiedData1, _ := datas[1].Simplify()
		mComplexBWithPrefixes := make(map[string]interface{})
		for k, v := range mComplex["b"].(map[string]interface{}) {
			mComplexBWithPrefixes[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, k)] = v
		}
		delete(simplifiedData1, common.PolyappIndustryID)
		delete(simplifiedData1, common.PolyappDomainID)
		delete(simplifiedData1, common.PolyappSchemaID)
		common.Dereference(simplifiedData1)
		if !reflect.DeepEqual(mComplexBWithPrefixes, simplifiedData1) {
			t.Error("datas[1] not expected value")
		}
		simplifiedData2, _ := datas[2].Simplify()
		mComplexCWithPrefixes := make([]interface{}, len(mComplex["c"].([]interface{})))
		for i := range mComplex["c"].([]interface{}) {
			mComplexCWithPrefixes[i] = make(map[string]interface{})
			for k, v := range mComplex["c"].([]interface{})[i].(map[string]interface{}) {
				mComplexCWithPrefixes[i].(map[string]interface{})[common.AddFieldPrefix(simpleSchema.IndustryID, simpleSchema.DomainID, simpleSchema.SchemaID, k)] = v
			}
		}
		delete(simplifiedData2, common.PolyappIndustryID)
		delete(simplifiedData2, common.PolyappDomainID)
		delete(simplifiedData2, common.PolyappSchemaID)
		common.Dereference(simplifiedData2)
		if !reflect.DeepEqual(mComplexCWithPrefixes[0].(map[string]interface{}), simplifiedData2) {
			t.Error("datas[2] not expected value")
		}
		simplifiedData3, _ := datas[3].Simplify()
		delete(simplifiedData3, common.PolyappIndustryID)
		delete(simplifiedData3, common.PolyappDomainID)
		delete(simplifiedData3, common.PolyappSchemaID)
		common.Dereference(simplifiedData3)
		if !reflect.DeepEqual(mComplexCWithPrefixes[1].(map[string]interface{}), simplifiedData3) {
			t.Error("datas[3] not expected value")
		}
	}
}
