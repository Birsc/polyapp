package actions

import (
	"bytes"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	textTemplate "text/template"
)

type MicroserviceBotGenerator struct {
	SchemaID                      string `suffix:"Schema ID"`
	GeneratedCodeandConfiguration []byte `suffix:"Generated Code and Configuration"`
	TaskID                        string `suffix:"Task ID"`
}

// ActionMicroserviceBotGenerator Generates code needed for a microservice Bot. The Bot is assumed to be coupled to a particular Schema ID.
func ActionMicroserviceBotGenerator(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	MicroserviceBotGenerator := MicroserviceBotGenerator{}
	err = common.DataIntoStructure(data, &MicroserviceBotGenerator)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	g, err := createBotScaffold(MicroserviceBotGenerator.TaskID, MicroserviceBotGenerator.SchemaID)
	if err != nil {
		return fmt.Errorf("createBotScaffold: %w", err)
	}
	var maindotgo bytes.Buffer
	maindotgo.WriteString(`package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}
	http.HandleFunc("/", {{.BotName}}Handler)

	// TLSPath is used for local development on SSL.
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
}
`)
	err = createTypeFromBotScaffold(g, &maindotgo)
	if err != nil {
		return fmt.Errorf("createTypeFromBotScaffold: %w", err)
	}

	maindotgo.WriteString(`
// {{.BotName}}Handler translates a request into a format usable by func {{.BotName}}
func {{.BotName}}Handler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 500)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 500)
		return
	}
	err = Do{{.BotName}}(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("jSON2App: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// Do{{.BotName}} {{.BotHelpText}}
func Do{{.BotName}}(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	{{.BotName}} := {{.BotName}}{}
	err = common.DataIntoStructure(&data, &{{.BotName}})
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	// Save any updates which happened to Data into the database
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &{{.BotName}}, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}
	
	return nil
}
`)

	dotdockerignore := `# The .dockerignore file excludes files from the container build process.
#
# https://docs.docker.com/engine/reference/builder/#dockerignore-file

# Exclude locally vendored dependencies.
vendor/

# Exclude "build-time" ignore files.
.dockerignore
.gcloudignore

# Exclude git history and configuration.
.gitignore

dotgitignore := ` + "`" + `*.exe` + "`" + `
dotgitlabdashcidotyml := ` + "`" + `image: registry.gitlab.com/polyapp-open-source/polyapp:latest

variables:
  PROJECT_GROUP: "polyapp-open-source"
  PROJECT_NAME: "JSON2App"
  PROJECT_NAME_SIMPLE: "jsontoapp"

# Other variables which are expected and where they are set.
# Variables which you must set can be set at https://gitlab.com//[group]/[project]/-/settings/ci_cd
# CI_PROJECT_DIR (set by Gitlab)
# GOOGLE_CLOUD_PROJECT_ID (YOU must set this)
# DEPLOY_KEY (YOU must set this)

# How to create a DEPLOY_KEY
# 1. Click "create a new service account" in the project you will deploy to: https://console.cloud.google.com/iam-admin/serviceaccounts/
# 2. I named mine "gitlab-ci-cd" with description "Continuously Deploy"
# 3. Give the service account the "Viewer" role and the "Service Account User" role (Service Account User may already be added).
# 4. Finish creating the service account.
# 5. If a key is not automatically downloaded to your computer you may need to add a new one. Click the service account on its email > "Keys" tab > Add Key > Create New Key > JSON > Create
# 6. Open the downloaded file. Copy the contents of the file. This is the key.

stages:
  - build
  - deploy

before_script:
  - mkdir -p /go/src/gitlab.com/$PROJECT_GROUP/go/src/_/builds
  - cp -r $CI_PROJECT_DIR /go/src/gitlab.com/$PROJECT_GROUP/$PROJECT_NAME
  - ln -s /go/src/gitlab.com/$PROJECT_GROUP/go/src/_/builds/$PROJECT_GROUP
  - go mod download

vet_fmt_no_lint_code:
  stage: build
  script:
    - go fmt ./...
    - go vet ./...

build:
  stage: build
  script:
    - go build -i -v

deploy_google_cloud_run:
  stage: deploy
  only:
    - master
  script:
    - echo $DEPLOY_KEY > /tmp/$CI_PIPELINE_ID.json
    - gcloud auth activate-service-account --key-file /tmp/$CI_PIPELINE_ID.json
    - gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --project $GOOGLE_CLOUD_PROJECT_ID
    # I tried with --ingress internal but it didn't work - I got a forbidden error.    - gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE
    - gcloud run deploy $PROJECT_NAME_SIMPLE --image gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --platform managed --quiet --region us-central1 --allow-unauthenticated --project $GOOGLE_CLOUD_PROJECT_ID

after_script:
  - rm /tmp/$CI_PIPELINE_ID.json
`
	Dockerfile := `# Use the offical golang image to create a binary.
# This is based on Debian and sets the GOPATH to /go.
# https://hub.docker.com/_/golang
FROM golang:1.16-buster as builder

# Create and change to the app directory.
WORKDIR /app

# Retrieve application dependencies.
# This allows the container build to reuse cached dependencies.
# Expecting to copy go.mod and if present go.sum.
COPY go.* ./
RUN go mod download

# Copy local code to the container image.
COPY . ./

# Build the binary.
RUN go build -v -o server

# Use the official Debian slim image for a lean production container.
# https://hub.docker.com/_/debian
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM debian:buster-slim
RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/*

# Copy the binary to the production image from the builder stage.
COPY --from=builder /app/server /app/server

# Run the web service on container startup.
CMD ["/app/server"]
`
	license := `MIT License

Copyright (c) This Software's Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
`
	READMEdotmd := `# Build and Run
First create a Git repository with "git init". Then initialize go mod with "go mod init". Then run "go run".

# Test
https://golang.org/pkg/net/http/httptest/

# Deploy
This guide assumes you have already installed Polyapp in the same Google Cloud Project you are using to install your Bot. More information: https://gitlab.com/polyapp-open-source/polyapp/-/blob/master/INSTALL_GUIDE.md

https://console.cloud.google.com/

git pull [your Git Repo]

cd [your Git Repo]

./google_cloud_deploy.sh
`
	google_cloud_deploydotsh := `PROJECT_ID=$(gcloud config get-value project)
gcloud services enable cloudbuild.googleapis.com --project=$PROJECT_ID

gcloud app deploy --project $PROJECT_ID --quiet
`
	var out bytes.Buffer
	out.WriteString(`-------------------- How To Use This File --------------------
You must copy and paste from this file into other files. The files listed here include: main.go, .dockerignore, Dockerfile, LICENSE, README.md, google_cloud_deploy.sh. Why? Unfortunately files downloaded directly from Polyapp - such as zip files - can get marked as malicious by Chrome. Chrome then prevents you from downloading and opening the files even if you tell Chrome the files are safe. I see this even when downloading Go files. Go figure.
-------------------- main.go --------------------
`)
	maindotgoTemplate, err := textTemplate.New("").Parse(maindotgo.String())
	if err != nil {
		return fmt.Errorf("textTemplate.New.Parse: %w", err)
	}
	err = maindotgoTemplate.Execute(&out, g)
	if err != nil {
		return fmt.Errorf("maindotgoTemplate.Execute: %w", err)
	}

	out.WriteString(`-------------------- .dockerignore --------------------
`)
	out.WriteString(dotdockerignore)
	out.WriteString(`-------------------- Dockerfile --------------------
`)
	out.WriteString(Dockerfile)
	out.WriteString(`-------------------- LICENSE --------------------
`)
	out.WriteString(license)
	out.WriteString(`-------------------- README.md --------------------
`)
	out.WriteString(READMEdotmd)
	out.WriteString(`-------------------- google_cloud_deploy.sh --------------------
`)
	out.WriteString(google_cloud_deploydotsh)
	MicroserviceBotGenerator.GeneratedCodeandConfiguration = out.Bytes()

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &MicroserviceBotGenerator, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix(request.IndustryID, request.DomainID, request.SchemaID, "Generated Code and Configuration") + "?cacheBuster=" + common.GetRandString(10)

	return nil
}
