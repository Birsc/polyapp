package actions

import (
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/fileCRUD"
	"net/url"
)

// RefIntoStructure takes the section ref and a pointer to the structure to populate. This function retrieves the relevant
// data as determined by the ref and populates the structure with that data.
//
// It is essentially a wrapper around DataIntoStructure which also extracts the Data from the Ref and Reads the Data from the database.
func RefIntoStructure(ref string, s interface{}, exportToJSON bool) error {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	d, err := allDB.ReadData(id)
	if err != nil {
		return fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	err = common.DataIntoStructure(&d, s)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	return nil
}
