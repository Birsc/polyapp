package fileCRUD

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestReadAll(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileCRUD_crudtest"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
  "FullName": "g0jcAvClf4WeMtZcjJOKlpiD8Q43"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionUser, "TestReadAll.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0666)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for DeprecateData: " + err.Error())
	}
	tests := []struct {
		name          string
		wantData      []*common.Data
		wantRole      []*common.Role
		wantSchema    []*common.Schema
		wantTask      []*common.Task
		wantUser      []*common.User
		wantUx        []*common.UX
		wantBot       []*common.Bot
		wantAction    []*common.Action
		wantPublicMap []*common.PublicMap
		wantCSS       []*common.CSS
		wantErr       bool
	}{
		{
			name:       "success test",
			wantData:   []*common.Data{},
			wantRole:   []*common.Role{},
			wantSchema: []*common.Schema{},
			wantTask:   []*common.Task{},
			wantUser: []*common.User{
				{
					FirestoreID: "TestReadAll",
					FullName:    common.String("g0jcAvClf4WeMtZcjJOKlpiD8Q43"),
				},
			},
			wantUx:        []*common.UX{},
			wantBot:       []*common.Bot{},
			wantAction:    []*common.Action{},
			wantPublicMap: []*common.PublicMap{},
			wantCSS:       []*common.CSS{},
			wantErr:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotData, gotRole, gotSchema, gotTask, gotUser, gotUx, gotBot, gotAction, gotPublicMap, gotCSS, err := ReadAll(0)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotData, tt.wantData) {
				t.Errorf("ReadAll() gotData = %v, want %v", gotData, tt.wantData)
			}
			if !reflect.DeepEqual(gotRole, tt.wantRole) {
				t.Errorf("ReadAll() gotRole = %v, want %v", gotRole, tt.wantRole)
			}
			if !reflect.DeepEqual(gotSchema, tt.wantSchema) {
				t.Errorf("ReadAll() gotSchema = %v, want %v", gotSchema, tt.wantSchema)
			}
			if !reflect.DeepEqual(gotTask, tt.wantTask) {
				t.Errorf("ReadAll() gotTask = %v, want %v", gotTask, tt.wantTask)
			}
			if !reflect.DeepEqual(gotUser, tt.wantUser) {
				t.Errorf("ReadAll() gotUser = %v, want %v", gotUser, tt.wantUser)
			}
			if !reflect.DeepEqual(gotUx, tt.wantUx) {
				t.Errorf("ReadAll() gotUx = %v, want %v", gotUx, tt.wantUx)
			}
			if !reflect.DeepEqual(gotBot, tt.wantBot) {
				t.Errorf("ReadAll() gotBot = %v, want %v", gotBot, tt.wantBot)
			}
			if !reflect.DeepEqual(gotAction, tt.wantAction) {
				t.Errorf("ReadAll() gotAction = %v, want %v", gotAction, tt.wantAction)
			}
			if !reflect.DeepEqual(gotPublicMap, tt.wantPublicMap) {
				t.Errorf("ReadAll() gotPublicMap = %v, want %v", gotPublicMap, tt.wantPublicMap)
			}
			if !reflect.DeepEqual(gotCSS, tt.wantCSS) {
				t.Errorf("ReadAll() gotCSS = %v, want %v", gotCSS, tt.wantCSS)
			}
		})
	}
}
