package fileCRUD

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func setupLastUpdate(dbDir string, t *testing.T) {
	testLastUpdateFile := filepath.Join(dbDir, "lastUpdate.json")
	err := ioutil.WriteFile(testLastUpdateFile, []byte(`{
  "why": "this file can be read to determine when the last update was & therefore if installation is necessary",
  "LastUpdate": 0
}`), 0666)
	if err != nil {
		t.Fatal(err)
	}
}

func TestCreateData(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testData"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	setupLastUpdate(dbDir, t)
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	err = CreateData(nil)
	if err == nil {
		t.Error("should fail if data is nil")
	}
	data := common.Data{
		FirestoreID: "TestCreateData",
		IndustryID:  "TestCreateData",
		DomainID:    "TestCreateData",
		SchemaID:    "TestCreateData",
	}
	testData := make(map[string]interface{})
	testData["a"] = "B"
	data.Init(testData)
	err = CreateData(&data)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	b, err := ioutil.ReadFile(filepath.Join(dbDir, common.CollectionData, "TestCreateData.json"))
	if err != nil {
		t.Fatal("couldn't check if the test set some data because the document did not exist")
	}
	if !bytes.Contains(b, []byte(`a":"B"`)) {
		t.Error("expected to have written some JSON out to the file")
	}
}

func TestReadData(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testData"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
	"polyappIndustryID":"TestReadData",
	"polyappSchemaID":"TestReadData",
	"polyappDomainID":"TestReadData",
	"a": "B"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionData, "TestReadData.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0666)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for ReadData")
	}

	d, err := ReadData("TestReadData")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if *d.S["a"] != "B" {
		t.Error("didn't set the 'a' string to 'B'")
	}
	if d.FirestoreID != "TestReadData" {
		t.Error("should set FirestoreID")
	}
}

func TestUpdateData(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testData"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}

	fileBytes := []byte(`{
	"polyappIndustryID":"TestUpdateData",
	"polyappSchemaID":"TestUpdateData",
	"polyappDomainID":"TestUpdateData",
	"a": "B"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionData, "TestUpdateData.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0666)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for UpdateData")
	}

	data := common.Data{
		FirestoreID: "TestUpdateData",
		IndustryID:  "TestUpdateData",
		DomainID:    "TestUpdateData",
		SchemaID:    "TestUpdateData",
	}
	testData := make(map[string]interface{})
	testData["a"] = "b"
	testData["b"] = false
	data.Init(testData)
	err = UpdateData(&data)
	if err != nil {
		t.Fatal("unexpected error while updating: " + err.Error())
	}

	// verify
	outBytes, err := ioutil.ReadFile(testFile)
	if err != nil {
		t.Fatal("couldn't read file: " + err.Error())
	}
	if !bytes.Contains(outBytes, []byte(`"a":"b"`)) {
		t.Error("string update didn't take")
	}
	if !bytes.Contains(outBytes, []byte(`"b":false`)) {
		t.Error("boolean update didn't take")
	}
}

func TestDeleteData(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testData"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
	"a": "B"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionData, "TestDeleteData.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0666)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for DeleteData")
	}

	err = DeleteData("TestDeleteData")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}

	_, err = os.Open(testFile)
	if !os.IsNotExist(err) {
		t.Error("the file should not exist since it should have been deleted")
	}
}

func TestDeprecateData(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testData"))
	err := os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
	"polyappDomainID":"TestDeprecateData",
	"polyappSchemaID":"TestDeprecateData",
	"polyappIndustryID":"TestDeprecateData",
	"a": "B"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionData, "TestDeprecateData.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0666)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for DeprecateData")
	}

	err = DeprecateData("TestDeprecateData", "TestDeprecateData", "TestDeprecateData", "TestDeprecateData")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}

	outBytes, err := ioutil.ReadFile(testFile)
	if err != nil {
		t.Fatal("couldn't read file: " + err.Error())
	}
	if !bytes.Contains(outBytes, []byte(`"polyappDeprecated":true`)) {
		t.Error("deprecation update didn't take")
	}
	if !bytes.Contains(outBytes, []byte(`"a":"B"`)) {
		t.Error("deleted data in DeprecateData")
	}
}
