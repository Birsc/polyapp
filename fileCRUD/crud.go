package fileCRUD

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// ReadAll reads all documents in all collections into arrays of data structures.
// It is used for importing / exporting an entire (small) database. Just remember
// this code does not have any handling for incremental DB changes so it's not suitable for big DBs.
func ReadAll(lastUpdate int64) (data []*common.Data, role []*common.Role, schema []*common.Schema, task []*common.Task,
	user []*common.User, ux []*common.UX, bot []*common.Bot, action []*common.Action, publicMap []*common.PublicMap, CSS []*common.CSS, err error) {
	data = make([]*common.Data, 0)
	role = make([]*common.Role, 0)
	schema = make([]*common.Schema, 0)
	task = make([]*common.Task, 0)
	user = make([]*common.User, 0)
	ux = make([]*common.UX, 0)
	bot = make([]*common.Bot, 0)
	action = make([]*common.Action, 0)
	publicMap = make([]*common.PublicMap, 0)
	CSS = make([]*common.CSS, 0)
	if baseDirectory == "" {
		err = errors.New("init was not called")
		return
	}

	files := make([]string, 0)
	err = filepath.Walk(baseDirectory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("getting next file in folder: %w", err)
		}
		if !info.IsDir() && info.ModTime().Unix() > lastUpdate {
			files = append(files, filepath.ToSlash(path))
		}
		return nil
	})
	if err != nil {
		err = fmt.Errorf("during filepath.Walk: %w", err)
		return
	}

	errorChannels := make([]chan error, 0)
	outChan := make(chan interface{})
	for _, file := range files {
		errChan := make(chan error)
		errorChannels = append(errorChannels, errChan)
		go func(filePath string, errChan chan error) {
			var thing interface{}
			if strings.Contains(filePath, "/data/") {
				fileName := filepath.Base(filePath)
				thing, err = ReadData(fileName[:len(fileName)-5])
			} else if strings.Contains(filePath, "/role/") {
				fileName := filepath.Base(filePath)
				roleLocal := common.Role{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&roleLocal)
				thing = &roleLocal
			} else if strings.Contains(filePath, "/schema/") {
				fileName := filepath.Base(filePath)
				schemaLocal := common.Schema{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&schemaLocal)
				thing = &schemaLocal
			} else if strings.Contains(filePath, "/task/") {
				fileName := filepath.Base(filePath)
				taskLocal := common.Task{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&taskLocal)
				thing = &taskLocal
			} else if strings.Contains(filePath, "/user/") {
				fileName := filepath.Base(filePath)
				userLocal := common.User{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&userLocal)
				thing = &userLocal
			} else if strings.Contains(filePath, "/ux/") {
				fileName := filepath.Base(filePath)
				uxLocal := common.UX{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&uxLocal)
				thing = &uxLocal
			} else if strings.Contains(filePath, "/bot/") {
				fileName := filepath.Base(filePath)
				// this little dance which converts to common.Queryable after Read is necessary as of Go 1.14
				botLocal := common.Bot{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&botLocal)
				thing = &botLocal
			} else if strings.Contains(filePath, "/action/") {
				fileName := filepath.Base(filePath)
				actionLocal := common.Action{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&actionLocal)
				thing = &actionLocal
			} else if strings.Contains(filePath, "/publicMap/") || strings.Contains(filePath, "/publicmap/") {
				fileName := filepath.Base(filePath)
				publicMapLocal := common.PublicMap{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&publicMapLocal)
				thing = &publicMapLocal
			} else if strings.Contains(filePath, "/css/") {
				fileName := filepath.Base(filePath)
				CSSLocal := common.CSS{
					FirestoreID: fileName[:len(fileName)-5],
				}
				err = Read(&CSSLocal)
				thing = &CSSLocal
			}
			if err != nil && strings.Contains(err.Error(), "invalid character '\\\\x00' looking for beginning of value") {
				// these are blank JSON files. The easiest way for the installer to deal with them is to pass a nil value.
				// TODO it would be nice if we could delete the file in the DB we're installing into.
				outChan <- nil
				errChan <- nil
				return
			} else if err != nil {
				outChan <- nil
				errChan <- fmt.Errorf("error with path: "+filePath+" err: %w", err)
				return
			}
			outChan <- thing
			errChan <- nil
		}(file, errChan)
	}

	for i := 0; i < len(files); i++ {
		something := <-outChan
		switch concrete := something.(type) {
		case nil:
		case *common.Data:
			data = append(data, concrete)
		case *common.Role:
			role = append(role, concrete)
		case *common.Schema:
			schema = append(schema, concrete)
		case *common.Task:
			task = append(task, concrete)
		case *common.User:
			user = append(user, concrete)
		case *common.UX:
			ux = append(ux, concrete)
		case *common.Bot:
			bot = append(bot, concrete)
		case *common.Action:
			action = append(action, concrete)
		case *common.PublicMap:
			publicMap = append(publicMap, concrete)
		case *common.CSS:
			CSS = append(CSS, concrete)
		case common.Data:
			data = append(data, &concrete)
		case common.Role:
			role = append(role, &concrete)
		case common.Schema:
			schema = append(schema, &concrete)
		case common.Task:
			task = append(task, &concrete)
		case common.User:
			user = append(user, &concrete)
		case common.UX:
			ux = append(ux, &concrete)
		case common.Bot:
			bot = append(bot, &concrete)
		case common.Action:
			action = append(action, &concrete)
		case common.PublicMap:
			publicMap = append(publicMap, &concrete)
		case common.CSS:
			CSS = append(CSS, &concrete)
		default:
			// should never happen
			err = errors.New("reached default case when collecting results for: " + string(i) + " file: " + files[i])
			return
		}
	}

	err = CompactErrors(errorChannels, "All errors in Install: ")
	return
}

// CompactErrors will wait for an error on each error channel and then compress the resulting error into
// a single error message, prepended with the 'headMessage'
func CompactErrors(errorChannels []chan error, headMessage string) (err error) {
	var finalErr string
	for _, errChan := range errorChannels {
		err = <-errChan
		if err != nil {
			finalErr = finalErr + " :CompactedErrorBegin: " + err.Error()
			err = nil
		}
	}
	if finalErr != "" {
		return errors.New(headMessage + finalErr)
	}
	return nil
}

// CreateData add the given Data object to the file system.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func CreateData(data *common.Data) error {
	return Create(data)
}

// ReadData reads some data from the file system.
func ReadData(firestoreID string) (common.Data, error) {
	d := common.Data{
		FirestoreID: firestoreID,
	}
	err := Read(&d)
	return d, err
}

// UpdateData adds some new data to the Data object in the file system.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func UpdateData(data *common.Data) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if common.GetCloudProvider() != "" {
		// Do not return an error if we are in a server environment; these writes are supposed to be localhost-only.
		return nil
	}
	if data.FirestoreID == "" {
		return errors.New("must provide FirestoreID in UpdateData")
	}

	// Read the document in, merge its contents, and then write it out.
	fileName := filepath.Join(dataDirectory, data.FirestoreID+".json")
	currentData, err := ReadData(data.FirestoreID)
	if err != nil {
		return fmt.Errorf("error Reading: %w", err)
	}
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	err = currentData.Merge(data)
	if err != nil {
		return fmt.Errorf("could not merge jsonData in: %w", err)
	}
	err = currentData.Validate()
	if err != nil {
		return fmt.Errorf("data invalid: %w", err)
	}
	jsonData, err := currentData.Simplify()
	if err != nil {
		return fmt.Errorf("data could not be simplified: %w", err)
	}
	for k := range data.Bytes {
		// do not store image data in JSON
		delete(jsonData, k)
	}
	mergedDataMarshalled, err := json.Marshal(jsonData)
	if err != nil {
		return fmt.Errorf("could not merge data: %w", err)
	}
	err = ioutil.WriteFile(fileName, mergedDataMarshalled, 0666)
	if err != nil {
		return fmt.Errorf("could not write new json in UpdateData: %w", err)
	}

	return nil
}

// DeleteData removes a document. Allows you to delete documents which do not exist.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func DeleteData(firestoreID string) error {
	d := common.Data{
		FirestoreID: firestoreID,
	}
	return Delete(&d)
}

// DeprecateData sets the 'polyappDeprecated' property.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func DeprecateData(firestoreID string, IndustryID string, DomainID string, ContextID string) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if common.GetCloudProvider() != "" {
		// Do not return an error if we are in a server environment; these writes are supposed to be localhost-only.
		return nil
	}
	if firestoreID == "" {
		return errors.New("not all inputs were populated for DeprecateData")
	}
	// don't bother to get the mux lock because this is all setup code and the lock will be acquired during UpdateData
	d := common.Data{
		FirestoreID: firestoreID,
		IndustryID:  IndustryID,
		DomainID:    DomainID,
		SchemaID:    ContextID,
		Deprecated:  common.Bool(true),
	}
	d.Init(nil)
	err := UpdateData(&d)
	if err != nil {
		return fmt.Errorf("failed to deprecate document: %w", err)
	}
	return nil
}

// Create add the given object to the file system.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func Create(queryable common.Queryable) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if common.GetCloudProvider() != "" {
		// Do not return an error if we are in a server environment; these writes are supposed to be localhost-only.
		return nil
	}
	if queryable == nil {
		return errors.New("queryable was nil in Create")
	}
	err := queryable.Validate()
	if err != nil {
		return fmt.Errorf("queryable invalid: %w", err)
	}

	jsonQueryable, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("queryable could not be simplified: %w", err)
	}
	for k := range jsonQueryable {
		switch jsonQueryable[k].(type) {
		case []byte:
			delete(jsonQueryable, k)
		case [][]byte:
			delete(jsonQueryable, k)
		case []interface{}:
			delete(jsonQueryable, k)
		}
	}
	d, err := json.Marshal(jsonQueryable)
	if err != nil {
		return fmt.Errorf("could not marshal queryable: %w", err)
	}

	fileName := filepath.Join(baseDirectory, queryable.CollectionName(), queryable.GetFirestoreID()+".json")
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	err = ioutil.WriteFile(fileName, d, 0666)
	if err != nil {
		// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
		// but I would rather pass the error up so we could potentially utilize other resources
		return fmt.Errorf("ioutil.WriteFile: %w", err)
	}
	return nil
}

// Read reads some queryable from the file system.
func Read(queryable common.Queryable) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if queryable.GetFirestoreID() == "" {
		return errors.New("not all inputs were populated for Read")
	}

	fileName := filepath.Join(baseDirectory, queryable.CollectionName(), queryable.GetFirestoreID()+".json")
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	d, err := ioutil.ReadFile(fileName)
	if queryable.CollectionName() != common.CollectionData && err != nil && (strings.Contains(err.Error(), "The system cannot find the file specified") || os.IsNotExist(err)) {
		// templates have their own folder
		fileName = filepath.Join(baseDirectory, queryable.CollectionName(), "templates", queryable.GetFirestoreID()+".json")
		d, err = ioutil.ReadFile(fileName)
	}
	if queryable.CollectionName() == common.CollectionData && err != nil && os.IsNotExist(err) {
		// first half of dataDirectory
		fileName = filepath.Join(dataDirectory, "half1", queryable.GetFirestoreID()+".json")
		d, err = ioutil.ReadFile(fileName)
	}
	if queryable.CollectionName() == common.CollectionData && err != nil && os.IsNotExist(err) {
		// other half of dataDirectory
		fileName = filepath.Join(dataDirectory, "half2", queryable.GetFirestoreID()+".json")
		d, err = ioutil.ReadFile(fileName)
	}
	if err != nil {
		return fmt.Errorf("couldn't read file: %w", err)
	}
	m := make(map[string]interface{})
	err = json.Unmarshal(d, &m)
	if err != nil {
		return fmt.Errorf("couldn't unmarshal while reading: %w", err)
	}
	err = queryable.Init(m)
	if err != nil {
		return fmt.Errorf("Init: %w", err)
	}
	return nil
}

// Update adds a new Queryable to its collection in the file system.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func Update(queryable common.Queryable) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if queryable.GetFirestoreID() == "" {
		return errors.New("no FirestoreID in queryable")
	}
	if common.GetCloudProvider() != "" {
		// Do not return an error if we are in a server environment; these writes are supposed to be localhost-only.
		return nil
	}

	// Read the document in, merge its contents, and then write it out.
	fileName := filepath.Join(baseDirectory, queryable.CollectionName(), queryable.GetFirestoreID()+".json")
	var queryableCurrent common.Queryable
	switch queryable.CollectionName() {
	case common.CollectionData:
		queryableCurrent = &common.Data{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionRole:
		queryableCurrent = &common.Role{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionUser:
		queryableCurrent = &common.User{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionUX:
		queryableCurrent = &common.UX{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionTask:
		queryableCurrent = &common.Task{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionSchema:
		queryableCurrent = &common.Schema{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionBot:
		queryableCurrent = &common.Bot{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionAction:
		queryableCurrent = &common.Action{
			FirestoreID: queryable.GetFirestoreID(),
		}
	case common.CollectionCSS:
		queryableCurrent = &common.CSS{
			FirestoreID: queryable.GetFirestoreID(),
		}
	}

	err := Read(queryableCurrent)
	if err != nil {
		return fmt.Errorf("error Reading: %w", err)
	}
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	queryableCurrentSimple, err := queryableCurrent.Simplify()
	if err != nil {
		return fmt.Errorf("queryableCurrent.Simplify: %w", err)
	}
	queryableSimple, err := queryable.Simplify()
	if err != nil {
		return fmt.Errorf("queryable.Simplify: %w", err)
	}
	for k := range queryableSimple {
		switch queryableSimple[k].(type) {
		case []byte:
			delete(queryableSimple, k)
		case [][]byte:
			delete(queryableSimple, k)
		case []interface{}:
			delete(queryableSimple, k)
		}
	}

	for k, v := range queryableSimple {
		// usually this is what the Merge() functions do, although this is not guaranteed to work for collection data
		queryableCurrentSimple[k] = v
	}
	// this assumes that Init "zeros out" the structure. Aka it assumes that Init resets the structure.
	err = queryableCurrent.Init(queryableCurrentSimple)
	if err != nil {
		return fmt.Errorf("queryableCurrent Init: %w", err)
	}
	err = queryableCurrent.Validate()
	if err != nil {
		return fmt.Errorf("post-merge invalid: %w", err)
	}
	mergedQueryableMarshalled, err := json.Marshal(queryableCurrent)
	if err != nil {
		return fmt.Errorf("could not marshal: %w", err)
	}
	err = ioutil.WriteFile(fileName, mergedQueryableMarshalled, 0666)
	if err != nil {
		return fmt.Errorf("could not write new json in Update: %w", err)
	}

	return nil
}

// Delete removes a document. Allows you to delete documents which do not exist.
//
// Note: only works on localhost because installation MUST originate from localhost & proceed through each environment
// thereafter. Adding files in Staging would mean the files would have skipped testing in Development.
func Delete(queryable common.Queryable) error {
	if baseDirectory == "" {
		return errors.New("init was not called")
	}
	if queryable == nil {
		return errors.New("not all inputs were populated for Delete")
	}
	if common.GetCloudProvider() != "" {
		// Do not return an error if we are in a server environment; these writes are supposed to be localhost-only.
		return nil
	}
	fileName := filepath.Join(baseDirectory, queryable.CollectionName(), queryable.GetFirestoreID()+".json")
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	_ = os.Remove(fileName)
	// ignore errors since we are allowing users to delete documents which do not exist.
	return nil
}
