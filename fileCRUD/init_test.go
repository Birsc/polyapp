package fileCRUD

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestInit(t *testing.T) {
	err := Init("")
	if err == nil {
		t.Error("should have failed since initBaseDirectory is empty")
	}
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testdata"))
	err = os.MkdirAll(dbDir, 0666)
	if err != nil {
		t.Fatal("couldn't set up the temp directory for TestInit")
	}
	err = Init(dbDir)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if !strings.Contains(dataDirectory, dbDir) || !strings.Contains(dataDirectory, common.CollectionData) {
		t.Error("didn't set data directory properly. Dir: " + dataDirectory)
	}
	if !strings.Contains(schemaDirectory, dbDir) || !strings.Contains(schemaDirectory, common.CollectionSchema) {
		t.Error("didn't set schema directory properly. Dir: " + schemaDirectory)
	}
	if !strings.Contains(userDirectory, dbDir) || !strings.Contains(userDirectory, common.CollectionUser) {
		t.Error("didn't set users directory properly. Dir: " + userDirectory)
	}

	// cleanup the test
	os.RemoveAll(dbDir)
}
