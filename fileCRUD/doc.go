// package fileCRUD contains APIs for CRUDing objects to and from a local filesystem using JSON.
// When done in a localhost or development environment which allows editing the file system, these files can be committed
// to the git repo. This can be helpful for migrating data between environments while ensuring each environment
// has the same data when the environment is created or updated.
package fileCRUD
