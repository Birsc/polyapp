package main

import (
	"cloud.google.com/go/firestore"
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"google.golang.org/api/iterator"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1"
	"net/http"
)

// PubSubMessage is the payload of a Pub/Sub event. Please refer to the docs for
// additional information regarding Pub/Sub events.
type PubSubMessage struct {
	Data []byte `json:"data"`
}

// RefreshEnvironment deletes all data in Firestore, Storage, and Secrets, and then recreates the environment from
// scratch without any additional data. The best use case for this is if you have a public environment and you want to
// refresh it periodically.
//
// Be warned: This function deletes files in Storage and Secrets which are not associated with Polyapp.
func RefreshEnvironment(_ context.Context, m PubSubMessage) error {
	var err error
	projectID := "polyapp-public"
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		return fmt.Errorf("firestore.NewClient: " + err.Error())
	}
	defer client.Close()
	// Delete all information in the system
	err = deleteCollection(ctx, client, client.Collection("data"), 450)
	if err != nil {
		fmt.Println("client.Delete data: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("schema"), 450)
	if err != nil {
		fmt.Println("client.Delete schema: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("user"), 450)
	if err != nil {
		fmt.Println("client.Delete user: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("role"), 450)
	if err != nil {
		fmt.Println("client.Delete role: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("ux"), 450)
	if err != nil {
		fmt.Println("client.Delete ux: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("task"), 450)
	if err != nil {
		fmt.Println("client.Delete task: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("bot"), 450)
	if err != nil {
		fmt.Println("client.Delete bot: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("action"), 450)
	if err != nil {
		fmt.Println("client.Delete action: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("publicmap"), 450)
	if err != nil {
		fmt.Println("client.Delete publicmap: " + err.Error())
	}
	err = deleteCollection(ctx, client, client.Collection("css"), 450)
	if err != nil {
		fmt.Println("client.Delete css: " + err.Error())
	}
	// deleting lastUpdate ensures that /polyapp/internal/JT2ZhFXXaKCyO184Qs3F/ runs and the environment is installed.
	err = deleteCollection(ctx, client, client.Collection("lastUpdate"), 450)
	if err != nil {
		fmt.Println("client.Delete lastUpdate: " + err.Error())
	}
	fmt.Println("Finished deleting data in Firestore")

	// Delete all of the things stored in all of the buckets
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: " + err.Error())
	}
	allBuckets := storageClient.Buckets(ctx, projectID)
	for {
		bucketAttrs, err := allBuckets.Next()
		if err != nil && err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("allBuckets.Next: " + err.Error())
		}
		q := storage.Query{
			Versions: false,
		}
		it := storageClient.Bucket(bucketAttrs.Name).Objects(ctx, &q)
		for {
			attrs, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return fmt.Errorf("Bucket(%q).Objects(): %w\n", bucketAttrs.Name, err)
			}
			err = storageClient.Bucket(bucketAttrs.Name).Object(attrs.Name).Delete(ctx)
			if err != nil {
				fmt.Printf("Delete Bucket %v Object %v\n", bucketAttrs.Name, attrs.Name)
			}
		}
	}
	fmt.Println("Finished deleting Objects")

	// Delete all of the secrets stored in the project
	secretManagerClient, err := secretmanager.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("secretmanager.NewClient: " + err.Error())
	}
	listSecrets := &secretmanagerpb.ListSecretsRequest{
		Parent: "projects/" + projectID,
	}
	secretsIterator := secretManagerClient.ListSecrets(ctx, listSecrets)
	for {
		s, err := secretsIterator.Next()
		if err != nil && err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("secret ListSecrets: " + err.Error())
		}
		deleteSecretReq := &secretmanagerpb.DeleteSecretRequest{
			Name: s.Name,
		}
		err = secretManagerClient.DeleteSecret(ctx, deleteSecretReq)
		if err != nil {
			fmt.Printf("DeleteSecret %v err: %v\n", s.Name, err.Error())
		}
	}
	fmt.Println("Finished Deleting Secrets")

	resp, err := http.DefaultClient.Get("https://" + projectID + ".appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/")
	if err != nil {
		return fmt.Errorf("http.DefaultClient.Get err: " + err.Error())
	}
	if resp.StatusCode != 200 {
		resp, err = http.DefaultClient.Get("https://" + projectID + ".appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/")
		if err != nil {
			return fmt.Errorf("http.DefaultClient.Get take 2 err: " + err.Error())
		}
	}
	if resp.StatusCode != 200 {
		fmt.Println("could not call https://" + projectID + ".appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/ so was not able to re-create the environment")
	}
	fmt.Println("Done")
	return nil
}

func deleteCollection(ctx context.Context, client *firestore.Client,
	ref *firestore.CollectionRef, batchSize int) error {

	for {
		// Get a batch of documents
		iter := ref.Limit(batchSize).Documents(ctx)
		numDeleted := 0

		// Iterate through the documents, adding
		// a delete operation for each one to a
		// WriteBatch.
		batch := client.Batch()
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return err
			}

			batch.Delete(doc.Ref)
			numDeleted++
		}

		// If there are no documents to delete,
		// the process is over.
		if numDeleted == 0 {
			return nil
		}

		_, err := batch.Commit(ctx)
		if err != nil {
			return err
		}
	}
}

/*
module example.com/cloudfunction

go 1.14

require (
    cloud.google.com/go v0.75.0
	cloud.google.com/go/firestore v1.4.0
	cloud.google.com/go/storage v1.12.0
	firebase.google.com/go v3.13.0+incompatible
	google.golang.org/api v0.37.0
	google.golang.org/genproto v0.0.0-20210126160654-44e461bb6506
	google.golang.org/grpc v1.35.0
)
*/
