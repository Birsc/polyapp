// command init uses the Firebase REST API to set up a new Firebase program programatically
// Documentation for REST APIs: https://firebase.google.com/docs/projects/api/workflow_set-up-and-manage-project
package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

type metadata struct {
	T string `json:"@type"`
}
type errorResp struct {
}
type mainResp struct {
	T             string `json:"@type"`
	ProjectID     string `json:"projectId"`
	ProjectNumber string `json:"projectNumber"`
	DisplayName   string `json:"displayName"`
	Name          string `json:"name"`
}
type operation struct {
	Name     string    `json:"name"` // Name is a unique ID of this operation
	Metadata metadata  `json:"metadata"`
	Done     bool      `json:"done"`
	Error    errorResp `json:"error"`
	Response mainResp  `json:"response"`
}

func main() {
	gcp := os.Getenv("GOOGLE_CLOUD_PROJECT")
	if len(os.Args) > 1 {
		gcp = os.Args[1]
	}
	if gcp == "" {
		fmt.Printf("GOOGLE_CLOUD_PROJECT not set\n")
		return
	}
	ctx := context.Background()
	cred, err := google.FindDefaultCredentials(ctx, []string{
		"https://www.googleapis.com/auth/cloud-platform",
		"https://www.googleapis.com/auth/firebase",
	}...)
	if err != nil {
		fmt.Printf("google.FindDefaultCredentials: %v", err.Error())
		return
	}

	token, err := cred.TokenSource.Token()
	if err != nil {
		fmt.Printf("cred.TokenSource.Token(): %v", err.Error())
		return
	}
	err = addFirebase(token, gcp)
	if err != nil {
		fmt.Printf("addFirebase: %v", err.Error())
		return
	}
	err = addWebApp(token, gcp)
	if err != nil {
		fmt.Printf("Error in addWebApp: %v", err.Error())
		return
	}
	err = enableAuthOptions(token, gcp)
	if err != nil {
		fmt.Printf("Error in enableAuthOptions: %v", err.Error())
		return
	}
}

func addFirebase(token *oauth2.Token, gcp string) error {
	uri := "https://firebase.googleapis.com/v1beta1/projects/" + gcp + ":addFirebase"
	req, err := http.NewRequest(http.MethodPost, uri, nil)
	if err != nil {
		return fmt.Errorf("http.NewRequest: %v", err.Error())
	}
	token.SetAuthHeader(req)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("http.DefaultClient.Do for req %v: %v", req.RequestURI, err.Error())
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll: %v", err.Error())
	}
	if resp.StatusCode == 409 && bytes.Contains(body, []byte("ALREADY_EXISTS")) {
		return nil
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("resp.StatusCode != 200. Status: " + resp.Status)
	}

	var op operation
	err = json.Unmarshal(body, &op)
	if err != nil {
		return fmt.Errorf("json.Unmarshal: %v", err.Error())
	}
	worked := op.Done
	if !op.Done {
		for {
			time.Sleep(time.Second * 3)
			operationGet, err := opGet(token, op.Name)
			if err != nil {
				return fmt.Errorf("opGet: %w", err)
			}
			if operationGet.Done && strings.HasSuffix(operationGet.Response.T, ".FirebaseProject") {
				// using strings.hassuffix since we may call the alpha, beta, or main APIs and the response string is a bit different if we do
				worked = true
			}
			if operationGet.Done {
				break
			}
			fmt.Println("Firebase has not yet been added to this project. Operation Name: " + op.Name)
		}
	}
	if !worked {
		return errors.New("failed to create the Firebase Project")
	}
	return nil
}

func opGet(token *oauth2.Token, opNameID string) (operation, error) {
	uri := "https://firebase.googleapis.com/v1beta1/" + opNameID
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return operation{}, fmt.Errorf("http.NewRequest: %v", err.Error())
	}
	token.SetAuthHeader(req)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return operation{}, fmt.Errorf("http.DefaultClient.Do for req %v: %v", req.RequestURI, err.Error())
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return operation{}, fmt.Errorf("ioutil.ReadAll: %v", err.Error())
	}
	if resp.StatusCode == 409 && bytes.Contains(body, []byte("ALREADY_EXISTS")) {
		return operation{
			Done: true,
		}, nil
	}
	if resp.StatusCode != 200 {
		return operation{}, fmt.Errorf("resp.StatusCode != 200. Status: " + resp.Status)
	}

	var op operation
	err = json.Unmarshal(body, &op)
	if err != nil && strings.Contains(err.Error(), "invalid character '<'") {
		return operation{}, nil
	} else if err != nil {
		return operation{}, fmt.Errorf("json.Unmarshal: %v", err.Error())
	}
	return op, nil
}

func addWebApp(token *oauth2.Token, gcp string) error {
	getWebApp := "https://firebase.googleapis.com/v1beta1/projects/" + gcp + "/webApps/"
	appID := ""
	type getResp struct {
		T         string `json:"@type"`
		Name      string `json:"name"`
		AppID     string `json:"appId"`
		ProjectID string `json:"projectId"`
		WebID     string `json:"webId"`
	}
	var getWebAppResponse struct {
		Apps []getResp `json:"apps"`
	}
	req, err := http.NewRequest(http.MethodGet, getWebApp, nil)
	if err != nil {
		return fmt.Errorf("http.NewRequest: %v", err.Error())
	}
	token.SetAuthHeader(req)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("http.Get(getWebApp): %w", err)
	}
	if response.StatusCode != 200 {
		return fmt.Errorf("could not get existing web apps: %w", err)
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll: %v", err.Error())
	}
	err = json.Unmarshal(body, &getWebAppResponse)
	if err != nil {
		return fmt.Errorf("json.Unmarshal: %v", err)
	}
	for i := range getWebAppResponse.Apps {
		if getWebAppResponse.Apps[i].ProjectID == gcp {
			appID = getWebAppResponse.Apps[i].AppID
			break
		}
	}

	if appID == "" {
		uri := "https://firebase.googleapis.com/v1beta1/projects/" + gcp + "/webApps"
		var addWebAppResponse struct {
			Name string `json:"name"`
		}
		req, err := http.NewRequest(http.MethodPost, uri, nil)
		if err != nil {
			return fmt.Errorf("http.NewRequest: %v", err.Error())
		}
		token.SetAuthHeader(req)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return fmt.Errorf("http.DefaultClient.Do for req %v: %v", req.RequestURI, err.Error())
		}
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("ioutil.ReadAll: %v", err.Error())
		}
		err = json.Unmarshal(body, &addWebAppResponse)
		if err != nil {
			return fmt.Errorf("json.Unmarshal: %v", err)
		}
		var opResponse struct {
			Name     string `json:"name"`
			Done     bool   `json:"done"`
			Response getResp
		}
		for {
			time.Sleep(time.Second * 3)
			req, err = http.NewRequest(http.MethodGet, "https://firebase.googleapis.com/v1beta1/"+addWebAppResponse.Name, nil)
			if err != nil {
				return fmt.Errorf("http.NewRequest for Done: %v", err.Error())
			}
			token.SetAuthHeader(req)
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				return fmt.Errorf("http.DefaultClient.Do for Done %v: %v", req.RequestURI, err.Error())
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return fmt.Errorf("ioutil.ReadAll: %v", err.Error())
			}
			err = json.Unmarshal(body, &opResponse)
			if err != nil {
				return fmt.Errorf("json.Unmarshal: %v", err)
			}
			if resp.StatusCode != 200 {
				return fmt.Errorf("resp.StatusCode != 200 while waiting for Done: %v", resp.StatusCode)
			}
			if opResponse.Done {
				break
			}
			fmt.Println("Web App has not yet been added")
		}
		appID = opResponse.Response.AppID
	}
	req, err = http.NewRequest(http.MethodGet, "https://firebase.googleapis.com/v1beta1/projects/"+gcp+"/webApps/"+appID+"/config", nil)
	if err != nil {
		return fmt.Errorf("http.NewRequest for Done: %v", err.Error())
	}
	token.SetAuthHeader(req)
	getConfigResp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("http.DefaultClient.Do for getConfig: %v", err.Error())
	}
	getConfigRespBody, err := ioutil.ReadAll(getConfigResp.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll: %w", err)
	}
	if getConfigResp.StatusCode != 200 {
		return fmt.Errorf("getConfigResp.StatusCode %v", getConfigResp.StatusCode)
	}
	var configResponse struct {
		ProjectID         string `json:"projectId"`
		AppID             string `json:"appId"`
		DatabaseURL       string `json:"databaseURL"`
		StorageBucket     string `json:"storageBucket"`
		LocationID        string `json:"locationId"`
		APIKey            string `json:"apiKey"`
		AuthDomain        string `json:"authDomain"`
		MessagingSenderID string `json:"messagingSenderId"`
		MeasurementID     string `json:"measurementId"`
	}
	err = json.Unmarshal(getConfigRespBody, &configResponse)
	if err != nil {
		return fmt.Errorf("json.Unmarshal for configResponse: %w", err)
	}
	publicPath := "./public"
	wd, _ := os.Getwd()
	if strings.Contains(wd, "init") {
		publicPath = "../../public/"
	}
	fileBytes, err := ioutil.ReadFile(publicPath + "/sw.js")
	if err != nil {
		return fmt.Errorf("ioutil.ReadFile: %w", err)
	}
	newFileBytes := bytes.Replace(fileBytes, []byte(`AIzaSyBSpSv7JdvWAle9sPwTNT8zItaVmtiiEQ4`), []byte(configResponse.APIKey), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214.firebaseapp.com`), []byte(configResponse.AuthDomain), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`https://high-sunlight-232214.firebaseio.com`), []byte(configResponse.DatabaseURL), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214`), []byte(configResponse.ProjectID), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214.appspot.com`), []byte(configResponse.StorageBucket), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`897117818320`), []byte(configResponse.MessagingSenderID), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`1:897117818320:web:e692cd28de93610465fdd5`), []byte(configResponse.AppID), 1)
	if configResponse.LocationID != "" {
		newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.AppID), []byte(configResponse.AppID+`",
    locationId: "`+configResponse.LocationID), 1)
		if configResponse.MeasurementID != "" {
			newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.LocationID), []byte(configResponse.LocationID+`",
    measurementId: "`+configResponse.MeasurementID), 1)
		}
	} else if configResponse.MeasurementID != "" {
		newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.AppID), []byte(configResponse.AppID+`",
    measurementId: "`+configResponse.MeasurementID), 1)
	}
	err = ioutil.WriteFile(publicPath+"/sw.js", newFileBytes, 0666)
	if err != nil {
		return fmt.Errorf("ioutil.WriteFile for sw.js: %v", err.Error())
	}
	fileBytes, err = ioutil.ReadFile(publicPath + "/main.js")
	if err != nil {
		return fmt.Errorf("ioutil.ReadFile for main.js: "+publicPath+"/main.js"+": %w", err)
	}
	newFileBytes = bytes.Replace(fileBytes, []byte(`AIzaSyBSpSv7JdvWAle9sPwTNT8zItaVmtiiEQ4`), []byte(configResponse.APIKey), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214.firebaseapp.com`), []byte(configResponse.AuthDomain), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`https://high-sunlight-232214.firebaseio.com`), []byte(configResponse.DatabaseURL), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214`), []byte(configResponse.ProjectID), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`high-sunlight-232214.appspot.com`), []byte(configResponse.StorageBucket), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`897117818320`), []byte(configResponse.MessagingSenderID), 1)
	newFileBytes = bytes.Replace(newFileBytes, []byte(`1:897117818320:web:e692cd28de93610465fdd5`), []byte(configResponse.AppID), 1)
	if configResponse.LocationID != "" {
		newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.AppID), []byte(configResponse.AppID+`",
    locationId: "`+configResponse.LocationID), 1)
		if configResponse.MeasurementID != "" {
			newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.LocationID), []byte(configResponse.LocationID+`",
    measurementId: "`+configResponse.MeasurementID), 1)
		}
	} else if configResponse.MeasurementID != "" {
		newFileBytes = bytes.Replace(newFileBytes, []byte(configResponse.AppID), []byte(configResponse.AppID+`",
    measurementId: "`+configResponse.MeasurementID), 1)
	}
	err = ioutil.WriteFile(publicPath+"/main.js", newFileBytes, 0666)
	if err != nil {
		return fmt.Errorf("ioutil.WriteFile for main.js: %v", err.Error())
	}
	return nil
}

// enableAuthOptions would perform a few manual-only steps needed by Firebase Auth.
// There is currently no API which allows us to do this behavior so we ask people to do it manually.
func enableAuthOptions(token *oauth2.Token, gcp string) error {
	/*
		There is some support to add a Google account as a federated identity provider:
		https://cloud.google.com/identity-platform/docs/reference/rest/v2/projects.defaultSupportedIdpConfigs/patch
		PATCH `projects/`+gcp+`/defaultSupportedIdpConfigs/google.com`
		`{
		  "name": "projects/"`+gcp+`"/defaultSupportedIdpConfigs/google.com",
		  "enabled": true,
		  "clientId": string,
		  "clientSecret": string
		}`

		There is currently no support for adding email/password or anonymous.
		when you make the changes, they are coming from the origin https://console.firebase.google.com
		With these request sources:
		PATCH to `/admin/v2/projects/`+gcp+`/config?updateMask=signIn.anonymous.enabled&alt=json&key=[key]`
		With payload: `{"signIn":{"anonymous":{"enabled":true}}}`
		then it does a GET here: `/admin/v2/projects/`+gcp+`/config?alt=json&key=[key]`

		Enabling email/password authentication:
		PATCH to `/admin/v2/projects/`+gcp+`/config?updateMask=signIn.email.enabled,signIn.email.passwordRequired&alt=json&key=[key]`
		With payload: `{"signIn":{"email":{"enabled":true,"passwordRequired":false}}}`

		Adding an authorized domain:
		PATCH to `/admin/v2/projects/`+gcp+`/config?updateMask=authorizedDomains&alt=json&key=[key]`
		With payload: `{"authorizedDomains":["`+gcp+`.firebaseapp.com","`+gcp+`.web.app","`+gcp+`.appspot.com"]}`

		You can probably combine all of these:
		PATCH to `/admin/v2/projects/`+gcp+`/config?updateMask=authorizedDomains,signIn.email.enabled,signIn.email.passwordRequired,signIn.anonymous.enabled&alt=json&key=[key]`
		With payload: `{"authorizedDomains":["`+gcp+`.firebaseapp.com","`+gcp+`.web.app","`+gcp+`.appspot.com"],"signIn":{"email":{"enabled":true,"passwordRequired":false},"anonymous":{"enabled":true}}}`

		Unfortunately I do not see any documentation suggesting this is supported.
	*/

	return nil
}
