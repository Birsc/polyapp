# Base Documents
## Why does Base Documents exist?
Every document in Base Documents helps to run Polyapp or a particular application of Polyapp.
Because Polyapp is "bootstrapped" a Task is used to change elements of the URL like the Domain, Task, or Data.
In order for any Task to load (including the Tasks which allow editing Tasks) it must be in the database.
Therefore, the Task for editing Tasks must be in the database _prior_ to someone trying to edit or create a new Task.
Therefore, you can't create the Task which edits Tasks after the system initializes.
Therefore, you must put those Tasks into the system before the first Task loads.
Therefore we need some mechanism which installs some documents into the database prior to the system being used for the first time.

## How does it work?
The function which performs this installation is called Install() in cmd/defaultservice/main.go

The documents which are installed into the database are stored in cmd/defaultservice/baseDocuments.

It is also possible to move blobs (image, videos, etc.) into a new environment when it is being installed.
This is helpful for moving information between environments like from Development -> Staging -> Production while
not storing those large blobs in the .git file system. To do this, Install() examines the blob storage associated with every
Data document which is installed into the new environment and if there is associated []byte or [][]byte data with that document
it installs that data into the new system.

## Notes on why certain documents are included
Basically, you can use this list to determine why a particular document is included in this Git repository.
When a new document is added to the git repository the ID of the document is added here along with a simple note.

### Role documents
polyappAdminRole The Polyapp Admin role. This role gives Users which have it complete control over the system which is very helpful and very dangerous.
polyappHubAPIRole The Hub API Role. This role has full access to polyapp / Hub. To restrict access further use middleware.
iqVEdZxBjgygmyUDzblH is the Admin role. This role gives the user complete access to the system, except for editing Tasks in the Industry domain.

### User documents
anonymous is used by the client for anonymous logins. It has no security rights.
polyappAdminUser is the admin user. This role gives the logged in user complete access to the system. The first step after installation should always be to change this user's password.
polyappHubAPIUser The Hub API User. This user has full access to polyapp / Hub via the polyappHubAPIRole.
p3gP9Lrox8pBxqQdeFUH Template User. This User is the base for when a new User is created. Edit it and you change permissions associated with new users.

### Data documents
##### Task Designer Task
2qo1QxeMTCidf946EYvT
3UGyXJj7unYFqkFIbVny
4Elseq0j2H0coKqhPbb7
BTCYM9Iw85yccZhUEcqs
CPJIfH2uOo5z7S27g7h5
EKYOzQbElNQkBhm1hVuL
IwYln7dGMFstjKUfEStZ
L4a7ZDOLofTbTWgz5NbW
lTvwFXhqipljNNulVCZceIzEG
piHlu2qSO5Ch29ITxYQP
pRpQBhSVslSutMW4HqIQ
uj38oz8UPi5AjoOGUDpc
w0nFttasgYb4hkIIly0w
WOkUlaPmaVkVcuGbcFvs
X4xw5xvnzzWz7T8EFPx0
rQRtNizAThBv0VuggbxT
JWmDCwcvn9Q26K8685tc
tVoBcMtpKXmBVseOsCRvawbJT
##### Fields subtask in the Task Designer
tLghiOXjTFnhSVxedQeFQPbhU
89p0wxPwqPSP9EaChVzJ
bvJHlISuHZRuPy1NtJ0S
D1wDtg0K1XfAHItvXz6n
O3Kix3PmG8j7pWO6j6GN
QRSd0oBn0FbweGG0ylmD
wxsP7xN0pUrL1y00IvXq
zzCnOtKbvSAvZm3AtbU6
##### Subtasks subtask for Edit Task
8wOCxkHY5r61Z2gDDqnb
82RjpfvuZnpDFLiU8tHk
lpGypSA68HFUdKm1HVgX
SD8w2G3URDlw2GwJv7e1
WlMmuJaIGsTlAdhQmWQkmdsPc
##### Bot/Action Designer for Edit Task
XubMqvgkOnRefREEZMemFEzYS
bzg8tbkSqsc83gQeUBS8
##### Action Designer Task
HFIoBoHSxQYGrLfwOlnxOqjvJ
SoZU8wIaCAW233KAHXOv
XfD7GDNIRVjeWLGFMe0x
cdqPO04HvHZTxpyrGEdA
##### Bot Designer Task
pkPGtjWTRAUgfAZhbpFsMFtvB
H2eLi6v1PPSba9rrszSB
TBxiJzLGpzlemQKjaHUD
VerATkiOm7iORXm0523O
abxvqD9VTZFtgSKbAI0e
##### Role Designer Task
sOHDuOZfYRKCbcTSNcXqxWVSI
zcNIOtief8NcRCSDWmSH
WPki7G8FGBJzGzUxLHR4
QaRqYLuqJRjztQFroMeG
3QSDS437q83DBKaIWJow
##### User Designer Task
HimLsweukCMJKHnCMZTwZMsOd
CzI4LUuf068JG6t6Xgra
ZoclnNihpt3n5KwrwxTs
aqtoVSu2uLhbxxuMkUDr
lG85BIA0lndhFtzyHIeS
lVeqHxXEu12tRRuZCflp
oU12XomvWgrq45rNr8Aa
stOMRonnPvNjdrI9UU82
wiITJQeUsQRA5VAjxRXx
Ek5pXahbSajc8nJA3udv
GW9alHTb9QUyfoJ6vczD
IoVegvbnbijEH8CYdwPK
VAbOG5bBSASXcfjruxCy
r8af36vx1DB3ZYpCkK4I
EtLrVBGAiKcwVPOBtYdI
jqgOMyePCDUDQpl54uEW
qvGruRJxLa96x2lzYh8L
##### Edit Bot Task
LuTqSyVdCJJWQfeZEOFlTlSbG
cdFjPUMciEBDojRR7NeK
##### Table Bot Task
VSIxiZPWpevXFQmhwrkxKhQNo
M26ct87S0fhKaKg2wUL1
##### Edit Role Bot Task
HeFRwSXoJjhfzFvFAGntafclg
5B0kVQdajec7I6nN32Vh
##### Edit User Bot Task
cWJvPcxdnXxngEFUgvCdaMNKO
NMNAGXxzKChFNQuT0Qn6
##### User Activity Task
QfrKTqtRJqotQwqleUJVlNvFz
Gvd2F0FDFgHgIDucRfNV
K9AcTCg42mRw5TBTw5oE
ctzf7GFa0fNqjtI85yEs
hZxGQk14OQW4DS4IyuGk
##### User Activity Bot Task
hAZWryMOhWdAhtQPvdjkrhszm
ZBQ0eH9iFp0Xzb2gdRvy
##### Home Page Default Task
CMdeichcNwGcDblXoYkfeoNEB
2tOsfJH8a1UQBP4Qndr1
##### Home Page Loading Bot
FousLVMucEkKdzUjhuiOBeePT
FxdTpPler1M8qQq2Jygq
##### User Designer - polyappAdminUser
QJ9HtXih2ojOlG4U8C0U
##### User Designer - anonymous
lVXrUAKrzZuxbUspBSXWbKhgn
##### Delete Data Bot
Zotid89aE1KSO5md4zrB
yobEwEPlDOdQQaVyXNkFhluns
##### Delete Data
klwtEiLUmDOcGZOuEUpyHHsjQ
0EPXeUbnGDaIzvvoOLIL
##### Delete Admin Bot
TJjrxCfQGlxcmDEIUtlPiXGyY
jkgsJ7sw2mO8698ewfuz
##### Delete Admin
IpingKxxAChuUnrdSJqinCQci
7PLfLSbxsfuiRTMraeli
L7RHMxSVhPfRn2qWiwfG
##### HTTPS Call Bot
wCHhssCsBXvqyHPkMwPwKldnC
8eyImhXSAgWSMkNJzKEQ
##### Hub Task
ZSvqddokabvTQlfhJuGBpqQDz
zKVhrsJu2PSrUmDwfpmP
5GvvyfL9VlCnjH6QVQLr
OHyIo13qJUOd4f4DPOdh
YrPi0wPWhLgAtS1i5lug
ozakSTCtQbWVK8NPmZ2d
zyhZM5tjW3rLIq2CnjMQ
##### Access (for Role) Task
GYqfxxoOhlQIwzizneXLlHgav
2rBk6904Hj0MbUaGAerh
74bQv7iQW5WNWTWnak8w
InEMOVXVX9aylTMckC33
LY08RZqJG9fTYhuLM3eX
WDzfO3psaVU6PpXzfTch
XQFGVvwuPDhVZS7RiiKJ
qTtZuP6Qd7klCmMHuMX6
psrxeiW9h5B9KDAzt8zN
##### polyappAdminRole in the Role Designer
sWxXfvJDlsVBPzmULdYqinwtI
WI2dpWDbGT63oX7xCs7u
##### API ID Mapping
zWnMjkNzvglaeCUARoKyQgTCO
1G7w2tcfXCu2YDWtruvG
AZdwULhFM20juL4Yp4ab
EbF8nMz1NXT4BWp5Q4Hu
QwvYe0V9FSBkb9KxPgyN
##### Mapping Local Data ID to Hub Data ID
PybeCFixKOYSyRtBcOChMrYoN
MCYFIYZDmzzLDEZYVTXA
UPZ3vKoSeY4Hq5s2aTMf
##### Template User
EtCKtxdfmz8UmmiGjgaO
##### Import Data
WeHtkloGpoxOIPkgBIEUALxdD
7difbMOvHsuGouXXQFWi
LHuPASvO41IXe6CVOW4S
1BNjA23LxKGDEuImL8Qv
8Q4L26Lax98N4PviMMC0
4vqQo0NBovs51p3OHxpo
SHOlaUrc6YGYVGHjOozV
##### Import Data Bot
0wSwsuBxR3VUDVIltQVp
bWxaoDMDlztgWBkwbddWKKLSj
##### Import Data And Create Task
0WIrh4R4yYkBK7C6PyI0
NK1D2mF9nBJHnKhGqSi4
SPSMOw4jIfNDxqVEOe0T
U6lRGQpEuNiwBVo60x4W
bWpj9JF44qY8OzmWUPUp
hrdUxldkKgHwRmttkUWeAgyac
xpvP8803RUXlQW0KWswa
##### Import Data And Create Task Bot
PPGQLLXJzkBEKULYMCHGdWuKp
sIFrO1dC320PHCsOxZGr
##### Export Data Task
OTJHwCieI9eXyAKL8Tex
cDvSYQV0018qtk4jT6if
vRDTEdIjgcfZiaZ1UEA0
hzjEMEkRbAyzinAJUdVnwOSDh
0iot14A0PfLw96bLk305
##### Export Data Bot
NLKHqiCCYEJpoXsfgkvCkcVzx
lobcVCzAmZ7dfVvxhmlL
##### Create Chart from Data Bot
XPiMgPiS0bNWowYZVQYF
pKPhiTWdoOmZLdsKkYOPqKNEO
##### Load Chart Javascript
LHOcn1mGcnQ8dntUs2jR
ZeosKBPRlBrJlCNeclInYSuqn
##### Create Chart From Data
0Zvs4T9qxTluLC0o6WKn
Nj44l4yGvRPzYLRMbPqx
hntimkPbTijutkPyPdbYFFIma
os5JRAEhAtodgMaAZ2LJ
uL8oEOoTC5T9A9XtDDR0
emyRItQw2xtQhBkg0FBB
iphT80F0Gn1Q1sG5r2kh
JWjC8APsolEPgAs2e5nS
dljXA1lrYXeCfTH1p1q7
m66Lgv90iKjeZJjQzHa4
TlHRNrqfUy2pmxoj4S80
paskVY7iuxR2YLfJ5Yd4
##### Chart Filters
TqCSfwZjBxHHVsKqtjWfOZtCO
Cd4xA5S60yJdNqlkewYX
UMsbzoOefwAIvTsioIiZ
lorTL0S8VuYxgzsHrBW8
##### Chart Time Filters
V0b1dtXEYqTabx7fpRW5
aAa31IyIyOMa6dbBbhpv
eXEuxyclQMoHfSVybCUyNacLx
uRjavwhauT0tGPnuvJT5
##### Productivity Chart Bot
cFhwrYNoIZYeTlnmN8C4
kkSYBqdlIAAClnEFlPPPdHneA
##### Productivity Chart
7EXNYxCPt9rMnzcQqz2Q
7r17Ubc36xIDBG0hfIXi
RvqW8767PQvy3hJMtGaO
cS9eZDW9JnedlTiuoZlW
mMEGpIC27xDQwm4IX9n9
IApIBtgFwdgGxjfVbXmIOCKIE
QLOhqrQ42y1TmhBkfFZD
##### Domain Description Task
0zYT0zYvKBWFDNaCluVd
B3LddAUKFUCQEz6s9hpt
MbkGJDRf1U5mONA655ay
YjMTKafw0EFmLQpFtdO7
dzngmQHkcISfJQCXSdCPdrmKM
##### Create Table from Data Bot
BMlzjicNRBhHSMmESevfukhAu
nLAxw2nWI6BEEZ6e4tpr
##### Create Table from Data
eDCeWmNZnHXLVgpWMsLykyxIP
Gt71TmL1Om0gfSOHkUsg
J1SJgoUs4rzNRsOTYFHy
KuCUyS0aORe6DNyNTsPF
a9G5pM8IUzWjncOyOy25
rhCwBLwL95IejpnXQJu1
aD5b8XeH3BdrsKr2wEyq
##### New Data Bot
SyQbMuP5gGI1CqWDiVg2
hLVipDofBEhVMptUDIadIxuQw
##### Load Tracking
wGzJmQipCNMABCnkFSZPwejGD
Vk19MIgPxruzCgFg2uZt
##### Done Tracking
UfueGdBjWNtLnJDLvqbLYKTmg
0GrnRHr2Uf1WatVcyule
##### Export App
FETsRfAMHoGmKduZTLIskDURP
LMH6HlZ8t2o2S7CKYeNl
WrV1q28zbSDvxq9MQJwZ
hbAxkFTEyHdvHBmAVQz9
##### Export App Bot
UCXfHFopCxejIORVkgyMkJJNK
anrG2PSiz6uyw0Dt5YRk
##### Import App
3q0SsI7qmLKc0Q0QJ6y0
3umLe1khnymfTU1sp2jD
ay0DtdaddRF0tye2jm1r
eYljCWadEWSSiFVDeRbczHwMd
##### Import App Bot
26xqzpoLLAVWWTDIC3OI
EhdmurVhPKmxXgNJHdzzShScs
##### Admin Role Data
DjWjRynyagnxjYyaAKekwClMM
##### Generate Bot Scaffold Bot
6D5GCzB4WtMT6HJuvqIO
IRyyHVKyFqrADJolhgDyAboGk
##### Generate Bot Scaffold
8eNqrkZsI8AsMki7zgub
EBYJj8gMH2AOwAswwbNw
OWcpElNtbLXAISuSCIYQTEkdm
##### Configure API Call
YElyoKKTELQvCOSJlvoeKzFWJ
4cYwST5cVwqJvb74k1eP
EkS9AdwCkWFtKs3ksC1s
NJidgfyAImv5jHCvtB4H
YcDhPVM951vpnxt1Kva7
aG3aZNemp2T1ecuzb8Ty
c3rZm9favgnzdE3TDsgy
e4IpIvWYgSlm7JolkGFe
fX2PAqvb2PnjsxVXL7Yo
hoX1TPZ1c0nDSa7o1mHz
mraizJVD3CvoVqApPEXK
wyHyU6zxY5kLYPsuqmt2
xXXm7AXyEQvCP7x823F5
ynLGRvyOKFbKtXTqj4cm
BElHd4YCadFkyRmB7vMk
EUluPdM1F1DMbwSnaTho
amWnSvj8ssKdhlvqUkoz
YcKr3y3tHLKGQzKq8a9H
mU8QOqDueoVsNjmIAap0
oa8Msw6G2eLcOLW2oJen
##### Configure API Call Bot
flGBHhjgZZNSFaDiTrjASrNgu
WmBOeTMqBOuJE5NVVSLa
##### API Call
UYYlZaUxaCaYlWMQcZEoIWLDz
O9w7Sp4ybVzFuRTYnCZK
kDWxobqg9ANCfVmgpVu9
l3MsfycUnkQdBGx1NWTi
tOHzAlZKBhroxde71ryQ
zxKBxn25cmVyHtUIUTZH
2lfrM9ounJvg9bG3oOLP
En9XR2muuSHyloJWCbaM
GEXoAb2Yz8piSJbB3O0D
ZQgPLVB90n8ZjvZfA76V
NsOjIbG1Jqy2TopvIWuc
##### API Call Bot
rvNNzydqrRsDOHjEAOZbxWmaY
LXmjINGL4odrmswMdnkw
##### External API Ingress Data Bot
LIpgfRiwFhqRYDfdiCNcOxHiL
WLszuhGLnrcgmHuynYec
##### External API Egress Data Bot
FKx8oPqJGJLytdB7eb4F
onoTOscdVDuqLYXZGrbDqhqNo
##### API Call Response Mapping
EUeoppSEIrwXf1YQ4baQ
mR9f3Fw0RcsUAqoIXRQK
xFxCTNGQdrmlkEhwWioAuKSqt
##### API Call Request Body Mapping
MFoFtvuGyWBIsqis2vZb
UxC0g8xn9BeBCBcszKUZ
VfmNjPpoHzgByADwCVlTOhUUF
##### API Call Request Query Mapping
APwYdyFR0JhV5V756gT5
oDy9xgoERdAdCJr1IDt1
qQzeWYqXPmjOqDAzeSSyxwlPH
##### Microservice Bot Generator
NbQuGg9SO54hgdLG56TO
ejHwL4jq315bQPJmiVOB
fJDdaJPQgKYTdvGYtsXLoaVsf
ibrwQIC6OaYzCOTJRAC1
##### Microservice Bot Generator Bot
d8wKkssoS4KG8AOdE7NF
jDFqQOEDIxIOgGAovsrajYtga
##### Populate from User Bot
diUWKHULzfPesWwSVAyQhDXfO
Hf2vyspM6TGQhP0pqUSy
##### Copy Task by Data ID Bot
QpmWKnBQFjCwNkKrcCqIemuQP
sXmtfqZgge1DBZtwka1z
##### Copy Task by Data ID
kUwMKwoGTEMPCqTWKtFzLajSA
U46NanEODGt2PjhAGahp
IwD0eR8NppfwkWLixulV
RYiAohfz978s2ywenCXa
4fuUwuOlnrzQbmjNVmA2
IadgQsOiY06EYYomh4tU
SOixmAcgTjEy8jqFol2k
kNbnCEoD9dKrASopwfMy
##### View Task Help Text
DIpSRrtkoaSTdvkwwCiyqjQtF
dXoLMxTBKfZtE78PRJah
sVvUccPX2ugYv2qtCwKQ
NfRaAvhftKhItSPMMjFJdOZVc
##### View Task Help Text Bot
X8FXviNnm2XbxMrCup39
nDkYbfWpxalOqjtypnyYFGgRo

### Task documents
dssHRRryWueHfJDfcCCMgCAsq Fields subtask in the Task Designer

MFAzIytaMcyKmeIqoXjkVGhpP Subtasks subtask in the Task Designer

gqyLWclKrvEaWHalHqUdHghrt Task Designer

jAedvDUlCMvBueRJKRtluarGb Action Designer

eoUIvRnzeFAwpWXRYUDAgCcsT Bot Designer

wHDJDjjuLcFzEfuUTcRmmrwvq Role Designer

hbcEvkNeshLeMYbUlRxxTmzGJ User Designer

FDBgGnmxnANfcItGYLWqSezqf User Activity Task

NrCEAuHUenfkPKXrwafnbwPKx Home Page Task

aunLmiMdBCHTFaICKcQqaNiTE Delete Data

ZywxnaovrLdeCqCMGQYifFeYj Delete Admin

timwXghrohGAmFRHdhusUojXj Hub

CPpLFgqXiPQifTQLrvwckTEIX Access (for Role)

JeyrWXyjjEweuzCFdoTBWorcA API ID Mapping

aRApCwKlMSsmkYDwMqzcsrKGc Mapping Local Data ID to Hub Data ID

NmWRIUMwmZsZTPkqtBiacDVjE Import Data

pfZwOtZRIPvIcKcfPGYtVahFJ Import Data and Create Task

fKgfaVERIycoCdJoaXmhDTSYr Export Data

ltygzBCYpjlqeXRhSVPZtzJKb Create Chart From Data

NLTnWOevtQumpNwRkncVoVwVy Chart Filters

hPgQYxCMcCIpsrmYSDQWgpQnt Chart Time Filters

WdOjfHXRMsRMDPXVnniFiSbCi Productivity Chart

KjMfWBAgmjSbKmuEbTVONAgYC Domain Description Task

zIoQjcmACDOdQsrKckUmmzLTZ Create Table from Data

NQCKyqbabPMBXNckFonFplZUp Export App

ZawOEQxDxpMkuPLvDjPaFzKeN Import App

YarBulDZmscLxtzHQmceUlngA Generate Bot Scaffold

LdzJcyFighLOeWKbGnLbkRTAs Configure API Call

EUNGGKCyRVgpkwppxsmwSaGqS API Call Bot

WrwPMQieQfBwDUqaiVIjUdSfr API Call Response Mapping

RobYvEenHrwFJpywvRijuCfLC API Call Request Body Mapping

ZvsqHknuxWMzsbIZaDbbtQnaN API Call Request Query Mapping

yfQCdSqMPefHAFAgMBfmTbGRE Microservice Bot Generator

fBBrYrFvIGjBnfwOPMkNVsDkh Copy Task by Data ID

GBWKfLRqZqcEepMjDJchkIhex View Task Help Text

### Schema documents
SszMuOVRaMIOUteoWiXClbpSC Fields subtask in the Task Designer

SYfKUkHjtbesOmcBsPVyimJjk Subtasks subtask in the Task Designer

gjGLMlcNApJyvRjmgQbopsXPI Task Designer

tnidUuNfnFUmfGXiFbrRVpIOE Action Designer

veKWxEWtCEnCDLknytuUPzMHY Bot Designer

ZqfNOGnsYQkMOnFpfiWOgdbFb Role Designer

TkfMDeIjfSaXCMSByYRsEbubp User Designer

iRQJHSitiUYNJJFfNSNCiFxwo User Activity Task

BJVHCgzBvaahJFBMOGTZSniwK Home Page Schema

LxxEpWrsXWUNBeUDWoRHuxckw Delete Data

qHKCnOqyRdtCMSjpPdqLpLNpO Delete Admin

tKPCahjxcZHviTXprUnFmHJSj Hub

AckJPcqmSVmgumHxFUveOzCQc Access (for Role)

boDZGOgWZLgjnnMGqcVNZKdcH API ID Mapping

CxeRslsyixAQcPSZbhzBCeuQl Mapping Local Data ID to Hub Data ID

FLDIivYCRvcKHvSJkPQMMVwTy Import Data

GpHQudsjtVMmyLgurwuIJUMcY Import Data and Create Task

uGhKgwWImpQhdXIPssYLfuxaO Export Data

nMyfcJQJSykcdLpvjqFRZXkeO Create Chart From Data

etAhazqUoXhSAuGKRnxtEDavk Chart Filters

mkKWpBnxSCurMxRvCUCdlhVkv Chart Time Filters

qHplnMMoSxQXXMRuPpkwkKhXt Productivity Chart

NkYfAyOAdEVtNZjbIcHLMVacL Domain Description Task

IYrpkFurrcTnXqbLuMawFknMd Create Table from Data

SChIkAEEgocnLkfLkmQKTfRdh Export App

mbwGVwGevBqIjneLlrwTrPkXv Import App

amqwvbsnpVBeIgSYGZrBZdgpB Generate Bot Scaffold

LpMdSjNKmHKKeSpnYYijzxJFW Configure API Call

qDcgEdMLOIZqdqmEAZgBslDZe API Call Bot

JOUclEYsyCPQLRcsiJVUdBzAt API Call Response Mapping

NzmecQWttNNpOdAfqqyJKUYFb API Call Request Body Mapping

InzJHBVAyldrwhaCqmXNbYJCd API Call Request Query Mapping

jFEeCOwJhetWvAWeLgjiUGKzM Microservice Bot Generator

SSQLBvnWZiUbdeWTsPaldUHSf Copy Task by Data ID

nEwbjkzzZiJjLhUOWYBhVEdzI View Task Help Text

### UX documents
tBrxmDfyKRTsOwlXVUgwqgfdo Fields subtask in the Task Designer

HOXSNlWYKJycecTKcTUCYZngH Subtasks subtask in the Task Designer

hRpIEGPgKvxSzvnCeDpAVcwgJ Task Designer

kXnCBDqLIVNEfUYGpDTOszpVa Action Designer

wEuFHLqvricbzBxgfdWqwljjH Bot Designer

WRvfDCRJZIjiyXLhByqvDimHu Role Designer

DOvLGDRFWXGbJoYPaHJaXAhaB User Designer

GfzLPQfRAFMPjKgiRCQmMoCDK Home Page UX

WpevfQVwoIgciDCGoMsTOdPaL User Activity Task

WmeXeedurGANykNmcGDuXJJiF Delete Data

GJUxEOcosbBXLjjLJHRKffCmg Delete Admin

EuykMbYuOVbRjtloPqXvpMCDM Hub

jBasZoLkntvyYxLtZaWcFIGbZ Access (for Role)

JJCBaEjtXFsJxhwtKVIifvUoP API ID Mapping

tzNRChPmlGYlxZzeTePwWnlHN Mapping Local Data ID to Hub Data ID

kgGbDPiPAmGvJawTgbfjyXzUS Import Data

pXfWQhvmQUbDpAvPvcHaYxGiE Import Data and Create Task

ZHrpqruzyePaJZLMXbGqiNNzg Export Data

cYdflemxtmQCdlsgiGVcsgFpm Create Chart From Data

FcnsazNFYDhnRBenulTdddarG Chart Filters

QMkZgWwaWPCvwQtHrtTtxBnzy Chart Time Filters

PYGQMhVtTmutXMIjZnherzCHJ Productivity Chart

IGNbJdNDTnQeryBukcqzFaKLP Domain Description Task

GjxnRTamLcVfyALAMjpWaywYm Create Table from Data

RoAOiUGVCjOxMxROyWWKSspWQ Export App

PTJNTfiqzPskDXtfkXeutfSxr Import App

QRSisBlijMRqnJIUtwQdWYJaD Generate Bot Scaffold

BhqrZLowmeKVBnjdtdcltoflx Configure API Call

CwcxWSGMgGPZhpszSupwoFEOG API Call Bot

EwOQjcwBHpwevbJNIJlgBDPmg API Call Response Mapping

FsSCMHJdftcofTOIuYZwAaSid API Call Request Body Mapping

pNRnvHsdvSlnoRMoWPpyiUFwQ API Call Request Query Mapping

cQUrnWzisOBSrWJWeDkRzgqOU Microservice Bot Generator

nVOHjtQtRcTPoMXGuGXDTLymV Copy Task by Data ID

pBEGavjujYrMqIbWcgQZHJLfK View Task Help Text

### Bot documents
htEkPykRhMPGQZbYupBEnSoZW is used by Edit Task.

YGfhqykG4DevPuRbs9og is used by Edit Bot.

nQviuzDEOJAMaNlXWYxACEHyJ is used by Table.

HVbpJJMkPILkkbmWnXlzaMzJt is used by Edit Role Bot.

kfMVkutcTRuEulnlAAdWGqPqR is used by the Edit User Bot.

OrQZUtWFxbuFlOywRCmVghFOk is used by User Activity Bot.

qTWBtJLRirWQSrYkwovAgPOKV Home Page Loading

VTsOSBDpowHDpgRlENgACpRCy Delete Data

YRofmufGpjLwppaABBABCEZDL Delete Admin

dplFLIDbLxEpMicZPIZBfRElq HTTPS Call

rgaOfYlXNiCdqZDJQMInWqGTD Import Data

CgVbTRLGqMILSZeRwHOOzKSgG Import Data and Create Task

somRYSmcgIOiIOTVTLXGkPEHv Export Data

kSqLqLnLWfWOvnniLmMkJVZeg Create Chart From Data

pTnrGpbRwsSDglVJpXHfRWILW Load Chart Javascript

IIHbrCXsTDjLxuzSAYsVfgkhP Productivity Chart Bot

AgEzVoDFisMTZISMlwWWMtply Create Table from Data

OpavhdWZFQQGEmcXGcpHZULZV New Data Bot

acfKuypDwkuzhLmRXoOCWUxrI Load Tracking

LOEDMRbHPbSUKljIHGnHaGIrX Done Tracking

uAqHnGjWCIKooXoxOUNiXtibg Export App Bot

EdMBtBxWbONSKGjOwavbPBmBc Import App Bot

LJPhWQPQVKCIDadrlUkmTSBCk Generate Bot Scaffold Bot

hpVLOZPRcKermjklytULzXHpQ Configure API Call Bot

fbOHJeQNXaFAdJOGSBBetSYBk API Call Bot

VdSyROckwzkoqwPshDLmBzEuE External API Ingress Data Bot

kiTAYFaOWeaUDQZwUEbSdnyzG External API Egress Data Bot

RwobHBnKvyVCguOssnpEXjVFg Microservice Bot Generator Bot

BGKEflUaAmmFkONzXEdtofpLI Populate from User

GCRKUfGuSjhIygJEKNUOKPeqX Copy Task by Data ID

BRqRwOmVZLkjQFooEgOuepWhN View Task Help Text Bot

### Action documents
oKfQEpJHvAbArgORrxKtxYGhU is used by Edit Task.

Q0hVeWCMDJKUQC3QXcad is used by Edit Bot.

KnjLfWOLfOLqhcPDrZHyqLRei is used by Table.

vQErjvQmTZEnqyAKZDRhcLUWb is used by Edit Role Bot.

IdskJMjbrXqajxBxuVJXwtvJO is used by the Edit User Bot.

lLQYDeuZOaBlyvEUYUqOMdLkQ is used by User Activity Bot.

gjYnEvPXMHcFdGTlaAqcVwgeT Home Page Loading

wFmbddWNPbKzUlOsaaCPPPEry Delete Data

YRofmufGpjLwppaABBABCEZDL Delete Admin

CTrHLcHiYHffRkhbDOhvPIfXo HTTPS Call

oriQJBXPffmjEfcerVJOYNDNZ Import Data

boKQEvtorYRFqCdMUWijWGdah Import Data and Create Task

xrPdHxuePZKncEKgFQQmUnUBj Export Data

pzdbvpiWPWbUrTKbpqMvLJQVP Create Chart From Data

puUsZhyrDsNMSKMMVaDyibOcX Load Chart Javascript

zUEtSfysgwLzkIYFYUrtZJPBe Productivity Chart Bot

KwJBzHvlTnzyBnFBCLsXYuVNK Create Table from Data

hkKOvLHidwIjfdxdTDaNzfpIw New Data Bot

gcnhOJfvgwJaedXHzWTKgCTqx Load Tracking

YSiZMLUgQhaNhEkwSfLFtpXwh Done Tracking

PNUCSBXgMQukkhjxiILQnutii Export App Bot

rzkOXmwwqIHNUJhjHZTwuvSpQ Import App Bot

hHKzAAsLzNKVucvzDrMAlSUie Generate Bot Scaffold Bot

RlhTsdyftQSuOLnRtGOeLWCxf Configure API Call Bot

IxyOiwotTKqEHdUcSQAKbpote API Call Bot

DZXdLZbKJPEsBFYwyXmaQGVMS External API Ingress Data Bot

msGVLEWTAxcXLMyGTfXvKpRjv External API Egress Data Bot

sbhvVUVkwoornwFCVZAUJrfNi Microservice Bot Generator Bot

HcstrovwcHRkNuVGMgcwRISqB Populate from User

zQPtEzFvjNBWqfzrHUVbbBzSI Copy Task by Data ID

urNxngaYxPhSXpVvRQPpiOtwv View Task Help Text Bot
