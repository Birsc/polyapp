// +build js

// Command serviceworker is Go code compiled to WASM which can respond to requests
// you might normally make to a web server. This might include validation requests,
// performing inference or other ML in the browser, or any other unprivileged work.
// It does NOT have access to the DOM, but it DOES have the ability to send messages
// in to the main thread.
package main

import (
	"fmt"
	"syscall/js"

	"gitlab.com/polyapp-open-source/polyapp/handlers"
)

var (
	endlessChannel chan string
)

func main() {
	self := js.Global().Get("self")
	if !self.Truthy() {
		panic("could not get a reference to self")
	}

	// You can comment out the following 3 lines if you want all requests
	// to go to the web server.
	//self.Set("POSTHandler", js.FuncOf(clientPOSTHandler))
	//self.Set("GETHandler", js.FuncOf(clientGETHandler))
	//fmt.Println("Handlers should be registered")

	// run this code indefinitely
	endlessChannel = make(chan string)
	<-endlessChannel
}

// clientPOSTHandler does similar work to what the server does when it receives a POST request.
// In fact, they share a lot of code. This should be kept in sync with handlers/DefaultService.go POSTHandler function
// Inputs are args = [event.request.json()] where the event was a 'fetch' event.
func clientPOSTHandler(this js.Value, args []js.Value) interface{} {
	POSTRequest, err := POSTRequest(this, args)
	if err != nil {
		fmt.Println("POSTRequest error: " + err.Error())
	}
	messageOutgoing, err := handlers.HandlePOST(POSTRequest)
	if err != nil {
		fmt.Println("Error in HandlePOST: " + err.Error())
		return nil
	}
	err = messageOutgoing.Validate()
	if err != nil {
		fmt.Println("MessageOutgoing is invalid: " + err.Error())
		return nil
	}
	return SetMessage(messageOutgoing)
}

// clientGETHandler does similar work to what the server does when it receives a GET request.
// In fact, they share a lot of code. This should be kept in sync with handlers/DefaultService.go GETHandler function
// Inputs are args = [event.request.json()] where the event was a 'fetch' event.
func clientGETHandler(this js.Value, args []js.Value) interface{} {
	req, err := GetRequest(this, args)
	if err != nil {
		fmt.Println("GetRequest failure: " + err.Error())
		return nil
	}
	response, err := handlers.HandleGET(req)
	if err != nil {
		fmt.Println("HandleGET failure: " + err.Error())
		return nil
	}
	err = response.Validate()
	if err != nil {
		fmt.Println("response is invalid: " + err.Error())
		return nil
	}
	return SetResponse(response)
}
