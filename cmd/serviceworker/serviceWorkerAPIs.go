// +build js

package main

import (
	"errors"
	"fmt"
	"net/url"
	"syscall/js"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// POSTRequest translates an incoming message to the POSTRequest format or throws an error.
func POSTRequest(this js.Value, args []js.Value) (messageIncoming common.POSTRequest, err error) {
	messageData := args[0]
	if !messageData.Truthy() {
		return messageIncoming, errors.New("messageData wasn't truthy")
	}
	if !messageData.Get("MessageID").Truthy() {
		return messageIncoming, errors.New("MessageID not included in message")
	}
	if !messageData.Get("IndustryID").Truthy() {
		return messageIncoming, errors.New("IndustryID not included in message")
	}
	if !messageData.Get("DomainID").Truthy() {
		return messageIncoming, errors.New("DomainID not included in message")
	}
	if !messageData.Get("SchemaID").Truthy() {
		return messageIncoming, errors.New("SchemaID not included in message")
	}
	if !messageData.Get("TaskID").Truthy() {
		return messageIncoming, errors.New("TaskID not included in message")
	}
	if !messageData.Get("Data").Truthy() {
		return messageIncoming, errors.New("Data not included in message")
	}
	if !messageData.Get("UXID").Truthy() {
		return messageIncoming, errors.New("UXID not included in message")
	}
	if !messageData.Get("SchemaID").Truthy() {
		return messageIncoming, errors.New("SchemaID not included in message")
	}
	if !messageData.Get("DataID").Truthy() {
		return messageIncoming, errors.New("DataID not included in message")
	}
	if !messageData.Get("UserID").Truthy() {
		return messageIncoming, errors.New("UserID not included in message")
	}

	dataUnasserted := common.ValueToMap(messageData.Get("Data"))
	dataAsserted := make(map[string]map[string]interface{})
	var ok bool
	for k, v := range dataUnasserted {
		dataAsserted[k], ok = v.(map[string]interface{})
		if !ok {
			return messageIncoming, errors.New("key: " + k + " in Data was not a map[string]interface{}")
		}
	}

	messageIncoming = common.POSTRequest{
		MessageID:          messageData.Get("MessageID").String(),
		IndustryID:         messageData.Get("IndustryID").String(),
		OverrideIndustryID: messageData.Get("OverrideIndustryID").String(),
		DomainID:           messageData.Get("DomainID").String(),
		OverrideDomainID:   messageData.Get("OverrideDomainID").String(),
		TaskID:             messageData.Get("TaskID").String(),
		OverrideTaskID:     messageData.Get("OverrideTaskID").String(),
		Data:               dataAsserted,
		UXID:               messageData.Get("UXID").String(),
		SchemaID:           messageData.Get("SchemaID").String(),
		DataID:             messageData.Get("DataID").String(),
		UserID:             messageData.Get("UserID").String(),
	}
	err = common.FixPOSTRequest(&messageIncoming)
	if err != nil {
		return common.POSTRequest{}, fmt.Errorf("FixPOSTRequest: %w", err)
	}
	err = messageIncoming.Validate()
	if err != nil {
		return common.POSTRequest{}, fmt.Errorf("invalid incoming message: %w", err)
	}
	return messageIncoming, nil
}

// SetMessage returns the message encoded into a JS object
func SetMessage(messageOutgoing common.POSTResponse) js.Value {
	messageObject := js.Global().Get("Object").New()
	messageObject.Set("MessageID", messageOutgoing.MessageID)

	// I'm doing this work to get an array of objects so that js.ValueOf works properly
	ModDOMsLocal := make([]interface{}, len(messageOutgoing.ModDOMs))
	for i, a := range messageOutgoing.ModDOMs {
		ajaxDOMObject := js.Global().Get("Object").New()
		ajaxDOMObject.Set("InsertSelector", a.InsertSelector)
		ajaxDOMObject.Set("DeleteSelector", a.DeleteSelector)

		ajaxDOMObject.Set("Action", a.Action)
		ajaxDOMObject.Set("HTML", a.HTML)
		ModDOMsLocal[i] = ajaxDOMObject
	}
	messageObject.Set("ModDOMs", js.ValueOf(ModDOMsLocal))
	return messageObject
}

func GetRequest(this js.Value, args []js.Value) (request common.GETRequest, err error) {
	rawURL := args[0].Get("url")
	if !rawURL.Truthy() {
		return common.GETRequest{}, errors.New("url not provided in request to WASM")
	}
	URL, err := url.ParseRequestURI(rawURL.String())
	if err != nil {
		return common.GETRequest{}, fmt.Errorf("invalid URL: %w", err)
	}
	request, err = common.CreateGETRequest(*URL)
	if err != nil {
		return common.GETRequest{}, fmt.Errorf("could not get request from URL: %w", err)
	}

	err = request.Validate()
	if err != nil {
		return common.GETRequest{}, fmt.Errorf("invalid incoming request: %w", err)
	}
	return request, nil
}

// SetResponse returns the message encoded into a JS object
func SetResponse(response common.GETResponse) js.Value {
	responseObject := js.Global().Get("Object").New()
	responseObject.Set("HTML", response.HTML)
	responseObject.Set("RedirectURL", response.RedirectURL)
	return responseObject
}

// ConsoleLog logs a single js.Value to console. This is prettier than using fmt with js values.
func ConsoleLog(v js.Value) {
	js.Global().Get("console").Call("log", v)
}
