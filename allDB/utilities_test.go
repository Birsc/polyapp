package allDB

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func Test_replaceSchemaInKey(t *testing.T) {
	type args struct {
		key         string
		newIndustry string
		newDomain   string
		newSchema   string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "empty",
			args: args{
				key:       "",
				newSchema: "",
			},
			want: "",
		},
		{
			name: "too short key",
			args: args{
				key:       "ind_dom",
				newSchema: "new schema",
			},
			want: "",
		},
		{
			name: "non ref",
			args: args{
				key:         "ind_dom_coll_field",
				newSchema:   "new coll",
				newIndustry: "ind",
				newDomain:   "dom",
			},
			want: "ind_dom_new coll_field",
		},
		{
			name: "ref",
			args: args{
				key:         "_ind_dom_coll_field",
				newSchema:   "new coll",
				newIndustry: "newind",
				newDomain:   "newdom",
			},
			want: "_newind_newdom_new coll_field",
		},
		{
			name: "key too long",
			args: args{
				key:         "ind_dom_coll_field_suffix",
				newSchema:   "new coll",
				newIndustry: "ind",
				newDomain:   "dom",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := common.ReplaceIndustryDomainSchemaInKey(tt.args.key, tt.args.newIndustry, tt.args.newDomain, tt.args.newSchema); got != tt.want {
				t.Errorf("replaceSchemaInKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
