package allDB

import (
	"errors"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// CopyData copies a Data document and all of its Ref and ARef children into a new array of Datas, where the first Data
// is the parent, the subsequent Datas are Refs, and the final Datas are ARefs.
// It does not go more than 1 Ref layer down - aka it is not recursive.
//
// This function sets the FirestoreID of every Data to a new value so it is safe to directly save the result to the database.
//
// CopyData does read child Refs from the database in parallel. It does NOT save anything to the database. This is because
// it is assumed that the copies will need to be modified slightly before storage and if we saved them to the database
// here, you would have to perform multiple writes within 1 second when you make the subsequent update. Since multiple
// writes in 1 second are not supposed to be allowed by Firestore, this function returns copies without writing them to the DB.
func CopyData(data *common.Data) (copies []common.Data, err error) {
	if data == nil {
		return nil, errors.New("data was nil")
	}
	original, err := data.Simplify()
	if err != nil {
		return nil, fmt.Errorf("data.Simplify: %w", err)
	}
	parentCopy := common.Data{}
	err = parentCopy.Init(original)
	if err != nil {
		return nil, fmt.Errorf("parentCopy.Init: %w", err)
	}
	parentCopy.FirestoreID = common.GetRandString(25)
	copies = make([]common.Data, 1)
	copies[0] = parentCopy
	errChanLen := 0
	errChan := make(chan error)
	var l sync.Mutex
	for k := range parentCopy.Ref {
		if parentCopy.Ref[k] != nil && *parentCopy.Ref[k] != "" {
			g, err := common.ParseRef(*parentCopy.Ref[k])
			if err != nil {
				return nil, fmt.Errorf("ParseRef: %w", err)
			}
			id := g.DataID
			if id != "" {
				errChanLen++
				go func(errChan chan error, k string, l *sync.Mutex, id string) {
					refData, err := ReadData(id)
					if err != nil {
						errChan <- fmt.Errorf("allDB.ReadData %v: %w", id, err)
						return
					}
					original, err := refData.Simplify()
					if err != nil {
						errChan <- fmt.Errorf("refData.Simplify %v: %w", id, err)
						return
					}
					refCopy := common.Data{}
					err = refCopy.Init(original)
					if err != nil {
						errChan <- fmt.Errorf("refCopy.Init %v: %w", id, err)
						return
					}
					refCopy.FirestoreID = common.GetRandString(25)
					l.Lock()
					copies = append(copies, refCopy)
					parentCopy.Ref[k] = common.String(common.CreateRef(g.IndustryID, g.DomainID, g.TaskID, g.UXID, g.SchemaID, refCopy.FirestoreID))
					l.Unlock()
					errChan <- nil
				}(errChan, k, &l, id)
			}
		}
	}
	combinedErr := ""
	for i := 0; i < errChanLen; i++ {
		errLocal := <-errChan
		if errLocal != nil {
			combinedErr += errLocal.Error() + "; "
		}
	}
	if combinedErr != "" {
		return nil, errors.New(combinedErr)
	}
	errChanLen = 0
	for k := range parentCopy.ARef {
		if parentCopy.ARef[k] != nil && len(parentCopy.ARef[k]) > 0 {
			for i := range parentCopy.ARef[k] {
				g, err := common.ParseRef(parentCopy.ARef[k][i])
				if err != nil {
					return nil, fmt.Errorf("ParseRef: %w", err)
				}
				id := g.DataID
				if id != "" {
					errChanLen++
					go func(errChan chan error, k string, i int, l *sync.Mutex, id string) {
						refData, err := ReadData(id)
						if err != nil {
							errChan <- fmt.Errorf("allDB.ReadData %v: %w", id, err)
							return
						}
						original, err := refData.Simplify()
						if err != nil {
							errChan <- fmt.Errorf("refData.Simplify %v: %w", id, err)
							return
						}
						refCopy := common.Data{}
						err = refCopy.Init(original)
						if err != nil {
							errChan <- fmt.Errorf("refCopy.Init %v: %w", id, err)
							return
						}
						refCopy.FirestoreID = common.GetRandString(25)
						l.Lock()
						copies = append(copies, refCopy)
						parentCopy.ARef[k][i] = common.CreateRef(g.IndustryID, g.DomainID, g.TaskID, g.UXID, g.SchemaID, refCopy.FirestoreID)
						l.Unlock()
						errChan <- nil
					}(errChan, k, i, &l, id)
				}
			}
		}
	}
	combinedErr = ""
	for i := 0; i < errChanLen; i++ {
		errLocal := <-errChan
		if errLocal != nil {
			combinedErr += errLocal.Error() + "; "
		}
	}
	if combinedErr != "" {
		return nil, errors.New(combinedErr)
	}

	return copies, nil
}

// ReadDataAndChildren returns a Data Document and all of its Ref and ARef children. The first Data is for the dataID
// passed in to the function. The others may be out of order.
//
// It does not go more than 1 Ref layer down - aka it is not recursive.
func ReadDataAndChildren(dataID string) ([]common.Data, error) {
	var err error
	chanLen := 0
	errChan := make(chan error)
	dataChan := make(chan common.Data)
	allData := make([]common.Data, 0)
	headData, err := ReadData(dataID)
	if err != nil {
		return nil, fmt.Errorf("ReadData: %w", err)
	}
	allData = append(allData, headData)
	for k := range headData.Ref {
		if headData.Ref[k] != nil && *headData.Ref[k] != "" {
			g, err := common.ParseRef(*headData.Ref[k])
			if err != nil {
				return nil, fmt.Errorf("ParseRef: %w", err)
			}
			id := g.DataID
			if id != "" {
				chanLen++
				go func(errChan chan error, id string) {
					data, err := ReadData(id)
					if err != nil {
						dataChan <- common.Data{}
						errChan <- fmt.Errorf("ReadData %v: %w", id, err)
						return
					}
					dataChan <- data
					errChan <- nil
				}(errChan, id)
			}
		}
	}
	for k := range headData.ARef {
		if headData.ARef[k] != nil && len(headData.ARef[k]) > 0 {
			for i := range headData.ARef[k] {
				g, err := common.ParseRef(headData.ARef[k][i])
				if err != nil {
					return nil, fmt.Errorf("ParseRef: %w", err)
				}
				id := g.DataID
				if id != "" {
					chanLen++
					go func(errChan chan error, id string) {
						data, err := ReadData(id)
						if err != nil {
							dataChan <- common.Data{}
							errChan <- fmt.Errorf("ReadData %v: %w", id, err)
							return
						}
						dataChan <- data
						errChan <- nil
					}(errChan, id)
				}
			}
		}
	}
	combinedErr := ""
	for i := 0; i < chanLen; i++ {
		d := <-dataChan
		errLocal := <-errChan
		if errLocal != nil {
			combinedErr += errLocal.Error() + "; "
		}
		allData = append(allData, d)
	}
	if combinedErr != "" {
		return nil, errors.New(combinedErr)
	}
	return allData, nil
}

// DeleteDataAndChildren deletes a Data Document ID and all of its Ref and ARef children.
//
// It does not go more than 1 Ref layer down - aka it is not recursive.
func DeleteDataAndChildren(dataID string) error {
	if dataID == "" {
		return errors.New("dataID was empty")
	}
	data, err := ReadData(dataID)
	if err != nil {
		return fmt.Errorf("ReadData %v: %w", dataID, err)
	}
	errChanLen := 0
	errChan := make(chan error)
	for k := range data.Ref {
		if data.Ref[k] != nil && *data.Ref[k] != "" {
			g, err := common.ParseRef(*data.Ref[k])
			if err != nil {
				return fmt.Errorf("ParseRef: %w", err)
			}
			id := g.DataID
			if id != "" {
				errChanLen++
				go func(errChan chan error, id string) {
					err = DeleteData(id)
					if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
						errChan <- fmt.Errorf("allDB.DeleteData %v: %w", id, err)
						return
					}
					errChan <- nil
				}(errChan, id)
			}
		}
	}
	combinedErr := ""
	for i := 0; i < errChanLen; i++ {
		errLocal := <-errChan
		if errLocal != nil {
			combinedErr += errLocal.Error() + "; "
		}
	}
	if combinedErr != "" {
		return errors.New(combinedErr)
	}
	errChanLen = 0
	for k := range data.ARef {
		if data.ARef[k] != nil && len(data.ARef[k]) > 0 {
			for i := range data.ARef[k] {
				g, err := common.ParseRef(data.ARef[k][i])
				if err != nil {
					return fmt.Errorf("ParseRef: %w", err)
				}
				id := g.DataID
				if id != "" {
					errChanLen++
					go func(errChan chan error, id string) {
						err := DeleteData(id)
						if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
							errChan <- fmt.Errorf("allDB.DeleteData %v: %w", id, err)
							return
						}
						errChan <- nil
					}(errChan, id)
				}
			}
		}
	}
	combinedErr = ""
	for i := 0; i < errChanLen; i++ {
		errLocal := <-errChan
		if errLocal != nil {
			combinedErr += errLocal.Error() + "; "
		}
	}
	if combinedErr != "" {
		return errors.New(combinedErr)
	}

	err = DeleteData(data.FirestoreID)
	if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
		return fmt.Errorf("DeleteData for main Data %v: %w", data.FirestoreID, err)
	}

	return nil
}
