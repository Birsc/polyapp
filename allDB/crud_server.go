//+build !js

package allDB

import (
	"errors"
	"firebase.google.com/go/auth"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/firebaseCRUD"
	"strings"
	"sync"

	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
)

// createData assumes the inputs are valid.
func createData(data *common.Data) error {
	firestoreErrChan := make(chan error, 1)

	go func(data *common.Data, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.CreateData(ctx, client, data)
		errChan <- err
	}(data, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readData assumes the inputs are valid.
func readData(firestoreID string) (common.Data, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Data{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	data, err := firestoreCRUD.ReadData(ctx, client, firestoreID)
	if err != nil {
		return common.Data{}, fmt.Errorf("error in Firestore's ReadData: %w", err)
	}

	return data, nil
}

// updateData assumes the inputs are valid.
func updateData(data *common.Data) error {
	firestoreErrChan := make(chan error, 1)

	go func(data *common.Data, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.UpdateData(ctx, client, data)
		errChan <- err
	}(data, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteData assumes the inputs are valid.
func deleteData(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)

	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.DeleteData(ctx, client, firestoreID)
		errChan <- err
	}(firestoreID, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createSchema assumes the inputs are valid.
func createSchema(schema *common.Schema) error {
	firestoreErrChan := make(chan error, 1)
	go func(schema *common.Schema, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.CreateSchema(ctx, client, schema)
		errChan <- err
	}(schema, firestoreErrChan)
	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readSchema assumes the inputs are valid.
func readSchema(firestoreID string) (common.Schema, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Schema{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	schema, err := firestoreCRUD.ReadSchema(ctx, client, firestoreID)
	if err != nil {
		return common.Schema{}, fmt.Errorf("error in Firestore's ReadSchema: %w", err)
	}

	return schema, nil
}

// updateSchema assumes the inputs are valid.
func updateSchema(schema *common.Schema) error {
	firestoreErrChan := make(chan error, 1)
	go func(schema *common.Schema, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.UpdateSchema(ctx, client, schema)
		errChan <- err
	}(schema, firestoreErrChan)
	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteSchema assumes the inputs are valid.
func deleteSchema(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.DeleteSchema(ctx, client, firestoreID)
		errChan <- err
	}(firestoreID, firestoreErrChan)
	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createUser assumes the inputs are valid.
func createUser(user *common.User) error {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
	if err != nil {
		return fmt.Errorf("firebaseCRUD.GetFirebaseClient: %w", err)
	}
	if user.FullName != nil && *user.FullName == "anonymous" {
		// do nothing
	} else if user.UID == nil || *user.UID == "" {
		// this user needs to be created in Firebase.
		params := (&auth.UserToCreate{}).Disabled(false)
		if user.Email != nil {
			params = params.Email(*user.Email)
		}
		if user.EmailVerified != nil {
			params = params.EmailVerified(*user.EmailVerified)
		}
		if user.FullName != nil {
			params = params.DisplayName(*user.FullName)
		}
		if user.PhoneNumber != nil && *user.PhoneNumber != "" {
			params = params.PhoneNumber(*user.PhoneNumber)
		}
		if user.PhotoURL != nil && *user.PhotoURL != "" {
			params = params.PhotoURL(*user.PhotoURL)
		}
		u, err := firebaseClient.CreateUser(firebaseCtx, params)
		if err != nil && strings.Contains(err.Error(), "EMAIL_EXISTS") {
			updateParams := (&auth.UserToUpdate{}).Disabled(false).Email(*user.Email)
			if user.EmailVerified != nil {
				updateParams = updateParams.EmailVerified(*user.EmailVerified)
			}
			if user.FullName != nil {
				updateParams = updateParams.DisplayName(*user.FullName)
			}
			if user.PhoneNumber != nil && *user.PhoneNumber != "" {
				updateParams = updateParams.PhoneNumber(*user.PhoneNumber)
			}
			if user.PhotoURL != nil && *user.PhotoURL != "" {
				updateParams = updateParams.PhotoURL(*user.PhotoURL)
			}
			getUser, err := firebaseClient.GetUserByEmail(firebaseCtx, *user.Email)
			if err != nil {
				return fmt.Errorf("firebaseClient.GetUserByEmail: %w", err)
			}
			u, err = firebaseClient.UpdateUser(firebaseCtx, getUser.UID, updateParams)
			if err != nil {
				return fmt.Errorf("firebaseClient.UpdateUser: %w", err)
			}
		} else if err != nil {
			return fmt.Errorf("firebaseClient.CreateUser: %w", err)
		}
		user.UID = common.String(u.UID)
	}

	err = firebaseClient.SetCustomUserClaims(firebaseCtx, *user.UID, map[string]interface{}{"UserID": user.FirestoreID})
	if err != nil {
		return fmt.Errorf("SetCustomUserClaims: %w", err)
	}

	err = firestoreCRUD.Create(ctx, client, user)
	if err != nil {
		return fmt.Errorf("firestoreCRUD.Create: %w", err)
	}
	return nil
}

// readUser assumes the inputs are valid.
func readUser(firestoreID string) (common.User, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.User{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	user := common.User{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &user)
	if err != nil {
		return common.User{}, fmt.Errorf("error in Firestore's ReadUser: %w", err)
	}
	return user, nil
}

// updateUser assumes the inputs are valid.
func updateUser(user *common.User) error {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
	if err != nil {
		return fmt.Errorf("firebaseCRUD.GetFirebaseClient: %w", err)
	}
	if user.FullName != nil && *user.FullName == "Anonymous" {
		// do nothing
	} else if user.UID == nil || *user.UID == "" {
		if user.Email == nil || *user.Email == "" {
			return errors.New("could not update User because there is no UID and there is no Email")
		}
		lookupUser, err := firebaseClient.GetUserByEmail(firebaseCtx, *user.Email)
		if err != nil {
			return fmt.Errorf("firebaseClient.GetUserByEmail: %w", err)
		}
		user.UID = common.String(lookupUser.UID)
	}
	params := (&auth.UserToUpdate{}).Disabled(false)
	if user.Email != nil {
		params = params.Email(*user.Email)
	}
	if user.EmailVerified != nil {
		params = params.EmailVerified(*user.EmailVerified)
	}
	if user.FullName != nil {
		params = params.DisplayName(*user.FullName)
	}
	if user.PhoneNumber != nil && *user.PhoneNumber != "" {
		params = params.PhoneNumber(*user.PhoneNumber)
	}
	if user.PhotoURL != nil && *user.PhotoURL != "" {
		params = params.PhotoURL(*user.PhotoURL)
	}
	if user.FullName != nil && *user.FullName == "Anonymous" {
		// do nothing
	} else {
		_, err = firebaseClient.UpdateUser(firebaseCtx, *user.UID, params)
		if err != nil && strings.Contains(err.Error(), "USER_NOT_FOUND") {
			params := (&auth.UserToCreate{}).Disabled(false)
			if user.Email != nil {
				params = params.Email(*user.Email)
			}
			if user.EmailVerified != nil {
				params = params.EmailVerified(*user.EmailVerified)
			}
			if user.FullName != nil {
				params = params.DisplayName(*user.FullName)
			}
			if user.PhoneNumber != nil && *user.PhoneNumber != "" {
				params = params.PhoneNumber(*user.PhoneNumber)
			}
			if user.PhotoURL != nil && *user.PhotoURL != "" {
				params = params.PhotoURL(*user.PhotoURL)
			}
			u, err := firebaseClient.CreateUser(firebaseCtx, params)
			if err != nil {
				return fmt.Errorf("firebaseClient.CreateUser: %w", err)
			}
			user.UID = common.String(u.UID)
			user.Email = common.String(u.Email)
		} else if err != nil {
			return fmt.Errorf("firebaseClient.UpdateUser: %w", err)
		}
	}

	err = firestoreCRUD.Update(ctx, client, user)
	if err != nil {
		return fmt.Errorf("firestoreCRUD.Update: %w", err)
	}
	return nil
}

// updateUserPassword in a way which does not save any information to one of our databases.
func updateUserPassword(user *common.User, newPassword string) error {
	firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
	if err != nil {
		return fmt.Errorf("firebaseCRUD.GetFirebaseClient: %w", err)
	}
	params := (&auth.UserToUpdate{}).Password(newPassword)
	_, err = firebaseClient.UpdateUser(firebaseCtx, *user.UID, params)
	if err != nil {
		return fmt.Errorf("firebaseClient.UpdateUser: %w", err)
	}
	return nil
}

// deleteUser assumes the inputs are valid.
func deleteUser(firestoreID string) error {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
	if err != nil {
		return fmt.Errorf("firebaseCRUD.GetFirebaseClient: %w", err)
	}
	user := common.User{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &user)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		return nil
	} else if err != nil {
		return fmt.Errorf("firestoreCRUD.Read: %w", err)
	}
	if user.UID != nil && *user.UID != "" {
		err = firebaseClient.DeleteUser(firebaseCtx, *user.UID)
		if err != nil && strings.Contains(err.Error(), "USER_NOT_FOUND") {
			// OK
		} else if err != nil {
			return fmt.Errorf("firebaseClient.DeleteUser: %w", err)
		}
	}
	err = firestoreCRUD.Delete(ctx, client, &user)
	if err != nil {
		return fmt.Errorf("firestoreCRUD.Delete: %w", err)
	}
	return nil
}

// createUX assumes the inputs are valid.
func createUX(ux *common.UX) error {
	firestoreErrChan := make(chan error, 1)
	go func(ux *common.UX, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, ux)
		errChan <- err
	}(ux, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readUX assumes the inputs are valid.
func readUX(firestoreID string) (common.UX, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.UX{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	ux := common.UX{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &ux)
	if err != nil {
		return common.UX{}, fmt.Errorf("error in Firestore's ReadUX: %w", err)
	}

	return ux, nil
}

// updateUX assumes the inputs are valid.
func updateUX(ux *common.UX) error {
	firestoreErrChan := make(chan error, 1)
	go func(ux *common.UX, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, ux)
		errChan <- err
	}(ux, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteUX assumes the inputs are valid.
func deleteUX(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		ux := common.UX{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &ux)
		errChan <- err
	}(firestoreID, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createTask assumes the inputs are valid.
func createTask(task *common.Task) error {
	firestoreErrChan := make(chan error, 1)
	go func(task *common.Task, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, task)
		errChan <- err
	}(task, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readTask assumes the inputs are valid.
func readTask(firestoreID string) (common.Task, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Task{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	task := common.Task{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &task)
	if err != nil {
		return common.Task{}, fmt.Errorf("error in Firestore's ReadTask: %w", err)
	}

	return task, nil
}

// updateTask assumes the inputs are valid.
func updateTask(task *common.Task) error {
	firestoreErrChan := make(chan error, 1)
	go func(task *common.Task, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, task)
		errChan <- err
	}(task, firestoreErrChan)
	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteTask assumes the inputs are valid.
func deleteTask(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		task := common.Task{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &task)
		errChan <- err
	}(firestoreID, firestoreErrChan)

	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createRole assumes the inputs are valid.
func createRole(role *common.Role) error {

	firestoreErrChan := make(chan error, 1)
	go func(role *common.Role, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, role)
		errChan <- err
	}(role, firestoreErrChan)

	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readRole assumes the inputs are valid.
func readRole(firestoreID string) (common.Role, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Role{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	role := common.Role{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &role)
	if err != nil {
		return common.Role{}, fmt.Errorf("error in Firestore's ReadRole: %w", err)
	}

	return role, nil
}

// updateRole assumes the inputs are valid.
func updateRole(role *common.Role) error {
	firestoreErrChan := make(chan error, 1)
	go func(role *common.Role, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, role)
		errChan <- err
	}(role, firestoreErrChan)

	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteRole assumes the inputs are valid.
func deleteRole(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		role := common.Role{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &role)
		errChan <- err
	}(firestoreID, firestoreErrChan)
	firestoreErr := <-firestoreErrChan
	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createBot assumes the inputs are valid.
func createBot(bot *common.Bot) error {

	firestoreErrChan := make(chan error, 1)
	go func(bot *common.Bot, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, bot)
		errChan <- err
	}(bot, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readBot assumes the inputs are valid.
func readBot(firestoreID string) (common.Bot, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Bot{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	bot := common.Bot{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &bot)
	if err != nil {
		return common.Bot{}, fmt.Errorf("error in Firestore's ReadBot: %w", err)
	}

	return bot, nil
}

// updateBot assumes the inputs are valid.
func updateBot(bot *common.Bot) error {
	firestoreErrChan := make(chan error, 1)
	go func(bot *common.Bot, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, bot)
		errChan <- err
	}(bot, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteBot assumes the inputs are valid.
func deleteBot(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		bot := common.Bot{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &bot)
		errChan <- err
	}(firestoreID, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createAction assumes the inputs are valid.
func createAction(action *common.Action) error {

	firestoreErrChan := make(chan error, 1)
	go func(action *common.Action, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, action)
		errChan <- err
	}(action, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

var (
	actionCache      map[string]common.Action
	actionCacheMutex sync.Mutex
)

// readAction assumes the inputs are valid.
//
// This implementation of readAction assumes that after the instance is started the values of an action
// document are NEVER updated. To update an Action, you would have to start the instance and therefore repopulate the cache.
//
// It DOES allow actions to be created.
func readAction(firestoreID string) (common.Action, error) {
	actionCacheMutex.Lock()
	if actionCache == nil {
		actionCache = make(map[string]common.Action)
	}
	actionCacheMutex.Unlock()
	actionFromCache, ok := actionCache[firestoreID]
	if ok {
		return actionFromCache, nil
	}
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.Action{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	action := common.Action{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &action)
	if err != nil {
		return common.Action{}, fmt.Errorf("error in Firestore's ReadAction: %w", err)
	}

	actionCacheMutex.Lock()
	actionCache[firestoreID] = action
	actionCacheMutex.Unlock()
	return action, nil
}

// updateAction assumes the inputs are valid.
func updateAction(action *common.Action) error {
	firestoreErrChan := make(chan error, 1)
	go func(action *common.Action, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, action)
		errChan <- err
	}(action, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteAction assumes the inputs are valid.
func deleteAction(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		action := common.Action{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &action)
		errChan <- err
	}(firestoreID, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createPublicMap assumes the inputs are valid.
func createPublicMap(publicMap *common.PublicMap) error {

	firestoreErrChan := make(chan error, 1)
	go func(publicMap *common.PublicMap, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, publicMap)
		errChan <- err
	}(publicMap, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readPublicMap assumes the inputs are valid.
func readPublicMap(firestoreID string) (common.PublicMap, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.PublicMap{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	publicMap := common.PublicMap{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &publicMap)
	if err != nil {
		return common.PublicMap{}, fmt.Errorf("error in Firestore's ReadPublicMap: %w", err)
	}

	return publicMap, nil
}

// updatePublicMap assumes the inputs are valid.
func updatePublicMap(publicMap *common.PublicMap) error {
	firestoreErrChan := make(chan error, 1)
	go func(publicMap *common.PublicMap, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, publicMap)
		errChan <- err
	}(publicMap, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deletePublicMap assumes the inputs are valid.
func deletePublicMap(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		publicMap := common.PublicMap{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &publicMap)
		errChan <- err
	}(firestoreID, firestoreErrChan)
	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}
	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// createCSS assumes the inputs are valid.
func createCSS(CSS *common.CSS) error {

	firestoreErrChan := make(chan error, 1)
	go func(CSS *common.CSS, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Create(ctx, client, CSS)
		errChan <- err
	}(CSS, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// readCSS assumes the inputs are valid.
func readCSS(firestoreID string) (common.CSS, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return common.CSS{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	CSS := common.CSS{
		FirestoreID: firestoreID,
	}
	err = firestoreCRUD.Read(ctx, client, &CSS)
	if err != nil {
		return common.CSS{}, fmt.Errorf("error in Firestore's ReadCSS: %w", err)
	}

	return CSS, nil
}

// updateCSS assumes the inputs are valid.
func updateCSS(CSS *common.CSS) error {
	firestoreErrChan := make(chan error, 1)
	go func(CSS *common.CSS, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		err = firestoreCRUD.Update(ctx, client, CSS)
		errChan <- err
	}(CSS, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}

// deleteCSS assumes the inputs are valid.
func deleteCSS(firestoreID string) error {
	firestoreErrChan := make(chan error, 1)
	go func(firestoreID string, errChan chan error) {
		ctx, client, err := firestoreCRUD.GetClient()
		if err != nil {
			errChan <- err
			return
		}
		CSS := common.CSS{
			FirestoreID: firestoreID,
		}
		err = firestoreCRUD.Delete(ctx, client, &CSS)
		errChan <- err
	}(firestoreID, firestoreErrChan)

	firestoreErr := <-firestoreErrChan

	errString := ""
	if firestoreErr != nil {
		errString += "firestoreErr: " + firestoreErr.Error() + ";"
	}

	if errString != "" {
		return errors.New(errString)
	}
	return nil
}
