package allDB

import (
	"strconv"
	"strings"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestAllDBQuery_QueryRead(t *testing.T) {
	// Querying the Data collection
	d := &common.Data{
		FirestoreID: "TestAllDBQuery_QueryRead",
		IndustryID:  "TestAllDBQuery_QueryRead",
		DomainID:    "TestAllDBQuery_QueryRead",
		SchemaID:    "TestAllDBQuery_QueryRead",
	}
	simpleData := map[string]interface{}{
		"TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_a": "b",
	}
	d.Init(simpleData)
	err := DeleteData("TestAllDBQuery_QueryRead")
	if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
		t.Fatal(err)
	}
	err = CreateSchema(&common.Schema{
		FirestoreID:  "TestAllDBQuery_QueryRead",
		IndustryID:   "TestAllDBQuery_QueryRead",
		DomainID:     "TestAllDBQuery_QueryRead",
		SchemaID:     "TestAllDBQuery_QueryRead",
		DataTypes:    map[string]string{"TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_a": "S"},
		DataHelpText: map[string]string{"TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_a": "a help text"},
		DataKeys:     []string{"TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_a"},
		IsPublic:     false,
		Deprecated:   nil,
	})
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not set up Schema: " + err.Error())
	}
	err = CreateData(d)
	if err != nil {
		t.Fatal("could not set document before trying to query it. err: " + err.Error())
	}

	var q common.Query = &Query{}
	q.Init("TestAllDBQuery_QueryRead", "TestAllDBQuery_QueryRead", "TestAllDBQuery_QueryRead", common.CollectionData)
	q.AddEquals("TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_TestAllDBQuery_QueryRead_a", "b")
	iter, err := q.QueryRead()
	if err != nil {
		t.Fatal("could not query read data: " + err.Error())
	}
	ct := 0
	for {
		d, err := iter.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			t.Error("error iterating: " + err.Error())
		}
		ct++
		if d.GetFirestoreID() != "TestAllDBQuery_QueryRead" {
			t.Error("unexpected document retrieved")
		}
	}
	if ct != 1 {
		t.Error("incorrect number of query results: " + strconv.Itoa(ct))
	}

	// Querying the Schema collection with array-contains
	s := &common.Schema{
		FirestoreID:  "TestAllDBQuery2",
		IndustryID:   "TestAllDBQuery2",
		DomainID:     "TestAllDBQuery2",
		SchemaID:     "TestAllDBQuery2",
		DataTypes:    map[string]string{"TestAllDBQuery2_TestAllDBQuery2_TestAllDBQuery2_a": "S"},
		DataHelpText: map[string]string{"TestAllDBQuery2_TestAllDBQuery2_TestAllDBQuery2_a": "generic"},
		DataKeys:     []string{"TestAllDBQuery2_TestAllDBQuery2_TestAllDBQuery2_a"},
	}
	err = DeleteSchema("TestAllDBQuery2")
	if err != nil {
		t.Fatal(err)
	}
	err = CreateSchema(s)
	if err != nil && !strings.Contains(err.Error(), "code = AlreadyExists") {
		t.Fatal("could not set document before trying to query it. err: " + err.Error())
	}

	var qArrayContains common.Query = &Query{}
	qArrayContains.Init("TestAllDBQuery2", "TestAllDBQuery2", "TestAllDBQuery2", common.CollectionSchema)
	err = qArrayContains.AddArrayContains("DataKeys", "TestAllDBQuery2_TestAllDBQuery2_TestAllDBQuery2_a")
	if err != nil {
		t.Fatal("could not set up query: " + err.Error())
	}
	iterArrayContains, err := qArrayContains.QueryRead()
	if err != nil {
		t.Fatal("could not query read data: " + err.Error())
	}
	ct = 0
	for {
		d, err := iterArrayContains.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			t.Error("error iterating: " + err.Error())
		}
		ct++
		if d.GetFirestoreID() != "TestAllDBQuery2" {
			t.Error("unexpected document retrieved")
		}
	}
	if ct != 1 {
		t.Error("incorrect number of query results: " + strconv.Itoa(ct))
	}
	_ = DeleteSchema("TestAllDBQuery2")
}
