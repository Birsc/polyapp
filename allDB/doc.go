// Package allDB rests just above the CRUD layer which is a series of packages handling different databases
// and caches. Requests to this package's APIs (aka exported functions) are database-agnostic and
// application-specific, like a request for a specific structure or a query
// for a list of documents. This layer returns a 'common' package data structure which isn't specific to
// any one database.
//
// The internals of this package may be a bit complicated. If the request is to the Data or User
// collections the data and control paths plunge quickly through this layer, perhaps stopping
// at a cache. Changes to the Schema require several other steps: TODO implement these other steps
// First, schema changes mean we need to create a new schema on the database and ensure they are
// linked to each other.
// Second, we need to decide what the 'master' schema is. For now let's assume it's the original schema.
// Third, there needs to be a conversion from the 'master' schema to the new schema and a conversion from
// the new schema back to the 'master' schema.
// Fourth, requests to data documents which are not using the 'master' schema need to have their data
// converted before that data is sent out from this layer.
//
// Why is all of that necessary? Many companies force their data to hold one single schema and convert
// all data to that schema when the update occurs.
// There are two problems with that approach. First the data model becomes inflexible for users which
// in turn restricts user's ability to make significant updates to their own tasks without a software
// developer helping them, which is then prohibitively expensive. Second, conversions
// on large amounts of data are often done all at once when new software is deployed. I don't want to wait for
// such conversions to occur before the software becomes available. Incremental conversions are the solution
// to this problem since they can run while there are several versions of a schema in use.
//
// One other note: This package is supposed to be used both on the client (JS / WASM) and
// on the server and in both cases its outputs should be identical. Consequently some
// files are built conditionally and this package's tests need to be run with both GOOS / GOARCH.
// However, at this time the JS / WASM code doesn't work yet.
package allDB
