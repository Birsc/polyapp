//+build !js

package allDB

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
)

func (q *Query) queryRead() (common.Iter, error) {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return &firestoreCRUD.DBIterator{}, fmt.Errorf("error in firestore's GetClient: %w", err)
	}
	firestoreQueries, err := allDBQueryToFirestoreQueries(ctx, client, q)
	if err != nil {
		return &firestoreCRUD.DBIterator{}, fmt.Errorf("error in allDBQueryToFirestoreQuery: %w", err)
	}
	if len(firestoreQueries) == 1 {
		iter, err := firestoreCRUD.QueryRead(ctx, client, q.IndustryID, q.DomainID, q.Schema, firestoreQueries[0])
		if err != nil {
			return &firestoreCRUD.DBIterator{}, fmt.Errorf("error in QueryReadData for firestore: %w", err)
		}
		return iter, nil
	}
	lengthOfResults := make([]int, len(firestoreQueries))
	firestoreQueryResults := make([]*firestore.DocumentIterator, len(firestoreQueries))
	for i := range firestoreQueries {
		lengthOfResults[i] = -1
		firestoreQueryResults[i] = firestoreQueries[i].Documents(ctx)
	}
	return firestoreCRUD.CreateCombinedDBIterator(firestoreQueryResults, lengthOfResults), nil
}

// allDBQueryToFirestoreQueries turns a single allDBQuery into one or more firestore.Query.
// It *may* return more than one query if there is an OR clause in Query.
// Ex: i_d_s_Number Field == "0" || i_d_s_Number Field == 0. If we didn't return multiple queries we could only
// search for == "0" which would exclude all number data types.
func allDBQueryToFirestoreQueries(ctx context.Context, client *firestore.Client, allDBQuery *Query) ([]firestore.Query, error) {
	queries := make([]firestore.Query, 0)
	query, err := allDBQueryToFirestoreQuery(ctx, client, allDBQuery)
	if err != nil {
		return nil, fmt.Errorf("allDBQueryToFirestoreQuery: %w", err)
	}
	queries = append(queries, query)

	// Note: this does a terrible job of handling multiple .Equals which can be converted to numbers.
	// In fact, it really only has been tested with one .Equals which either does or does not convert to a number.
	newEquals := make(map[string]interface{})
	useNewEquals := false
	for k, v := range allDBQuery.Equals {
		switch v.(type) {
		case string:
			floatValue, err := strconv.ParseFloat(strings.TrimSpace(v.(string)), 64)
			if err != nil {
				newEquals[k] = v
			} else {
				useNewEquals = true
				newEquals[k] = floatValue
			}
		default:
			newEquals[k] = v
		}
	}
	if useNewEquals {
		copyAllDBQuery := &Query{
			IndustryID:           allDBQuery.IndustryID,
			DomainID:             allDBQuery.DomainID,
			Schema:               allDBQuery.Schema,
			FirestoreCollection:  allDBQuery.FirestoreCollection,
			Equals:               newEquals,
			ArrayContains:        allDBQuery.ArrayContains,
			SortBy:               allDBQuery.SortBy,
			SortDirection:        allDBQuery.SortDirection,
			StartAfterDocumentID: allDBQuery.StartAfterDocumentID,
			Offset:               allDBQuery.Offset,
			Length:               allDBQuery.Length,
		}
		query, err := allDBQueryToFirestoreQuery(ctx, client, copyAllDBQuery)
		if err != nil {
			return nil, fmt.Errorf("allDBQueryToFirestorQuery: %w", err)
		}
		queries = append(queries, query)
	}
	return queries, nil
}

// allDBQueryToFirestoreQuery converts from the Query format to the Firestore format.
func allDBQueryToFirestoreQuery(ctx context.Context, client *firestore.Client, allDBQuery *Query) (firestore.Query, error) {
	err := allDBQuery.validate()
	if err != nil {
		return firestore.Query{}, fmt.Errorf("validation failure: %w", err)
	}
	query := client.Collection(allDBQuery.FirestoreCollection).Query

	for k, v := range allDBQuery.Equals {
		query = query.Where(k, "==", v)
	}
	for k, v := range allDBQuery.ArrayContains {
		query = query.Where(k, "array-contains", v)
	}
	for i, key := range allDBQuery.SortBy {
		if allDBQuery.SortDirection[i] == "asc" {
			query = query.OrderBy(key, firestore.Asc)
		} else {
			query = query.OrderBy(key, firestore.Desc)
		}
	}
	if allDBQuery.StartAfterDocumentID != "" {
		// According to Firestore billing and pagination documentation this is the correct way to paginate data without
		// incurring read costs for every piece of data up until this piece of data.
		startAfterDocSnapshot, err := client.Collection(allDBQuery.FirestoreCollection).Doc(allDBQuery.StartAfterDocumentID).Get(ctx)
		if err != nil {
			return query, fmt.Errorf("client.Collection(%v).Doc(%v).Get(ctx): %w", allDBQuery.FirestoreCollection, allDBQuery.StartAfterDocumentID, err)
		}
		query = query.StartAfter(startAfterDocSnapshot)
	} else if allDBQuery.Offset != 0 {
		query = query.Offset(allDBQuery.Offset)
	}
	if allDBQuery.Length != 0 {
		query = query.Limit(allDBQuery.Length)
	}
	return query, nil
}
