package allDB

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/integrity"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// There are 3 handled possibilities for schema conversions:
// 1. A type has changed type, like Int to Float. This is fixed by ConvertDataToMatchSchema when possible.
// 2. A field has been added. When the field is read, integrity.BackPopulate will add this field to the common.Data
// 3. A field has been removed and now there is some extra field. This is fixed by common.RemoveExtraFields.
// Unhandled possibilities include:
// 1. A field has changed name, but not changed type. We want to transfer the data which used to be associated with
// a field "Some Feld" into a field "Some Field" because we made a mistake and want to fix it. Currently this
// field would be populated with the zero value for its type and the existing data would be deleted.
// To fix this error we would need a mapping from old field name to new field name. Then a conversion would need to be
// run, either incrementally as data is retrieved or all at once.

// CreateData is a CRUD request to the Data collection
func CreateData(data *common.Data) error {
	if data == nil {
		return errors.New("data not set")
	}
	err := data.Validate()
	if err != nil {
		return fmt.Errorf("data invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(data)
	if err != nil {
		return fmt.Errorf("data can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(data)
	if err != nil {
		return fmt.Errorf("data change is dangerous: %w", err)
	}
	if data.SchemaCache.FirestoreID == "" {
		data.SchemaCache, err = ReadSchema(data.SchemaID)
		if err != nil {
			return fmt.Errorf("firestoreCRUD.ReadSchema: %w", err)
		}
	}
	err = integrity.BackPopulate(data)
	if err != nil {
		return fmt.Errorf("integrity.BackPopulate: %w", err)
	}
	err = common.ConvertDataToMatchSchema(data, &data.SchemaCache)
	if err != nil {
		return fmt.Errorf("common.ConvertDataToMatchSchema: %w", err)
	}
	err = common.RemoveExtraFields(data, &data.SchemaCache)
	if err != nil {
		return fmt.Errorf("common.RemoveExtraField: %w", err)
	}
	err = integrity.Convert(data)
	if err != nil {
		return fmt.Errorf("integrity.Convert: %w", err)
	}
	return createData(data)
}

// ReadData is a CRUD request to the Data collection. It assumes that data in the database is in the master schema format.
// TODO when the DB is not always in master schema format, this will need to call a conversion function on the returned readData.
func ReadData(firestoreID string) (common.Data, error) {
	if firestoreID == "" {
		return common.Data{}, errors.New("not all inputs set")
	}
	return readData(firestoreID)
}

// UpdateData is a CRUD request to the Data collection
func UpdateData(data *common.Data) error {
	if data == nil {
		return errors.New("data not set")
	}
	err := integrity.ValidateAllDBs(data)
	if err != nil {
		return fmt.Errorf("data can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(data)
	if err != nil {
		return fmt.Errorf("data change is dangerous: %w", err)
	}
	if data.SchemaCache.FirestoreID == "" {
		data.SchemaCache, err = ReadSchema(data.SchemaID)
		if err != nil {
			return fmt.Errorf("firestoreCRUD.ReadSchema: %w", err)
		}
	}
	err = common.ConvertDataToMatchSchema(data, &data.SchemaCache)
	if err != nil {
		return fmt.Errorf("common.ConvertDataToMatchSchema: %w", err)
	}
	err = common.RemoveExtraFields(data, &data.SchemaCache)
	if err != nil {
		return fmt.Errorf("common.RemoveExtraField: %w", err)
	}
	err = integrity.Convert(data)
	if err != nil {
		return fmt.Errorf("integrity.Convert: %w", err)
	}
	return updateData(data)
}

// DeleteData is a CRUD request to the Data collection.
//
// No error is thrown if the data does not exist in one or more databases.
func DeleteData(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteData(firestoreID)
}

// CreateSchema is a CRUD request to the Schema collection
func CreateSchema(schema *common.Schema) error {
	if schema == nil {
		return errors.New("schema not set")
	}
	err := schema.Validate()
	if err != nil {
		return fmt.Errorf("schema invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(schema)
	if err != nil {
		return fmt.Errorf("schema can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(schema)
	if err != nil {
		return fmt.Errorf("schema change is dangerous: %w", err)
	}
	return createSchema(schema)
}

// ReadSchema is a CRUD request to the Schema collection
func ReadSchema(firestoreID string) (common.Schema, error) {
	return readSchema(firestoreID)
}

// UpdateSchema is a CRUD request to the Schema collection
func UpdateSchema(schema *common.Schema) error {
	if schema == nil {
		return errors.New("schema not set")
	}
	err := integrity.ValidateAllDBs(schema)
	if err != nil {
		return fmt.Errorf("schema can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(schema)
	if err != nil {
		return fmt.Errorf("schema change is dangerous: %w", err)
	}
	return updateSchema(schema)
}

// DeleteSchema is a CRUD request to the Schema collection
func DeleteSchema(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteSchema(firestoreID)
}

// UpdateUserPassword updates the User's password. Passwords have special handling.
func UpdateUserPassword(user *common.User, newPassword string) error {
	if user == nil {
		return errors.New("user not set")
	}
	if user.UID == nil || *user.UID == "" {
		return errors.New("UID not provided")
	}
	return updateUserPassword(user, newPassword)
}

// CreateUser is a CRUD request to the User collection
func CreateUser(user *common.User) error {
	if user == nil {
		return errors.New("user not set")
	}
	err := user.Validate()
	if err != nil {
		return fmt.Errorf("user invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(user)
	if err != nil {
		return fmt.Errorf("user can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(user)
	if err != nil {
		return fmt.Errorf("user change is dangerous: %w", err)
	}
	return createUser(user)
}

// ReadUser is a CRUD request to the User collection
func ReadUser(firestoreID string) (common.User, error) {
	if firestoreID == "" {
		return common.User{}, errors.New("not all inputs set")
	}
	return readUser(firestoreID)
}

// UpdateUser is a CRUD request to the User collection
func UpdateUser(user *common.User) error {
	if user == nil {
		return errors.New("user not set")
	}
	err := integrity.ValidateAllDBs(user)
	if err != nil {
		return fmt.Errorf("user can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(user)
	if err != nil {
		return fmt.Errorf("user change is dangerous: %w", err)
	}
	return updateUser(user)
}

// DeleteUser is a CRUD request to the User collection
func DeleteUser(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteUser(firestoreID)
}

// CreateUX is a CRUD request to the UX collection
func CreateUX(ux *common.UX) error {
	if ux == nil {
		return errors.New("ux not set")
	}
	err := ux.Validate()
	if err != nil {
		return fmt.Errorf("ux invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(ux)
	if err != nil {
		return fmt.Errorf("ux can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(ux)
	if err != nil {
		return fmt.Errorf("ux change is dangerous: %w", err)
	}
	return createUX(ux)
}

// ReadUX is a CRUD request to the UX collection
func ReadUX(firestoreID string) (common.UX, error) {
	if firestoreID == "" {
		return common.UX{}, errors.New("not all inputs set")
	}
	return readUX(firestoreID)
}

// UpdateUX is a CRUD request to the UX collection
func UpdateUX(ux *common.UX) error {
	if ux == nil {
		return errors.New("ux not set")
	}
	err := integrity.ValidateAllDBs(ux)
	if err != nil {
		return fmt.Errorf("ux can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(ux)
	if err != nil {
		return fmt.Errorf("ux change is dangerous: %w", err)
	}
	return updateUX(ux)
}

// DeleteUX is a CRUD request to the UX collection
func DeleteUX(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteUX(firestoreID)
}

// CreateTask is a CRUD request to the Task collection
func CreateTask(task *common.Task) error {
	if task == nil {
		return errors.New("task not set")
	}
	err := task.Validate()
	if err != nil {
		return fmt.Errorf("task invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(task)
	if err != nil {
		return fmt.Errorf("task can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(task)
	if err != nil {
		return fmt.Errorf("task change is dangerous: %w", err)
	}
	return createTask(task)
}

// ReadTask is a CRUD request to the Task collection
func ReadTask(firestoreID string) (common.Task, error) {
	if firestoreID == "" {
		return common.Task{}, errors.New("not all inputs set")
	}
	return readTask(firestoreID)
}

// UpdateTask is a CRUD request to the Task collection
func UpdateTask(task *common.Task) error {
	if task == nil {
		return errors.New("task not set")
	}
	err := integrity.ValidateAllDBs(task)
	if err != nil {
		return fmt.Errorf("task can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(task)
	if err != nil {
		return fmt.Errorf("task change is dangerous: %w", err)
	}
	return updateTask(task)
}

// DeleteTask is a CRUD request to the Task collection
func DeleteTask(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteTask(firestoreID)
}

// CreateRole is a CRUD request to the Role collection
func CreateRole(role *common.Role) error {
	if role == nil {
		return errors.New("role not set")
	}
	err := role.Validate()
	if err != nil {
		return fmt.Errorf("role invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(role)
	if err != nil {
		return fmt.Errorf("role can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(role)
	if err != nil {
		return fmt.Errorf("role change is dangerous: %w", err)
	}
	return createRole(role)
}

// ReadRole is a CRUD request to the Role collection
func ReadRole(firestoreID string) (common.Role, error) {
	if firestoreID == "" {
		return common.Role{}, errors.New("not all inputs set")
	}
	return readRole(firestoreID)
}

// UpdateRole is a CRUD request to the Role collection
func UpdateRole(role *common.Role) error {
	if role == nil {
		return errors.New("role not set")
	}
	err := integrity.ValidateAllDBs(role)
	if err != nil {
		return fmt.Errorf("role can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(role)
	if err != nil {
		return fmt.Errorf("role change is dangerous: %w", err)
	}
	return updateRole(role)
}

// DeleteRole is a CRUD request to the Role collection
func DeleteRole(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteRole(firestoreID)
}

// CreateBot is a CRUD request to the Bot collection
func CreateBot(bot *common.Bot) error {
	if bot == nil {
		return errors.New("bot not set")
	}
	err := bot.Validate()
	if err != nil {
		return fmt.Errorf("bot invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(bot)
	if err != nil {
		return fmt.Errorf("bot can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(bot)
	if err != nil {
		return fmt.Errorf("bot change is dangerous: %w", err)
	}
	return createBot(bot)
}

// ReadBot is a CRUD request to the Bot collection
func ReadBot(firestoreID string) (common.Bot, error) {
	if firestoreID == "" {
		return common.Bot{}, errors.New("not all inputs set")
	}
	return readBot(firestoreID)
}

// UpdateBot is a CRUD request to the Bot collection
func UpdateBot(bot *common.Bot) error {
	if bot == nil {
		return errors.New("bot not set")
	}
	err := integrity.ValidateAllDBs(bot)
	if err != nil {
		return fmt.Errorf("bot can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(bot)
	if err != nil {
		return fmt.Errorf("bot change is dangerous: %w", err)
	}
	return updateBot(bot)
}

// DeleteBot is a CRUD request to the Bot collection
func DeleteBot(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteBot(firestoreID)
}

// CreateAction is a CRUD request to the Action collection
func CreateAction(action *common.Action) error {
	if action == nil {
		return errors.New("action not set")
	}
	err := action.Validate()
	if err != nil {
		return fmt.Errorf("action invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(action)
	if err != nil {
		return fmt.Errorf("action can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(action)
	if err != nil {
		return fmt.Errorf("action change is dangerous: %w", err)
	}
	return createAction(action)
}

// ReadAction is a CRUD request to the Action collection
func ReadAction(firestoreID string) (common.Action, error) {
	if firestoreID == "" {
		return common.Action{}, errors.New("not all inputs set")
	}
	return readAction(firestoreID)
}

// UpdateAction is a CRUD request to the Action collection
func UpdateAction(action *common.Action) error {
	if action == nil {
		return errors.New("action not set")
	}
	err := integrity.ValidateAllDBs(action)
	if err != nil {
		return fmt.Errorf("action can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(action)
	if err != nil {
		return fmt.Errorf("action change is dangerous: %w", err)
	}
	return updateAction(action)
}

// DeleteAction is a CRUD request to the Action collection
func DeleteAction(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteAction(firestoreID)
}

// CreatePublicMap is a CRUD request to the PublicMap collection
func CreatePublicMap(publicMap *common.PublicMap) error {
	if publicMap == nil {
		return errors.New("publicMap not set")
	}
	err := publicMap.Validate()
	if err != nil {
		return fmt.Errorf("publicMap invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(publicMap)
	if err != nil {
		return fmt.Errorf("publicMap can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(publicMap)
	if err != nil {
		return fmt.Errorf("publicMap change is dangerous: %w", err)
	}
	return createPublicMap(publicMap)
}

// ReadPublicMap is a CRUD request to the PublicMap collection
func ReadPublicMap(firestoreID string) (common.PublicMap, error) {
	return readPublicMap(firestoreID)
}

// UpdatePublicMap is a CRUD request to the PublicMap collection
func UpdatePublicMap(publicMap *common.PublicMap) error {
	if publicMap == nil {
		return errors.New("publicMap not set")
	}
	err := integrity.ValidateAllDBs(publicMap)
	if err != nil {
		return fmt.Errorf("publicMap can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(publicMap)
	if err != nil {
		return fmt.Errorf("publicMap change is dangerous: %w", err)
	}
	return updatePublicMap(publicMap)
}

// DeletePublicMap is a CRUD request to the PublicMap collection
func DeletePublicMap(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deletePublicMap(firestoreID)
}

// CreateCSS is a CRUD request to the CSS collection
func CreateCSS(CSS *common.CSS) error {
	if CSS == nil {
		return errors.New("CSS not set")
	}
	err := CSS.Validate()
	if err != nil {
		return fmt.Errorf("CSS invalid: %w", err)
	}
	err = integrity.ValidateAllDBs(CSS)
	if err != nil {
		return fmt.Errorf("CSS can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(CSS)
	if err != nil {
		return fmt.Errorf("CSS change is dangerous: %w", err)
	}
	return createCSS(CSS)
}

// ReadCSS is a CRUD request to the CSS collection
func ReadCSS(firestoreID string) (common.CSS, error) {
	return readCSS(firestoreID)
}

// UpdateCSS is a CRUD request to the CSS collection
func UpdateCSS(CSS *common.CSS) error {
	if CSS == nil {
		return errors.New("CSS not set")
	}
	err := integrity.ValidateAllDBs(CSS)
	if err != nil {
		return fmt.Errorf("CSS can't be handled: %w", err)
	}
	err = integrity.ValidateLanguages(CSS)
	if err != nil {
		return fmt.Errorf("CSS change is dangerous: %w", err)
	}
	return updateCSS(CSS)
}

// DeleteCSS is a CRUD request to the CSS collection
func DeleteCSS(firestoreID string) error {
	if firestoreID == "" {
		return errors.New("not all inputs set")
	}
	return deleteCSS(firestoreID)
}
